package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.TrchDiGarDao;
import it.accenture.jnais.commons.data.to.ITrchDiGar;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbsd980Data;
import it.accenture.jnais.ws.redefines.TgaAbbAnnuUlt;
import it.accenture.jnais.ws.redefines.TgaAbbTotIni;
import it.accenture.jnais.ws.redefines.TgaAbbTotUlt;
import it.accenture.jnais.ws.redefines.TgaAcqExp;
import it.accenture.jnais.ws.redefines.TgaAlqCommisInter;
import it.accenture.jnais.ws.redefines.TgaAlqProvAcq;
import it.accenture.jnais.ws.redefines.TgaAlqProvInc;
import it.accenture.jnais.ws.redefines.TgaAlqProvRicor;
import it.accenture.jnais.ws.redefines.TgaAlqRemunAss;
import it.accenture.jnais.ws.redefines.TgaAlqScon;
import it.accenture.jnais.ws.redefines.TgaBnsGiaLiqto;
import it.accenture.jnais.ws.redefines.TgaCommisGest;
import it.accenture.jnais.ws.redefines.TgaCommisInter;
import it.accenture.jnais.ws.redefines.TgaCosRunAssva;
import it.accenture.jnais.ws.redefines.TgaCosRunAssvaIdc;
import it.accenture.jnais.ws.redefines.TgaCptInOpzRivto;
import it.accenture.jnais.ws.redefines.TgaCptMinScad;
import it.accenture.jnais.ws.redefines.TgaCptRshMor;
import it.accenture.jnais.ws.redefines.TgaDtEffStab;
import it.accenture.jnais.ws.redefines.TgaDtEmis;
import it.accenture.jnais.ws.redefines.TgaDtIniValTar;
import it.accenture.jnais.ws.redefines.TgaDtScad;
import it.accenture.jnais.ws.redefines.TgaDtUltAdegPrePr;
import it.accenture.jnais.ws.redefines.TgaDtVldtProd;
import it.accenture.jnais.ws.redefines.TgaDurAa;
import it.accenture.jnais.ws.redefines.TgaDurAbb;
import it.accenture.jnais.ws.redefines.TgaDurGg;
import it.accenture.jnais.ws.redefines.TgaDurMm;
import it.accenture.jnais.ws.redefines.TgaEtaAa1oAssto;
import it.accenture.jnais.ws.redefines.TgaEtaAa2oAssto;
import it.accenture.jnais.ws.redefines.TgaEtaAa3oAssto;
import it.accenture.jnais.ws.redefines.TgaEtaMm1oAssto;
import it.accenture.jnais.ws.redefines.TgaEtaMm2oAssto;
import it.accenture.jnais.ws.redefines.TgaEtaMm3oAssto;
import it.accenture.jnais.ws.redefines.TgaIdMoviChiu;
import it.accenture.jnais.ws.redefines.TgaImpAder;
import it.accenture.jnais.ws.redefines.TgaImpAltSopr;
import it.accenture.jnais.ws.redefines.TgaImpAz;
import it.accenture.jnais.ws.redefines.TgaImpbCommisInter;
import it.accenture.jnais.ws.redefines.TgaImpBns;
import it.accenture.jnais.ws.redefines.TgaImpBnsAntic;
import it.accenture.jnais.ws.redefines.TgaImpbProvAcq;
import it.accenture.jnais.ws.redefines.TgaImpbProvInc;
import it.accenture.jnais.ws.redefines.TgaImpbProvRicor;
import it.accenture.jnais.ws.redefines.TgaImpbRemunAss;
import it.accenture.jnais.ws.redefines.TgaImpbVisEnd2000;
import it.accenture.jnais.ws.redefines.TgaImpCarAcq;
import it.accenture.jnais.ws.redefines.TgaImpCarGest;
import it.accenture.jnais.ws.redefines.TgaImpCarInc;
import it.accenture.jnais.ws.redefines.TgaImpScon;
import it.accenture.jnais.ws.redefines.TgaImpSoprProf;
import it.accenture.jnais.ws.redefines.TgaImpSoprSan;
import it.accenture.jnais.ws.redefines.TgaImpSoprSpo;
import it.accenture.jnais.ws.redefines.TgaImpSoprTec;
import it.accenture.jnais.ws.redefines.TgaImpTfr;
import it.accenture.jnais.ws.redefines.TgaImpTfrStrc;
import it.accenture.jnais.ws.redefines.TgaImpTrasfe;
import it.accenture.jnais.ws.redefines.TgaImpVolo;
import it.accenture.jnais.ws.redefines.TgaIncrPre;
import it.accenture.jnais.ws.redefines.TgaIncrPrstz;
import it.accenture.jnais.ws.redefines.TgaIntrMora;
import it.accenture.jnais.ws.redefines.TgaManfeeAntic;
import it.accenture.jnais.ws.redefines.TgaManfeeRicor;
import it.accenture.jnais.ws.redefines.TgaMatuEnd2000;
import it.accenture.jnais.ws.redefines.TgaMinGarto;
import it.accenture.jnais.ws.redefines.TgaMinTrnut;
import it.accenture.jnais.ws.redefines.TgaNumGgRival;
import it.accenture.jnais.ws.redefines.TgaOldTsTec;
import it.accenture.jnais.ws.redefines.TgaPcCommisGest;
import it.accenture.jnais.ws.redefines.TgaPcIntrRiat;
import it.accenture.jnais.ws.redefines.TgaPcRetr;
import it.accenture.jnais.ws.redefines.TgaPcRipPre;
import it.accenture.jnais.ws.redefines.TgaPreAttDiTrch;
import it.accenture.jnais.ws.redefines.TgaPreCasoMor;
import it.accenture.jnais.ws.redefines.TgaPreIniNet;
import it.accenture.jnais.ws.redefines.TgaPreInvrioIni;
import it.accenture.jnais.ws.redefines.TgaPreInvrioUlt;
import it.accenture.jnais.ws.redefines.TgaPreLrd;
import it.accenture.jnais.ws.redefines.TgaPrePattuito;
import it.accenture.jnais.ws.redefines.TgaPrePpIni;
import it.accenture.jnais.ws.redefines.TgaPrePpUlt;
import it.accenture.jnais.ws.redefines.TgaPreRivto;
import it.accenture.jnais.ws.redefines.TgaPreStab;
import it.accenture.jnais.ws.redefines.TgaPreTariIni;
import it.accenture.jnais.ws.redefines.TgaPreTariUlt;
import it.accenture.jnais.ws.redefines.TgaPreUniRivto;
import it.accenture.jnais.ws.redefines.TgaProv1aaAcq;
import it.accenture.jnais.ws.redefines.TgaProv2aaAcq;
import it.accenture.jnais.ws.redefines.TgaProvInc;
import it.accenture.jnais.ws.redefines.TgaProvRicor;
import it.accenture.jnais.ws.redefines.TgaPrstzAggIni;
import it.accenture.jnais.ws.redefines.TgaPrstzAggUlt;
import it.accenture.jnais.ws.redefines.TgaPrstzIni;
import it.accenture.jnais.ws.redefines.TgaPrstzIniNewfis;
import it.accenture.jnais.ws.redefines.TgaPrstzIniNforz;
import it.accenture.jnais.ws.redefines.TgaPrstzIniStab;
import it.accenture.jnais.ws.redefines.TgaPrstzRidIni;
import it.accenture.jnais.ws.redefines.TgaPrstzUlt;
import it.accenture.jnais.ws.redefines.TgaRatLrd;
import it.accenture.jnais.ws.redefines.TgaRemunAss;
import it.accenture.jnais.ws.redefines.TgaRendtoLrd;
import it.accenture.jnais.ws.redefines.TgaRendtoRetr;
import it.accenture.jnais.ws.redefines.TgaRenIniTsTec0;
import it.accenture.jnais.ws.redefines.TgaRisMat;
import it.accenture.jnais.ws.redefines.TgaTsRivalFis;
import it.accenture.jnais.ws.redefines.TgaTsRivalIndiciz;
import it.accenture.jnais.ws.redefines.TgaTsRivalNet;
import it.accenture.jnais.ws.redefines.TgaVisEnd2000;
import it.accenture.jnais.ws.redefines.TgaVisEnd2000Nforz;
import it.accenture.jnais.ws.TrchDiGar;

/**Original name: LDBSD980<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  04 GIU 2019.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbsd980 extends Program implements ITrchDiGar {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private TrchDiGarDao trchDiGarDao = new TrchDiGarDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbsd980Data ws = new Ldbsd980Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: TRCH-DI-GAR
    private TrchDiGar trchDiGar;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBSD980_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, TrchDiGar trchDiGar) {
        this.idsv0003 = idsv0003;
        this.trchDiGar = trchDiGar;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbsd980 getInstance() {
        return ((Ldbsd980)Programs.getInstance(Ldbsd980.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBSD980'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBSD980");
        // COB_CODE: MOVE 'TRCH-DI-GAR' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("TRCH-DI-GAR");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-EFF CURSOR FOR
        //              SELECT
        //                     ID_TRCH_DI_GAR
        //                    ,ID_GAR
        //                    ,ID_ADES
        //                    ,ID_POLI
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_DECOR
        //                    ,DT_SCAD
        //                    ,IB_OGG
        //                    ,TP_RGM_FISC
        //                    ,DT_EMIS
        //                    ,TP_TRCH
        //                    ,DUR_AA
        //                    ,DUR_MM
        //                    ,DUR_GG
        //                    ,PRE_CASO_MOR
        //                    ,PC_INTR_RIAT
        //                    ,IMP_BNS_ANTIC
        //                    ,PRE_INI_NET
        //                    ,PRE_PP_INI
        //                    ,PRE_PP_ULT
        //                    ,PRE_TARI_INI
        //                    ,PRE_TARI_ULT
        //                    ,PRE_INVRIO_INI
        //                    ,PRE_INVRIO_ULT
        //                    ,PRE_RIVTO
        //                    ,IMP_SOPR_PROF
        //                    ,IMP_SOPR_SAN
        //                    ,IMP_SOPR_SPO
        //                    ,IMP_SOPR_TEC
        //                    ,IMP_ALT_SOPR
        //                    ,PRE_STAB
        //                    ,DT_EFF_STAB
        //                    ,TS_RIVAL_FIS
        //                    ,TS_RIVAL_INDICIZ
        //                    ,OLD_TS_TEC
        //                    ,RAT_LRD
        //                    ,PRE_LRD
        //                    ,PRSTZ_INI
        //                    ,PRSTZ_ULT
        //                    ,CPT_IN_OPZ_RIVTO
        //                    ,PRSTZ_INI_STAB
        //                    ,CPT_RSH_MOR
        //                    ,PRSTZ_RID_INI
        //                    ,FL_CAR_CONT
        //                    ,BNS_GIA_LIQTO
        //                    ,IMP_BNS
        //                    ,COD_DVS
        //                    ,PRSTZ_INI_NEWFIS
        //                    ,IMP_SCON
        //                    ,ALQ_SCON
        //                    ,IMP_CAR_ACQ
        //                    ,IMP_CAR_INC
        //                    ,IMP_CAR_GEST
        //                    ,ETA_AA_1O_ASSTO
        //                    ,ETA_MM_1O_ASSTO
        //                    ,ETA_AA_2O_ASSTO
        //                    ,ETA_MM_2O_ASSTO
        //                    ,ETA_AA_3O_ASSTO
        //                    ,ETA_MM_3O_ASSTO
        //                    ,RENDTO_LRD
        //                    ,PC_RETR
        //                    ,RENDTO_RETR
        //                    ,MIN_GARTO
        //                    ,MIN_TRNUT
        //                    ,PRE_ATT_DI_TRCH
        //                    ,MATU_END2000
        //                    ,ABB_TOT_INI
        //                    ,ABB_TOT_ULT
        //                    ,ABB_ANNU_ULT
        //                    ,DUR_ABB
        //                    ,TP_ADEG_ABB
        //                    ,MOD_CALC
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,VIS_END2000
        //                    ,DT_VLDT_PROD
        //                    ,DT_INI_VAL_TAR
        //                    ,IMPB_VIS_END2000
        //                    ,REN_INI_TS_TEC_0
        //                    ,PC_RIP_PRE
        //                    ,FL_IMPORTI_FORZ
        //                    ,PRSTZ_INI_NFORZ
        //                    ,VIS_END2000_NFORZ
        //                    ,INTR_MORA
        //                    ,MANFEE_ANTIC
        //                    ,MANFEE_RICOR
        //                    ,PRE_UNI_RIVTO
        //                    ,PROV_1AA_ACQ
        //                    ,PROV_2AA_ACQ
        //                    ,PROV_RICOR
        //                    ,PROV_INC
        //                    ,ALQ_PROV_ACQ
        //                    ,ALQ_PROV_INC
        //                    ,ALQ_PROV_RICOR
        //                    ,IMPB_PROV_ACQ
        //                    ,IMPB_PROV_INC
        //                    ,IMPB_PROV_RICOR
        //                    ,FL_PROV_FORZ
        //                    ,PRSTZ_AGG_INI
        //                    ,INCR_PRE
        //                    ,INCR_PRSTZ
        //                    ,DT_ULT_ADEG_PRE_PR
        //                    ,PRSTZ_AGG_ULT
        //                    ,TS_RIVAL_NET
        //                    ,PRE_PATTUITO
        //                    ,TP_RIVAL
        //                    ,RIS_MAT
        //                    ,CPT_MIN_SCAD
        //                    ,COMMIS_GEST
        //                    ,TP_MANFEE_APPL
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,PC_COMMIS_GEST
        //                    ,NUM_GG_RIVAL
        //                    ,IMP_TRASFE
        //                    ,IMP_TFR_STRC
        //                    ,ACQ_EXP
        //                    ,REMUN_ASS
        //                    ,COMMIS_INTER
        //                    ,ALQ_REMUN_ASS
        //                    ,ALQ_COMMIS_INTER
        //                    ,IMPB_REMUN_ASS
        //                    ,IMPB_COMMIS_INTER
        //                    ,COS_RUN_ASSVA
        //                    ,COS_RUN_ASSVA_IDC
        //              FROM TRCH_DI_GAR
        //              WHERE      ID_ADES     = :TGA-ID-ADES
        //                    AND ID_POLI     = :TGA-ID-POLI
        //                    AND ID_MOVI_CRZ = :TGA-ID-MOVI-CRZ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_TRCH_DI_GAR
        //                    ,ID_GAR
        //                    ,ID_ADES
        //                    ,ID_POLI
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_DECOR
        //                    ,DT_SCAD
        //                    ,IB_OGG
        //                    ,TP_RGM_FISC
        //                    ,DT_EMIS
        //                    ,TP_TRCH
        //                    ,DUR_AA
        //                    ,DUR_MM
        //                    ,DUR_GG
        //                    ,PRE_CASO_MOR
        //                    ,PC_INTR_RIAT
        //                    ,IMP_BNS_ANTIC
        //                    ,PRE_INI_NET
        //                    ,PRE_PP_INI
        //                    ,PRE_PP_ULT
        //                    ,PRE_TARI_INI
        //                    ,PRE_TARI_ULT
        //                    ,PRE_INVRIO_INI
        //                    ,PRE_INVRIO_ULT
        //                    ,PRE_RIVTO
        //                    ,IMP_SOPR_PROF
        //                    ,IMP_SOPR_SAN
        //                    ,IMP_SOPR_SPO
        //                    ,IMP_SOPR_TEC
        //                    ,IMP_ALT_SOPR
        //                    ,PRE_STAB
        //                    ,DT_EFF_STAB
        //                    ,TS_RIVAL_FIS
        //                    ,TS_RIVAL_INDICIZ
        //                    ,OLD_TS_TEC
        //                    ,RAT_LRD
        //                    ,PRE_LRD
        //                    ,PRSTZ_INI
        //                    ,PRSTZ_ULT
        //                    ,CPT_IN_OPZ_RIVTO
        //                    ,PRSTZ_INI_STAB
        //                    ,CPT_RSH_MOR
        //                    ,PRSTZ_RID_INI
        //                    ,FL_CAR_CONT
        //                    ,BNS_GIA_LIQTO
        //                    ,IMP_BNS
        //                    ,COD_DVS
        //                    ,PRSTZ_INI_NEWFIS
        //                    ,IMP_SCON
        //                    ,ALQ_SCON
        //                    ,IMP_CAR_ACQ
        //                    ,IMP_CAR_INC
        //                    ,IMP_CAR_GEST
        //                    ,ETA_AA_1O_ASSTO
        //                    ,ETA_MM_1O_ASSTO
        //                    ,ETA_AA_2O_ASSTO
        //                    ,ETA_MM_2O_ASSTO
        //                    ,ETA_AA_3O_ASSTO
        //                    ,ETA_MM_3O_ASSTO
        //                    ,RENDTO_LRD
        //                    ,PC_RETR
        //                    ,RENDTO_RETR
        //                    ,MIN_GARTO
        //                    ,MIN_TRNUT
        //                    ,PRE_ATT_DI_TRCH
        //                    ,MATU_END2000
        //                    ,ABB_TOT_INI
        //                    ,ABB_TOT_ULT
        //                    ,ABB_ANNU_ULT
        //                    ,DUR_ABB
        //                    ,TP_ADEG_ABB
        //                    ,MOD_CALC
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,VIS_END2000
        //                    ,DT_VLDT_PROD
        //                    ,DT_INI_VAL_TAR
        //                    ,IMPB_VIS_END2000
        //                    ,REN_INI_TS_TEC_0
        //                    ,PC_RIP_PRE
        //                    ,FL_IMPORTI_FORZ
        //                    ,PRSTZ_INI_NFORZ
        //                    ,VIS_END2000_NFORZ
        //                    ,INTR_MORA
        //                    ,MANFEE_ANTIC
        //                    ,MANFEE_RICOR
        //                    ,PRE_UNI_RIVTO
        //                    ,PROV_1AA_ACQ
        //                    ,PROV_2AA_ACQ
        //                    ,PROV_RICOR
        //                    ,PROV_INC
        //                    ,ALQ_PROV_ACQ
        //                    ,ALQ_PROV_INC
        //                    ,ALQ_PROV_RICOR
        //                    ,IMPB_PROV_ACQ
        //                    ,IMPB_PROV_INC
        //                    ,IMPB_PROV_RICOR
        //                    ,FL_PROV_FORZ
        //                    ,PRSTZ_AGG_INI
        //                    ,INCR_PRE
        //                    ,INCR_PRSTZ
        //                    ,DT_ULT_ADEG_PRE_PR
        //                    ,PRSTZ_AGG_ULT
        //                    ,TS_RIVAL_NET
        //                    ,PRE_PATTUITO
        //                    ,TP_RIVAL
        //                    ,RIS_MAT
        //                    ,CPT_MIN_SCAD
        //                    ,COMMIS_GEST
        //                    ,TP_MANFEE_APPL
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,PC_COMMIS_GEST
        //                    ,NUM_GG_RIVAL
        //                    ,IMP_TRASFE
        //                    ,IMP_TFR_STRC
        //                    ,ACQ_EXP
        //                    ,REMUN_ASS
        //                    ,COMMIS_INTER
        //                    ,ALQ_REMUN_ASS
        //                    ,ALQ_COMMIS_INTER
        //                    ,IMPB_REMUN_ASS
        //                    ,IMPB_COMMIS_INTER
        //                    ,COS_RUN_ASSVA
        //                    ,COS_RUN_ASSVA_IDC
        //             INTO
        //                :TGA-ID-TRCH-DI-GAR
        //               ,:TGA-ID-GAR
        //               ,:TGA-ID-ADES
        //               ,:TGA-ID-POLI
        //               ,:TGA-ID-MOVI-CRZ
        //               ,:TGA-ID-MOVI-CHIU
        //                :IND-TGA-ID-MOVI-CHIU
        //               ,:TGA-DT-INI-EFF-DB
        //               ,:TGA-DT-END-EFF-DB
        //               ,:TGA-COD-COMP-ANIA
        //               ,:TGA-DT-DECOR-DB
        //               ,:TGA-DT-SCAD-DB
        //                :IND-TGA-DT-SCAD
        //               ,:TGA-IB-OGG
        //                :IND-TGA-IB-OGG
        //               ,:TGA-TP-RGM-FISC
        //               ,:TGA-DT-EMIS-DB
        //                :IND-TGA-DT-EMIS
        //               ,:TGA-TP-TRCH
        //               ,:TGA-DUR-AA
        //                :IND-TGA-DUR-AA
        //               ,:TGA-DUR-MM
        //                :IND-TGA-DUR-MM
        //               ,:TGA-DUR-GG
        //                :IND-TGA-DUR-GG
        //               ,:TGA-PRE-CASO-MOR
        //                :IND-TGA-PRE-CASO-MOR
        //               ,:TGA-PC-INTR-RIAT
        //                :IND-TGA-PC-INTR-RIAT
        //               ,:TGA-IMP-BNS-ANTIC
        //                :IND-TGA-IMP-BNS-ANTIC
        //               ,:TGA-PRE-INI-NET
        //                :IND-TGA-PRE-INI-NET
        //               ,:TGA-PRE-PP-INI
        //                :IND-TGA-PRE-PP-INI
        //               ,:TGA-PRE-PP-ULT
        //                :IND-TGA-PRE-PP-ULT
        //               ,:TGA-PRE-TARI-INI
        //                :IND-TGA-PRE-TARI-INI
        //               ,:TGA-PRE-TARI-ULT
        //                :IND-TGA-PRE-TARI-ULT
        //               ,:TGA-PRE-INVRIO-INI
        //                :IND-TGA-PRE-INVRIO-INI
        //               ,:TGA-PRE-INVRIO-ULT
        //                :IND-TGA-PRE-INVRIO-ULT
        //               ,:TGA-PRE-RIVTO
        //                :IND-TGA-PRE-RIVTO
        //               ,:TGA-IMP-SOPR-PROF
        //                :IND-TGA-IMP-SOPR-PROF
        //               ,:TGA-IMP-SOPR-SAN
        //                :IND-TGA-IMP-SOPR-SAN
        //               ,:TGA-IMP-SOPR-SPO
        //                :IND-TGA-IMP-SOPR-SPO
        //               ,:TGA-IMP-SOPR-TEC
        //                :IND-TGA-IMP-SOPR-TEC
        //               ,:TGA-IMP-ALT-SOPR
        //                :IND-TGA-IMP-ALT-SOPR
        //               ,:TGA-PRE-STAB
        //                :IND-TGA-PRE-STAB
        //               ,:TGA-DT-EFF-STAB-DB
        //                :IND-TGA-DT-EFF-STAB
        //               ,:TGA-TS-RIVAL-FIS
        //                :IND-TGA-TS-RIVAL-FIS
        //               ,:TGA-TS-RIVAL-INDICIZ
        //                :IND-TGA-TS-RIVAL-INDICIZ
        //               ,:TGA-OLD-TS-TEC
        //                :IND-TGA-OLD-TS-TEC
        //               ,:TGA-RAT-LRD
        //                :IND-TGA-RAT-LRD
        //               ,:TGA-PRE-LRD
        //                :IND-TGA-PRE-LRD
        //               ,:TGA-PRSTZ-INI
        //                :IND-TGA-PRSTZ-INI
        //               ,:TGA-PRSTZ-ULT
        //                :IND-TGA-PRSTZ-ULT
        //               ,:TGA-CPT-IN-OPZ-RIVTO
        //                :IND-TGA-CPT-IN-OPZ-RIVTO
        //               ,:TGA-PRSTZ-INI-STAB
        //                :IND-TGA-PRSTZ-INI-STAB
        //               ,:TGA-CPT-RSH-MOR
        //                :IND-TGA-CPT-RSH-MOR
        //               ,:TGA-PRSTZ-RID-INI
        //                :IND-TGA-PRSTZ-RID-INI
        //               ,:TGA-FL-CAR-CONT
        //                :IND-TGA-FL-CAR-CONT
        //               ,:TGA-BNS-GIA-LIQTO
        //                :IND-TGA-BNS-GIA-LIQTO
        //               ,:TGA-IMP-BNS
        //                :IND-TGA-IMP-BNS
        //               ,:TGA-COD-DVS
        //               ,:TGA-PRSTZ-INI-NEWFIS
        //                :IND-TGA-PRSTZ-INI-NEWFIS
        //               ,:TGA-IMP-SCON
        //                :IND-TGA-IMP-SCON
        //               ,:TGA-ALQ-SCON
        //                :IND-TGA-ALQ-SCON
        //               ,:TGA-IMP-CAR-ACQ
        //                :IND-TGA-IMP-CAR-ACQ
        //               ,:TGA-IMP-CAR-INC
        //                :IND-TGA-IMP-CAR-INC
        //               ,:TGA-IMP-CAR-GEST
        //                :IND-TGA-IMP-CAR-GEST
        //               ,:TGA-ETA-AA-1O-ASSTO
        //                :IND-TGA-ETA-AA-1O-ASSTO
        //               ,:TGA-ETA-MM-1O-ASSTO
        //                :IND-TGA-ETA-MM-1O-ASSTO
        //               ,:TGA-ETA-AA-2O-ASSTO
        //                :IND-TGA-ETA-AA-2O-ASSTO
        //               ,:TGA-ETA-MM-2O-ASSTO
        //                :IND-TGA-ETA-MM-2O-ASSTO
        //               ,:TGA-ETA-AA-3O-ASSTO
        //                :IND-TGA-ETA-AA-3O-ASSTO
        //               ,:TGA-ETA-MM-3O-ASSTO
        //                :IND-TGA-ETA-MM-3O-ASSTO
        //               ,:TGA-RENDTO-LRD
        //                :IND-TGA-RENDTO-LRD
        //               ,:TGA-PC-RETR
        //                :IND-TGA-PC-RETR
        //               ,:TGA-RENDTO-RETR
        //                :IND-TGA-RENDTO-RETR
        //               ,:TGA-MIN-GARTO
        //                :IND-TGA-MIN-GARTO
        //               ,:TGA-MIN-TRNUT
        //                :IND-TGA-MIN-TRNUT
        //               ,:TGA-PRE-ATT-DI-TRCH
        //                :IND-TGA-PRE-ATT-DI-TRCH
        //               ,:TGA-MATU-END2000
        //                :IND-TGA-MATU-END2000
        //               ,:TGA-ABB-TOT-INI
        //                :IND-TGA-ABB-TOT-INI
        //               ,:TGA-ABB-TOT-ULT
        //                :IND-TGA-ABB-TOT-ULT
        //               ,:TGA-ABB-ANNU-ULT
        //                :IND-TGA-ABB-ANNU-ULT
        //               ,:TGA-DUR-ABB
        //                :IND-TGA-DUR-ABB
        //               ,:TGA-TP-ADEG-ABB
        //                :IND-TGA-TP-ADEG-ABB
        //               ,:TGA-MOD-CALC
        //                :IND-TGA-MOD-CALC
        //               ,:TGA-IMP-AZ
        //                :IND-TGA-IMP-AZ
        //               ,:TGA-IMP-ADER
        //                :IND-TGA-IMP-ADER
        //               ,:TGA-IMP-TFR
        //                :IND-TGA-IMP-TFR
        //               ,:TGA-IMP-VOLO
        //                :IND-TGA-IMP-VOLO
        //               ,:TGA-VIS-END2000
        //                :IND-TGA-VIS-END2000
        //               ,:TGA-DT-VLDT-PROD-DB
        //                :IND-TGA-DT-VLDT-PROD
        //               ,:TGA-DT-INI-VAL-TAR-DB
        //                :IND-TGA-DT-INI-VAL-TAR
        //               ,:TGA-IMPB-VIS-END2000
        //                :IND-TGA-IMPB-VIS-END2000
        //               ,:TGA-REN-INI-TS-TEC-0
        //                :IND-TGA-REN-INI-TS-TEC-0
        //               ,:TGA-PC-RIP-PRE
        //                :IND-TGA-PC-RIP-PRE
        //               ,:TGA-FL-IMPORTI-FORZ
        //                :IND-TGA-FL-IMPORTI-FORZ
        //               ,:TGA-PRSTZ-INI-NFORZ
        //                :IND-TGA-PRSTZ-INI-NFORZ
        //               ,:TGA-VIS-END2000-NFORZ
        //                :IND-TGA-VIS-END2000-NFORZ
        //               ,:TGA-INTR-MORA
        //                :IND-TGA-INTR-MORA
        //               ,:TGA-MANFEE-ANTIC
        //                :IND-TGA-MANFEE-ANTIC
        //               ,:TGA-MANFEE-RICOR
        //                :IND-TGA-MANFEE-RICOR
        //               ,:TGA-PRE-UNI-RIVTO
        //                :IND-TGA-PRE-UNI-RIVTO
        //               ,:TGA-PROV-1AA-ACQ
        //                :IND-TGA-PROV-1AA-ACQ
        //               ,:TGA-PROV-2AA-ACQ
        //                :IND-TGA-PROV-2AA-ACQ
        //               ,:TGA-PROV-RICOR
        //                :IND-TGA-PROV-RICOR
        //               ,:TGA-PROV-INC
        //                :IND-TGA-PROV-INC
        //               ,:TGA-ALQ-PROV-ACQ
        //                :IND-TGA-ALQ-PROV-ACQ
        //               ,:TGA-ALQ-PROV-INC
        //                :IND-TGA-ALQ-PROV-INC
        //               ,:TGA-ALQ-PROV-RICOR
        //                :IND-TGA-ALQ-PROV-RICOR
        //               ,:TGA-IMPB-PROV-ACQ
        //                :IND-TGA-IMPB-PROV-ACQ
        //               ,:TGA-IMPB-PROV-INC
        //                :IND-TGA-IMPB-PROV-INC
        //               ,:TGA-IMPB-PROV-RICOR
        //                :IND-TGA-IMPB-PROV-RICOR
        //               ,:TGA-FL-PROV-FORZ
        //                :IND-TGA-FL-PROV-FORZ
        //               ,:TGA-PRSTZ-AGG-INI
        //                :IND-TGA-PRSTZ-AGG-INI
        //               ,:TGA-INCR-PRE
        //                :IND-TGA-INCR-PRE
        //               ,:TGA-INCR-PRSTZ
        //                :IND-TGA-INCR-PRSTZ
        //               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
        //                :IND-TGA-DT-ULT-ADEG-PRE-PR
        //               ,:TGA-PRSTZ-AGG-ULT
        //                :IND-TGA-PRSTZ-AGG-ULT
        //               ,:TGA-TS-RIVAL-NET
        //                :IND-TGA-TS-RIVAL-NET
        //               ,:TGA-PRE-PATTUITO
        //                :IND-TGA-PRE-PATTUITO
        //               ,:TGA-TP-RIVAL
        //                :IND-TGA-TP-RIVAL
        //               ,:TGA-RIS-MAT
        //                :IND-TGA-RIS-MAT
        //               ,:TGA-CPT-MIN-SCAD
        //                :IND-TGA-CPT-MIN-SCAD
        //               ,:TGA-COMMIS-GEST
        //                :IND-TGA-COMMIS-GEST
        //               ,:TGA-TP-MANFEE-APPL
        //                :IND-TGA-TP-MANFEE-APPL
        //               ,:TGA-DS-RIGA
        //               ,:TGA-DS-OPER-SQL
        //               ,:TGA-DS-VER
        //               ,:TGA-DS-TS-INI-CPTZ
        //               ,:TGA-DS-TS-END-CPTZ
        //               ,:TGA-DS-UTENTE
        //               ,:TGA-DS-STATO-ELAB
        //               ,:TGA-PC-COMMIS-GEST
        //                :IND-TGA-PC-COMMIS-GEST
        //               ,:TGA-NUM-GG-RIVAL
        //                :IND-TGA-NUM-GG-RIVAL
        //               ,:TGA-IMP-TRASFE
        //                :IND-TGA-IMP-TRASFE
        //               ,:TGA-IMP-TFR-STRC
        //                :IND-TGA-IMP-TFR-STRC
        //               ,:TGA-ACQ-EXP
        //                :IND-TGA-ACQ-EXP
        //               ,:TGA-REMUN-ASS
        //                :IND-TGA-REMUN-ASS
        //               ,:TGA-COMMIS-INTER
        //                :IND-TGA-COMMIS-INTER
        //               ,:TGA-ALQ-REMUN-ASS
        //                :IND-TGA-ALQ-REMUN-ASS
        //               ,:TGA-ALQ-COMMIS-INTER
        //                :IND-TGA-ALQ-COMMIS-INTER
        //               ,:TGA-IMPB-REMUN-ASS
        //                :IND-TGA-IMPB-REMUN-ASS
        //               ,:TGA-IMPB-COMMIS-INTER
        //                :IND-TGA-IMPB-COMMIS-INTER
        //               ,:TGA-COS-RUN-ASSVA
        //                :IND-TGA-COS-RUN-ASSVA
        //               ,:TGA-COS-RUN-ASSVA-IDC
        //                :IND-TGA-COS-RUN-ASSVA-IDC
        //             FROM TRCH_DI_GAR
        //             WHERE      ID_ADES     = :TGA-ID-ADES
        //                    AND ID_POLI     = :TGA-ID-POLI
        //                    AND ID_MOVI_CRZ = :TGA-ID-MOVI-CRZ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        trchDiGarDao.selectRec8(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF
        //           END-EXEC.
        trchDiGarDao.openCEff44(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF
        //           END-EXEC.
        trchDiGarDao.closeCEff44();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF
        //           INTO
        //                :TGA-ID-TRCH-DI-GAR
        //               ,:TGA-ID-GAR
        //               ,:TGA-ID-ADES
        //               ,:TGA-ID-POLI
        //               ,:TGA-ID-MOVI-CRZ
        //               ,:TGA-ID-MOVI-CHIU
        //                :IND-TGA-ID-MOVI-CHIU
        //               ,:TGA-DT-INI-EFF-DB
        //               ,:TGA-DT-END-EFF-DB
        //               ,:TGA-COD-COMP-ANIA
        //               ,:TGA-DT-DECOR-DB
        //               ,:TGA-DT-SCAD-DB
        //                :IND-TGA-DT-SCAD
        //               ,:TGA-IB-OGG
        //                :IND-TGA-IB-OGG
        //               ,:TGA-TP-RGM-FISC
        //               ,:TGA-DT-EMIS-DB
        //                :IND-TGA-DT-EMIS
        //               ,:TGA-TP-TRCH
        //               ,:TGA-DUR-AA
        //                :IND-TGA-DUR-AA
        //               ,:TGA-DUR-MM
        //                :IND-TGA-DUR-MM
        //               ,:TGA-DUR-GG
        //                :IND-TGA-DUR-GG
        //               ,:TGA-PRE-CASO-MOR
        //                :IND-TGA-PRE-CASO-MOR
        //               ,:TGA-PC-INTR-RIAT
        //                :IND-TGA-PC-INTR-RIAT
        //               ,:TGA-IMP-BNS-ANTIC
        //                :IND-TGA-IMP-BNS-ANTIC
        //               ,:TGA-PRE-INI-NET
        //                :IND-TGA-PRE-INI-NET
        //               ,:TGA-PRE-PP-INI
        //                :IND-TGA-PRE-PP-INI
        //               ,:TGA-PRE-PP-ULT
        //                :IND-TGA-PRE-PP-ULT
        //               ,:TGA-PRE-TARI-INI
        //                :IND-TGA-PRE-TARI-INI
        //               ,:TGA-PRE-TARI-ULT
        //                :IND-TGA-PRE-TARI-ULT
        //               ,:TGA-PRE-INVRIO-INI
        //                :IND-TGA-PRE-INVRIO-INI
        //               ,:TGA-PRE-INVRIO-ULT
        //                :IND-TGA-PRE-INVRIO-ULT
        //               ,:TGA-PRE-RIVTO
        //                :IND-TGA-PRE-RIVTO
        //               ,:TGA-IMP-SOPR-PROF
        //                :IND-TGA-IMP-SOPR-PROF
        //               ,:TGA-IMP-SOPR-SAN
        //                :IND-TGA-IMP-SOPR-SAN
        //               ,:TGA-IMP-SOPR-SPO
        //                :IND-TGA-IMP-SOPR-SPO
        //               ,:TGA-IMP-SOPR-TEC
        //                :IND-TGA-IMP-SOPR-TEC
        //               ,:TGA-IMP-ALT-SOPR
        //                :IND-TGA-IMP-ALT-SOPR
        //               ,:TGA-PRE-STAB
        //                :IND-TGA-PRE-STAB
        //               ,:TGA-DT-EFF-STAB-DB
        //                :IND-TGA-DT-EFF-STAB
        //               ,:TGA-TS-RIVAL-FIS
        //                :IND-TGA-TS-RIVAL-FIS
        //               ,:TGA-TS-RIVAL-INDICIZ
        //                :IND-TGA-TS-RIVAL-INDICIZ
        //               ,:TGA-OLD-TS-TEC
        //                :IND-TGA-OLD-TS-TEC
        //               ,:TGA-RAT-LRD
        //                :IND-TGA-RAT-LRD
        //               ,:TGA-PRE-LRD
        //                :IND-TGA-PRE-LRD
        //               ,:TGA-PRSTZ-INI
        //                :IND-TGA-PRSTZ-INI
        //               ,:TGA-PRSTZ-ULT
        //                :IND-TGA-PRSTZ-ULT
        //               ,:TGA-CPT-IN-OPZ-RIVTO
        //                :IND-TGA-CPT-IN-OPZ-RIVTO
        //               ,:TGA-PRSTZ-INI-STAB
        //                :IND-TGA-PRSTZ-INI-STAB
        //               ,:TGA-CPT-RSH-MOR
        //                :IND-TGA-CPT-RSH-MOR
        //               ,:TGA-PRSTZ-RID-INI
        //                :IND-TGA-PRSTZ-RID-INI
        //               ,:TGA-FL-CAR-CONT
        //                :IND-TGA-FL-CAR-CONT
        //               ,:TGA-BNS-GIA-LIQTO
        //                :IND-TGA-BNS-GIA-LIQTO
        //               ,:TGA-IMP-BNS
        //                :IND-TGA-IMP-BNS
        //               ,:TGA-COD-DVS
        //               ,:TGA-PRSTZ-INI-NEWFIS
        //                :IND-TGA-PRSTZ-INI-NEWFIS
        //               ,:TGA-IMP-SCON
        //                :IND-TGA-IMP-SCON
        //               ,:TGA-ALQ-SCON
        //                :IND-TGA-ALQ-SCON
        //               ,:TGA-IMP-CAR-ACQ
        //                :IND-TGA-IMP-CAR-ACQ
        //               ,:TGA-IMP-CAR-INC
        //                :IND-TGA-IMP-CAR-INC
        //               ,:TGA-IMP-CAR-GEST
        //                :IND-TGA-IMP-CAR-GEST
        //               ,:TGA-ETA-AA-1O-ASSTO
        //                :IND-TGA-ETA-AA-1O-ASSTO
        //               ,:TGA-ETA-MM-1O-ASSTO
        //                :IND-TGA-ETA-MM-1O-ASSTO
        //               ,:TGA-ETA-AA-2O-ASSTO
        //                :IND-TGA-ETA-AA-2O-ASSTO
        //               ,:TGA-ETA-MM-2O-ASSTO
        //                :IND-TGA-ETA-MM-2O-ASSTO
        //               ,:TGA-ETA-AA-3O-ASSTO
        //                :IND-TGA-ETA-AA-3O-ASSTO
        //               ,:TGA-ETA-MM-3O-ASSTO
        //                :IND-TGA-ETA-MM-3O-ASSTO
        //               ,:TGA-RENDTO-LRD
        //                :IND-TGA-RENDTO-LRD
        //               ,:TGA-PC-RETR
        //                :IND-TGA-PC-RETR
        //               ,:TGA-RENDTO-RETR
        //                :IND-TGA-RENDTO-RETR
        //               ,:TGA-MIN-GARTO
        //                :IND-TGA-MIN-GARTO
        //               ,:TGA-MIN-TRNUT
        //                :IND-TGA-MIN-TRNUT
        //               ,:TGA-PRE-ATT-DI-TRCH
        //                :IND-TGA-PRE-ATT-DI-TRCH
        //               ,:TGA-MATU-END2000
        //                :IND-TGA-MATU-END2000
        //               ,:TGA-ABB-TOT-INI
        //                :IND-TGA-ABB-TOT-INI
        //               ,:TGA-ABB-TOT-ULT
        //                :IND-TGA-ABB-TOT-ULT
        //               ,:TGA-ABB-ANNU-ULT
        //                :IND-TGA-ABB-ANNU-ULT
        //               ,:TGA-DUR-ABB
        //                :IND-TGA-DUR-ABB
        //               ,:TGA-TP-ADEG-ABB
        //                :IND-TGA-TP-ADEG-ABB
        //               ,:TGA-MOD-CALC
        //                :IND-TGA-MOD-CALC
        //               ,:TGA-IMP-AZ
        //                :IND-TGA-IMP-AZ
        //               ,:TGA-IMP-ADER
        //                :IND-TGA-IMP-ADER
        //               ,:TGA-IMP-TFR
        //                :IND-TGA-IMP-TFR
        //               ,:TGA-IMP-VOLO
        //                :IND-TGA-IMP-VOLO
        //               ,:TGA-VIS-END2000
        //                :IND-TGA-VIS-END2000
        //               ,:TGA-DT-VLDT-PROD-DB
        //                :IND-TGA-DT-VLDT-PROD
        //               ,:TGA-DT-INI-VAL-TAR-DB
        //                :IND-TGA-DT-INI-VAL-TAR
        //               ,:TGA-IMPB-VIS-END2000
        //                :IND-TGA-IMPB-VIS-END2000
        //               ,:TGA-REN-INI-TS-TEC-0
        //                :IND-TGA-REN-INI-TS-TEC-0
        //               ,:TGA-PC-RIP-PRE
        //                :IND-TGA-PC-RIP-PRE
        //               ,:TGA-FL-IMPORTI-FORZ
        //                :IND-TGA-FL-IMPORTI-FORZ
        //               ,:TGA-PRSTZ-INI-NFORZ
        //                :IND-TGA-PRSTZ-INI-NFORZ
        //               ,:TGA-VIS-END2000-NFORZ
        //                :IND-TGA-VIS-END2000-NFORZ
        //               ,:TGA-INTR-MORA
        //                :IND-TGA-INTR-MORA
        //               ,:TGA-MANFEE-ANTIC
        //                :IND-TGA-MANFEE-ANTIC
        //               ,:TGA-MANFEE-RICOR
        //                :IND-TGA-MANFEE-RICOR
        //               ,:TGA-PRE-UNI-RIVTO
        //                :IND-TGA-PRE-UNI-RIVTO
        //               ,:TGA-PROV-1AA-ACQ
        //                :IND-TGA-PROV-1AA-ACQ
        //               ,:TGA-PROV-2AA-ACQ
        //                :IND-TGA-PROV-2AA-ACQ
        //               ,:TGA-PROV-RICOR
        //                :IND-TGA-PROV-RICOR
        //               ,:TGA-PROV-INC
        //                :IND-TGA-PROV-INC
        //               ,:TGA-ALQ-PROV-ACQ
        //                :IND-TGA-ALQ-PROV-ACQ
        //               ,:TGA-ALQ-PROV-INC
        //                :IND-TGA-ALQ-PROV-INC
        //               ,:TGA-ALQ-PROV-RICOR
        //                :IND-TGA-ALQ-PROV-RICOR
        //               ,:TGA-IMPB-PROV-ACQ
        //                :IND-TGA-IMPB-PROV-ACQ
        //               ,:TGA-IMPB-PROV-INC
        //                :IND-TGA-IMPB-PROV-INC
        //               ,:TGA-IMPB-PROV-RICOR
        //                :IND-TGA-IMPB-PROV-RICOR
        //               ,:TGA-FL-PROV-FORZ
        //                :IND-TGA-FL-PROV-FORZ
        //               ,:TGA-PRSTZ-AGG-INI
        //                :IND-TGA-PRSTZ-AGG-INI
        //               ,:TGA-INCR-PRE
        //                :IND-TGA-INCR-PRE
        //               ,:TGA-INCR-PRSTZ
        //                :IND-TGA-INCR-PRSTZ
        //               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
        //                :IND-TGA-DT-ULT-ADEG-PRE-PR
        //               ,:TGA-PRSTZ-AGG-ULT
        //                :IND-TGA-PRSTZ-AGG-ULT
        //               ,:TGA-TS-RIVAL-NET
        //                :IND-TGA-TS-RIVAL-NET
        //               ,:TGA-PRE-PATTUITO
        //                :IND-TGA-PRE-PATTUITO
        //               ,:TGA-TP-RIVAL
        //                :IND-TGA-TP-RIVAL
        //               ,:TGA-RIS-MAT
        //                :IND-TGA-RIS-MAT
        //               ,:TGA-CPT-MIN-SCAD
        //                :IND-TGA-CPT-MIN-SCAD
        //               ,:TGA-COMMIS-GEST
        //                :IND-TGA-COMMIS-GEST
        //               ,:TGA-TP-MANFEE-APPL
        //                :IND-TGA-TP-MANFEE-APPL
        //               ,:TGA-DS-RIGA
        //               ,:TGA-DS-OPER-SQL
        //               ,:TGA-DS-VER
        //               ,:TGA-DS-TS-INI-CPTZ
        //               ,:TGA-DS-TS-END-CPTZ
        //               ,:TGA-DS-UTENTE
        //               ,:TGA-DS-STATO-ELAB
        //               ,:TGA-PC-COMMIS-GEST
        //                :IND-TGA-PC-COMMIS-GEST
        //               ,:TGA-NUM-GG-RIVAL
        //                :IND-TGA-NUM-GG-RIVAL
        //               ,:TGA-IMP-TRASFE
        //                :IND-TGA-IMP-TRASFE
        //               ,:TGA-IMP-TFR-STRC
        //                :IND-TGA-IMP-TFR-STRC
        //               ,:TGA-ACQ-EXP
        //                :IND-TGA-ACQ-EXP
        //               ,:TGA-REMUN-ASS
        //                :IND-TGA-REMUN-ASS
        //               ,:TGA-COMMIS-INTER
        //                :IND-TGA-COMMIS-INTER
        //               ,:TGA-ALQ-REMUN-ASS
        //                :IND-TGA-ALQ-REMUN-ASS
        //               ,:TGA-ALQ-COMMIS-INTER
        //                :IND-TGA-ALQ-COMMIS-INTER
        //               ,:TGA-IMPB-REMUN-ASS
        //                :IND-TGA-IMPB-REMUN-ASS
        //               ,:TGA-IMPB-COMMIS-INTER
        //                :IND-TGA-IMPB-COMMIS-INTER
        //               ,:TGA-COS-RUN-ASSVA
        //                :IND-TGA-COS-RUN-ASSVA
        //               ,:TGA-COS-RUN-ASSVA-IDC
        //                :IND-TGA-COS-RUN-ASSVA-IDC
        //           END-EXEC.
        trchDiGarDao.fetchCEff44(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF THRU A270-EX
            a270CloseCursorWcEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-CPZ CURSOR FOR
        //              SELECT
        //                     ID_TRCH_DI_GAR
        //                    ,ID_GAR
        //                    ,ID_ADES
        //                    ,ID_POLI
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_DECOR
        //                    ,DT_SCAD
        //                    ,IB_OGG
        //                    ,TP_RGM_FISC
        //                    ,DT_EMIS
        //                    ,TP_TRCH
        //                    ,DUR_AA
        //                    ,DUR_MM
        //                    ,DUR_GG
        //                    ,PRE_CASO_MOR
        //                    ,PC_INTR_RIAT
        //                    ,IMP_BNS_ANTIC
        //                    ,PRE_INI_NET
        //                    ,PRE_PP_INI
        //                    ,PRE_PP_ULT
        //                    ,PRE_TARI_INI
        //                    ,PRE_TARI_ULT
        //                    ,PRE_INVRIO_INI
        //                    ,PRE_INVRIO_ULT
        //                    ,PRE_RIVTO
        //                    ,IMP_SOPR_PROF
        //                    ,IMP_SOPR_SAN
        //                    ,IMP_SOPR_SPO
        //                    ,IMP_SOPR_TEC
        //                    ,IMP_ALT_SOPR
        //                    ,PRE_STAB
        //                    ,DT_EFF_STAB
        //                    ,TS_RIVAL_FIS
        //                    ,TS_RIVAL_INDICIZ
        //                    ,OLD_TS_TEC
        //                    ,RAT_LRD
        //                    ,PRE_LRD
        //                    ,PRSTZ_INI
        //                    ,PRSTZ_ULT
        //                    ,CPT_IN_OPZ_RIVTO
        //                    ,PRSTZ_INI_STAB
        //                    ,CPT_RSH_MOR
        //                    ,PRSTZ_RID_INI
        //                    ,FL_CAR_CONT
        //                    ,BNS_GIA_LIQTO
        //                    ,IMP_BNS
        //                    ,COD_DVS
        //                    ,PRSTZ_INI_NEWFIS
        //                    ,IMP_SCON
        //                    ,ALQ_SCON
        //                    ,IMP_CAR_ACQ
        //                    ,IMP_CAR_INC
        //                    ,IMP_CAR_GEST
        //                    ,ETA_AA_1O_ASSTO
        //                    ,ETA_MM_1O_ASSTO
        //                    ,ETA_AA_2O_ASSTO
        //                    ,ETA_MM_2O_ASSTO
        //                    ,ETA_AA_3O_ASSTO
        //                    ,ETA_MM_3O_ASSTO
        //                    ,RENDTO_LRD
        //                    ,PC_RETR
        //                    ,RENDTO_RETR
        //                    ,MIN_GARTO
        //                    ,MIN_TRNUT
        //                    ,PRE_ATT_DI_TRCH
        //                    ,MATU_END2000
        //                    ,ABB_TOT_INI
        //                    ,ABB_TOT_ULT
        //                    ,ABB_ANNU_ULT
        //                    ,DUR_ABB
        //                    ,TP_ADEG_ABB
        //                    ,MOD_CALC
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,VIS_END2000
        //                    ,DT_VLDT_PROD
        //                    ,DT_INI_VAL_TAR
        //                    ,IMPB_VIS_END2000
        //                    ,REN_INI_TS_TEC_0
        //                    ,PC_RIP_PRE
        //                    ,FL_IMPORTI_FORZ
        //                    ,PRSTZ_INI_NFORZ
        //                    ,VIS_END2000_NFORZ
        //                    ,INTR_MORA
        //                    ,MANFEE_ANTIC
        //                    ,MANFEE_RICOR
        //                    ,PRE_UNI_RIVTO
        //                    ,PROV_1AA_ACQ
        //                    ,PROV_2AA_ACQ
        //                    ,PROV_RICOR
        //                    ,PROV_INC
        //                    ,ALQ_PROV_ACQ
        //                    ,ALQ_PROV_INC
        //                    ,ALQ_PROV_RICOR
        //                    ,IMPB_PROV_ACQ
        //                    ,IMPB_PROV_INC
        //                    ,IMPB_PROV_RICOR
        //                    ,FL_PROV_FORZ
        //                    ,PRSTZ_AGG_INI
        //                    ,INCR_PRE
        //                    ,INCR_PRSTZ
        //                    ,DT_ULT_ADEG_PRE_PR
        //                    ,PRSTZ_AGG_ULT
        //                    ,TS_RIVAL_NET
        //                    ,PRE_PATTUITO
        //                    ,TP_RIVAL
        //                    ,RIS_MAT
        //                    ,CPT_MIN_SCAD
        //                    ,COMMIS_GEST
        //                    ,TP_MANFEE_APPL
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,PC_COMMIS_GEST
        //                    ,NUM_GG_RIVAL
        //                    ,IMP_TRASFE
        //                    ,IMP_TFR_STRC
        //                    ,ACQ_EXP
        //                    ,REMUN_ASS
        //                    ,COMMIS_INTER
        //                    ,ALQ_REMUN_ASS
        //                    ,ALQ_COMMIS_INTER
        //                    ,IMPB_REMUN_ASS
        //                    ,IMPB_COMMIS_INTER
        //                    ,COS_RUN_ASSVA
        //                    ,COS_RUN_ASSVA_IDC
        //              FROM TRCH_DI_GAR
        //              WHERE      ID_ADES     = :TGA-ID-ADES
        //                        AND ID_POLI     = :TGA-ID-POLI
        //                        AND ID_MOVI_CRZ = :TGA-ID-MOVI-CRZ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_TRCH_DI_GAR
        //                    ,ID_GAR
        //                    ,ID_ADES
        //                    ,ID_POLI
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_DECOR
        //                    ,DT_SCAD
        //                    ,IB_OGG
        //                    ,TP_RGM_FISC
        //                    ,DT_EMIS
        //                    ,TP_TRCH
        //                    ,DUR_AA
        //                    ,DUR_MM
        //                    ,DUR_GG
        //                    ,PRE_CASO_MOR
        //                    ,PC_INTR_RIAT
        //                    ,IMP_BNS_ANTIC
        //                    ,PRE_INI_NET
        //                    ,PRE_PP_INI
        //                    ,PRE_PP_ULT
        //                    ,PRE_TARI_INI
        //                    ,PRE_TARI_ULT
        //                    ,PRE_INVRIO_INI
        //                    ,PRE_INVRIO_ULT
        //                    ,PRE_RIVTO
        //                    ,IMP_SOPR_PROF
        //                    ,IMP_SOPR_SAN
        //                    ,IMP_SOPR_SPO
        //                    ,IMP_SOPR_TEC
        //                    ,IMP_ALT_SOPR
        //                    ,PRE_STAB
        //                    ,DT_EFF_STAB
        //                    ,TS_RIVAL_FIS
        //                    ,TS_RIVAL_INDICIZ
        //                    ,OLD_TS_TEC
        //                    ,RAT_LRD
        //                    ,PRE_LRD
        //                    ,PRSTZ_INI
        //                    ,PRSTZ_ULT
        //                    ,CPT_IN_OPZ_RIVTO
        //                    ,PRSTZ_INI_STAB
        //                    ,CPT_RSH_MOR
        //                    ,PRSTZ_RID_INI
        //                    ,FL_CAR_CONT
        //                    ,BNS_GIA_LIQTO
        //                    ,IMP_BNS
        //                    ,COD_DVS
        //                    ,PRSTZ_INI_NEWFIS
        //                    ,IMP_SCON
        //                    ,ALQ_SCON
        //                    ,IMP_CAR_ACQ
        //                    ,IMP_CAR_INC
        //                    ,IMP_CAR_GEST
        //                    ,ETA_AA_1O_ASSTO
        //                    ,ETA_MM_1O_ASSTO
        //                    ,ETA_AA_2O_ASSTO
        //                    ,ETA_MM_2O_ASSTO
        //                    ,ETA_AA_3O_ASSTO
        //                    ,ETA_MM_3O_ASSTO
        //                    ,RENDTO_LRD
        //                    ,PC_RETR
        //                    ,RENDTO_RETR
        //                    ,MIN_GARTO
        //                    ,MIN_TRNUT
        //                    ,PRE_ATT_DI_TRCH
        //                    ,MATU_END2000
        //                    ,ABB_TOT_INI
        //                    ,ABB_TOT_ULT
        //                    ,ABB_ANNU_ULT
        //                    ,DUR_ABB
        //                    ,TP_ADEG_ABB
        //                    ,MOD_CALC
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,VIS_END2000
        //                    ,DT_VLDT_PROD
        //                    ,DT_INI_VAL_TAR
        //                    ,IMPB_VIS_END2000
        //                    ,REN_INI_TS_TEC_0
        //                    ,PC_RIP_PRE
        //                    ,FL_IMPORTI_FORZ
        //                    ,PRSTZ_INI_NFORZ
        //                    ,VIS_END2000_NFORZ
        //                    ,INTR_MORA
        //                    ,MANFEE_ANTIC
        //                    ,MANFEE_RICOR
        //                    ,PRE_UNI_RIVTO
        //                    ,PROV_1AA_ACQ
        //                    ,PROV_2AA_ACQ
        //                    ,PROV_RICOR
        //                    ,PROV_INC
        //                    ,ALQ_PROV_ACQ
        //                    ,ALQ_PROV_INC
        //                    ,ALQ_PROV_RICOR
        //                    ,IMPB_PROV_ACQ
        //                    ,IMPB_PROV_INC
        //                    ,IMPB_PROV_RICOR
        //                    ,FL_PROV_FORZ
        //                    ,PRSTZ_AGG_INI
        //                    ,INCR_PRE
        //                    ,INCR_PRSTZ
        //                    ,DT_ULT_ADEG_PRE_PR
        //                    ,PRSTZ_AGG_ULT
        //                    ,TS_RIVAL_NET
        //                    ,PRE_PATTUITO
        //                    ,TP_RIVAL
        //                    ,RIS_MAT
        //                    ,CPT_MIN_SCAD
        //                    ,COMMIS_GEST
        //                    ,TP_MANFEE_APPL
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,PC_COMMIS_GEST
        //                    ,NUM_GG_RIVAL
        //                    ,IMP_TRASFE
        //                    ,IMP_TFR_STRC
        //                    ,ACQ_EXP
        //                    ,REMUN_ASS
        //                    ,COMMIS_INTER
        //                    ,ALQ_REMUN_ASS
        //                    ,ALQ_COMMIS_INTER
        //                    ,IMPB_REMUN_ASS
        //                    ,IMPB_COMMIS_INTER
        //                    ,COS_RUN_ASSVA
        //                    ,COS_RUN_ASSVA_IDC
        //             INTO
        //                :TGA-ID-TRCH-DI-GAR
        //               ,:TGA-ID-GAR
        //               ,:TGA-ID-ADES
        //               ,:TGA-ID-POLI
        //               ,:TGA-ID-MOVI-CRZ
        //               ,:TGA-ID-MOVI-CHIU
        //                :IND-TGA-ID-MOVI-CHIU
        //               ,:TGA-DT-INI-EFF-DB
        //               ,:TGA-DT-END-EFF-DB
        //               ,:TGA-COD-COMP-ANIA
        //               ,:TGA-DT-DECOR-DB
        //               ,:TGA-DT-SCAD-DB
        //                :IND-TGA-DT-SCAD
        //               ,:TGA-IB-OGG
        //                :IND-TGA-IB-OGG
        //               ,:TGA-TP-RGM-FISC
        //               ,:TGA-DT-EMIS-DB
        //                :IND-TGA-DT-EMIS
        //               ,:TGA-TP-TRCH
        //               ,:TGA-DUR-AA
        //                :IND-TGA-DUR-AA
        //               ,:TGA-DUR-MM
        //                :IND-TGA-DUR-MM
        //               ,:TGA-DUR-GG
        //                :IND-TGA-DUR-GG
        //               ,:TGA-PRE-CASO-MOR
        //                :IND-TGA-PRE-CASO-MOR
        //               ,:TGA-PC-INTR-RIAT
        //                :IND-TGA-PC-INTR-RIAT
        //               ,:TGA-IMP-BNS-ANTIC
        //                :IND-TGA-IMP-BNS-ANTIC
        //               ,:TGA-PRE-INI-NET
        //                :IND-TGA-PRE-INI-NET
        //               ,:TGA-PRE-PP-INI
        //                :IND-TGA-PRE-PP-INI
        //               ,:TGA-PRE-PP-ULT
        //                :IND-TGA-PRE-PP-ULT
        //               ,:TGA-PRE-TARI-INI
        //                :IND-TGA-PRE-TARI-INI
        //               ,:TGA-PRE-TARI-ULT
        //                :IND-TGA-PRE-TARI-ULT
        //               ,:TGA-PRE-INVRIO-INI
        //                :IND-TGA-PRE-INVRIO-INI
        //               ,:TGA-PRE-INVRIO-ULT
        //                :IND-TGA-PRE-INVRIO-ULT
        //               ,:TGA-PRE-RIVTO
        //                :IND-TGA-PRE-RIVTO
        //               ,:TGA-IMP-SOPR-PROF
        //                :IND-TGA-IMP-SOPR-PROF
        //               ,:TGA-IMP-SOPR-SAN
        //                :IND-TGA-IMP-SOPR-SAN
        //               ,:TGA-IMP-SOPR-SPO
        //                :IND-TGA-IMP-SOPR-SPO
        //               ,:TGA-IMP-SOPR-TEC
        //                :IND-TGA-IMP-SOPR-TEC
        //               ,:TGA-IMP-ALT-SOPR
        //                :IND-TGA-IMP-ALT-SOPR
        //               ,:TGA-PRE-STAB
        //                :IND-TGA-PRE-STAB
        //               ,:TGA-DT-EFF-STAB-DB
        //                :IND-TGA-DT-EFF-STAB
        //               ,:TGA-TS-RIVAL-FIS
        //                :IND-TGA-TS-RIVAL-FIS
        //               ,:TGA-TS-RIVAL-INDICIZ
        //                :IND-TGA-TS-RIVAL-INDICIZ
        //               ,:TGA-OLD-TS-TEC
        //                :IND-TGA-OLD-TS-TEC
        //               ,:TGA-RAT-LRD
        //                :IND-TGA-RAT-LRD
        //               ,:TGA-PRE-LRD
        //                :IND-TGA-PRE-LRD
        //               ,:TGA-PRSTZ-INI
        //                :IND-TGA-PRSTZ-INI
        //               ,:TGA-PRSTZ-ULT
        //                :IND-TGA-PRSTZ-ULT
        //               ,:TGA-CPT-IN-OPZ-RIVTO
        //                :IND-TGA-CPT-IN-OPZ-RIVTO
        //               ,:TGA-PRSTZ-INI-STAB
        //                :IND-TGA-PRSTZ-INI-STAB
        //               ,:TGA-CPT-RSH-MOR
        //                :IND-TGA-CPT-RSH-MOR
        //               ,:TGA-PRSTZ-RID-INI
        //                :IND-TGA-PRSTZ-RID-INI
        //               ,:TGA-FL-CAR-CONT
        //                :IND-TGA-FL-CAR-CONT
        //               ,:TGA-BNS-GIA-LIQTO
        //                :IND-TGA-BNS-GIA-LIQTO
        //               ,:TGA-IMP-BNS
        //                :IND-TGA-IMP-BNS
        //               ,:TGA-COD-DVS
        //               ,:TGA-PRSTZ-INI-NEWFIS
        //                :IND-TGA-PRSTZ-INI-NEWFIS
        //               ,:TGA-IMP-SCON
        //                :IND-TGA-IMP-SCON
        //               ,:TGA-ALQ-SCON
        //                :IND-TGA-ALQ-SCON
        //               ,:TGA-IMP-CAR-ACQ
        //                :IND-TGA-IMP-CAR-ACQ
        //               ,:TGA-IMP-CAR-INC
        //                :IND-TGA-IMP-CAR-INC
        //               ,:TGA-IMP-CAR-GEST
        //                :IND-TGA-IMP-CAR-GEST
        //               ,:TGA-ETA-AA-1O-ASSTO
        //                :IND-TGA-ETA-AA-1O-ASSTO
        //               ,:TGA-ETA-MM-1O-ASSTO
        //                :IND-TGA-ETA-MM-1O-ASSTO
        //               ,:TGA-ETA-AA-2O-ASSTO
        //                :IND-TGA-ETA-AA-2O-ASSTO
        //               ,:TGA-ETA-MM-2O-ASSTO
        //                :IND-TGA-ETA-MM-2O-ASSTO
        //               ,:TGA-ETA-AA-3O-ASSTO
        //                :IND-TGA-ETA-AA-3O-ASSTO
        //               ,:TGA-ETA-MM-3O-ASSTO
        //                :IND-TGA-ETA-MM-3O-ASSTO
        //               ,:TGA-RENDTO-LRD
        //                :IND-TGA-RENDTO-LRD
        //               ,:TGA-PC-RETR
        //                :IND-TGA-PC-RETR
        //               ,:TGA-RENDTO-RETR
        //                :IND-TGA-RENDTO-RETR
        //               ,:TGA-MIN-GARTO
        //                :IND-TGA-MIN-GARTO
        //               ,:TGA-MIN-TRNUT
        //                :IND-TGA-MIN-TRNUT
        //               ,:TGA-PRE-ATT-DI-TRCH
        //                :IND-TGA-PRE-ATT-DI-TRCH
        //               ,:TGA-MATU-END2000
        //                :IND-TGA-MATU-END2000
        //               ,:TGA-ABB-TOT-INI
        //                :IND-TGA-ABB-TOT-INI
        //               ,:TGA-ABB-TOT-ULT
        //                :IND-TGA-ABB-TOT-ULT
        //               ,:TGA-ABB-ANNU-ULT
        //                :IND-TGA-ABB-ANNU-ULT
        //               ,:TGA-DUR-ABB
        //                :IND-TGA-DUR-ABB
        //               ,:TGA-TP-ADEG-ABB
        //                :IND-TGA-TP-ADEG-ABB
        //               ,:TGA-MOD-CALC
        //                :IND-TGA-MOD-CALC
        //               ,:TGA-IMP-AZ
        //                :IND-TGA-IMP-AZ
        //               ,:TGA-IMP-ADER
        //                :IND-TGA-IMP-ADER
        //               ,:TGA-IMP-TFR
        //                :IND-TGA-IMP-TFR
        //               ,:TGA-IMP-VOLO
        //                :IND-TGA-IMP-VOLO
        //               ,:TGA-VIS-END2000
        //                :IND-TGA-VIS-END2000
        //               ,:TGA-DT-VLDT-PROD-DB
        //                :IND-TGA-DT-VLDT-PROD
        //               ,:TGA-DT-INI-VAL-TAR-DB
        //                :IND-TGA-DT-INI-VAL-TAR
        //               ,:TGA-IMPB-VIS-END2000
        //                :IND-TGA-IMPB-VIS-END2000
        //               ,:TGA-REN-INI-TS-TEC-0
        //                :IND-TGA-REN-INI-TS-TEC-0
        //               ,:TGA-PC-RIP-PRE
        //                :IND-TGA-PC-RIP-PRE
        //               ,:TGA-FL-IMPORTI-FORZ
        //                :IND-TGA-FL-IMPORTI-FORZ
        //               ,:TGA-PRSTZ-INI-NFORZ
        //                :IND-TGA-PRSTZ-INI-NFORZ
        //               ,:TGA-VIS-END2000-NFORZ
        //                :IND-TGA-VIS-END2000-NFORZ
        //               ,:TGA-INTR-MORA
        //                :IND-TGA-INTR-MORA
        //               ,:TGA-MANFEE-ANTIC
        //                :IND-TGA-MANFEE-ANTIC
        //               ,:TGA-MANFEE-RICOR
        //                :IND-TGA-MANFEE-RICOR
        //               ,:TGA-PRE-UNI-RIVTO
        //                :IND-TGA-PRE-UNI-RIVTO
        //               ,:TGA-PROV-1AA-ACQ
        //                :IND-TGA-PROV-1AA-ACQ
        //               ,:TGA-PROV-2AA-ACQ
        //                :IND-TGA-PROV-2AA-ACQ
        //               ,:TGA-PROV-RICOR
        //                :IND-TGA-PROV-RICOR
        //               ,:TGA-PROV-INC
        //                :IND-TGA-PROV-INC
        //               ,:TGA-ALQ-PROV-ACQ
        //                :IND-TGA-ALQ-PROV-ACQ
        //               ,:TGA-ALQ-PROV-INC
        //                :IND-TGA-ALQ-PROV-INC
        //               ,:TGA-ALQ-PROV-RICOR
        //                :IND-TGA-ALQ-PROV-RICOR
        //               ,:TGA-IMPB-PROV-ACQ
        //                :IND-TGA-IMPB-PROV-ACQ
        //               ,:TGA-IMPB-PROV-INC
        //                :IND-TGA-IMPB-PROV-INC
        //               ,:TGA-IMPB-PROV-RICOR
        //                :IND-TGA-IMPB-PROV-RICOR
        //               ,:TGA-FL-PROV-FORZ
        //                :IND-TGA-FL-PROV-FORZ
        //               ,:TGA-PRSTZ-AGG-INI
        //                :IND-TGA-PRSTZ-AGG-INI
        //               ,:TGA-INCR-PRE
        //                :IND-TGA-INCR-PRE
        //               ,:TGA-INCR-PRSTZ
        //                :IND-TGA-INCR-PRSTZ
        //               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
        //                :IND-TGA-DT-ULT-ADEG-PRE-PR
        //               ,:TGA-PRSTZ-AGG-ULT
        //                :IND-TGA-PRSTZ-AGG-ULT
        //               ,:TGA-TS-RIVAL-NET
        //                :IND-TGA-TS-RIVAL-NET
        //               ,:TGA-PRE-PATTUITO
        //                :IND-TGA-PRE-PATTUITO
        //               ,:TGA-TP-RIVAL
        //                :IND-TGA-TP-RIVAL
        //               ,:TGA-RIS-MAT
        //                :IND-TGA-RIS-MAT
        //               ,:TGA-CPT-MIN-SCAD
        //                :IND-TGA-CPT-MIN-SCAD
        //               ,:TGA-COMMIS-GEST
        //                :IND-TGA-COMMIS-GEST
        //               ,:TGA-TP-MANFEE-APPL
        //                :IND-TGA-TP-MANFEE-APPL
        //               ,:TGA-DS-RIGA
        //               ,:TGA-DS-OPER-SQL
        //               ,:TGA-DS-VER
        //               ,:TGA-DS-TS-INI-CPTZ
        //               ,:TGA-DS-TS-END-CPTZ
        //               ,:TGA-DS-UTENTE
        //               ,:TGA-DS-STATO-ELAB
        //               ,:TGA-PC-COMMIS-GEST
        //                :IND-TGA-PC-COMMIS-GEST
        //               ,:TGA-NUM-GG-RIVAL
        //                :IND-TGA-NUM-GG-RIVAL
        //               ,:TGA-IMP-TRASFE
        //                :IND-TGA-IMP-TRASFE
        //               ,:TGA-IMP-TFR-STRC
        //                :IND-TGA-IMP-TFR-STRC
        //               ,:TGA-ACQ-EXP
        //                :IND-TGA-ACQ-EXP
        //               ,:TGA-REMUN-ASS
        //                :IND-TGA-REMUN-ASS
        //               ,:TGA-COMMIS-INTER
        //                :IND-TGA-COMMIS-INTER
        //               ,:TGA-ALQ-REMUN-ASS
        //                :IND-TGA-ALQ-REMUN-ASS
        //               ,:TGA-ALQ-COMMIS-INTER
        //                :IND-TGA-ALQ-COMMIS-INTER
        //               ,:TGA-IMPB-REMUN-ASS
        //                :IND-TGA-IMPB-REMUN-ASS
        //               ,:TGA-IMPB-COMMIS-INTER
        //                :IND-TGA-IMPB-COMMIS-INTER
        //               ,:TGA-COS-RUN-ASSVA
        //                :IND-TGA-COS-RUN-ASSVA
        //               ,:TGA-COS-RUN-ASSVA-IDC
        //                :IND-TGA-COS-RUN-ASSVA-IDC
        //             FROM TRCH_DI_GAR
        //             WHERE      ID_ADES     = :TGA-ID-ADES
        //                    AND ID_POLI     = :TGA-ID-POLI
        //                    AND ID_MOVI_CRZ = :TGA-ID-MOVI-CRZ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        trchDiGarDao.selectRec9(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ
        //           END-EXEC.
        trchDiGarDao.openCCpz44(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ
        //           END-EXEC.
        trchDiGarDao.closeCCpz44();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ
        //           INTO
        //                :TGA-ID-TRCH-DI-GAR
        //               ,:TGA-ID-GAR
        //               ,:TGA-ID-ADES
        //               ,:TGA-ID-POLI
        //               ,:TGA-ID-MOVI-CRZ
        //               ,:TGA-ID-MOVI-CHIU
        //                :IND-TGA-ID-MOVI-CHIU
        //               ,:TGA-DT-INI-EFF-DB
        //               ,:TGA-DT-END-EFF-DB
        //               ,:TGA-COD-COMP-ANIA
        //               ,:TGA-DT-DECOR-DB
        //               ,:TGA-DT-SCAD-DB
        //                :IND-TGA-DT-SCAD
        //               ,:TGA-IB-OGG
        //                :IND-TGA-IB-OGG
        //               ,:TGA-TP-RGM-FISC
        //               ,:TGA-DT-EMIS-DB
        //                :IND-TGA-DT-EMIS
        //               ,:TGA-TP-TRCH
        //               ,:TGA-DUR-AA
        //                :IND-TGA-DUR-AA
        //               ,:TGA-DUR-MM
        //                :IND-TGA-DUR-MM
        //               ,:TGA-DUR-GG
        //                :IND-TGA-DUR-GG
        //               ,:TGA-PRE-CASO-MOR
        //                :IND-TGA-PRE-CASO-MOR
        //               ,:TGA-PC-INTR-RIAT
        //                :IND-TGA-PC-INTR-RIAT
        //               ,:TGA-IMP-BNS-ANTIC
        //                :IND-TGA-IMP-BNS-ANTIC
        //               ,:TGA-PRE-INI-NET
        //                :IND-TGA-PRE-INI-NET
        //               ,:TGA-PRE-PP-INI
        //                :IND-TGA-PRE-PP-INI
        //               ,:TGA-PRE-PP-ULT
        //                :IND-TGA-PRE-PP-ULT
        //               ,:TGA-PRE-TARI-INI
        //                :IND-TGA-PRE-TARI-INI
        //               ,:TGA-PRE-TARI-ULT
        //                :IND-TGA-PRE-TARI-ULT
        //               ,:TGA-PRE-INVRIO-INI
        //                :IND-TGA-PRE-INVRIO-INI
        //               ,:TGA-PRE-INVRIO-ULT
        //                :IND-TGA-PRE-INVRIO-ULT
        //               ,:TGA-PRE-RIVTO
        //                :IND-TGA-PRE-RIVTO
        //               ,:TGA-IMP-SOPR-PROF
        //                :IND-TGA-IMP-SOPR-PROF
        //               ,:TGA-IMP-SOPR-SAN
        //                :IND-TGA-IMP-SOPR-SAN
        //               ,:TGA-IMP-SOPR-SPO
        //                :IND-TGA-IMP-SOPR-SPO
        //               ,:TGA-IMP-SOPR-TEC
        //                :IND-TGA-IMP-SOPR-TEC
        //               ,:TGA-IMP-ALT-SOPR
        //                :IND-TGA-IMP-ALT-SOPR
        //               ,:TGA-PRE-STAB
        //                :IND-TGA-PRE-STAB
        //               ,:TGA-DT-EFF-STAB-DB
        //                :IND-TGA-DT-EFF-STAB
        //               ,:TGA-TS-RIVAL-FIS
        //                :IND-TGA-TS-RIVAL-FIS
        //               ,:TGA-TS-RIVAL-INDICIZ
        //                :IND-TGA-TS-RIVAL-INDICIZ
        //               ,:TGA-OLD-TS-TEC
        //                :IND-TGA-OLD-TS-TEC
        //               ,:TGA-RAT-LRD
        //                :IND-TGA-RAT-LRD
        //               ,:TGA-PRE-LRD
        //                :IND-TGA-PRE-LRD
        //               ,:TGA-PRSTZ-INI
        //                :IND-TGA-PRSTZ-INI
        //               ,:TGA-PRSTZ-ULT
        //                :IND-TGA-PRSTZ-ULT
        //               ,:TGA-CPT-IN-OPZ-RIVTO
        //                :IND-TGA-CPT-IN-OPZ-RIVTO
        //               ,:TGA-PRSTZ-INI-STAB
        //                :IND-TGA-PRSTZ-INI-STAB
        //               ,:TGA-CPT-RSH-MOR
        //                :IND-TGA-CPT-RSH-MOR
        //               ,:TGA-PRSTZ-RID-INI
        //                :IND-TGA-PRSTZ-RID-INI
        //               ,:TGA-FL-CAR-CONT
        //                :IND-TGA-FL-CAR-CONT
        //               ,:TGA-BNS-GIA-LIQTO
        //                :IND-TGA-BNS-GIA-LIQTO
        //               ,:TGA-IMP-BNS
        //                :IND-TGA-IMP-BNS
        //               ,:TGA-COD-DVS
        //               ,:TGA-PRSTZ-INI-NEWFIS
        //                :IND-TGA-PRSTZ-INI-NEWFIS
        //               ,:TGA-IMP-SCON
        //                :IND-TGA-IMP-SCON
        //               ,:TGA-ALQ-SCON
        //                :IND-TGA-ALQ-SCON
        //               ,:TGA-IMP-CAR-ACQ
        //                :IND-TGA-IMP-CAR-ACQ
        //               ,:TGA-IMP-CAR-INC
        //                :IND-TGA-IMP-CAR-INC
        //               ,:TGA-IMP-CAR-GEST
        //                :IND-TGA-IMP-CAR-GEST
        //               ,:TGA-ETA-AA-1O-ASSTO
        //                :IND-TGA-ETA-AA-1O-ASSTO
        //               ,:TGA-ETA-MM-1O-ASSTO
        //                :IND-TGA-ETA-MM-1O-ASSTO
        //               ,:TGA-ETA-AA-2O-ASSTO
        //                :IND-TGA-ETA-AA-2O-ASSTO
        //               ,:TGA-ETA-MM-2O-ASSTO
        //                :IND-TGA-ETA-MM-2O-ASSTO
        //               ,:TGA-ETA-AA-3O-ASSTO
        //                :IND-TGA-ETA-AA-3O-ASSTO
        //               ,:TGA-ETA-MM-3O-ASSTO
        //                :IND-TGA-ETA-MM-3O-ASSTO
        //               ,:TGA-RENDTO-LRD
        //                :IND-TGA-RENDTO-LRD
        //               ,:TGA-PC-RETR
        //                :IND-TGA-PC-RETR
        //               ,:TGA-RENDTO-RETR
        //                :IND-TGA-RENDTO-RETR
        //               ,:TGA-MIN-GARTO
        //                :IND-TGA-MIN-GARTO
        //               ,:TGA-MIN-TRNUT
        //                :IND-TGA-MIN-TRNUT
        //               ,:TGA-PRE-ATT-DI-TRCH
        //                :IND-TGA-PRE-ATT-DI-TRCH
        //               ,:TGA-MATU-END2000
        //                :IND-TGA-MATU-END2000
        //               ,:TGA-ABB-TOT-INI
        //                :IND-TGA-ABB-TOT-INI
        //               ,:TGA-ABB-TOT-ULT
        //                :IND-TGA-ABB-TOT-ULT
        //               ,:TGA-ABB-ANNU-ULT
        //                :IND-TGA-ABB-ANNU-ULT
        //               ,:TGA-DUR-ABB
        //                :IND-TGA-DUR-ABB
        //               ,:TGA-TP-ADEG-ABB
        //                :IND-TGA-TP-ADEG-ABB
        //               ,:TGA-MOD-CALC
        //                :IND-TGA-MOD-CALC
        //               ,:TGA-IMP-AZ
        //                :IND-TGA-IMP-AZ
        //               ,:TGA-IMP-ADER
        //                :IND-TGA-IMP-ADER
        //               ,:TGA-IMP-TFR
        //                :IND-TGA-IMP-TFR
        //               ,:TGA-IMP-VOLO
        //                :IND-TGA-IMP-VOLO
        //               ,:TGA-VIS-END2000
        //                :IND-TGA-VIS-END2000
        //               ,:TGA-DT-VLDT-PROD-DB
        //                :IND-TGA-DT-VLDT-PROD
        //               ,:TGA-DT-INI-VAL-TAR-DB
        //                :IND-TGA-DT-INI-VAL-TAR
        //               ,:TGA-IMPB-VIS-END2000
        //                :IND-TGA-IMPB-VIS-END2000
        //               ,:TGA-REN-INI-TS-TEC-0
        //                :IND-TGA-REN-INI-TS-TEC-0
        //               ,:TGA-PC-RIP-PRE
        //                :IND-TGA-PC-RIP-PRE
        //               ,:TGA-FL-IMPORTI-FORZ
        //                :IND-TGA-FL-IMPORTI-FORZ
        //               ,:TGA-PRSTZ-INI-NFORZ
        //                :IND-TGA-PRSTZ-INI-NFORZ
        //               ,:TGA-VIS-END2000-NFORZ
        //                :IND-TGA-VIS-END2000-NFORZ
        //               ,:TGA-INTR-MORA
        //                :IND-TGA-INTR-MORA
        //               ,:TGA-MANFEE-ANTIC
        //                :IND-TGA-MANFEE-ANTIC
        //               ,:TGA-MANFEE-RICOR
        //                :IND-TGA-MANFEE-RICOR
        //               ,:TGA-PRE-UNI-RIVTO
        //                :IND-TGA-PRE-UNI-RIVTO
        //               ,:TGA-PROV-1AA-ACQ
        //                :IND-TGA-PROV-1AA-ACQ
        //               ,:TGA-PROV-2AA-ACQ
        //                :IND-TGA-PROV-2AA-ACQ
        //               ,:TGA-PROV-RICOR
        //                :IND-TGA-PROV-RICOR
        //               ,:TGA-PROV-INC
        //                :IND-TGA-PROV-INC
        //               ,:TGA-ALQ-PROV-ACQ
        //                :IND-TGA-ALQ-PROV-ACQ
        //               ,:TGA-ALQ-PROV-INC
        //                :IND-TGA-ALQ-PROV-INC
        //               ,:TGA-ALQ-PROV-RICOR
        //                :IND-TGA-ALQ-PROV-RICOR
        //               ,:TGA-IMPB-PROV-ACQ
        //                :IND-TGA-IMPB-PROV-ACQ
        //               ,:TGA-IMPB-PROV-INC
        //                :IND-TGA-IMPB-PROV-INC
        //               ,:TGA-IMPB-PROV-RICOR
        //                :IND-TGA-IMPB-PROV-RICOR
        //               ,:TGA-FL-PROV-FORZ
        //                :IND-TGA-FL-PROV-FORZ
        //               ,:TGA-PRSTZ-AGG-INI
        //                :IND-TGA-PRSTZ-AGG-INI
        //               ,:TGA-INCR-PRE
        //                :IND-TGA-INCR-PRE
        //               ,:TGA-INCR-PRSTZ
        //                :IND-TGA-INCR-PRSTZ
        //               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
        //                :IND-TGA-DT-ULT-ADEG-PRE-PR
        //               ,:TGA-PRSTZ-AGG-ULT
        //                :IND-TGA-PRSTZ-AGG-ULT
        //               ,:TGA-TS-RIVAL-NET
        //                :IND-TGA-TS-RIVAL-NET
        //               ,:TGA-PRE-PATTUITO
        //                :IND-TGA-PRE-PATTUITO
        //               ,:TGA-TP-RIVAL
        //                :IND-TGA-TP-RIVAL
        //               ,:TGA-RIS-MAT
        //                :IND-TGA-RIS-MAT
        //               ,:TGA-CPT-MIN-SCAD
        //                :IND-TGA-CPT-MIN-SCAD
        //               ,:TGA-COMMIS-GEST
        //                :IND-TGA-COMMIS-GEST
        //               ,:TGA-TP-MANFEE-APPL
        //                :IND-TGA-TP-MANFEE-APPL
        //               ,:TGA-DS-RIGA
        //               ,:TGA-DS-OPER-SQL
        //               ,:TGA-DS-VER
        //               ,:TGA-DS-TS-INI-CPTZ
        //               ,:TGA-DS-TS-END-CPTZ
        //               ,:TGA-DS-UTENTE
        //               ,:TGA-DS-STATO-ELAB
        //               ,:TGA-PC-COMMIS-GEST
        //                :IND-TGA-PC-COMMIS-GEST
        //               ,:TGA-NUM-GG-RIVAL
        //                :IND-TGA-NUM-GG-RIVAL
        //               ,:TGA-IMP-TRASFE
        //                :IND-TGA-IMP-TRASFE
        //               ,:TGA-IMP-TFR-STRC
        //                :IND-TGA-IMP-TFR-STRC
        //               ,:TGA-ACQ-EXP
        //                :IND-TGA-ACQ-EXP
        //               ,:TGA-REMUN-ASS
        //                :IND-TGA-REMUN-ASS
        //               ,:TGA-COMMIS-INTER
        //                :IND-TGA-COMMIS-INTER
        //               ,:TGA-ALQ-REMUN-ASS
        //                :IND-TGA-ALQ-REMUN-ASS
        //               ,:TGA-ALQ-COMMIS-INTER
        //                :IND-TGA-ALQ-COMMIS-INTER
        //               ,:TGA-IMPB-REMUN-ASS
        //                :IND-TGA-IMPB-REMUN-ASS
        //               ,:TGA-IMPB-COMMIS-INTER
        //                :IND-TGA-IMPB-COMMIS-INTER
        //               ,:TGA-COS-RUN-ASSVA
        //                :IND-TGA-COS-RUN-ASSVA
        //               ,:TGA-COS-RUN-ASSVA-IDC
        //                :IND-TGA-COS-RUN-ASSVA-IDC
        //           END-EXEC.
        trchDiGarDao.fetchCCpz44(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ THRU B270-EX
            b270CloseCursorWcCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-TGA-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO TGA-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ID-MOVI-CHIU-NULL
            trchDiGar.getTgaIdMoviChiu().setTgaIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaIdMoviChiu.Len.TGA_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-TGA-DT-SCAD = -1
        //              MOVE HIGH-VALUES TO TGA-DT-SCAD-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDtScad() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DT-SCAD-NULL
            trchDiGar.getTgaDtScad().setTgaDtScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDtScad.Len.TGA_DT_SCAD_NULL));
        }
        // COB_CODE: IF IND-TGA-IB-OGG = -1
        //              MOVE HIGH-VALUES TO TGA-IB-OGG-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getIbOgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IB-OGG-NULL
            trchDiGar.setTgaIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TrchDiGar.Len.TGA_IB_OGG));
        }
        // COB_CODE: IF IND-TGA-DT-EMIS = -1
        //              MOVE HIGH-VALUES TO TGA-DT-EMIS-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDtEmis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DT-EMIS-NULL
            trchDiGar.getTgaDtEmis().setTgaDtEmisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDtEmis.Len.TGA_DT_EMIS_NULL));
        }
        // COB_CODE: IF IND-TGA-DUR-AA = -1
        //              MOVE HIGH-VALUES TO TGA-DUR-AA-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDurAa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DUR-AA-NULL
            trchDiGar.getTgaDurAa().setTgaDurAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDurAa.Len.TGA_DUR_AA_NULL));
        }
        // COB_CODE: IF IND-TGA-DUR-MM = -1
        //              MOVE HIGH-VALUES TO TGA-DUR-MM-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDurMm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DUR-MM-NULL
            trchDiGar.getTgaDurMm().setTgaDurMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDurMm.Len.TGA_DUR_MM_NULL));
        }
        // COB_CODE: IF IND-TGA-DUR-GG = -1
        //              MOVE HIGH-VALUES TO TGA-DUR-GG-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDurGg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DUR-GG-NULL
            trchDiGar.getTgaDurGg().setTgaDurGgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDurGg.Len.TGA_DUR_GG_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-CASO-MOR = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-CASO-MOR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreCasoMor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-CASO-MOR-NULL
            trchDiGar.getTgaPreCasoMor().setTgaPreCasoMorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreCasoMor.Len.TGA_PRE_CASO_MOR_NULL));
        }
        // COB_CODE: IF IND-TGA-PC-INTR-RIAT = -1
        //              MOVE HIGH-VALUES TO TGA-PC-INTR-RIAT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPcIntrRiat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PC-INTR-RIAT-NULL
            trchDiGar.getTgaPcIntrRiat().setTgaPcIntrRiatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPcIntrRiat.Len.TGA_PC_INTR_RIAT_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-BNS-ANTIC = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-BNS-ANTIC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpBnsAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-BNS-ANTIC-NULL
            trchDiGar.getTgaImpBnsAntic().setTgaImpBnsAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpBnsAntic.Len.TGA_IMP_BNS_ANTIC_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-INI-NET = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-INI-NET-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreIniNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-INI-NET-NULL
            trchDiGar.getTgaPreIniNet().setTgaPreIniNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreIniNet.Len.TGA_PRE_INI_NET_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-PP-INI = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-PP-INI-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrePpIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-PP-INI-NULL
            trchDiGar.getTgaPrePpIni().setTgaPrePpIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrePpIni.Len.TGA_PRE_PP_INI_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-PP-ULT = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-PP-ULT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrePpUlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-PP-ULT-NULL
            trchDiGar.getTgaPrePpUlt().setTgaPrePpUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrePpUlt.Len.TGA_PRE_PP_ULT_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-TARI-INI = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-TARI-INI-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreTariIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-TARI-INI-NULL
            trchDiGar.getTgaPreTariIni().setTgaPreTariIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreTariIni.Len.TGA_PRE_TARI_INI_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-TARI-ULT = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-TARI-ULT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreTariUlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-TARI-ULT-NULL
            trchDiGar.getTgaPreTariUlt().setTgaPreTariUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreTariUlt.Len.TGA_PRE_TARI_ULT_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-INVRIO-INI = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-INVRIO-INI-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreInvrioIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-INVRIO-INI-NULL
            trchDiGar.getTgaPreInvrioIni().setTgaPreInvrioIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreInvrioIni.Len.TGA_PRE_INVRIO_INI_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-INVRIO-ULT = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-INVRIO-ULT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreInvrioUlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-INVRIO-ULT-NULL
            trchDiGar.getTgaPreInvrioUlt().setTgaPreInvrioUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreInvrioUlt.Len.TGA_PRE_INVRIO_ULT_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-RIVTO = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-RIVTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreRivto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-RIVTO-NULL
            trchDiGar.getTgaPreRivto().setTgaPreRivtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreRivto.Len.TGA_PRE_RIVTO_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-SOPR-PROF = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-SOPR-PROF-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpSoprProf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-SOPR-PROF-NULL
            trchDiGar.getTgaImpSoprProf().setTgaImpSoprProfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpSoprProf.Len.TGA_IMP_SOPR_PROF_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-SOPR-SAN = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-SOPR-SAN-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpSoprSan() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-SOPR-SAN-NULL
            trchDiGar.getTgaImpSoprSan().setTgaImpSoprSanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpSoprSan.Len.TGA_IMP_SOPR_SAN_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-SOPR-SPO = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-SOPR-SPO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpSoprSpo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-SOPR-SPO-NULL
            trchDiGar.getTgaImpSoprSpo().setTgaImpSoprSpoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpSoprSpo.Len.TGA_IMP_SOPR_SPO_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-SOPR-TEC = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-SOPR-TEC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpSoprTec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-SOPR-TEC-NULL
            trchDiGar.getTgaImpSoprTec().setTgaImpSoprTecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpSoprTec.Len.TGA_IMP_SOPR_TEC_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-ALT-SOPR = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-ALT-SOPR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpAltSopr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-ALT-SOPR-NULL
            trchDiGar.getTgaImpAltSopr().setTgaImpAltSoprNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpAltSopr.Len.TGA_IMP_ALT_SOPR_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-STAB = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-STAB-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreStab() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-STAB-NULL
            trchDiGar.getTgaPreStab().setTgaPreStabNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreStab.Len.TGA_PRE_STAB_NULL));
        }
        // COB_CODE: IF IND-TGA-DT-EFF-STAB = -1
        //              MOVE HIGH-VALUES TO TGA-DT-EFF-STAB-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDtEffStab() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DT-EFF-STAB-NULL
            trchDiGar.getTgaDtEffStab().setTgaDtEffStabNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDtEffStab.Len.TGA_DT_EFF_STAB_NULL));
        }
        // COB_CODE: IF IND-TGA-TS-RIVAL-FIS = -1
        //              MOVE HIGH-VALUES TO TGA-TS-RIVAL-FIS-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getTsRivalFis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-TS-RIVAL-FIS-NULL
            trchDiGar.getTgaTsRivalFis().setTgaTsRivalFisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaTsRivalFis.Len.TGA_TS_RIVAL_FIS_NULL));
        }
        // COB_CODE: IF IND-TGA-TS-RIVAL-INDICIZ = -1
        //              MOVE HIGH-VALUES TO TGA-TS-RIVAL-INDICIZ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getTsRivalIndiciz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-TS-RIVAL-INDICIZ-NULL
            trchDiGar.getTgaTsRivalIndiciz().setTgaTsRivalIndicizNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaTsRivalIndiciz.Len.TGA_TS_RIVAL_INDICIZ_NULL));
        }
        // COB_CODE: IF IND-TGA-OLD-TS-TEC = -1
        //              MOVE HIGH-VALUES TO TGA-OLD-TS-TEC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getOldTsTec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-OLD-TS-TEC-NULL
            trchDiGar.getTgaOldTsTec().setTgaOldTsTecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaOldTsTec.Len.TGA_OLD_TS_TEC_NULL));
        }
        // COB_CODE: IF IND-TGA-RAT-LRD = -1
        //              MOVE HIGH-VALUES TO TGA-RAT-LRD-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getRatLrd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-RAT-LRD-NULL
            trchDiGar.getTgaRatLrd().setTgaRatLrdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaRatLrd.Len.TGA_RAT_LRD_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-LRD = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-LRD-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreLrd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-LRD-NULL
            trchDiGar.getTgaPreLrd().setTgaPreLrdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreLrd.Len.TGA_PRE_LRD_NULL));
        }
        // COB_CODE: IF IND-TGA-PRSTZ-INI = -1
        //              MOVE HIGH-VALUES TO TGA-PRSTZ-INI-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrstzIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRSTZ-INI-NULL
            trchDiGar.getTgaPrstzIni().setTgaPrstzIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrstzIni.Len.TGA_PRSTZ_INI_NULL));
        }
        // COB_CODE: IF IND-TGA-PRSTZ-ULT = -1
        //              MOVE HIGH-VALUES TO TGA-PRSTZ-ULT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrstzUlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRSTZ-ULT-NULL
            trchDiGar.getTgaPrstzUlt().setTgaPrstzUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrstzUlt.Len.TGA_PRSTZ_ULT_NULL));
        }
        // COB_CODE: IF IND-TGA-CPT-IN-OPZ-RIVTO = -1
        //              MOVE HIGH-VALUES TO TGA-CPT-IN-OPZ-RIVTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getCptInOpzRivto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-CPT-IN-OPZ-RIVTO-NULL
            trchDiGar.getTgaCptInOpzRivto().setTgaCptInOpzRivtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaCptInOpzRivto.Len.TGA_CPT_IN_OPZ_RIVTO_NULL));
        }
        // COB_CODE: IF IND-TGA-PRSTZ-INI-STAB = -1
        //              MOVE HIGH-VALUES TO TGA-PRSTZ-INI-STAB-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrstzIniStab() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRSTZ-INI-STAB-NULL
            trchDiGar.getTgaPrstzIniStab().setTgaPrstzIniStabNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrstzIniStab.Len.TGA_PRSTZ_INI_STAB_NULL));
        }
        // COB_CODE: IF IND-TGA-CPT-RSH-MOR = -1
        //              MOVE HIGH-VALUES TO TGA-CPT-RSH-MOR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getCptRshMor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-CPT-RSH-MOR-NULL
            trchDiGar.getTgaCptRshMor().setTgaCptRshMorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaCptRshMor.Len.TGA_CPT_RSH_MOR_NULL));
        }
        // COB_CODE: IF IND-TGA-PRSTZ-RID-INI = -1
        //              MOVE HIGH-VALUES TO TGA-PRSTZ-RID-INI-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrstzRidIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRSTZ-RID-INI-NULL
            trchDiGar.getTgaPrstzRidIni().setTgaPrstzRidIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrstzRidIni.Len.TGA_PRSTZ_RID_INI_NULL));
        }
        // COB_CODE: IF IND-TGA-FL-CAR-CONT = -1
        //              MOVE HIGH-VALUES TO TGA-FL-CAR-CONT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getFlCarCont() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-FL-CAR-CONT-NULL
            trchDiGar.setTgaFlCarCont(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TGA-BNS-GIA-LIQTO = -1
        //              MOVE HIGH-VALUES TO TGA-BNS-GIA-LIQTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getBnsGiaLiqto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-BNS-GIA-LIQTO-NULL
            trchDiGar.getTgaBnsGiaLiqto().setTgaBnsGiaLiqtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaBnsGiaLiqto.Len.TGA_BNS_GIA_LIQTO_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-BNS = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-BNS-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpBns() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-BNS-NULL
            trchDiGar.getTgaImpBns().setTgaImpBnsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpBns.Len.TGA_IMP_BNS_NULL));
        }
        // COB_CODE: IF IND-TGA-PRSTZ-INI-NEWFIS = -1
        //              MOVE HIGH-VALUES TO TGA-PRSTZ-INI-NEWFIS-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrstzIniNewfis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRSTZ-INI-NEWFIS-NULL
            trchDiGar.getTgaPrstzIniNewfis().setTgaPrstzIniNewfisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrstzIniNewfis.Len.TGA_PRSTZ_INI_NEWFIS_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-SCON = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-SCON-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpScon() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-SCON-NULL
            trchDiGar.getTgaImpScon().setTgaImpSconNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpScon.Len.TGA_IMP_SCON_NULL));
        }
        // COB_CODE: IF IND-TGA-ALQ-SCON = -1
        //              MOVE HIGH-VALUES TO TGA-ALQ-SCON-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAlqScon() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ALQ-SCON-NULL
            trchDiGar.getTgaAlqScon().setTgaAlqSconNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAlqScon.Len.TGA_ALQ_SCON_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-CAR-ACQ = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-CAR-ACQ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpCarAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-CAR-ACQ-NULL
            trchDiGar.getTgaImpCarAcq().setTgaImpCarAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpCarAcq.Len.TGA_IMP_CAR_ACQ_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-CAR-INC = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-CAR-INC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpCarInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-CAR-INC-NULL
            trchDiGar.getTgaImpCarInc().setTgaImpCarIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpCarInc.Len.TGA_IMP_CAR_INC_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-CAR-GEST = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-CAR-GEST-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpCarGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-CAR-GEST-NULL
            trchDiGar.getTgaImpCarGest().setTgaImpCarGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpCarGest.Len.TGA_IMP_CAR_GEST_NULL));
        }
        // COB_CODE: IF IND-TGA-ETA-AA-1O-ASSTO = -1
        //              MOVE HIGH-VALUES TO TGA-ETA-AA-1O-ASSTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getEtaAa1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ETA-AA-1O-ASSTO-NULL
            trchDiGar.getTgaEtaAa1oAssto().setTgaEtaAa1oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaEtaAa1oAssto.Len.TGA_ETA_AA1O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-TGA-ETA-MM-1O-ASSTO = -1
        //              MOVE HIGH-VALUES TO TGA-ETA-MM-1O-ASSTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getEtaMm1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ETA-MM-1O-ASSTO-NULL
            trchDiGar.getTgaEtaMm1oAssto().setTgaEtaMm1oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaEtaMm1oAssto.Len.TGA_ETA_MM1O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-TGA-ETA-AA-2O-ASSTO = -1
        //              MOVE HIGH-VALUES TO TGA-ETA-AA-2O-ASSTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getEtaAa2oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ETA-AA-2O-ASSTO-NULL
            trchDiGar.getTgaEtaAa2oAssto().setTgaEtaAa2oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaEtaAa2oAssto.Len.TGA_ETA_AA2O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-TGA-ETA-MM-2O-ASSTO = -1
        //              MOVE HIGH-VALUES TO TGA-ETA-MM-2O-ASSTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getEtaMm2oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ETA-MM-2O-ASSTO-NULL
            trchDiGar.getTgaEtaMm2oAssto().setTgaEtaMm2oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaEtaMm2oAssto.Len.TGA_ETA_MM2O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-TGA-ETA-AA-3O-ASSTO = -1
        //              MOVE HIGH-VALUES TO TGA-ETA-AA-3O-ASSTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getEtaAa3oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ETA-AA-3O-ASSTO-NULL
            trchDiGar.getTgaEtaAa3oAssto().setTgaEtaAa3oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaEtaAa3oAssto.Len.TGA_ETA_AA3O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-TGA-ETA-MM-3O-ASSTO = -1
        //              MOVE HIGH-VALUES TO TGA-ETA-MM-3O-ASSTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getEtaMm3oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ETA-MM-3O-ASSTO-NULL
            trchDiGar.getTgaEtaMm3oAssto().setTgaEtaMm3oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaEtaMm3oAssto.Len.TGA_ETA_MM3O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-TGA-RENDTO-LRD = -1
        //              MOVE HIGH-VALUES TO TGA-RENDTO-LRD-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getRendtoLrd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-RENDTO-LRD-NULL
            trchDiGar.getTgaRendtoLrd().setTgaRendtoLrdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaRendtoLrd.Len.TGA_RENDTO_LRD_NULL));
        }
        // COB_CODE: IF IND-TGA-PC-RETR = -1
        //              MOVE HIGH-VALUES TO TGA-PC-RETR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPcRetr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PC-RETR-NULL
            trchDiGar.getTgaPcRetr().setTgaPcRetrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPcRetr.Len.TGA_PC_RETR_NULL));
        }
        // COB_CODE: IF IND-TGA-RENDTO-RETR = -1
        //              MOVE HIGH-VALUES TO TGA-RENDTO-RETR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getRendtoRetr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-RENDTO-RETR-NULL
            trchDiGar.getTgaRendtoRetr().setTgaRendtoRetrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaRendtoRetr.Len.TGA_RENDTO_RETR_NULL));
        }
        // COB_CODE: IF IND-TGA-MIN-GARTO = -1
        //              MOVE HIGH-VALUES TO TGA-MIN-GARTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getMinGarto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-MIN-GARTO-NULL
            trchDiGar.getTgaMinGarto().setTgaMinGartoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaMinGarto.Len.TGA_MIN_GARTO_NULL));
        }
        // COB_CODE: IF IND-TGA-MIN-TRNUT = -1
        //              MOVE HIGH-VALUES TO TGA-MIN-TRNUT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getMinTrnut() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-MIN-TRNUT-NULL
            trchDiGar.getTgaMinTrnut().setTgaMinTrnutNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaMinTrnut.Len.TGA_MIN_TRNUT_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-ATT-DI-TRCH = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-ATT-DI-TRCH-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreAttDiTrch() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-ATT-DI-TRCH-NULL
            trchDiGar.getTgaPreAttDiTrch().setTgaPreAttDiTrchNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreAttDiTrch.Len.TGA_PRE_ATT_DI_TRCH_NULL));
        }
        // COB_CODE: IF IND-TGA-MATU-END2000 = -1
        //              MOVE HIGH-VALUES TO TGA-MATU-END2000-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getMatuEnd2000() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-MATU-END2000-NULL
            trchDiGar.getTgaMatuEnd2000().setTgaMatuEnd2000Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaMatuEnd2000.Len.TGA_MATU_END2000_NULL));
        }
        // COB_CODE: IF IND-TGA-ABB-TOT-INI = -1
        //              MOVE HIGH-VALUES TO TGA-ABB-TOT-INI-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAbbTotIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ABB-TOT-INI-NULL
            trchDiGar.getTgaAbbTotIni().setTgaAbbTotIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAbbTotIni.Len.TGA_ABB_TOT_INI_NULL));
        }
        // COB_CODE: IF IND-TGA-ABB-TOT-ULT = -1
        //              MOVE HIGH-VALUES TO TGA-ABB-TOT-ULT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAbbTotUlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ABB-TOT-ULT-NULL
            trchDiGar.getTgaAbbTotUlt().setTgaAbbTotUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAbbTotUlt.Len.TGA_ABB_TOT_ULT_NULL));
        }
        // COB_CODE: IF IND-TGA-ABB-ANNU-ULT = -1
        //              MOVE HIGH-VALUES TO TGA-ABB-ANNU-ULT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAbbAnnuUlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ABB-ANNU-ULT-NULL
            trchDiGar.getTgaAbbAnnuUlt().setTgaAbbAnnuUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAbbAnnuUlt.Len.TGA_ABB_ANNU_ULT_NULL));
        }
        // COB_CODE: IF IND-TGA-DUR-ABB = -1
        //              MOVE HIGH-VALUES TO TGA-DUR-ABB-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDurAbb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DUR-ABB-NULL
            trchDiGar.getTgaDurAbb().setTgaDurAbbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDurAbb.Len.TGA_DUR_ABB_NULL));
        }
        // COB_CODE: IF IND-TGA-TP-ADEG-ABB = -1
        //              MOVE HIGH-VALUES TO TGA-TP-ADEG-ABB-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getTpAdegAbb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-TP-ADEG-ABB-NULL
            trchDiGar.setTgaTpAdegAbb(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TGA-MOD-CALC = -1
        //              MOVE HIGH-VALUES TO TGA-MOD-CALC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getModCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-MOD-CALC-NULL
            trchDiGar.setTgaModCalc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TrchDiGar.Len.TGA_MOD_CALC));
        }
        // COB_CODE: IF IND-TGA-IMP-AZ = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-AZ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-AZ-NULL
            trchDiGar.getTgaImpAz().setTgaImpAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpAz.Len.TGA_IMP_AZ_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-ADER = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-ADER-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpAder() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-ADER-NULL
            trchDiGar.getTgaImpAder().setTgaImpAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpAder.Len.TGA_IMP_ADER_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-TFR = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-TFR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-TFR-NULL
            trchDiGar.getTgaImpTfr().setTgaImpTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpTfr.Len.TGA_IMP_TFR_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-VOLO = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-VOLO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpVolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-VOLO-NULL
            trchDiGar.getTgaImpVolo().setTgaImpVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpVolo.Len.TGA_IMP_VOLO_NULL));
        }
        // COB_CODE: IF IND-TGA-VIS-END2000 = -1
        //              MOVE HIGH-VALUES TO TGA-VIS-END2000-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getVisEnd2000() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-VIS-END2000-NULL
            trchDiGar.getTgaVisEnd2000().setTgaVisEnd2000Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaVisEnd2000.Len.TGA_VIS_END2000_NULL));
        }
        // COB_CODE: IF IND-TGA-DT-VLDT-PROD = -1
        //              MOVE HIGH-VALUES TO TGA-DT-VLDT-PROD-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDtVldtProd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DT-VLDT-PROD-NULL
            trchDiGar.getTgaDtVldtProd().setTgaDtVldtProdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDtVldtProd.Len.TGA_DT_VLDT_PROD_NULL));
        }
        // COB_CODE: IF IND-TGA-DT-INI-VAL-TAR = -1
        //              MOVE HIGH-VALUES TO TGA-DT-INI-VAL-TAR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDtIniValTar() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DT-INI-VAL-TAR-NULL
            trchDiGar.getTgaDtIniValTar().setTgaDtIniValTarNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDtIniValTar.Len.TGA_DT_INI_VAL_TAR_NULL));
        }
        // COB_CODE: IF IND-TGA-IMPB-VIS-END2000 = -1
        //              MOVE HIGH-VALUES TO TGA-IMPB-VIS-END2000-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpbVisEnd2000() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMPB-VIS-END2000-NULL
            trchDiGar.getTgaImpbVisEnd2000().setTgaImpbVisEnd2000Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpbVisEnd2000.Len.TGA_IMPB_VIS_END2000_NULL));
        }
        // COB_CODE: IF IND-TGA-REN-INI-TS-TEC-0 = -1
        //              MOVE HIGH-VALUES TO TGA-REN-INI-TS-TEC-0-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getRenIniTsTec0() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-REN-INI-TS-TEC-0-NULL
            trchDiGar.getTgaRenIniTsTec0().setTgaRenIniTsTec0Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaRenIniTsTec0.Len.TGA_REN_INI_TS_TEC0_NULL));
        }
        // COB_CODE: IF IND-TGA-PC-RIP-PRE = -1
        //              MOVE HIGH-VALUES TO TGA-PC-RIP-PRE-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPcRipPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PC-RIP-PRE-NULL
            trchDiGar.getTgaPcRipPre().setTgaPcRipPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPcRipPre.Len.TGA_PC_RIP_PRE_NULL));
        }
        // COB_CODE: IF IND-TGA-FL-IMPORTI-FORZ = -1
        //              MOVE HIGH-VALUES TO TGA-FL-IMPORTI-FORZ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getFlImportiForz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-FL-IMPORTI-FORZ-NULL
            trchDiGar.setTgaFlImportiForz(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TGA-PRSTZ-INI-NFORZ = -1
        //              MOVE HIGH-VALUES TO TGA-PRSTZ-INI-NFORZ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrstzIniNforz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRSTZ-INI-NFORZ-NULL
            trchDiGar.getTgaPrstzIniNforz().setTgaPrstzIniNforzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrstzIniNforz.Len.TGA_PRSTZ_INI_NFORZ_NULL));
        }
        // COB_CODE: IF IND-TGA-VIS-END2000-NFORZ = -1
        //              MOVE HIGH-VALUES TO TGA-VIS-END2000-NFORZ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getVisEnd2000Nforz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-VIS-END2000-NFORZ-NULL
            trchDiGar.getTgaVisEnd2000Nforz().setTgaVisEnd2000NforzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaVisEnd2000Nforz.Len.TGA_VIS_END2000_NFORZ_NULL));
        }
        // COB_CODE: IF IND-TGA-INTR-MORA = -1
        //              MOVE HIGH-VALUES TO TGA-INTR-MORA-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getIntrMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-INTR-MORA-NULL
            trchDiGar.getTgaIntrMora().setTgaIntrMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaIntrMora.Len.TGA_INTR_MORA_NULL));
        }
        // COB_CODE: IF IND-TGA-MANFEE-ANTIC = -1
        //              MOVE HIGH-VALUES TO TGA-MANFEE-ANTIC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getManfeeAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-MANFEE-ANTIC-NULL
            trchDiGar.getTgaManfeeAntic().setTgaManfeeAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaManfeeAntic.Len.TGA_MANFEE_ANTIC_NULL));
        }
        // COB_CODE: IF IND-TGA-MANFEE-RICOR = -1
        //              MOVE HIGH-VALUES TO TGA-MANFEE-RICOR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getManfeeRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-MANFEE-RICOR-NULL
            trchDiGar.getTgaManfeeRicor().setTgaManfeeRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaManfeeRicor.Len.TGA_MANFEE_RICOR_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-UNI-RIVTO = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-UNI-RIVTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreUniRivto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-UNI-RIVTO-NULL
            trchDiGar.getTgaPreUniRivto().setTgaPreUniRivtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreUniRivto.Len.TGA_PRE_UNI_RIVTO_NULL));
        }
        // COB_CODE: IF IND-TGA-PROV-1AA-ACQ = -1
        //              MOVE HIGH-VALUES TO TGA-PROV-1AA-ACQ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getProv1aaAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PROV-1AA-ACQ-NULL
            trchDiGar.getTgaProv1aaAcq().setTgaProv1aaAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaProv1aaAcq.Len.TGA_PROV1AA_ACQ_NULL));
        }
        // COB_CODE: IF IND-TGA-PROV-2AA-ACQ = -1
        //              MOVE HIGH-VALUES TO TGA-PROV-2AA-ACQ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getProv2aaAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PROV-2AA-ACQ-NULL
            trchDiGar.getTgaProv2aaAcq().setTgaProv2aaAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaProv2aaAcq.Len.TGA_PROV2AA_ACQ_NULL));
        }
        // COB_CODE: IF IND-TGA-PROV-RICOR = -1
        //              MOVE HIGH-VALUES TO TGA-PROV-RICOR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getProvRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PROV-RICOR-NULL
            trchDiGar.getTgaProvRicor().setTgaProvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaProvRicor.Len.TGA_PROV_RICOR_NULL));
        }
        // COB_CODE: IF IND-TGA-PROV-INC = -1
        //              MOVE HIGH-VALUES TO TGA-PROV-INC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getProvInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PROV-INC-NULL
            trchDiGar.getTgaProvInc().setTgaProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaProvInc.Len.TGA_PROV_INC_NULL));
        }
        // COB_CODE: IF IND-TGA-ALQ-PROV-ACQ = -1
        //              MOVE HIGH-VALUES TO TGA-ALQ-PROV-ACQ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAlqProvAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ALQ-PROV-ACQ-NULL
            trchDiGar.getTgaAlqProvAcq().setTgaAlqProvAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAlqProvAcq.Len.TGA_ALQ_PROV_ACQ_NULL));
        }
        // COB_CODE: IF IND-TGA-ALQ-PROV-INC = -1
        //              MOVE HIGH-VALUES TO TGA-ALQ-PROV-INC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAlqProvInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ALQ-PROV-INC-NULL
            trchDiGar.getTgaAlqProvInc().setTgaAlqProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAlqProvInc.Len.TGA_ALQ_PROV_INC_NULL));
        }
        // COB_CODE: IF IND-TGA-ALQ-PROV-RICOR = -1
        //              MOVE HIGH-VALUES TO TGA-ALQ-PROV-RICOR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAlqProvRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ALQ-PROV-RICOR-NULL
            trchDiGar.getTgaAlqProvRicor().setTgaAlqProvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAlqProvRicor.Len.TGA_ALQ_PROV_RICOR_NULL));
        }
        // COB_CODE: IF IND-TGA-IMPB-PROV-ACQ = -1
        //              MOVE HIGH-VALUES TO TGA-IMPB-PROV-ACQ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpbProvAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMPB-PROV-ACQ-NULL
            trchDiGar.getTgaImpbProvAcq().setTgaImpbProvAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpbProvAcq.Len.TGA_IMPB_PROV_ACQ_NULL));
        }
        // COB_CODE: IF IND-TGA-IMPB-PROV-INC = -1
        //              MOVE HIGH-VALUES TO TGA-IMPB-PROV-INC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpbProvInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMPB-PROV-INC-NULL
            trchDiGar.getTgaImpbProvInc().setTgaImpbProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpbProvInc.Len.TGA_IMPB_PROV_INC_NULL));
        }
        // COB_CODE: IF IND-TGA-IMPB-PROV-RICOR = -1
        //              MOVE HIGH-VALUES TO TGA-IMPB-PROV-RICOR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpbProvRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMPB-PROV-RICOR-NULL
            trchDiGar.getTgaImpbProvRicor().setTgaImpbProvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpbProvRicor.Len.TGA_IMPB_PROV_RICOR_NULL));
        }
        // COB_CODE: IF IND-TGA-FL-PROV-FORZ = -1
        //              MOVE HIGH-VALUES TO TGA-FL-PROV-FORZ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getFlProvForz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-FL-PROV-FORZ-NULL
            trchDiGar.setTgaFlProvForz(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TGA-PRSTZ-AGG-INI = -1
        //              MOVE HIGH-VALUES TO TGA-PRSTZ-AGG-INI-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrstzAggIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRSTZ-AGG-INI-NULL
            trchDiGar.getTgaPrstzAggIni().setTgaPrstzAggIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrstzAggIni.Len.TGA_PRSTZ_AGG_INI_NULL));
        }
        // COB_CODE: IF IND-TGA-INCR-PRE = -1
        //              MOVE HIGH-VALUES TO TGA-INCR-PRE-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getIncrPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-INCR-PRE-NULL
            trchDiGar.getTgaIncrPre().setTgaIncrPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaIncrPre.Len.TGA_INCR_PRE_NULL));
        }
        // COB_CODE: IF IND-TGA-INCR-PRSTZ = -1
        //              MOVE HIGH-VALUES TO TGA-INCR-PRSTZ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getIncrPrstz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-INCR-PRSTZ-NULL
            trchDiGar.getTgaIncrPrstz().setTgaIncrPrstzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaIncrPrstz.Len.TGA_INCR_PRSTZ_NULL));
        }
        // COB_CODE: IF IND-TGA-DT-ULT-ADEG-PRE-PR = -1
        //              MOVE HIGH-VALUES TO TGA-DT-ULT-ADEG-PRE-PR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDtUltAdegPrePr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DT-ULT-ADEG-PRE-PR-NULL
            trchDiGar.getTgaDtUltAdegPrePr().setTgaDtUltAdegPrePrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDtUltAdegPrePr.Len.TGA_DT_ULT_ADEG_PRE_PR_NULL));
        }
        // COB_CODE: IF IND-TGA-PRSTZ-AGG-ULT = -1
        //              MOVE HIGH-VALUES TO TGA-PRSTZ-AGG-ULT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrstzAggUlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRSTZ-AGG-ULT-NULL
            trchDiGar.getTgaPrstzAggUlt().setTgaPrstzAggUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrstzAggUlt.Len.TGA_PRSTZ_AGG_ULT_NULL));
        }
        // COB_CODE: IF IND-TGA-TS-RIVAL-NET = -1
        //              MOVE HIGH-VALUES TO TGA-TS-RIVAL-NET-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getTsRivalNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-TS-RIVAL-NET-NULL
            trchDiGar.getTgaTsRivalNet().setTgaTsRivalNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaTsRivalNet.Len.TGA_TS_RIVAL_NET_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-PATTUITO = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-PATTUITO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrePattuito() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-PATTUITO-NULL
            trchDiGar.getTgaPrePattuito().setTgaPrePattuitoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrePattuito.Len.TGA_PRE_PATTUITO_NULL));
        }
        // COB_CODE: IF IND-TGA-TP-RIVAL = -1
        //              MOVE HIGH-VALUES TO TGA-TP-RIVAL-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getTpRival() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-TP-RIVAL-NULL
            trchDiGar.setTgaTpRival(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TrchDiGar.Len.TGA_TP_RIVAL));
        }
        // COB_CODE: IF IND-TGA-RIS-MAT = -1
        //              MOVE HIGH-VALUES TO TGA-RIS-MAT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getRisMat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-RIS-MAT-NULL
            trchDiGar.getTgaRisMat().setTgaRisMatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaRisMat.Len.TGA_RIS_MAT_NULL));
        }
        // COB_CODE: IF IND-TGA-CPT-MIN-SCAD = -1
        //              MOVE HIGH-VALUES TO TGA-CPT-MIN-SCAD-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getCptMinScad() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-CPT-MIN-SCAD-NULL
            trchDiGar.getTgaCptMinScad().setTgaCptMinScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaCptMinScad.Len.TGA_CPT_MIN_SCAD_NULL));
        }
        // COB_CODE: IF IND-TGA-COMMIS-GEST = -1
        //              MOVE HIGH-VALUES TO TGA-COMMIS-GEST-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getCommisGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-COMMIS-GEST-NULL
            trchDiGar.getTgaCommisGest().setTgaCommisGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaCommisGest.Len.TGA_COMMIS_GEST_NULL));
        }
        // COB_CODE: IF IND-TGA-TP-MANFEE-APPL = -1
        //              MOVE HIGH-VALUES TO TGA-TP-MANFEE-APPL-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getTpManfeeAppl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-TP-MANFEE-APPL-NULL
            trchDiGar.setTgaTpManfeeAppl(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TrchDiGar.Len.TGA_TP_MANFEE_APPL));
        }
        // COB_CODE: IF IND-TGA-PC-COMMIS-GEST = -1
        //              MOVE HIGH-VALUES TO TGA-PC-COMMIS-GEST-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPcCommisGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PC-COMMIS-GEST-NULL
            trchDiGar.getTgaPcCommisGest().setTgaPcCommisGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPcCommisGest.Len.TGA_PC_COMMIS_GEST_NULL));
        }
        // COB_CODE: IF IND-TGA-NUM-GG-RIVAL = -1
        //              MOVE HIGH-VALUES TO TGA-NUM-GG-RIVAL-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getNumGgRival() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-NUM-GG-RIVAL-NULL
            trchDiGar.getTgaNumGgRival().setTgaNumGgRivalNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaNumGgRival.Len.TGA_NUM_GG_RIVAL_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-TRASFE = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-TRASFE-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpTrasfe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-TRASFE-NULL
            trchDiGar.getTgaImpTrasfe().setTgaImpTrasfeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpTrasfe.Len.TGA_IMP_TRASFE_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-TFR-STRC = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-TFR-STRC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpTfrStrc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-TFR-STRC-NULL
            trchDiGar.getTgaImpTfrStrc().setTgaImpTfrStrcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpTfrStrc.Len.TGA_IMP_TFR_STRC_NULL));
        }
        // COB_CODE: IF IND-TGA-ACQ-EXP = -1
        //              MOVE HIGH-VALUES TO TGA-ACQ-EXP-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAcqExp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ACQ-EXP-NULL
            trchDiGar.getTgaAcqExp().setTgaAcqExpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAcqExp.Len.TGA_ACQ_EXP_NULL));
        }
        // COB_CODE: IF IND-TGA-REMUN-ASS = -1
        //              MOVE HIGH-VALUES TO TGA-REMUN-ASS-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getRemunAss() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-REMUN-ASS-NULL
            trchDiGar.getTgaRemunAss().setTgaRemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaRemunAss.Len.TGA_REMUN_ASS_NULL));
        }
        // COB_CODE: IF IND-TGA-COMMIS-INTER = -1
        //              MOVE HIGH-VALUES TO TGA-COMMIS-INTER-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getCommisInter() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-COMMIS-INTER-NULL
            trchDiGar.getTgaCommisInter().setTgaCommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaCommisInter.Len.TGA_COMMIS_INTER_NULL));
        }
        // COB_CODE: IF IND-TGA-ALQ-REMUN-ASS = -1
        //              MOVE HIGH-VALUES TO TGA-ALQ-REMUN-ASS-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAlqRemunAss() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ALQ-REMUN-ASS-NULL
            trchDiGar.getTgaAlqRemunAss().setTgaAlqRemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAlqRemunAss.Len.TGA_ALQ_REMUN_ASS_NULL));
        }
        // COB_CODE: IF IND-TGA-ALQ-COMMIS-INTER = -1
        //              MOVE HIGH-VALUES TO TGA-ALQ-COMMIS-INTER-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAlqCommisInter() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ALQ-COMMIS-INTER-NULL
            trchDiGar.getTgaAlqCommisInter().setTgaAlqCommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAlqCommisInter.Len.TGA_ALQ_COMMIS_INTER_NULL));
        }
        // COB_CODE: IF IND-TGA-IMPB-REMUN-ASS = -1
        //              MOVE HIGH-VALUES TO TGA-IMPB-REMUN-ASS-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpbRemunAss() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMPB-REMUN-ASS-NULL
            trchDiGar.getTgaImpbRemunAss().setTgaImpbRemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpbRemunAss.Len.TGA_IMPB_REMUN_ASS_NULL));
        }
        // COB_CODE: IF IND-TGA-IMPB-COMMIS-INTER = -1
        //              MOVE HIGH-VALUES TO TGA-IMPB-COMMIS-INTER-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpbCommisInter() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMPB-COMMIS-INTER-NULL
            trchDiGar.getTgaImpbCommisInter().setTgaImpbCommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpbCommisInter.Len.TGA_IMPB_COMMIS_INTER_NULL));
        }
        // COB_CODE: IF IND-TGA-COS-RUN-ASSVA = -1
        //              MOVE HIGH-VALUES TO TGA-COS-RUN-ASSVA-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getCosRunAssva() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-COS-RUN-ASSVA-NULL
            trchDiGar.getTgaCosRunAssva().setTgaCosRunAssvaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaCosRunAssva.Len.TGA_COS_RUN_ASSVA_NULL));
        }
        // COB_CODE: IF IND-TGA-COS-RUN-ASSVA-IDC = -1
        //              MOVE HIGH-VALUES TO TGA-COS-RUN-ASSVA-IDC-NULL
        //           END-IF.
        if (ws.getIndTrchDiGar().getCosRunAssvaIdc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-COS-RUN-ASSVA-IDC-NULL
            trchDiGar.getTgaCosRunAssvaIdc().setTgaCosRunAssvaIdcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaCosRunAssvaIdc.Len.TGA_COS_RUN_ASSVA_IDC_NULL));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE TGA-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-INI-EFF
        trchDiGar.setTgaDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE TGA-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-END-EFF
        trchDiGar.setTgaDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE TGA-DT-DECOR-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getDecorDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-DECOR
        trchDiGar.setTgaDtDecor(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-TGA-DT-SCAD = 0
        //               MOVE WS-DATE-N      TO TGA-DT-SCAD
        //           END-IF
        if (ws.getIndTrchDiGar().getDtScad() == 0) {
            // COB_CODE: MOVE TGA-DT-SCAD-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getScadDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-SCAD
            trchDiGar.getTgaDtScad().setTgaDtScad(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TGA-DT-EMIS = 0
        //               MOVE WS-DATE-N      TO TGA-DT-EMIS
        //           END-IF
        if (ws.getIndTrchDiGar().getDtEmis() == 0) {
            // COB_CODE: MOVE TGA-DT-EMIS-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getEmisDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-EMIS
            trchDiGar.getTgaDtEmis().setTgaDtEmis(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TGA-DT-EFF-STAB = 0
        //               MOVE WS-DATE-N      TO TGA-DT-EFF-STAB
        //           END-IF
        if (ws.getIndTrchDiGar().getDtEffStab() == 0) {
            // COB_CODE: MOVE TGA-DT-EFF-STAB-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getEffStabDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-EFF-STAB
            trchDiGar.getTgaDtEffStab().setTgaDtEffStab(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TGA-DT-VLDT-PROD = 0
        //               MOVE WS-DATE-N      TO TGA-DT-VLDT-PROD
        //           END-IF
        if (ws.getIndTrchDiGar().getDtVldtProd() == 0) {
            // COB_CODE: MOVE TGA-DT-VLDT-PROD-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getVldtProdDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-VLDT-PROD
            trchDiGar.getTgaDtVldtProd().setTgaDtVldtProd(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TGA-DT-INI-VAL-TAR = 0
        //               MOVE WS-DATE-N      TO TGA-DT-INI-VAL-TAR
        //           END-IF
        if (ws.getIndTrchDiGar().getDtIniValTar() == 0) {
            // COB_CODE: MOVE TGA-DT-INI-VAL-TAR-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getIniValTarDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-INI-VAL-TAR
            trchDiGar.getTgaDtIniValTar().setTgaDtIniValTar(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TGA-DT-ULT-ADEG-PRE-PR = 0
        //               MOVE WS-DATE-N      TO TGA-DT-ULT-ADEG-PRE-PR
        //           END-IF.
        if (ws.getIndTrchDiGar().getDtUltAdegPrePr() == 0) {
            // COB_CODE: MOVE TGA-DT-ULT-ADEG-PRE-PR-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getUltAdegPrePrDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-ULT-ADEG-PRE-PR
            trchDiGar.getTgaDtUltAdegPrePr().setTgaDtUltAdegPrePr(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public AfDecimal getAbbAnnuUlt() {
        return trchDiGar.getTgaAbbAnnuUlt().getTgaAbbAnnuUlt();
    }

    @Override
    public void setAbbAnnuUlt(AfDecimal abbAnnuUlt) {
        this.trchDiGar.getTgaAbbAnnuUlt().setTgaAbbAnnuUlt(abbAnnuUlt.copy());
    }

    @Override
    public AfDecimal getAbbAnnuUltObj() {
        if (ws.getIndTrchDiGar().getAbbAnnuUlt() >= 0) {
            return getAbbAnnuUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAbbAnnuUltObj(AfDecimal abbAnnuUltObj) {
        if (abbAnnuUltObj != null) {
            setAbbAnnuUlt(new AfDecimal(abbAnnuUltObj, 15, 3));
            ws.getIndTrchDiGar().setAbbAnnuUlt(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAbbAnnuUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getAbbTotIni() {
        return trchDiGar.getTgaAbbTotIni().getTgaAbbTotIni();
    }

    @Override
    public void setAbbTotIni(AfDecimal abbTotIni) {
        this.trchDiGar.getTgaAbbTotIni().setTgaAbbTotIni(abbTotIni.copy());
    }

    @Override
    public AfDecimal getAbbTotIniObj() {
        if (ws.getIndTrchDiGar().getAbbTotIni() >= 0) {
            return getAbbTotIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAbbTotIniObj(AfDecimal abbTotIniObj) {
        if (abbTotIniObj != null) {
            setAbbTotIni(new AfDecimal(abbTotIniObj, 15, 3));
            ws.getIndTrchDiGar().setAbbTotIni(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAbbTotIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getAbbTotUlt() {
        return trchDiGar.getTgaAbbTotUlt().getTgaAbbTotUlt();
    }

    @Override
    public void setAbbTotUlt(AfDecimal abbTotUlt) {
        this.trchDiGar.getTgaAbbTotUlt().setTgaAbbTotUlt(abbTotUlt.copy());
    }

    @Override
    public AfDecimal getAbbTotUltObj() {
        if (ws.getIndTrchDiGar().getAbbTotUlt() >= 0) {
            return getAbbTotUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAbbTotUltObj(AfDecimal abbTotUltObj) {
        if (abbTotUltObj != null) {
            setAbbTotUlt(new AfDecimal(abbTotUltObj, 15, 3));
            ws.getIndTrchDiGar().setAbbTotUlt(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAbbTotUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getAcqExp() {
        return trchDiGar.getTgaAcqExp().getTgaAcqExp();
    }

    @Override
    public void setAcqExp(AfDecimal acqExp) {
        this.trchDiGar.getTgaAcqExp().setTgaAcqExp(acqExp.copy());
    }

    @Override
    public AfDecimal getAcqExpObj() {
        if (ws.getIndTrchDiGar().getAcqExp() >= 0) {
            return getAcqExp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAcqExpObj(AfDecimal acqExpObj) {
        if (acqExpObj != null) {
            setAcqExp(new AfDecimal(acqExpObj, 15, 3));
            ws.getIndTrchDiGar().setAcqExp(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAcqExp(((short)-1));
        }
    }

    @Override
    public AfDecimal getAlqCommisInter() {
        return trchDiGar.getTgaAlqCommisInter().getTgaAlqCommisInter();
    }

    @Override
    public void setAlqCommisInter(AfDecimal alqCommisInter) {
        this.trchDiGar.getTgaAlqCommisInter().setTgaAlqCommisInter(alqCommisInter.copy());
    }

    @Override
    public AfDecimal getAlqCommisInterObj() {
        if (ws.getIndTrchDiGar().getAlqCommisInter() >= 0) {
            return getAlqCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAlqCommisInterObj(AfDecimal alqCommisInterObj) {
        if (alqCommisInterObj != null) {
            setAlqCommisInter(new AfDecimal(alqCommisInterObj, 6, 3));
            ws.getIndTrchDiGar().setAlqCommisInter(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAlqCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getAlqProvAcq() {
        return trchDiGar.getTgaAlqProvAcq().getTgaAlqProvAcq();
    }

    @Override
    public void setAlqProvAcq(AfDecimal alqProvAcq) {
        this.trchDiGar.getTgaAlqProvAcq().setTgaAlqProvAcq(alqProvAcq.copy());
    }

    @Override
    public AfDecimal getAlqProvAcqObj() {
        if (ws.getIndTrchDiGar().getAlqProvAcq() >= 0) {
            return getAlqProvAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAlqProvAcqObj(AfDecimal alqProvAcqObj) {
        if (alqProvAcqObj != null) {
            setAlqProvAcq(new AfDecimal(alqProvAcqObj, 6, 3));
            ws.getIndTrchDiGar().setAlqProvAcq(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAlqProvAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getAlqProvInc() {
        return trchDiGar.getTgaAlqProvInc().getTgaAlqProvInc();
    }

    @Override
    public void setAlqProvInc(AfDecimal alqProvInc) {
        this.trchDiGar.getTgaAlqProvInc().setTgaAlqProvInc(alqProvInc.copy());
    }

    @Override
    public AfDecimal getAlqProvIncObj() {
        if (ws.getIndTrchDiGar().getAlqProvInc() >= 0) {
            return getAlqProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAlqProvIncObj(AfDecimal alqProvIncObj) {
        if (alqProvIncObj != null) {
            setAlqProvInc(new AfDecimal(alqProvIncObj, 6, 3));
            ws.getIndTrchDiGar().setAlqProvInc(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAlqProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getAlqProvRicor() {
        return trchDiGar.getTgaAlqProvRicor().getTgaAlqProvRicor();
    }

    @Override
    public void setAlqProvRicor(AfDecimal alqProvRicor) {
        this.trchDiGar.getTgaAlqProvRicor().setTgaAlqProvRicor(alqProvRicor.copy());
    }

    @Override
    public AfDecimal getAlqProvRicorObj() {
        if (ws.getIndTrchDiGar().getAlqProvRicor() >= 0) {
            return getAlqProvRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAlqProvRicorObj(AfDecimal alqProvRicorObj) {
        if (alqProvRicorObj != null) {
            setAlqProvRicor(new AfDecimal(alqProvRicorObj, 6, 3));
            ws.getIndTrchDiGar().setAlqProvRicor(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAlqProvRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getAlqRemunAss() {
        return trchDiGar.getTgaAlqRemunAss().getTgaAlqRemunAss();
    }

    @Override
    public void setAlqRemunAss(AfDecimal alqRemunAss) {
        this.trchDiGar.getTgaAlqRemunAss().setTgaAlqRemunAss(alqRemunAss.copy());
    }

    @Override
    public AfDecimal getAlqRemunAssObj() {
        if (ws.getIndTrchDiGar().getAlqRemunAss() >= 0) {
            return getAlqRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAlqRemunAssObj(AfDecimal alqRemunAssObj) {
        if (alqRemunAssObj != null) {
            setAlqRemunAss(new AfDecimal(alqRemunAssObj, 6, 3));
            ws.getIndTrchDiGar().setAlqRemunAss(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAlqRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getAlqScon() {
        return trchDiGar.getTgaAlqScon().getTgaAlqScon();
    }

    @Override
    public void setAlqScon(AfDecimal alqScon) {
        this.trchDiGar.getTgaAlqScon().setTgaAlqScon(alqScon.copy());
    }

    @Override
    public AfDecimal getAlqSconObj() {
        if (ws.getIndTrchDiGar().getAlqScon() >= 0) {
            return getAlqScon();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAlqSconObj(AfDecimal alqSconObj) {
        if (alqSconObj != null) {
            setAlqScon(new AfDecimal(alqSconObj, 6, 3));
            ws.getIndTrchDiGar().setAlqScon(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAlqScon(((short)-1));
        }
    }

    @Override
    public AfDecimal getBnsGiaLiqto() {
        return trchDiGar.getTgaBnsGiaLiqto().getTgaBnsGiaLiqto();
    }

    @Override
    public void setBnsGiaLiqto(AfDecimal bnsGiaLiqto) {
        this.trchDiGar.getTgaBnsGiaLiqto().setTgaBnsGiaLiqto(bnsGiaLiqto.copy());
    }

    @Override
    public AfDecimal getBnsGiaLiqtoObj() {
        if (ws.getIndTrchDiGar().getBnsGiaLiqto() >= 0) {
            return getBnsGiaLiqto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setBnsGiaLiqtoObj(AfDecimal bnsGiaLiqtoObj) {
        if (bnsGiaLiqtoObj != null) {
            setBnsGiaLiqto(new AfDecimal(bnsGiaLiqtoObj, 15, 3));
            ws.getIndTrchDiGar().setBnsGiaLiqto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setBnsGiaLiqto(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return trchDiGar.getTgaCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.trchDiGar.setTgaCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodDvs() {
        return trchDiGar.getTgaCodDvs();
    }

    @Override
    public void setCodDvs(String codDvs) {
        this.trchDiGar.setTgaCodDvs(codDvs);
    }

    @Override
    public AfDecimal getCommisGest() {
        return trchDiGar.getTgaCommisGest().getTgaCommisGest();
    }

    @Override
    public void setCommisGest(AfDecimal commisGest) {
        this.trchDiGar.getTgaCommisGest().setTgaCommisGest(commisGest.copy());
    }

    @Override
    public AfDecimal getCommisGestObj() {
        if (ws.getIndTrchDiGar().getCommisGest() >= 0) {
            return getCommisGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCommisGestObj(AfDecimal commisGestObj) {
        if (commisGestObj != null) {
            setCommisGest(new AfDecimal(commisGestObj, 15, 3));
            ws.getIndTrchDiGar().setCommisGest(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setCommisGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getCommisInter() {
        return trchDiGar.getTgaCommisInter().getTgaCommisInter();
    }

    @Override
    public void setCommisInter(AfDecimal commisInter) {
        this.trchDiGar.getTgaCommisInter().setTgaCommisInter(commisInter.copy());
    }

    @Override
    public AfDecimal getCommisInterObj() {
        if (ws.getIndTrchDiGar().getCommisInter() >= 0) {
            return getCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCommisInterObj(AfDecimal commisInterObj) {
        if (commisInterObj != null) {
            setCommisInter(new AfDecimal(commisInterObj, 15, 3));
            ws.getIndTrchDiGar().setCommisInter(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getCosRunAssva() {
        return trchDiGar.getTgaCosRunAssva().getTgaCosRunAssva();
    }

    @Override
    public void setCosRunAssva(AfDecimal cosRunAssva) {
        this.trchDiGar.getTgaCosRunAssva().setTgaCosRunAssva(cosRunAssva.copy());
    }

    @Override
    public AfDecimal getCosRunAssvaIdc() {
        return trchDiGar.getTgaCosRunAssvaIdc().getTgaCosRunAssvaIdc();
    }

    @Override
    public void setCosRunAssvaIdc(AfDecimal cosRunAssvaIdc) {
        this.trchDiGar.getTgaCosRunAssvaIdc().setTgaCosRunAssvaIdc(cosRunAssvaIdc.copy());
    }

    @Override
    public AfDecimal getCosRunAssvaIdcObj() {
        if (ws.getIndTrchDiGar().getCosRunAssvaIdc() >= 0) {
            return getCosRunAssvaIdc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCosRunAssvaIdcObj(AfDecimal cosRunAssvaIdcObj) {
        if (cosRunAssvaIdcObj != null) {
            setCosRunAssvaIdc(new AfDecimal(cosRunAssvaIdcObj, 15, 3));
            ws.getIndTrchDiGar().setCosRunAssvaIdc(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setCosRunAssvaIdc(((short)-1));
        }
    }

    @Override
    public AfDecimal getCosRunAssvaObj() {
        if (ws.getIndTrchDiGar().getCosRunAssva() >= 0) {
            return getCosRunAssva();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCosRunAssvaObj(AfDecimal cosRunAssvaObj) {
        if (cosRunAssvaObj != null) {
            setCosRunAssva(new AfDecimal(cosRunAssvaObj, 15, 3));
            ws.getIndTrchDiGar().setCosRunAssva(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setCosRunAssva(((short)-1));
        }
    }

    @Override
    public AfDecimal getCptInOpzRivto() {
        return trchDiGar.getTgaCptInOpzRivto().getTgaCptInOpzRivto();
    }

    @Override
    public void setCptInOpzRivto(AfDecimal cptInOpzRivto) {
        this.trchDiGar.getTgaCptInOpzRivto().setTgaCptInOpzRivto(cptInOpzRivto.copy());
    }

    @Override
    public AfDecimal getCptInOpzRivtoObj() {
        if (ws.getIndTrchDiGar().getCptInOpzRivto() >= 0) {
            return getCptInOpzRivto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCptInOpzRivtoObj(AfDecimal cptInOpzRivtoObj) {
        if (cptInOpzRivtoObj != null) {
            setCptInOpzRivto(new AfDecimal(cptInOpzRivtoObj, 15, 3));
            ws.getIndTrchDiGar().setCptInOpzRivto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setCptInOpzRivto(((short)-1));
        }
    }

    @Override
    public AfDecimal getCptMinScad() {
        return trchDiGar.getTgaCptMinScad().getTgaCptMinScad();
    }

    @Override
    public void setCptMinScad(AfDecimal cptMinScad) {
        this.trchDiGar.getTgaCptMinScad().setTgaCptMinScad(cptMinScad.copy());
    }

    @Override
    public AfDecimal getCptMinScadObj() {
        if (ws.getIndTrchDiGar().getCptMinScad() >= 0) {
            return getCptMinScad();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCptMinScadObj(AfDecimal cptMinScadObj) {
        if (cptMinScadObj != null) {
            setCptMinScad(new AfDecimal(cptMinScadObj, 15, 3));
            ws.getIndTrchDiGar().setCptMinScad(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setCptMinScad(((short)-1));
        }
    }

    @Override
    public AfDecimal getCptRshMor() {
        return trchDiGar.getTgaCptRshMor().getTgaCptRshMor();
    }

    @Override
    public void setCptRshMor(AfDecimal cptRshMor) {
        this.trchDiGar.getTgaCptRshMor().setTgaCptRshMor(cptRshMor.copy());
    }

    @Override
    public AfDecimal getCptRshMorObj() {
        if (ws.getIndTrchDiGar().getCptRshMor() >= 0) {
            return getCptRshMor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCptRshMorObj(AfDecimal cptRshMorObj) {
        if (cptRshMorObj != null) {
            setCptRshMor(new AfDecimal(cptRshMorObj, 15, 3));
            ws.getIndTrchDiGar().setCptRshMor(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setCptRshMor(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return trchDiGar.getTgaDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.trchDiGar.setTgaDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return trchDiGar.getTgaDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.trchDiGar.setTgaDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return trchDiGar.getTgaDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.trchDiGar.setTgaDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return trchDiGar.getTgaDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.trchDiGar.setTgaDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return trchDiGar.getTgaDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.trchDiGar.setTgaDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return trchDiGar.getTgaDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.trchDiGar.setTgaDsVer(dsVer);
    }

    @Override
    public String getDtDecorDb() {
        return ws.getTrchDiGarDb().getDecorDb();
    }

    @Override
    public void setDtDecorDb(String dtDecorDb) {
        this.ws.getTrchDiGarDb().setDecorDb(dtDecorDb);
    }

    @Override
    public String getDtEffStabDb() {
        return ws.getTrchDiGarDb().getEffStabDb();
    }

    @Override
    public void setDtEffStabDb(String dtEffStabDb) {
        this.ws.getTrchDiGarDb().setEffStabDb(dtEffStabDb);
    }

    @Override
    public String getDtEffStabDbObj() {
        if (ws.getIndTrchDiGar().getDtEffStab() >= 0) {
            return getDtEffStabDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEffStabDbObj(String dtEffStabDbObj) {
        if (dtEffStabDbObj != null) {
            setDtEffStabDb(dtEffStabDbObj);
            ws.getIndTrchDiGar().setDtEffStab(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDtEffStab(((short)-1));
        }
    }

    @Override
    public String getDtEmisDb() {
        return ws.getTrchDiGarDb().getEmisDb();
    }

    @Override
    public void setDtEmisDb(String dtEmisDb) {
        this.ws.getTrchDiGarDb().setEmisDb(dtEmisDb);
    }

    @Override
    public String getDtEmisDbObj() {
        if (ws.getIndTrchDiGar().getDtEmis() >= 0) {
            return getDtEmisDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEmisDbObj(String dtEmisDbObj) {
        if (dtEmisDbObj != null) {
            setDtEmisDb(dtEmisDbObj);
            ws.getIndTrchDiGar().setDtEmis(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDtEmis(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getTrchDiGarDb().getEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getTrchDiGarDb().setEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getTrchDiGarDb().getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getTrchDiGarDb().setIniEffDb(dtIniEffDb);
    }

    @Override
    public String getDtIniValTarDb() {
        return ws.getTrchDiGarDb().getIniValTarDb();
    }

    @Override
    public void setDtIniValTarDb(String dtIniValTarDb) {
        this.ws.getTrchDiGarDb().setIniValTarDb(dtIniValTarDb);
    }

    @Override
    public String getDtIniValTarDbObj() {
        if (ws.getIndTrchDiGar().getDtIniValTar() >= 0) {
            return getDtIniValTarDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtIniValTarDbObj(String dtIniValTarDbObj) {
        if (dtIniValTarDbObj != null) {
            setDtIniValTarDb(dtIniValTarDbObj);
            ws.getIndTrchDiGar().setDtIniValTar(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDtIniValTar(((short)-1));
        }
    }

    @Override
    public String getDtScadDb() {
        return ws.getTrchDiGarDb().getScadDb();
    }

    @Override
    public void setDtScadDb(String dtScadDb) {
        this.ws.getTrchDiGarDb().setScadDb(dtScadDb);
    }

    @Override
    public String getDtScadDbObj() {
        if (ws.getIndTrchDiGar().getDtScad() >= 0) {
            return getDtScadDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtScadDbObj(String dtScadDbObj) {
        if (dtScadDbObj != null) {
            setDtScadDb(dtScadDbObj);
            ws.getIndTrchDiGar().setDtScad(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDtScad(((short)-1));
        }
    }

    @Override
    public String getDtUltAdegPrePrDb() {
        return ws.getTrchDiGarDb().getUltAdegPrePrDb();
    }

    @Override
    public void setDtUltAdegPrePrDb(String dtUltAdegPrePrDb) {
        this.ws.getTrchDiGarDb().setUltAdegPrePrDb(dtUltAdegPrePrDb);
    }

    @Override
    public String getDtUltAdegPrePrDbObj() {
        if (ws.getIndTrchDiGar().getDtUltAdegPrePr() >= 0) {
            return getDtUltAdegPrePrDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltAdegPrePrDbObj(String dtUltAdegPrePrDbObj) {
        if (dtUltAdegPrePrDbObj != null) {
            setDtUltAdegPrePrDb(dtUltAdegPrePrDbObj);
            ws.getIndTrchDiGar().setDtUltAdegPrePr(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDtUltAdegPrePr(((short)-1));
        }
    }

    @Override
    public String getDtVldtProdDb() {
        return ws.getTrchDiGarDb().getVldtProdDb();
    }

    @Override
    public void setDtVldtProdDb(String dtVldtProdDb) {
        this.ws.getTrchDiGarDb().setVldtProdDb(dtVldtProdDb);
    }

    @Override
    public String getDtVldtProdDbObj() {
        if (ws.getIndTrchDiGar().getDtVldtProd() >= 0) {
            return getDtVldtProdDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtVldtProdDbObj(String dtVldtProdDbObj) {
        if (dtVldtProdDbObj != null) {
            setDtVldtProdDb(dtVldtProdDbObj);
            ws.getIndTrchDiGar().setDtVldtProd(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDtVldtProd(((short)-1));
        }
    }

    @Override
    public int getDurAa() {
        return trchDiGar.getTgaDurAa().getTgaDurAa();
    }

    @Override
    public void setDurAa(int durAa) {
        this.trchDiGar.getTgaDurAa().setTgaDurAa(durAa);
    }

    @Override
    public Integer getDurAaObj() {
        if (ws.getIndTrchDiGar().getDurAa() >= 0) {
            return ((Integer)getDurAa());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurAaObj(Integer durAaObj) {
        if (durAaObj != null) {
            setDurAa(((int)durAaObj));
            ws.getIndTrchDiGar().setDurAa(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDurAa(((short)-1));
        }
    }

    @Override
    public int getDurAbb() {
        return trchDiGar.getTgaDurAbb().getTgaDurAbb();
    }

    @Override
    public void setDurAbb(int durAbb) {
        this.trchDiGar.getTgaDurAbb().setTgaDurAbb(durAbb);
    }

    @Override
    public Integer getDurAbbObj() {
        if (ws.getIndTrchDiGar().getDurAbb() >= 0) {
            return ((Integer)getDurAbb());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurAbbObj(Integer durAbbObj) {
        if (durAbbObj != null) {
            setDurAbb(((int)durAbbObj));
            ws.getIndTrchDiGar().setDurAbb(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDurAbb(((short)-1));
        }
    }

    @Override
    public int getDurGg() {
        return trchDiGar.getTgaDurGg().getTgaDurGg();
    }

    @Override
    public void setDurGg(int durGg) {
        this.trchDiGar.getTgaDurGg().setTgaDurGg(durGg);
    }

    @Override
    public Integer getDurGgObj() {
        if (ws.getIndTrchDiGar().getDurGg() >= 0) {
            return ((Integer)getDurGg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurGgObj(Integer durGgObj) {
        if (durGgObj != null) {
            setDurGg(((int)durGgObj));
            ws.getIndTrchDiGar().setDurGg(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDurGg(((short)-1));
        }
    }

    @Override
    public int getDurMm() {
        return trchDiGar.getTgaDurMm().getTgaDurMm();
    }

    @Override
    public void setDurMm(int durMm) {
        this.trchDiGar.getTgaDurMm().setTgaDurMm(durMm);
    }

    @Override
    public Integer getDurMmObj() {
        if (ws.getIndTrchDiGar().getDurMm() >= 0) {
            return ((Integer)getDurMm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurMmObj(Integer durMmObj) {
        if (durMmObj != null) {
            setDurMm(((int)durMmObj));
            ws.getIndTrchDiGar().setDurMm(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDurMm(((short)-1));
        }
    }

    @Override
    public short getEtaAa1oAssto() {
        return trchDiGar.getTgaEtaAa1oAssto().getTgaEtaAa1oAssto();
    }

    @Override
    public void setEtaAa1oAssto(short etaAa1oAssto) {
        this.trchDiGar.getTgaEtaAa1oAssto().setTgaEtaAa1oAssto(etaAa1oAssto);
    }

    @Override
    public Short getEtaAa1oAsstoObj() {
        if (ws.getIndTrchDiGar().getEtaAa1oAssto() >= 0) {
            return ((Short)getEtaAa1oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaAa1oAsstoObj(Short etaAa1oAsstoObj) {
        if (etaAa1oAsstoObj != null) {
            setEtaAa1oAssto(((short)etaAa1oAsstoObj));
            ws.getIndTrchDiGar().setEtaAa1oAssto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setEtaAa1oAssto(((short)-1));
        }
    }

    @Override
    public short getEtaAa2oAssto() {
        return trchDiGar.getTgaEtaAa2oAssto().getTgaEtaAa2oAssto();
    }

    @Override
    public void setEtaAa2oAssto(short etaAa2oAssto) {
        this.trchDiGar.getTgaEtaAa2oAssto().setTgaEtaAa2oAssto(etaAa2oAssto);
    }

    @Override
    public Short getEtaAa2oAsstoObj() {
        if (ws.getIndTrchDiGar().getEtaAa2oAssto() >= 0) {
            return ((Short)getEtaAa2oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaAa2oAsstoObj(Short etaAa2oAsstoObj) {
        if (etaAa2oAsstoObj != null) {
            setEtaAa2oAssto(((short)etaAa2oAsstoObj));
            ws.getIndTrchDiGar().setEtaAa2oAssto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setEtaAa2oAssto(((short)-1));
        }
    }

    @Override
    public short getEtaAa3oAssto() {
        return trchDiGar.getTgaEtaAa3oAssto().getTgaEtaAa3oAssto();
    }

    @Override
    public void setEtaAa3oAssto(short etaAa3oAssto) {
        this.trchDiGar.getTgaEtaAa3oAssto().setTgaEtaAa3oAssto(etaAa3oAssto);
    }

    @Override
    public Short getEtaAa3oAsstoObj() {
        if (ws.getIndTrchDiGar().getEtaAa3oAssto() >= 0) {
            return ((Short)getEtaAa3oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaAa3oAsstoObj(Short etaAa3oAsstoObj) {
        if (etaAa3oAsstoObj != null) {
            setEtaAa3oAssto(((short)etaAa3oAsstoObj));
            ws.getIndTrchDiGar().setEtaAa3oAssto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setEtaAa3oAssto(((short)-1));
        }
    }

    @Override
    public short getEtaMm1oAssto() {
        return trchDiGar.getTgaEtaMm1oAssto().getTgaEtaMm1oAssto();
    }

    @Override
    public void setEtaMm1oAssto(short etaMm1oAssto) {
        this.trchDiGar.getTgaEtaMm1oAssto().setTgaEtaMm1oAssto(etaMm1oAssto);
    }

    @Override
    public Short getEtaMm1oAsstoObj() {
        if (ws.getIndTrchDiGar().getEtaMm1oAssto() >= 0) {
            return ((Short)getEtaMm1oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaMm1oAsstoObj(Short etaMm1oAsstoObj) {
        if (etaMm1oAsstoObj != null) {
            setEtaMm1oAssto(((short)etaMm1oAsstoObj));
            ws.getIndTrchDiGar().setEtaMm1oAssto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setEtaMm1oAssto(((short)-1));
        }
    }

    @Override
    public short getEtaMm2oAssto() {
        return trchDiGar.getTgaEtaMm2oAssto().getTgaEtaMm2oAssto();
    }

    @Override
    public void setEtaMm2oAssto(short etaMm2oAssto) {
        this.trchDiGar.getTgaEtaMm2oAssto().setTgaEtaMm2oAssto(etaMm2oAssto);
    }

    @Override
    public Short getEtaMm2oAsstoObj() {
        if (ws.getIndTrchDiGar().getEtaMm2oAssto() >= 0) {
            return ((Short)getEtaMm2oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaMm2oAsstoObj(Short etaMm2oAsstoObj) {
        if (etaMm2oAsstoObj != null) {
            setEtaMm2oAssto(((short)etaMm2oAsstoObj));
            ws.getIndTrchDiGar().setEtaMm2oAssto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setEtaMm2oAssto(((short)-1));
        }
    }

    @Override
    public short getEtaMm3oAssto() {
        return trchDiGar.getTgaEtaMm3oAssto().getTgaEtaMm3oAssto();
    }

    @Override
    public void setEtaMm3oAssto(short etaMm3oAssto) {
        this.trchDiGar.getTgaEtaMm3oAssto().setTgaEtaMm3oAssto(etaMm3oAssto);
    }

    @Override
    public Short getEtaMm3oAsstoObj() {
        if (ws.getIndTrchDiGar().getEtaMm3oAssto() >= 0) {
            return ((Short)getEtaMm3oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaMm3oAsstoObj(Short etaMm3oAsstoObj) {
        if (etaMm3oAsstoObj != null) {
            setEtaMm3oAssto(((short)etaMm3oAsstoObj));
            ws.getIndTrchDiGar().setEtaMm3oAssto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setEtaMm3oAssto(((short)-1));
        }
    }

    @Override
    public char getFlCarCont() {
        return trchDiGar.getTgaFlCarCont();
    }

    @Override
    public void setFlCarCont(char flCarCont) {
        this.trchDiGar.setTgaFlCarCont(flCarCont);
    }

    @Override
    public Character getFlCarContObj() {
        if (ws.getIndTrchDiGar().getFlCarCont() >= 0) {
            return ((Character)getFlCarCont());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlCarContObj(Character flCarContObj) {
        if (flCarContObj != null) {
            setFlCarCont(((char)flCarContObj));
            ws.getIndTrchDiGar().setFlCarCont(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setFlCarCont(((short)-1));
        }
    }

    @Override
    public char getFlImportiForz() {
        return trchDiGar.getTgaFlImportiForz();
    }

    @Override
    public void setFlImportiForz(char flImportiForz) {
        this.trchDiGar.setTgaFlImportiForz(flImportiForz);
    }

    @Override
    public Character getFlImportiForzObj() {
        if (ws.getIndTrchDiGar().getFlImportiForz() >= 0) {
            return ((Character)getFlImportiForz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlImportiForzObj(Character flImportiForzObj) {
        if (flImportiForzObj != null) {
            setFlImportiForz(((char)flImportiForzObj));
            ws.getIndTrchDiGar().setFlImportiForz(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setFlImportiForz(((short)-1));
        }
    }

    @Override
    public char getFlProvForz() {
        return trchDiGar.getTgaFlProvForz();
    }

    @Override
    public void setFlProvForz(char flProvForz) {
        this.trchDiGar.setTgaFlProvForz(flProvForz);
    }

    @Override
    public Character getFlProvForzObj() {
        if (ws.getIndTrchDiGar().getFlProvForz() >= 0) {
            return ((Character)getFlProvForz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlProvForzObj(Character flProvForzObj) {
        if (flProvForzObj != null) {
            setFlProvForz(((char)flProvForzObj));
            ws.getIndTrchDiGar().setFlProvForz(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setFlProvForz(((short)-1));
        }
    }

    @Override
    public String getIbOgg() {
        return trchDiGar.getTgaIbOgg();
    }

    @Override
    public void setIbOgg(String ibOgg) {
        this.trchDiGar.setTgaIbOgg(ibOgg);
    }

    @Override
    public String getIbOggObj() {
        if (ws.getIndTrchDiGar().getIbOgg() >= 0) {
            return getIbOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbOggObj(String ibOggObj) {
        if (ibOggObj != null) {
            setIbOgg(ibOggObj);
            ws.getIndTrchDiGar().setIbOgg(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setIbOgg(((short)-1));
        }
    }

    @Override
    public int getIdGar() {
        return trchDiGar.getTgaIdGar();
    }

    @Override
    public void setIdGar(int idGar) {
        this.trchDiGar.setTgaIdGar(idGar);
    }

    @Override
    public int getIdMoviChiu() {
        return trchDiGar.getTgaIdMoviChiu().getTgaIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.trchDiGar.getTgaIdMoviChiu().setTgaIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndTrchDiGar().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndTrchDiGar().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdTrchDiGar() {
        return trchDiGar.getTgaIdTrchDiGar();
    }

    @Override
    public void setIdTrchDiGar(int idTrchDiGar) {
        this.trchDiGar.setTgaIdTrchDiGar(idTrchDiGar);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public AfDecimal getImpAder() {
        return trchDiGar.getTgaImpAder().getTgaImpAder();
    }

    @Override
    public void setImpAder(AfDecimal impAder) {
        this.trchDiGar.getTgaImpAder().setTgaImpAder(impAder.copy());
    }

    @Override
    public AfDecimal getImpAderObj() {
        if (ws.getIndTrchDiGar().getImpAder() >= 0) {
            return getImpAder();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpAderObj(AfDecimal impAderObj) {
        if (impAderObj != null) {
            setImpAder(new AfDecimal(impAderObj, 15, 3));
            ws.getIndTrchDiGar().setImpAder(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpAder(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpAltSopr() {
        return trchDiGar.getTgaImpAltSopr().getTgaImpAltSopr();
    }

    @Override
    public void setImpAltSopr(AfDecimal impAltSopr) {
        this.trchDiGar.getTgaImpAltSopr().setTgaImpAltSopr(impAltSopr.copy());
    }

    @Override
    public AfDecimal getImpAltSoprObj() {
        if (ws.getIndTrchDiGar().getImpAltSopr() >= 0) {
            return getImpAltSopr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpAltSoprObj(AfDecimal impAltSoprObj) {
        if (impAltSoprObj != null) {
            setImpAltSopr(new AfDecimal(impAltSoprObj, 15, 3));
            ws.getIndTrchDiGar().setImpAltSopr(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpAltSopr(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpAz() {
        return trchDiGar.getTgaImpAz().getTgaImpAz();
    }

    @Override
    public void setImpAz(AfDecimal impAz) {
        this.trchDiGar.getTgaImpAz().setTgaImpAz(impAz.copy());
    }

    @Override
    public AfDecimal getImpAzObj() {
        if (ws.getIndTrchDiGar().getImpAz() >= 0) {
            return getImpAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpAzObj(AfDecimal impAzObj) {
        if (impAzObj != null) {
            setImpAz(new AfDecimal(impAzObj, 15, 3));
            ws.getIndTrchDiGar().setImpAz(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpAz(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpBns() {
        return trchDiGar.getTgaImpBns().getTgaImpBns();
    }

    @Override
    public AfDecimal getImpBnsAntic() {
        return trchDiGar.getTgaImpBnsAntic().getTgaImpBnsAntic();
    }

    @Override
    public void setImpBnsAntic(AfDecimal impBnsAntic) {
        this.trchDiGar.getTgaImpBnsAntic().setTgaImpBnsAntic(impBnsAntic.copy());
    }

    @Override
    public AfDecimal getImpBnsAnticObj() {
        if (ws.getIndTrchDiGar().getImpBnsAntic() >= 0) {
            return getImpBnsAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpBnsAnticObj(AfDecimal impBnsAnticObj) {
        if (impBnsAnticObj != null) {
            setImpBnsAntic(new AfDecimal(impBnsAnticObj, 15, 3));
            ws.getIndTrchDiGar().setImpBnsAntic(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpBnsAntic(((short)-1));
        }
    }

    @Override
    public void setImpBns(AfDecimal impBns) {
        this.trchDiGar.getTgaImpBns().setTgaImpBns(impBns.copy());
    }

    @Override
    public AfDecimal getImpBnsObj() {
        if (ws.getIndTrchDiGar().getImpBns() >= 0) {
            return getImpBns();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpBnsObj(AfDecimal impBnsObj) {
        if (impBnsObj != null) {
            setImpBns(new AfDecimal(impBnsObj, 15, 3));
            ws.getIndTrchDiGar().setImpBns(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpBns(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpCarAcq() {
        return trchDiGar.getTgaImpCarAcq().getTgaImpCarAcq();
    }

    @Override
    public void setImpCarAcq(AfDecimal impCarAcq) {
        this.trchDiGar.getTgaImpCarAcq().setTgaImpCarAcq(impCarAcq.copy());
    }

    @Override
    public AfDecimal getImpCarAcqObj() {
        if (ws.getIndTrchDiGar().getImpCarAcq() >= 0) {
            return getImpCarAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpCarAcqObj(AfDecimal impCarAcqObj) {
        if (impCarAcqObj != null) {
            setImpCarAcq(new AfDecimal(impCarAcqObj, 15, 3));
            ws.getIndTrchDiGar().setImpCarAcq(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpCarAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpCarGest() {
        return trchDiGar.getTgaImpCarGest().getTgaImpCarGest();
    }

    @Override
    public void setImpCarGest(AfDecimal impCarGest) {
        this.trchDiGar.getTgaImpCarGest().setTgaImpCarGest(impCarGest.copy());
    }

    @Override
    public AfDecimal getImpCarGestObj() {
        if (ws.getIndTrchDiGar().getImpCarGest() >= 0) {
            return getImpCarGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpCarGestObj(AfDecimal impCarGestObj) {
        if (impCarGestObj != null) {
            setImpCarGest(new AfDecimal(impCarGestObj, 15, 3));
            ws.getIndTrchDiGar().setImpCarGest(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpCarGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpCarInc() {
        return trchDiGar.getTgaImpCarInc().getTgaImpCarInc();
    }

    @Override
    public void setImpCarInc(AfDecimal impCarInc) {
        this.trchDiGar.getTgaImpCarInc().setTgaImpCarInc(impCarInc.copy());
    }

    @Override
    public AfDecimal getImpCarIncObj() {
        if (ws.getIndTrchDiGar().getImpCarInc() >= 0) {
            return getImpCarInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpCarIncObj(AfDecimal impCarIncObj) {
        if (impCarIncObj != null) {
            setImpCarInc(new AfDecimal(impCarIncObj, 15, 3));
            ws.getIndTrchDiGar().setImpCarInc(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpCarInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpScon() {
        return trchDiGar.getTgaImpScon().getTgaImpScon();
    }

    @Override
    public void setImpScon(AfDecimal impScon) {
        this.trchDiGar.getTgaImpScon().setTgaImpScon(impScon.copy());
    }

    @Override
    public AfDecimal getImpSconObj() {
        if (ws.getIndTrchDiGar().getImpScon() >= 0) {
            return getImpScon();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpSconObj(AfDecimal impSconObj) {
        if (impSconObj != null) {
            setImpScon(new AfDecimal(impSconObj, 15, 3));
            ws.getIndTrchDiGar().setImpScon(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpScon(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpSoprProf() {
        return trchDiGar.getTgaImpSoprProf().getTgaImpSoprProf();
    }

    @Override
    public void setImpSoprProf(AfDecimal impSoprProf) {
        this.trchDiGar.getTgaImpSoprProf().setTgaImpSoprProf(impSoprProf.copy());
    }

    @Override
    public AfDecimal getImpSoprProfObj() {
        if (ws.getIndTrchDiGar().getImpSoprProf() >= 0) {
            return getImpSoprProf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpSoprProfObj(AfDecimal impSoprProfObj) {
        if (impSoprProfObj != null) {
            setImpSoprProf(new AfDecimal(impSoprProfObj, 15, 3));
            ws.getIndTrchDiGar().setImpSoprProf(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpSoprProf(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpSoprSan() {
        return trchDiGar.getTgaImpSoprSan().getTgaImpSoprSan();
    }

    @Override
    public void setImpSoprSan(AfDecimal impSoprSan) {
        this.trchDiGar.getTgaImpSoprSan().setTgaImpSoprSan(impSoprSan.copy());
    }

    @Override
    public AfDecimal getImpSoprSanObj() {
        if (ws.getIndTrchDiGar().getImpSoprSan() >= 0) {
            return getImpSoprSan();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpSoprSanObj(AfDecimal impSoprSanObj) {
        if (impSoprSanObj != null) {
            setImpSoprSan(new AfDecimal(impSoprSanObj, 15, 3));
            ws.getIndTrchDiGar().setImpSoprSan(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpSoprSan(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpSoprSpo() {
        return trchDiGar.getTgaImpSoprSpo().getTgaImpSoprSpo();
    }

    @Override
    public void setImpSoprSpo(AfDecimal impSoprSpo) {
        this.trchDiGar.getTgaImpSoprSpo().setTgaImpSoprSpo(impSoprSpo.copy());
    }

    @Override
    public AfDecimal getImpSoprSpoObj() {
        if (ws.getIndTrchDiGar().getImpSoprSpo() >= 0) {
            return getImpSoprSpo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpSoprSpoObj(AfDecimal impSoprSpoObj) {
        if (impSoprSpoObj != null) {
            setImpSoprSpo(new AfDecimal(impSoprSpoObj, 15, 3));
            ws.getIndTrchDiGar().setImpSoprSpo(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpSoprSpo(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpSoprTec() {
        return trchDiGar.getTgaImpSoprTec().getTgaImpSoprTec();
    }

    @Override
    public void setImpSoprTec(AfDecimal impSoprTec) {
        this.trchDiGar.getTgaImpSoprTec().setTgaImpSoprTec(impSoprTec.copy());
    }

    @Override
    public AfDecimal getImpSoprTecObj() {
        if (ws.getIndTrchDiGar().getImpSoprTec() >= 0) {
            return getImpSoprTec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpSoprTecObj(AfDecimal impSoprTecObj) {
        if (impSoprTecObj != null) {
            setImpSoprTec(new AfDecimal(impSoprTecObj, 15, 3));
            ws.getIndTrchDiGar().setImpSoprTec(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpSoprTec(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTfr() {
        return trchDiGar.getTgaImpTfr().getTgaImpTfr();
    }

    @Override
    public void setImpTfr(AfDecimal impTfr) {
        this.trchDiGar.getTgaImpTfr().setTgaImpTfr(impTfr.copy());
    }

    @Override
    public AfDecimal getImpTfrObj() {
        if (ws.getIndTrchDiGar().getImpTfr() >= 0) {
            return getImpTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTfrObj(AfDecimal impTfrObj) {
        if (impTfrObj != null) {
            setImpTfr(new AfDecimal(impTfrObj, 15, 3));
            ws.getIndTrchDiGar().setImpTfr(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTfrStrc() {
        return trchDiGar.getTgaImpTfrStrc().getTgaImpTfrStrc();
    }

    @Override
    public void setImpTfrStrc(AfDecimal impTfrStrc) {
        this.trchDiGar.getTgaImpTfrStrc().setTgaImpTfrStrc(impTfrStrc.copy());
    }

    @Override
    public AfDecimal getImpTfrStrcObj() {
        if (ws.getIndTrchDiGar().getImpTfrStrc() >= 0) {
            return getImpTfrStrc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTfrStrcObj(AfDecimal impTfrStrcObj) {
        if (impTfrStrcObj != null) {
            setImpTfrStrc(new AfDecimal(impTfrStrcObj, 15, 3));
            ws.getIndTrchDiGar().setImpTfrStrc(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpTfrStrc(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTrasfe() {
        return trchDiGar.getTgaImpTrasfe().getTgaImpTrasfe();
    }

    @Override
    public void setImpTrasfe(AfDecimal impTrasfe) {
        this.trchDiGar.getTgaImpTrasfe().setTgaImpTrasfe(impTrasfe.copy());
    }

    @Override
    public AfDecimal getImpTrasfeObj() {
        if (ws.getIndTrchDiGar().getImpTrasfe() >= 0) {
            return getImpTrasfe();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTrasfeObj(AfDecimal impTrasfeObj) {
        if (impTrasfeObj != null) {
            setImpTrasfe(new AfDecimal(impTrasfeObj, 15, 3));
            ws.getIndTrchDiGar().setImpTrasfe(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpTrasfe(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpVolo() {
        return trchDiGar.getTgaImpVolo().getTgaImpVolo();
    }

    @Override
    public void setImpVolo(AfDecimal impVolo) {
        this.trchDiGar.getTgaImpVolo().setTgaImpVolo(impVolo.copy());
    }

    @Override
    public AfDecimal getImpVoloObj() {
        if (ws.getIndTrchDiGar().getImpVolo() >= 0) {
            return getImpVolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpVoloObj(AfDecimal impVoloObj) {
        if (impVoloObj != null) {
            setImpVolo(new AfDecimal(impVoloObj, 15, 3));
            ws.getIndTrchDiGar().setImpVolo(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpVolo(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbCommisInter() {
        return trchDiGar.getTgaImpbCommisInter().getTgaImpbCommisInter();
    }

    @Override
    public void setImpbCommisInter(AfDecimal impbCommisInter) {
        this.trchDiGar.getTgaImpbCommisInter().setTgaImpbCommisInter(impbCommisInter.copy());
    }

    @Override
    public AfDecimal getImpbCommisInterObj() {
        if (ws.getIndTrchDiGar().getImpbCommisInter() >= 0) {
            return getImpbCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbCommisInterObj(AfDecimal impbCommisInterObj) {
        if (impbCommisInterObj != null) {
            setImpbCommisInter(new AfDecimal(impbCommisInterObj, 15, 3));
            ws.getIndTrchDiGar().setImpbCommisInter(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpbCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbProvAcq() {
        return trchDiGar.getTgaImpbProvAcq().getTgaImpbProvAcq();
    }

    @Override
    public void setImpbProvAcq(AfDecimal impbProvAcq) {
        this.trchDiGar.getTgaImpbProvAcq().setTgaImpbProvAcq(impbProvAcq.copy());
    }

    @Override
    public AfDecimal getImpbProvAcqObj() {
        if (ws.getIndTrchDiGar().getImpbProvAcq() >= 0) {
            return getImpbProvAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbProvAcqObj(AfDecimal impbProvAcqObj) {
        if (impbProvAcqObj != null) {
            setImpbProvAcq(new AfDecimal(impbProvAcqObj, 15, 3));
            ws.getIndTrchDiGar().setImpbProvAcq(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpbProvAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbProvInc() {
        return trchDiGar.getTgaImpbProvInc().getTgaImpbProvInc();
    }

    @Override
    public void setImpbProvInc(AfDecimal impbProvInc) {
        this.trchDiGar.getTgaImpbProvInc().setTgaImpbProvInc(impbProvInc.copy());
    }

    @Override
    public AfDecimal getImpbProvIncObj() {
        if (ws.getIndTrchDiGar().getImpbProvInc() >= 0) {
            return getImpbProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbProvIncObj(AfDecimal impbProvIncObj) {
        if (impbProvIncObj != null) {
            setImpbProvInc(new AfDecimal(impbProvIncObj, 15, 3));
            ws.getIndTrchDiGar().setImpbProvInc(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpbProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbProvRicor() {
        return trchDiGar.getTgaImpbProvRicor().getTgaImpbProvRicor();
    }

    @Override
    public void setImpbProvRicor(AfDecimal impbProvRicor) {
        this.trchDiGar.getTgaImpbProvRicor().setTgaImpbProvRicor(impbProvRicor.copy());
    }

    @Override
    public AfDecimal getImpbProvRicorObj() {
        if (ws.getIndTrchDiGar().getImpbProvRicor() >= 0) {
            return getImpbProvRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbProvRicorObj(AfDecimal impbProvRicorObj) {
        if (impbProvRicorObj != null) {
            setImpbProvRicor(new AfDecimal(impbProvRicorObj, 15, 3));
            ws.getIndTrchDiGar().setImpbProvRicor(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpbProvRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbRemunAss() {
        return trchDiGar.getTgaImpbRemunAss().getTgaImpbRemunAss();
    }

    @Override
    public void setImpbRemunAss(AfDecimal impbRemunAss) {
        this.trchDiGar.getTgaImpbRemunAss().setTgaImpbRemunAss(impbRemunAss.copy());
    }

    @Override
    public AfDecimal getImpbRemunAssObj() {
        if (ws.getIndTrchDiGar().getImpbRemunAss() >= 0) {
            return getImpbRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbRemunAssObj(AfDecimal impbRemunAssObj) {
        if (impbRemunAssObj != null) {
            setImpbRemunAss(new AfDecimal(impbRemunAssObj, 15, 3));
            ws.getIndTrchDiGar().setImpbRemunAss(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpbRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbVisEnd2000() {
        return trchDiGar.getTgaImpbVisEnd2000().getTgaImpbVisEnd2000();
    }

    @Override
    public void setImpbVisEnd2000(AfDecimal impbVisEnd2000) {
        this.trchDiGar.getTgaImpbVisEnd2000().setTgaImpbVisEnd2000(impbVisEnd2000.copy());
    }

    @Override
    public AfDecimal getImpbVisEnd2000Obj() {
        if (ws.getIndTrchDiGar().getImpbVisEnd2000() >= 0) {
            return getImpbVisEnd2000();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbVisEnd2000Obj(AfDecimal impbVisEnd2000Obj) {
        if (impbVisEnd2000Obj != null) {
            setImpbVisEnd2000(new AfDecimal(impbVisEnd2000Obj, 15, 3));
            ws.getIndTrchDiGar().setImpbVisEnd2000(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpbVisEnd2000(((short)-1));
        }
    }

    @Override
    public AfDecimal getIncrPre() {
        return trchDiGar.getTgaIncrPre().getTgaIncrPre();
    }

    @Override
    public void setIncrPre(AfDecimal incrPre) {
        this.trchDiGar.getTgaIncrPre().setTgaIncrPre(incrPre.copy());
    }

    @Override
    public AfDecimal getIncrPreObj() {
        if (ws.getIndTrchDiGar().getIncrPre() >= 0) {
            return getIncrPre();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIncrPreObj(AfDecimal incrPreObj) {
        if (incrPreObj != null) {
            setIncrPre(new AfDecimal(incrPreObj, 15, 3));
            ws.getIndTrchDiGar().setIncrPre(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setIncrPre(((short)-1));
        }
    }

    @Override
    public AfDecimal getIncrPrstz() {
        return trchDiGar.getTgaIncrPrstz().getTgaIncrPrstz();
    }

    @Override
    public void setIncrPrstz(AfDecimal incrPrstz) {
        this.trchDiGar.getTgaIncrPrstz().setTgaIncrPrstz(incrPrstz.copy());
    }

    @Override
    public AfDecimal getIncrPrstzObj() {
        if (ws.getIndTrchDiGar().getIncrPrstz() >= 0) {
            return getIncrPrstz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIncrPrstzObj(AfDecimal incrPrstzObj) {
        if (incrPrstzObj != null) {
            setIncrPrstz(new AfDecimal(incrPrstzObj, 15, 3));
            ws.getIndTrchDiGar().setIncrPrstz(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setIncrPrstz(((short)-1));
        }
    }

    @Override
    public AfDecimal getIntrMora() {
        return trchDiGar.getTgaIntrMora().getTgaIntrMora();
    }

    @Override
    public void setIntrMora(AfDecimal intrMora) {
        this.trchDiGar.getTgaIntrMora().setTgaIntrMora(intrMora.copy());
    }

    @Override
    public AfDecimal getIntrMoraObj() {
        if (ws.getIndTrchDiGar().getIntrMora() >= 0) {
            return getIntrMora();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIntrMoraObj(AfDecimal intrMoraObj) {
        if (intrMoraObj != null) {
            setIntrMora(new AfDecimal(intrMoraObj, 15, 3));
            ws.getIndTrchDiGar().setIntrMora(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setIntrMora(((short)-1));
        }
    }

    @Override
    public AfDecimal getManfeeAntic() {
        return trchDiGar.getTgaManfeeAntic().getTgaManfeeAntic();
    }

    @Override
    public void setManfeeAntic(AfDecimal manfeeAntic) {
        this.trchDiGar.getTgaManfeeAntic().setTgaManfeeAntic(manfeeAntic.copy());
    }

    @Override
    public AfDecimal getManfeeAnticObj() {
        if (ws.getIndTrchDiGar().getManfeeAntic() >= 0) {
            return getManfeeAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setManfeeAnticObj(AfDecimal manfeeAnticObj) {
        if (manfeeAnticObj != null) {
            setManfeeAntic(new AfDecimal(manfeeAnticObj, 15, 3));
            ws.getIndTrchDiGar().setManfeeAntic(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setManfeeAntic(((short)-1));
        }
    }

    @Override
    public AfDecimal getManfeeRicor() {
        return trchDiGar.getTgaManfeeRicor().getTgaManfeeRicor();
    }

    @Override
    public void setManfeeRicor(AfDecimal manfeeRicor) {
        this.trchDiGar.getTgaManfeeRicor().setTgaManfeeRicor(manfeeRicor.copy());
    }

    @Override
    public AfDecimal getManfeeRicorObj() {
        if (ws.getIndTrchDiGar().getManfeeRicor() >= 0) {
            return getManfeeRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setManfeeRicorObj(AfDecimal manfeeRicorObj) {
        if (manfeeRicorObj != null) {
            setManfeeRicor(new AfDecimal(manfeeRicorObj, 15, 3));
            ws.getIndTrchDiGar().setManfeeRicor(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setManfeeRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getMatuEnd2000() {
        return trchDiGar.getTgaMatuEnd2000().getTgaMatuEnd2000();
    }

    @Override
    public void setMatuEnd2000(AfDecimal matuEnd2000) {
        this.trchDiGar.getTgaMatuEnd2000().setTgaMatuEnd2000(matuEnd2000.copy());
    }

    @Override
    public AfDecimal getMatuEnd2000Obj() {
        if (ws.getIndTrchDiGar().getMatuEnd2000() >= 0) {
            return getMatuEnd2000();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMatuEnd2000Obj(AfDecimal matuEnd2000Obj) {
        if (matuEnd2000Obj != null) {
            setMatuEnd2000(new AfDecimal(matuEnd2000Obj, 15, 3));
            ws.getIndTrchDiGar().setMatuEnd2000(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setMatuEnd2000(((short)-1));
        }
    }

    @Override
    public AfDecimal getMinGarto() {
        return trchDiGar.getTgaMinGarto().getTgaMinGarto();
    }

    @Override
    public void setMinGarto(AfDecimal minGarto) {
        this.trchDiGar.getTgaMinGarto().setTgaMinGarto(minGarto.copy());
    }

    @Override
    public AfDecimal getMinGartoObj() {
        if (ws.getIndTrchDiGar().getMinGarto() >= 0) {
            return getMinGarto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMinGartoObj(AfDecimal minGartoObj) {
        if (minGartoObj != null) {
            setMinGarto(new AfDecimal(minGartoObj, 14, 9));
            ws.getIndTrchDiGar().setMinGarto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setMinGarto(((short)-1));
        }
    }

    @Override
    public AfDecimal getMinTrnut() {
        return trchDiGar.getTgaMinTrnut().getTgaMinTrnut();
    }

    @Override
    public void setMinTrnut(AfDecimal minTrnut) {
        this.trchDiGar.getTgaMinTrnut().setTgaMinTrnut(minTrnut.copy());
    }

    @Override
    public AfDecimal getMinTrnutObj() {
        if (ws.getIndTrchDiGar().getMinTrnut() >= 0) {
            return getMinTrnut();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMinTrnutObj(AfDecimal minTrnutObj) {
        if (minTrnutObj != null) {
            setMinTrnut(new AfDecimal(minTrnutObj, 14, 9));
            ws.getIndTrchDiGar().setMinTrnut(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setMinTrnut(((short)-1));
        }
    }

    @Override
    public String getModCalc() {
        return trchDiGar.getTgaModCalc();
    }

    @Override
    public void setModCalc(String modCalc) {
        this.trchDiGar.setTgaModCalc(modCalc);
    }

    @Override
    public String getModCalcObj() {
        if (ws.getIndTrchDiGar().getModCalc() >= 0) {
            return getModCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setModCalcObj(String modCalcObj) {
        if (modCalcObj != null) {
            setModCalc(modCalcObj);
            ws.getIndTrchDiGar().setModCalc(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setModCalc(((short)-1));
        }
    }

    @Override
    public int getNumGgRival() {
        return trchDiGar.getTgaNumGgRival().getTgaNumGgRival();
    }

    @Override
    public void setNumGgRival(int numGgRival) {
        this.trchDiGar.getTgaNumGgRival().setTgaNumGgRival(numGgRival);
    }

    @Override
    public Integer getNumGgRivalObj() {
        if (ws.getIndTrchDiGar().getNumGgRival() >= 0) {
            return ((Integer)getNumGgRival());
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumGgRivalObj(Integer numGgRivalObj) {
        if (numGgRivalObj != null) {
            setNumGgRival(((int)numGgRivalObj));
            ws.getIndTrchDiGar().setNumGgRival(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setNumGgRival(((short)-1));
        }
    }

    @Override
    public AfDecimal getOldTsTec() {
        return trchDiGar.getTgaOldTsTec().getTgaOldTsTec();
    }

    @Override
    public void setOldTsTec(AfDecimal oldTsTec) {
        this.trchDiGar.getTgaOldTsTec().setTgaOldTsTec(oldTsTec.copy());
    }

    @Override
    public AfDecimal getOldTsTecObj() {
        if (ws.getIndTrchDiGar().getOldTsTec() >= 0) {
            return getOldTsTec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setOldTsTecObj(AfDecimal oldTsTecObj) {
        if (oldTsTecObj != null) {
            setOldTsTec(new AfDecimal(oldTsTecObj, 14, 9));
            ws.getIndTrchDiGar().setOldTsTec(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setOldTsTec(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcCommisGest() {
        return trchDiGar.getTgaPcCommisGest().getTgaPcCommisGest();
    }

    @Override
    public void setPcCommisGest(AfDecimal pcCommisGest) {
        this.trchDiGar.getTgaPcCommisGest().setTgaPcCommisGest(pcCommisGest.copy());
    }

    @Override
    public AfDecimal getPcCommisGestObj() {
        if (ws.getIndTrchDiGar().getPcCommisGest() >= 0) {
            return getPcCommisGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcCommisGestObj(AfDecimal pcCommisGestObj) {
        if (pcCommisGestObj != null) {
            setPcCommisGest(new AfDecimal(pcCommisGestObj, 6, 3));
            ws.getIndTrchDiGar().setPcCommisGest(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPcCommisGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcIntrRiat() {
        return trchDiGar.getTgaPcIntrRiat().getTgaPcIntrRiat();
    }

    @Override
    public void setPcIntrRiat(AfDecimal pcIntrRiat) {
        this.trchDiGar.getTgaPcIntrRiat().setTgaPcIntrRiat(pcIntrRiat.copy());
    }

    @Override
    public AfDecimal getPcIntrRiatObj() {
        if (ws.getIndTrchDiGar().getPcIntrRiat() >= 0) {
            return getPcIntrRiat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcIntrRiatObj(AfDecimal pcIntrRiatObj) {
        if (pcIntrRiatObj != null) {
            setPcIntrRiat(new AfDecimal(pcIntrRiatObj, 6, 3));
            ws.getIndTrchDiGar().setPcIntrRiat(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPcIntrRiat(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRetr() {
        return trchDiGar.getTgaPcRetr().getTgaPcRetr();
    }

    @Override
    public void setPcRetr(AfDecimal pcRetr) {
        this.trchDiGar.getTgaPcRetr().setTgaPcRetr(pcRetr.copy());
    }

    @Override
    public AfDecimal getPcRetrObj() {
        if (ws.getIndTrchDiGar().getPcRetr() >= 0) {
            return getPcRetr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRetrObj(AfDecimal pcRetrObj) {
        if (pcRetrObj != null) {
            setPcRetr(new AfDecimal(pcRetrObj, 6, 3));
            ws.getIndTrchDiGar().setPcRetr(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPcRetr(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRipPre() {
        return trchDiGar.getTgaPcRipPre().getTgaPcRipPre();
    }

    @Override
    public void setPcRipPre(AfDecimal pcRipPre) {
        this.trchDiGar.getTgaPcRipPre().setTgaPcRipPre(pcRipPre.copy());
    }

    @Override
    public AfDecimal getPcRipPreObj() {
        if (ws.getIndTrchDiGar().getPcRipPre() >= 0) {
            return getPcRipPre();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRipPreObj(AfDecimal pcRipPreObj) {
        if (pcRipPreObj != null) {
            setPcRipPre(new AfDecimal(pcRipPreObj, 6, 3));
            ws.getIndTrchDiGar().setPcRipPre(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPcRipPre(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreAttDiTrch() {
        return trchDiGar.getTgaPreAttDiTrch().getTgaPreAttDiTrch();
    }

    @Override
    public void setPreAttDiTrch(AfDecimal preAttDiTrch) {
        this.trchDiGar.getTgaPreAttDiTrch().setTgaPreAttDiTrch(preAttDiTrch.copy());
    }

    @Override
    public AfDecimal getPreAttDiTrchObj() {
        if (ws.getIndTrchDiGar().getPreAttDiTrch() >= 0) {
            return getPreAttDiTrch();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreAttDiTrchObj(AfDecimal preAttDiTrchObj) {
        if (preAttDiTrchObj != null) {
            setPreAttDiTrch(new AfDecimal(preAttDiTrchObj, 15, 3));
            ws.getIndTrchDiGar().setPreAttDiTrch(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreAttDiTrch(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreCasoMor() {
        return trchDiGar.getTgaPreCasoMor().getTgaPreCasoMor();
    }

    @Override
    public void setPreCasoMor(AfDecimal preCasoMor) {
        this.trchDiGar.getTgaPreCasoMor().setTgaPreCasoMor(preCasoMor.copy());
    }

    @Override
    public AfDecimal getPreCasoMorObj() {
        if (ws.getIndTrchDiGar().getPreCasoMor() >= 0) {
            return getPreCasoMor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreCasoMorObj(AfDecimal preCasoMorObj) {
        if (preCasoMorObj != null) {
            setPreCasoMor(new AfDecimal(preCasoMorObj, 15, 3));
            ws.getIndTrchDiGar().setPreCasoMor(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreCasoMor(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreIniNet() {
        return trchDiGar.getTgaPreIniNet().getTgaPreIniNet();
    }

    @Override
    public void setPreIniNet(AfDecimal preIniNet) {
        this.trchDiGar.getTgaPreIniNet().setTgaPreIniNet(preIniNet.copy());
    }

    @Override
    public AfDecimal getPreIniNetObj() {
        if (ws.getIndTrchDiGar().getPreIniNet() >= 0) {
            return getPreIniNet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreIniNetObj(AfDecimal preIniNetObj) {
        if (preIniNetObj != null) {
            setPreIniNet(new AfDecimal(preIniNetObj, 15, 3));
            ws.getIndTrchDiGar().setPreIniNet(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreIniNet(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreInvrioIni() {
        return trchDiGar.getTgaPreInvrioIni().getTgaPreInvrioIni();
    }

    @Override
    public void setPreInvrioIni(AfDecimal preInvrioIni) {
        this.trchDiGar.getTgaPreInvrioIni().setTgaPreInvrioIni(preInvrioIni.copy());
    }

    @Override
    public AfDecimal getPreInvrioIniObj() {
        if (ws.getIndTrchDiGar().getPreInvrioIni() >= 0) {
            return getPreInvrioIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreInvrioIniObj(AfDecimal preInvrioIniObj) {
        if (preInvrioIniObj != null) {
            setPreInvrioIni(new AfDecimal(preInvrioIniObj, 15, 3));
            ws.getIndTrchDiGar().setPreInvrioIni(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreInvrioIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreInvrioUlt() {
        return trchDiGar.getTgaPreInvrioUlt().getTgaPreInvrioUlt();
    }

    @Override
    public void setPreInvrioUlt(AfDecimal preInvrioUlt) {
        this.trchDiGar.getTgaPreInvrioUlt().setTgaPreInvrioUlt(preInvrioUlt.copy());
    }

    @Override
    public AfDecimal getPreInvrioUltObj() {
        if (ws.getIndTrchDiGar().getPreInvrioUlt() >= 0) {
            return getPreInvrioUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreInvrioUltObj(AfDecimal preInvrioUltObj) {
        if (preInvrioUltObj != null) {
            setPreInvrioUlt(new AfDecimal(preInvrioUltObj, 15, 3));
            ws.getIndTrchDiGar().setPreInvrioUlt(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreInvrioUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreLrd() {
        return trchDiGar.getTgaPreLrd().getTgaPreLrd();
    }

    @Override
    public void setPreLrd(AfDecimal preLrd) {
        this.trchDiGar.getTgaPreLrd().setTgaPreLrd(preLrd.copy());
    }

    @Override
    public AfDecimal getPreLrdObj() {
        if (ws.getIndTrchDiGar().getPreLrd() >= 0) {
            return getPreLrd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreLrdObj(AfDecimal preLrdObj) {
        if (preLrdObj != null) {
            setPreLrd(new AfDecimal(preLrdObj, 15, 3));
            ws.getIndTrchDiGar().setPreLrd(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreLrd(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrePattuito() {
        return trchDiGar.getTgaPrePattuito().getTgaPrePattuito();
    }

    @Override
    public void setPrePattuito(AfDecimal prePattuito) {
        this.trchDiGar.getTgaPrePattuito().setTgaPrePattuito(prePattuito.copy());
    }

    @Override
    public AfDecimal getPrePattuitoObj() {
        if (ws.getIndTrchDiGar().getPrePattuito() >= 0) {
            return getPrePattuito();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrePattuitoObj(AfDecimal prePattuitoObj) {
        if (prePattuitoObj != null) {
            setPrePattuito(new AfDecimal(prePattuitoObj, 15, 3));
            ws.getIndTrchDiGar().setPrePattuito(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrePattuito(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrePpIni() {
        return trchDiGar.getTgaPrePpIni().getTgaPrePpIni();
    }

    @Override
    public void setPrePpIni(AfDecimal prePpIni) {
        this.trchDiGar.getTgaPrePpIni().setTgaPrePpIni(prePpIni.copy());
    }

    @Override
    public AfDecimal getPrePpIniObj() {
        if (ws.getIndTrchDiGar().getPrePpIni() >= 0) {
            return getPrePpIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrePpIniObj(AfDecimal prePpIniObj) {
        if (prePpIniObj != null) {
            setPrePpIni(new AfDecimal(prePpIniObj, 15, 3));
            ws.getIndTrchDiGar().setPrePpIni(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrePpIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrePpUlt() {
        return trchDiGar.getTgaPrePpUlt().getTgaPrePpUlt();
    }

    @Override
    public void setPrePpUlt(AfDecimal prePpUlt) {
        this.trchDiGar.getTgaPrePpUlt().setTgaPrePpUlt(prePpUlt.copy());
    }

    @Override
    public AfDecimal getPrePpUltObj() {
        if (ws.getIndTrchDiGar().getPrePpUlt() >= 0) {
            return getPrePpUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrePpUltObj(AfDecimal prePpUltObj) {
        if (prePpUltObj != null) {
            setPrePpUlt(new AfDecimal(prePpUltObj, 15, 3));
            ws.getIndTrchDiGar().setPrePpUlt(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrePpUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreRivto() {
        return trchDiGar.getTgaPreRivto().getTgaPreRivto();
    }

    @Override
    public void setPreRivto(AfDecimal preRivto) {
        this.trchDiGar.getTgaPreRivto().setTgaPreRivto(preRivto.copy());
    }

    @Override
    public AfDecimal getPreRivtoObj() {
        if (ws.getIndTrchDiGar().getPreRivto() >= 0) {
            return getPreRivto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreRivtoObj(AfDecimal preRivtoObj) {
        if (preRivtoObj != null) {
            setPreRivto(new AfDecimal(preRivtoObj, 15, 3));
            ws.getIndTrchDiGar().setPreRivto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreRivto(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreStab() {
        return trchDiGar.getTgaPreStab().getTgaPreStab();
    }

    @Override
    public void setPreStab(AfDecimal preStab) {
        this.trchDiGar.getTgaPreStab().setTgaPreStab(preStab.copy());
    }

    @Override
    public AfDecimal getPreStabObj() {
        if (ws.getIndTrchDiGar().getPreStab() >= 0) {
            return getPreStab();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreStabObj(AfDecimal preStabObj) {
        if (preStabObj != null) {
            setPreStab(new AfDecimal(preStabObj, 15, 3));
            ws.getIndTrchDiGar().setPreStab(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreStab(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreTariIni() {
        return trchDiGar.getTgaPreTariIni().getTgaPreTariIni();
    }

    @Override
    public void setPreTariIni(AfDecimal preTariIni) {
        this.trchDiGar.getTgaPreTariIni().setTgaPreTariIni(preTariIni.copy());
    }

    @Override
    public AfDecimal getPreTariIniObj() {
        if (ws.getIndTrchDiGar().getPreTariIni() >= 0) {
            return getPreTariIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreTariIniObj(AfDecimal preTariIniObj) {
        if (preTariIniObj != null) {
            setPreTariIni(new AfDecimal(preTariIniObj, 15, 3));
            ws.getIndTrchDiGar().setPreTariIni(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreTariIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreTariUlt() {
        return trchDiGar.getTgaPreTariUlt().getTgaPreTariUlt();
    }

    @Override
    public void setPreTariUlt(AfDecimal preTariUlt) {
        this.trchDiGar.getTgaPreTariUlt().setTgaPreTariUlt(preTariUlt.copy());
    }

    @Override
    public AfDecimal getPreTariUltObj() {
        if (ws.getIndTrchDiGar().getPreTariUlt() >= 0) {
            return getPreTariUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreTariUltObj(AfDecimal preTariUltObj) {
        if (preTariUltObj != null) {
            setPreTariUlt(new AfDecimal(preTariUltObj, 15, 3));
            ws.getIndTrchDiGar().setPreTariUlt(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreTariUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreUniRivto() {
        return trchDiGar.getTgaPreUniRivto().getTgaPreUniRivto();
    }

    @Override
    public void setPreUniRivto(AfDecimal preUniRivto) {
        this.trchDiGar.getTgaPreUniRivto().setTgaPreUniRivto(preUniRivto.copy());
    }

    @Override
    public AfDecimal getPreUniRivtoObj() {
        if (ws.getIndTrchDiGar().getPreUniRivto() >= 0) {
            return getPreUniRivto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreUniRivtoObj(AfDecimal preUniRivtoObj) {
        if (preUniRivtoObj != null) {
            setPreUniRivto(new AfDecimal(preUniRivtoObj, 15, 3));
            ws.getIndTrchDiGar().setPreUniRivto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreUniRivto(((short)-1));
        }
    }

    @Override
    public AfDecimal getProv1aaAcq() {
        return trchDiGar.getTgaProv1aaAcq().getTgaProv1aaAcq();
    }

    @Override
    public void setProv1aaAcq(AfDecimal prov1aaAcq) {
        this.trchDiGar.getTgaProv1aaAcq().setTgaProv1aaAcq(prov1aaAcq.copy());
    }

    @Override
    public AfDecimal getProv1aaAcqObj() {
        if (ws.getIndTrchDiGar().getProv1aaAcq() >= 0) {
            return getProv1aaAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setProv1aaAcqObj(AfDecimal prov1aaAcqObj) {
        if (prov1aaAcqObj != null) {
            setProv1aaAcq(new AfDecimal(prov1aaAcqObj, 15, 3));
            ws.getIndTrchDiGar().setProv1aaAcq(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setProv1aaAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getProv2aaAcq() {
        return trchDiGar.getTgaProv2aaAcq().getTgaProv2aaAcq();
    }

    @Override
    public void setProv2aaAcq(AfDecimal prov2aaAcq) {
        this.trchDiGar.getTgaProv2aaAcq().setTgaProv2aaAcq(prov2aaAcq.copy());
    }

    @Override
    public AfDecimal getProv2aaAcqObj() {
        if (ws.getIndTrchDiGar().getProv2aaAcq() >= 0) {
            return getProv2aaAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setProv2aaAcqObj(AfDecimal prov2aaAcqObj) {
        if (prov2aaAcqObj != null) {
            setProv2aaAcq(new AfDecimal(prov2aaAcqObj, 15, 3));
            ws.getIndTrchDiGar().setProv2aaAcq(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setProv2aaAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getProvInc() {
        return trchDiGar.getTgaProvInc().getTgaProvInc();
    }

    @Override
    public void setProvInc(AfDecimal provInc) {
        this.trchDiGar.getTgaProvInc().setTgaProvInc(provInc.copy());
    }

    @Override
    public AfDecimal getProvIncObj() {
        if (ws.getIndTrchDiGar().getProvInc() >= 0) {
            return getProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setProvIncObj(AfDecimal provIncObj) {
        if (provIncObj != null) {
            setProvInc(new AfDecimal(provIncObj, 15, 3));
            ws.getIndTrchDiGar().setProvInc(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getProvRicor() {
        return trchDiGar.getTgaProvRicor().getTgaProvRicor();
    }

    @Override
    public void setProvRicor(AfDecimal provRicor) {
        this.trchDiGar.getTgaProvRicor().setTgaProvRicor(provRicor.copy());
    }

    @Override
    public AfDecimal getProvRicorObj() {
        if (ws.getIndTrchDiGar().getProvRicor() >= 0) {
            return getProvRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setProvRicorObj(AfDecimal provRicorObj) {
        if (provRicorObj != null) {
            setProvRicor(new AfDecimal(provRicorObj, 15, 3));
            ws.getIndTrchDiGar().setProvRicor(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setProvRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrstzAggIni() {
        return trchDiGar.getTgaPrstzAggIni().getTgaPrstzAggIni();
    }

    @Override
    public void setPrstzAggIni(AfDecimal prstzAggIni) {
        this.trchDiGar.getTgaPrstzAggIni().setTgaPrstzAggIni(prstzAggIni.copy());
    }

    @Override
    public AfDecimal getPrstzAggIniObj() {
        if (ws.getIndTrchDiGar().getPrstzAggIni() >= 0) {
            return getPrstzAggIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrstzAggIniObj(AfDecimal prstzAggIniObj) {
        if (prstzAggIniObj != null) {
            setPrstzAggIni(new AfDecimal(prstzAggIniObj, 15, 3));
            ws.getIndTrchDiGar().setPrstzAggIni(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrstzAggIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrstzAggUlt() {
        return trchDiGar.getTgaPrstzAggUlt().getTgaPrstzAggUlt();
    }

    @Override
    public void setPrstzAggUlt(AfDecimal prstzAggUlt) {
        this.trchDiGar.getTgaPrstzAggUlt().setTgaPrstzAggUlt(prstzAggUlt.copy());
    }

    @Override
    public AfDecimal getPrstzAggUltObj() {
        if (ws.getIndTrchDiGar().getPrstzAggUlt() >= 0) {
            return getPrstzAggUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrstzAggUltObj(AfDecimal prstzAggUltObj) {
        if (prstzAggUltObj != null) {
            setPrstzAggUlt(new AfDecimal(prstzAggUltObj, 15, 3));
            ws.getIndTrchDiGar().setPrstzAggUlt(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrstzAggUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrstzIni() {
        return trchDiGar.getTgaPrstzIni().getTgaPrstzIni();
    }

    @Override
    public void setPrstzIni(AfDecimal prstzIni) {
        this.trchDiGar.getTgaPrstzIni().setTgaPrstzIni(prstzIni.copy());
    }

    @Override
    public AfDecimal getPrstzIniNewfis() {
        return trchDiGar.getTgaPrstzIniNewfis().getTgaPrstzIniNewfis();
    }

    @Override
    public void setPrstzIniNewfis(AfDecimal prstzIniNewfis) {
        this.trchDiGar.getTgaPrstzIniNewfis().setTgaPrstzIniNewfis(prstzIniNewfis.copy());
    }

    @Override
    public AfDecimal getPrstzIniNewfisObj() {
        if (ws.getIndTrchDiGar().getPrstzIniNewfis() >= 0) {
            return getPrstzIniNewfis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrstzIniNewfisObj(AfDecimal prstzIniNewfisObj) {
        if (prstzIniNewfisObj != null) {
            setPrstzIniNewfis(new AfDecimal(prstzIniNewfisObj, 15, 3));
            ws.getIndTrchDiGar().setPrstzIniNewfis(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrstzIniNewfis(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrstzIniNforz() {
        return trchDiGar.getTgaPrstzIniNforz().getTgaPrstzIniNforz();
    }

    @Override
    public void setPrstzIniNforz(AfDecimal prstzIniNforz) {
        this.trchDiGar.getTgaPrstzIniNforz().setTgaPrstzIniNforz(prstzIniNforz.copy());
    }

    @Override
    public AfDecimal getPrstzIniNforzObj() {
        if (ws.getIndTrchDiGar().getPrstzIniNforz() >= 0) {
            return getPrstzIniNforz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrstzIniNforzObj(AfDecimal prstzIniNforzObj) {
        if (prstzIniNforzObj != null) {
            setPrstzIniNforz(new AfDecimal(prstzIniNforzObj, 15, 3));
            ws.getIndTrchDiGar().setPrstzIniNforz(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrstzIniNforz(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrstzIniObj() {
        if (ws.getIndTrchDiGar().getPrstzIni() >= 0) {
            return getPrstzIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrstzIniObj(AfDecimal prstzIniObj) {
        if (prstzIniObj != null) {
            setPrstzIni(new AfDecimal(prstzIniObj, 15, 3));
            ws.getIndTrchDiGar().setPrstzIni(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrstzIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrstzIniStab() {
        return trchDiGar.getTgaPrstzIniStab().getTgaPrstzIniStab();
    }

    @Override
    public void setPrstzIniStab(AfDecimal prstzIniStab) {
        this.trchDiGar.getTgaPrstzIniStab().setTgaPrstzIniStab(prstzIniStab.copy());
    }

    @Override
    public AfDecimal getPrstzIniStabObj() {
        if (ws.getIndTrchDiGar().getPrstzIniStab() >= 0) {
            return getPrstzIniStab();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrstzIniStabObj(AfDecimal prstzIniStabObj) {
        if (prstzIniStabObj != null) {
            setPrstzIniStab(new AfDecimal(prstzIniStabObj, 15, 3));
            ws.getIndTrchDiGar().setPrstzIniStab(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrstzIniStab(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrstzRidIni() {
        return trchDiGar.getTgaPrstzRidIni().getTgaPrstzRidIni();
    }

    @Override
    public void setPrstzRidIni(AfDecimal prstzRidIni) {
        this.trchDiGar.getTgaPrstzRidIni().setTgaPrstzRidIni(prstzRidIni.copy());
    }

    @Override
    public AfDecimal getPrstzRidIniObj() {
        if (ws.getIndTrchDiGar().getPrstzRidIni() >= 0) {
            return getPrstzRidIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrstzRidIniObj(AfDecimal prstzRidIniObj) {
        if (prstzRidIniObj != null) {
            setPrstzRidIni(new AfDecimal(prstzRidIniObj, 15, 3));
            ws.getIndTrchDiGar().setPrstzRidIni(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrstzRidIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrstzUlt() {
        return trchDiGar.getTgaPrstzUlt().getTgaPrstzUlt();
    }

    @Override
    public void setPrstzUlt(AfDecimal prstzUlt) {
        this.trchDiGar.getTgaPrstzUlt().setTgaPrstzUlt(prstzUlt.copy());
    }

    @Override
    public AfDecimal getPrstzUltObj() {
        if (ws.getIndTrchDiGar().getPrstzUlt() >= 0) {
            return getPrstzUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrstzUltObj(AfDecimal prstzUltObj) {
        if (prstzUltObj != null) {
            setPrstzUlt(new AfDecimal(prstzUltObj, 15, 3));
            ws.getIndTrchDiGar().setPrstzUlt(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrstzUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getRatLrd() {
        return trchDiGar.getTgaRatLrd().getTgaRatLrd();
    }

    @Override
    public void setRatLrd(AfDecimal ratLrd) {
        this.trchDiGar.getTgaRatLrd().setTgaRatLrd(ratLrd.copy());
    }

    @Override
    public AfDecimal getRatLrdObj() {
        if (ws.getIndTrchDiGar().getRatLrd() >= 0) {
            return getRatLrd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRatLrdObj(AfDecimal ratLrdObj) {
        if (ratLrdObj != null) {
            setRatLrd(new AfDecimal(ratLrdObj, 15, 3));
            ws.getIndTrchDiGar().setRatLrd(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setRatLrd(((short)-1));
        }
    }

    @Override
    public AfDecimal getRemunAss() {
        return trchDiGar.getTgaRemunAss().getTgaRemunAss();
    }

    @Override
    public void setRemunAss(AfDecimal remunAss) {
        this.trchDiGar.getTgaRemunAss().setTgaRemunAss(remunAss.copy());
    }

    @Override
    public AfDecimal getRemunAssObj() {
        if (ws.getIndTrchDiGar().getRemunAss() >= 0) {
            return getRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRemunAssObj(AfDecimal remunAssObj) {
        if (remunAssObj != null) {
            setRemunAss(new AfDecimal(remunAssObj, 15, 3));
            ws.getIndTrchDiGar().setRemunAss(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getRenIniTsTec0() {
        return trchDiGar.getTgaRenIniTsTec0().getTgaRenIniTsTec0();
    }

    @Override
    public void setRenIniTsTec0(AfDecimal renIniTsTec0) {
        this.trchDiGar.getTgaRenIniTsTec0().setTgaRenIniTsTec0(renIniTsTec0.copy());
    }

    @Override
    public AfDecimal getRenIniTsTec0Obj() {
        if (ws.getIndTrchDiGar().getRenIniTsTec0() >= 0) {
            return getRenIniTsTec0();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRenIniTsTec0Obj(AfDecimal renIniTsTec0Obj) {
        if (renIniTsTec0Obj != null) {
            setRenIniTsTec0(new AfDecimal(renIniTsTec0Obj, 15, 3));
            ws.getIndTrchDiGar().setRenIniTsTec0(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setRenIniTsTec0(((short)-1));
        }
    }

    @Override
    public AfDecimal getRendtoLrd() {
        return trchDiGar.getTgaRendtoLrd().getTgaRendtoLrd();
    }

    @Override
    public void setRendtoLrd(AfDecimal rendtoLrd) {
        this.trchDiGar.getTgaRendtoLrd().setTgaRendtoLrd(rendtoLrd.copy());
    }

    @Override
    public AfDecimal getRendtoLrdObj() {
        if (ws.getIndTrchDiGar().getRendtoLrd() >= 0) {
            return getRendtoLrd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRendtoLrdObj(AfDecimal rendtoLrdObj) {
        if (rendtoLrdObj != null) {
            setRendtoLrd(new AfDecimal(rendtoLrdObj, 14, 9));
            ws.getIndTrchDiGar().setRendtoLrd(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setRendtoLrd(((short)-1));
        }
    }

    @Override
    public AfDecimal getRendtoRetr() {
        return trchDiGar.getTgaRendtoRetr().getTgaRendtoRetr();
    }

    @Override
    public void setRendtoRetr(AfDecimal rendtoRetr) {
        this.trchDiGar.getTgaRendtoRetr().setTgaRendtoRetr(rendtoRetr.copy());
    }

    @Override
    public AfDecimal getRendtoRetrObj() {
        if (ws.getIndTrchDiGar().getRendtoRetr() >= 0) {
            return getRendtoRetr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRendtoRetrObj(AfDecimal rendtoRetrObj) {
        if (rendtoRetrObj != null) {
            setRendtoRetr(new AfDecimal(rendtoRetrObj, 14, 9));
            ws.getIndTrchDiGar().setRendtoRetr(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setRendtoRetr(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisMat() {
        return trchDiGar.getTgaRisMat().getTgaRisMat();
    }

    @Override
    public void setRisMat(AfDecimal risMat) {
        this.trchDiGar.getTgaRisMat().setTgaRisMat(risMat.copy());
    }

    @Override
    public AfDecimal getRisMatObj() {
        if (ws.getIndTrchDiGar().getRisMat() >= 0) {
            return getRisMat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisMatObj(AfDecimal risMatObj) {
        if (risMatObj != null) {
            setRisMat(new AfDecimal(risMatObj, 15, 3));
            ws.getIndTrchDiGar().setRisMat(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setRisMat(((short)-1));
        }
    }

    @Override
    public long getTgaDsRiga() {
        return trchDiGar.getTgaDsRiga();
    }

    @Override
    public void setTgaDsRiga(long tgaDsRiga) {
        this.trchDiGar.setTgaDsRiga(tgaDsRiga);
    }

    @Override
    public int getTgaIdAdes() {
        return trchDiGar.getTgaIdAdes();
    }

    @Override
    public void setTgaIdAdes(int tgaIdAdes) {
        this.trchDiGar.setTgaIdAdes(tgaIdAdes);
    }

    @Override
    public int getTgaIdMoviCrz() {
        return trchDiGar.getTgaIdMoviCrz();
    }

    @Override
    public void setTgaIdMoviCrz(int tgaIdMoviCrz) {
        this.trchDiGar.setTgaIdMoviCrz(tgaIdMoviCrz);
    }

    @Override
    public int getTgaIdPoli() {
        return trchDiGar.getTgaIdPoli();
    }

    @Override
    public void setTgaIdPoli(int tgaIdPoli) {
        this.trchDiGar.setTgaIdPoli(tgaIdPoli);
    }

    @Override
    public char getTpAdegAbb() {
        return trchDiGar.getTgaTpAdegAbb();
    }

    @Override
    public void setTpAdegAbb(char tpAdegAbb) {
        this.trchDiGar.setTgaTpAdegAbb(tpAdegAbb);
    }

    @Override
    public Character getTpAdegAbbObj() {
        if (ws.getIndTrchDiGar().getTpAdegAbb() >= 0) {
            return ((Character)getTpAdegAbb());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpAdegAbbObj(Character tpAdegAbbObj) {
        if (tpAdegAbbObj != null) {
            setTpAdegAbb(((char)tpAdegAbbObj));
            ws.getIndTrchDiGar().setTpAdegAbb(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setTpAdegAbb(((short)-1));
        }
    }

    @Override
    public String getTpManfeeAppl() {
        return trchDiGar.getTgaTpManfeeAppl();
    }

    @Override
    public void setTpManfeeAppl(String tpManfeeAppl) {
        this.trchDiGar.setTgaTpManfeeAppl(tpManfeeAppl);
    }

    @Override
    public String getTpManfeeApplObj() {
        if (ws.getIndTrchDiGar().getTpManfeeAppl() >= 0) {
            return getTpManfeeAppl();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpManfeeApplObj(String tpManfeeApplObj) {
        if (tpManfeeApplObj != null) {
            setTpManfeeAppl(tpManfeeApplObj);
            ws.getIndTrchDiGar().setTpManfeeAppl(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setTpManfeeAppl(((short)-1));
        }
    }

    @Override
    public String getTpRgmFisc() {
        return trchDiGar.getTgaTpRgmFisc();
    }

    @Override
    public void setTpRgmFisc(String tpRgmFisc) {
        this.trchDiGar.setTgaTpRgmFisc(tpRgmFisc);
    }

    @Override
    public String getTpRival() {
        return trchDiGar.getTgaTpRival();
    }

    @Override
    public void setTpRival(String tpRival) {
        this.trchDiGar.setTgaTpRival(tpRival);
    }

    @Override
    public String getTpRivalObj() {
        if (ws.getIndTrchDiGar().getTpRival() >= 0) {
            return getTpRival();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpRivalObj(String tpRivalObj) {
        if (tpRivalObj != null) {
            setTpRival(tpRivalObj);
            ws.getIndTrchDiGar().setTpRival(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setTpRival(((short)-1));
        }
    }

    @Override
    public String getTpTrch() {
        return trchDiGar.getTgaTpTrch();
    }

    @Override
    public void setTpTrch(String tpTrch) {
        this.trchDiGar.setTgaTpTrch(tpTrch);
    }

    @Override
    public AfDecimal getTsRivalFis() {
        return trchDiGar.getTgaTsRivalFis().getTgaTsRivalFis();
    }

    @Override
    public void setTsRivalFis(AfDecimal tsRivalFis) {
        this.trchDiGar.getTgaTsRivalFis().setTgaTsRivalFis(tsRivalFis.copy());
    }

    @Override
    public AfDecimal getTsRivalFisObj() {
        if (ws.getIndTrchDiGar().getTsRivalFis() >= 0) {
            return getTsRivalFis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTsRivalFisObj(AfDecimal tsRivalFisObj) {
        if (tsRivalFisObj != null) {
            setTsRivalFis(new AfDecimal(tsRivalFisObj, 14, 9));
            ws.getIndTrchDiGar().setTsRivalFis(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setTsRivalFis(((short)-1));
        }
    }

    @Override
    public AfDecimal getTsRivalIndiciz() {
        return trchDiGar.getTgaTsRivalIndiciz().getTgaTsRivalIndiciz();
    }

    @Override
    public void setTsRivalIndiciz(AfDecimal tsRivalIndiciz) {
        this.trchDiGar.getTgaTsRivalIndiciz().setTgaTsRivalIndiciz(tsRivalIndiciz.copy());
    }

    @Override
    public AfDecimal getTsRivalIndicizObj() {
        if (ws.getIndTrchDiGar().getTsRivalIndiciz() >= 0) {
            return getTsRivalIndiciz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTsRivalIndicizObj(AfDecimal tsRivalIndicizObj) {
        if (tsRivalIndicizObj != null) {
            setTsRivalIndiciz(new AfDecimal(tsRivalIndicizObj, 14, 9));
            ws.getIndTrchDiGar().setTsRivalIndiciz(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setTsRivalIndiciz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTsRivalNet() {
        return trchDiGar.getTgaTsRivalNet().getTgaTsRivalNet();
    }

    @Override
    public void setTsRivalNet(AfDecimal tsRivalNet) {
        this.trchDiGar.getTgaTsRivalNet().setTgaTsRivalNet(tsRivalNet.copy());
    }

    @Override
    public AfDecimal getTsRivalNetObj() {
        if (ws.getIndTrchDiGar().getTsRivalNet() >= 0) {
            return getTsRivalNet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTsRivalNetObj(AfDecimal tsRivalNetObj) {
        if (tsRivalNetObj != null) {
            setTsRivalNet(new AfDecimal(tsRivalNetObj, 14, 9));
            ws.getIndTrchDiGar().setTsRivalNet(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setTsRivalNet(((short)-1));
        }
    }

    @Override
    public AfDecimal getVisEnd2000() {
        return trchDiGar.getTgaVisEnd2000().getTgaVisEnd2000();
    }

    @Override
    public void setVisEnd2000(AfDecimal visEnd2000) {
        this.trchDiGar.getTgaVisEnd2000().setTgaVisEnd2000(visEnd2000.copy());
    }

    @Override
    public AfDecimal getVisEnd2000Nforz() {
        return trchDiGar.getTgaVisEnd2000Nforz().getTgaVisEnd2000Nforz();
    }

    @Override
    public void setVisEnd2000Nforz(AfDecimal visEnd2000Nforz) {
        this.trchDiGar.getTgaVisEnd2000Nforz().setTgaVisEnd2000Nforz(visEnd2000Nforz.copy());
    }

    @Override
    public AfDecimal getVisEnd2000NforzObj() {
        if (ws.getIndTrchDiGar().getVisEnd2000Nforz() >= 0) {
            return getVisEnd2000Nforz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setVisEnd2000NforzObj(AfDecimal visEnd2000NforzObj) {
        if (visEnd2000NforzObj != null) {
            setVisEnd2000Nforz(new AfDecimal(visEnd2000NforzObj, 15, 3));
            ws.getIndTrchDiGar().setVisEnd2000Nforz(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setVisEnd2000Nforz(((short)-1));
        }
    }

    @Override
    public AfDecimal getVisEnd2000Obj() {
        if (ws.getIndTrchDiGar().getVisEnd2000() >= 0) {
            return getVisEnd2000();
        }
        else {
            return null;
        }
    }

    @Override
    public void setVisEnd2000Obj(AfDecimal visEnd2000Obj) {
        if (visEnd2000Obj != null) {
            setVisEnd2000(new AfDecimal(visEnd2000Obj, 15, 3));
            ws.getIndTrchDiGar().setVisEnd2000(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setVisEnd2000(((short)-1));
        }
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
