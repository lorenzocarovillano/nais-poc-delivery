package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.AnagDatoDao;
import it.accenture.jnais.commons.data.to.IAnagDato;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.AnagDato;
import it.accenture.jnais.ws.Idss0080Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.AdaLunghezzaDato;
import it.accenture.jnais.ws.redefines.AdaPrecisioneDato;

/**Original name: IDSS0080<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  21 NOV 2007.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idss0080 extends Program implements IAnagDato {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private AnagDatoDao anagDatoDao = new AnagDatoDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idss0080Data ws = new Idss0080Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: ANAG-DATO
    private AnagDato anagDato;

    //==== METHODS ====
    /**Original name: PROGRAM_IDSS0080_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, AnagDato anagDato) {
        this.idsv0003 = idsv0003;
        this.anagDato = anagDato;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: PERFORM A300-ELABORA             THRU A300-EX
        a300Elabora();
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idss0080 getInstance() {
        return ((Idss0080)Programs.getInstance(Idss0080.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDSS0080'               TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDSS0080");
        // COB_CODE: MOVE 'ANAG_DATO'              TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("ANAG_DATO");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A300-ELABORA<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle
	 * ----
	 * *****************************************************************</pre>*/
    private void a300Elabora() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT                 THRU A310-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A360-OPEN-CURSOR            THRU A360-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A370-CLOSE-CURSOR           THRU A370-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A380-FETCH-FIRST            THRU A380-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A390-FETCH-NEXT             THRU A390-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER           TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT                 THRU A310-EX
            a310Select();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A360-OPEN-CURSOR            THRU A360-EX
            a360OpenCursor();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A370-CLOSE-CURSOR           THRU A370-EX
            a370CloseCursor();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A380-FETCH-FIRST            THRU A380-EX
            a380FetchFirst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT             THRU A390-EX
            a390FetchNext();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER           TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A305-DECLARE-CURSOR<br>
	 * <pre>***************************************************************</pre>*/
    private void a305DeclareCursor() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE CUR-ADA CURSOR WITH HOLD FOR
        //              SELECT
        //                COD_COMPAGNIA_ANIA
        //                ,COD_DATO
        //                ,DESC_DATO
        //                ,TIPO_DATO
        //                ,LUNGHEZZA_DATO
        //                ,PRECISIONE_DATO
        //                ,COD_DOMINIO
        //                ,FORMATTAZIONE_DATO
        //                ,DS_UTENTE
        //             FROM ANAG_DATO
        //             WHERE     COD_COMPAGNIA_ANIA = :ADA-COD-COMPAGNIA-ANIA
        //                   AND COD_DATO           = :ADA-COD-DATO
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT<br>
	 * <pre>*****************************************************************</pre>*/
    private void a310Select() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                COD_COMPAGNIA_ANIA
        //                ,COD_DATO
        //                ,DESC_DATO
        //                ,TIPO_DATO
        //                ,LUNGHEZZA_DATO
        //                ,PRECISIONE_DATO
        //                ,COD_DOMINIO
        //                ,FORMATTAZIONE_DATO
        //                ,DS_UTENTE
        //             INTO
        //                :ADA-COD-COMPAGNIA-ANIA
        //               ,:ADA-COD-DATO
        //               ,:ADA-DESC-DATO-VCHAR
        //                :IND-ADA-DESC-DATO
        //               ,:ADA-TIPO-DATO
        //                :IND-ADA-TIPO-DATO
        //               ,:ADA-LUNGHEZZA-DATO
        //                :IND-ADA-LUNGHEZZA-DATO
        //               ,:ADA-PRECISIONE-DATO
        //                :IND-ADA-PRECISIONE-DATO
        //               ,:ADA-COD-DOMINIO
        //                :IND-ADA-COD-DOMINIO
        //               ,:ADA-FORMATTAZIONE-DATO
        //                :IND-ADA-FORMATTAZIONE-DATO
        //               ,:ADA-DS-UTENTE
        //             FROM ANAG_DATO
        //             WHERE     COD_COMPAGNIA_ANIA = :ADA-COD-COMPAGNIA-ANIA
        //                   AND COD_DATO           = :ADA-COD-DATO
        //           END-EXEC.
        anagDatoDao.selectRec(anagDato.getAdaCodCompagniaAnia(), anagDato.getAdaCodDato(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A360-OPEN-CURSOR<br>
	 * <pre>*****************************************************************</pre>*/
    private void a360OpenCursor() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR THRU A305-EX.
        a305DeclareCursor();
        // COB_CODE: EXEC SQL
        //                OPEN CUR-ADA
        //           END-EXEC.
        anagDatoDao.openCurAda(anagDato.getAdaCodCompagniaAnia(), anagDato.getAdaCodDato());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR<br>
	 * <pre>*****************************************************************</pre>*/
    private void a370CloseCursor() {
        // COB_CODE: EXEC SQL
        //                CLOSE CUR-ADA
        //           END-EXEC.
        anagDatoDao.closeCurAda();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A380-FETCH-FIRST<br>
	 * <pre>*****************************************************************</pre>*/
    private void a380FetchFirst() {
        // COB_CODE: PERFORM A360-OPEN-CURSOR    THRU A360-EX.
        a360OpenCursor();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A390-FETCH-NEXT THRU A390-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT THRU A390-EX
            a390FetchNext();
        }
    }

    /**Original name: A390-FETCH-NEXT<br>
	 * <pre>*****************************************************************</pre>*/
    private void a390FetchNext() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-ADA
        //           INTO
        //                :ADA-COD-COMPAGNIA-ANIA
        //               ,:ADA-COD-DATO
        //               ,:ADA-DESC-DATO-VCHAR
        //                :IND-ADA-DESC-DATO
        //               ,:ADA-TIPO-DATO
        //                :IND-ADA-TIPO-DATO
        //               ,:ADA-LUNGHEZZA-DATO
        //                :IND-ADA-LUNGHEZZA-DATO
        //               ,:ADA-PRECISIONE-DATO
        //                :IND-ADA-PRECISIONE-DATO
        //               ,:ADA-COD-DOMINIO
        //                :IND-ADA-COD-DOMINIO
        //               ,:ADA-FORMATTAZIONE-DATO
        //                :IND-ADA-FORMATTAZIONE-DATO
        //               ,:ADA-DS-UTENTE
        //           END-EXEC.
        anagDatoDao.fetchCurAda(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR THRU A370-EX
            a370CloseCursor();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-ADA-DESC-DATO = -1
        //              MOVE HIGH-VALUES TO ADA-DESC-DATO
        //           END-IF
        if (ws.getIndAnagDato().getDescDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADA-DESC-DATO
            anagDato.setAdaDescDato(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AnagDato.Len.ADA_DESC_DATO));
        }
        // COB_CODE: IF IND-ADA-TIPO-DATO = -1
        //              MOVE HIGH-VALUES TO ADA-TIPO-DATO-NULL
        //           END-IF
        if (ws.getIndAnagDato().getTipoDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADA-TIPO-DATO-NULL
            anagDato.setAdaTipoDato(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AnagDato.Len.ADA_TIPO_DATO));
        }
        // COB_CODE: IF IND-ADA-LUNGHEZZA-DATO = -1
        //              MOVE HIGH-VALUES TO ADA-LUNGHEZZA-DATO-NULL
        //           END-IF
        if (ws.getIndAnagDato().getLunghezzaDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADA-LUNGHEZZA-DATO-NULL
            anagDato.getAdaLunghezzaDato().setAdaLunghezzaDatoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdaLunghezzaDato.Len.ADA_LUNGHEZZA_DATO_NULL));
        }
        // COB_CODE: IF IND-ADA-PRECISIONE-DATO = -1
        //              MOVE HIGH-VALUES TO ADA-PRECISIONE-DATO-NULL
        //           END-IF
        if (ws.getIndAnagDato().getPrecisioneDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADA-PRECISIONE-DATO-NULL
            anagDato.getAdaPrecisioneDato().setAdaPrecisioneDatoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdaPrecisioneDato.Len.ADA_PRECISIONE_DATO_NULL));
        }
        // COB_CODE: IF IND-ADA-COD-DOMINIO = -1
        //              MOVE HIGH-VALUES TO ADA-COD-DOMINIO
        //           END-IF
        if (ws.getIndAnagDato().getCodDominio() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADA-COD-DOMINIO
            anagDato.setAdaCodDominio(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AnagDato.Len.ADA_COD_DOMINIO));
        }
        // COB_CODE: IF IND-ADA-FORMATTAZIONE-DATO = -1
        //              MOVE HIGH-VALUES TO ADA-FORMATTAZIONE-DATO
        //           END-IF.
        if (ws.getIndAnagDato().getFormattazioneDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADA-FORMATTAZIONE-DATO
            anagDato.setAdaFormattazioneDato(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AnagDato.Len.ADA_FORMATTAZIONE_DATO));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF ADA-DESC-DATO
        //                       TO ADA-DESC-DATO-LEN.
        anagDato.setAdaDescDatoLen(((short)AnagDato.Len.ADA_DESC_DATO));
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=25, because the code is unreachable.
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=33, because the code is unreachable.
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    @Override
    public int getCodCompagniaAnia() {
        return anagDato.getAdaCodCompagniaAnia();
    }

    @Override
    public void setCodCompagniaAnia(int codCompagniaAnia) {
        this.anagDato.setAdaCodCompagniaAnia(codCompagniaAnia);
    }

    @Override
    public String getCodDato() {
        return anagDato.getAdaCodDato();
    }

    @Override
    public void setCodDato(String codDato) {
        this.anagDato.setAdaCodDato(codDato);
    }

    @Override
    public String getCodDominio() {
        return anagDato.getAdaCodDominio();
    }

    @Override
    public void setCodDominio(String codDominio) {
        this.anagDato.setAdaCodDominio(codDominio);
    }

    @Override
    public String getCodDominioObj() {
        if (ws.getIndAnagDato().getCodDominio() >= 0) {
            return getCodDominio();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodDominioObj(String codDominioObj) {
        if (codDominioObj != null) {
            setCodDominio(codDominioObj);
            ws.getIndAnagDato().setTypeRecord(((short)0));
        }
        else {
            ws.getIndAnagDato().setTypeRecord(((short)-1));
        }
    }

    @Override
    public String getDescDatoVchar() {
        return anagDato.getAdaDescDatoVcharFormatted();
    }

    @Override
    public void setDescDatoVchar(String descDatoVchar) {
        this.anagDato.setAdaDescDatoVcharFormatted(descDatoVchar);
    }

    @Override
    public String getDescDatoVcharObj() {
        if (ws.getIndAnagDato().getDescDato() >= 0) {
            return getDescDatoVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDescDatoVcharObj(String descDatoVcharObj) {
        if (descDatoVcharObj != null) {
            setDescDatoVchar(descDatoVcharObj);
            ws.getIndAnagDato().setFlContiguousData(((short)0));
        }
        else {
            ws.getIndAnagDato().setFlContiguousData(((short)-1));
        }
    }

    @Override
    public String getDsUtente() {
        return anagDato.getAdaDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.anagDato.setAdaDsUtente(dsUtente);
    }

    @Override
    public String getFormattazioneDato() {
        return anagDato.getAdaFormattazioneDato();
    }

    @Override
    public void setFormattazioneDato(String formattazioneDato) {
        this.anagDato.setAdaFormattazioneDato(formattazioneDato);
    }

    @Override
    public String getFormattazioneDatoObj() {
        if (ws.getIndAnagDato().getFormattazioneDato() >= 0) {
            return getFormattazioneDato();
        }
        else {
            return null;
        }
    }

    @Override
    public void setFormattazioneDatoObj(String formattazioneDatoObj) {
        if (formattazioneDatoObj != null) {
            setFormattazioneDato(formattazioneDatoObj);
            ws.getIndAnagDato().setFormattazioneDato(((short)0));
        }
        else {
            ws.getIndAnagDato().setFormattazioneDato(((short)-1));
        }
    }

    @Override
    public int getLunghezzaDato() {
        return anagDato.getAdaLunghezzaDato().getAdaLunghezzaDato();
    }

    @Override
    public void setLunghezzaDato(int lunghezzaDato) {
        this.anagDato.getAdaLunghezzaDato().setAdaLunghezzaDato(lunghezzaDato);
    }

    @Override
    public Integer getLunghezzaDatoObj() {
        if (ws.getIndAnagDato().getLunghezzaDato() >= 0) {
            return ((Integer)getLunghezzaDato());
        }
        else {
            return null;
        }
    }

    @Override
    public void setLunghezzaDatoObj(Integer lunghezzaDatoObj) {
        if (lunghezzaDatoObj != null) {
            setLunghezzaDato(((int)lunghezzaDatoObj));
            ws.getIndAnagDato().setLunghezzaDato(((short)0));
        }
        else {
            ws.getIndAnagDato().setLunghezzaDato(((short)-1));
        }
    }

    @Override
    public short getPrecisioneDato() {
        return anagDato.getAdaPrecisioneDato().getAdaPrecisioneDato();
    }

    @Override
    public void setPrecisioneDato(short precisioneDato) {
        this.anagDato.getAdaPrecisioneDato().setAdaPrecisioneDato(precisioneDato);
    }

    @Override
    public Short getPrecisioneDatoObj() {
        if (ws.getIndAnagDato().getPrecisioneDato() >= 0) {
            return ((Short)getPrecisioneDato());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrecisioneDatoObj(Short precisioneDatoObj) {
        if (precisioneDatoObj != null) {
            setPrecisioneDato(((short)precisioneDatoObj));
            ws.getIndAnagDato().setPrecisioneDato(((short)0));
        }
        else {
            ws.getIndAnagDato().setPrecisioneDato(((short)-1));
        }
    }

    @Override
    public String getTipoDato() {
        return anagDato.getAdaTipoDato();
    }

    @Override
    public void setTipoDato(String tipoDato) {
        this.anagDato.setAdaTipoDato(tipoDato);
    }

    @Override
    public String getTipoDatoObj() {
        if (ws.getIndAnagDato().getTipoDato() >= 0) {
            return getTipoDato();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTipoDatoObj(String tipoDatoObj) {
        if (tipoDatoObj != null) {
            setTipoDato(tipoDatoObj);
            ws.getIndAnagDato().setTipoDato(((short)0));
        }
        else {
            ws.getIndAnagDato().setTipoDato(((short)-1));
        }
    }
}
