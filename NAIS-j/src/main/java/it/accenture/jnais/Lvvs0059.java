package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0059Data;

/**Original name: LVVS0059<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0059
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CONTROLLO SALTO RATA
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0059 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0059Data ws = new Lvvs0059Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0059
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0059_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0059 getInstance() {
        return ((Lvvs0059)Programs.getInstance(Lvvs0059.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: INITIALIZE LDBV2091.
        initLdbv2091();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: INITIALIZE WK-DATA-OUTPUT
        //                      WK-DATA-X-12.
        ws.setWkDataOutput(new AfDecimal(0, 11, 7));
        initWkDataX12();
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //             END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: EVALUATE DPOL-TP-LIV-GENZ-TIT
            //             WHEN 'PO'
            //               PERFORM S1250-CALCOLA-DATA1         THRU S1250-EX
            //             WHEN 'AD'
            //               PERFORM S1250-CALCOLA-DATA1         THRU S1250-EX
            //           END-EVALUATE
            switch (ws.getLccvpol1().getDati().getWpolTpLivGenzTit()) {

                case "PO":// COB_CODE: MOVE IVVC0213-ID-POLIZZA     TO LDBV2091-ID-OGG
                    ws.getLdbv2091().setIdOgg(ivvc0213.getIdPolizza());
                    // COB_CODE: MOVE 'PO'                    TO LDBV2091-TP-OGG
                    ws.getLdbv2091().setTpOgg("PO");
                    // COB_CODE: MOVE 'IN'                    TO LDBV2091-TP-STAT-TIT-01
                    ws.getLdbv2091().setTpStatTit01("IN");
                    // COB_CODE: PERFORM S1250-CALCOLA-DATA1         THRU S1250-EX
                    s1250CalcolaData1();
                    break;

                case "AD":// COB_CODE: MOVE IVVC0213-ID-ADESIONE    TO LDBV2091-ID-OGG
                    ws.getLdbv2091().setIdOgg(ivvc0213.getIdAdesione());
                    // COB_CODE: MOVE 'AD'                    TO LDBV2091-TP-OGG
                    ws.getLdbv2091().setTpOgg("AD");
                    // COB_CODE: MOVE 'IN'                    TO LDBV2091-TP-STAT-TIT-01
                    ws.getLdbv2091().setTpStatTit01("IN");
                    // COB_CODE: PERFORM S1250-CALCOLA-DATA1         THRU S1250-EX
                    s1250CalcolaData1();
                    break;

                default:break;
            }
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-POLI
        //                TO DPOL-AREA-POLIZZA
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasPoli())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DPOL-AREA-POLIZZA
            ws.setDpolAreaPolizzaFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1250-CALCOLA-DATA1<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA DATA 1
	 * ----------------------------------------------------------------*</pre>*/
    private void s1250CalcolaData1() {
        Ldbs2090 ldbs2090 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'LDBS2090'                     TO WK-CALL-PGM.
        ws.setWkCallPgm("LDBS2090");
        //
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 LDBV2091
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            ldbs2090 = Ldbs2090.getInstance();
            ldbs2090.run(idsv0003, ws.getLdbv2091());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBS2090 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS2090 ERRORE CHIAMATA - T0000-TRATTA-MATRICE");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: IF LDBV2091-TOT-PREMI NOT GREATER WS-TOT-RITENUTA
            //              MOVE LDBV2091-TOT-PREMI      TO IVVC0213-VAL-IMP-O
            //           ELSE
            //              MOVE WS-TOT-RITENUTA         TO IVVC0213-VAL-IMP-O
            //           END-IF
            if (ws.getLdbv2091().getTotPremi().compareTo(ws.getWsTotRitenuta()) <= 0) {
                // COB_CODE: MOVE LDBV2091-TOT-PREMI      TO IVVC0213-VAL-IMP-O
                ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getLdbv2091().getTotPremi(), 18, 7));
            }
            else {
                // COB_CODE: MOVE WS-TOT-RITENUTA         TO IVVC0213-VAL-IMP-O
                ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getWsTotRitenuta(), 18, 7));
            }
        }
        else {
            // COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LDBS2090 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS2090 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //           OR IDSV0003-SQLCODE = -305
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER            TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initLdbv2091() {
        ws.getLdbv2091().setIdOgg(0);
        ws.getLdbv2091().setTpOgg("");
        ws.getLdbv2091().setTpStatTit01("");
        ws.getLdbv2091().setTpStatTit02("");
        ws.getLdbv2091().setTpStatTit03("");
        ws.getLdbv2091().setTpStatTit04("");
        ws.getLdbv2091().setTpStatTit05("");
        ws.getLdbv2091().setTotPremi(new AfDecimal(0, 18, 3));
    }

    public void initWkDataX12() {
        ws.getWkDataX12().setxAa("");
        ws.getWkDataX12().setVirogla(Types.SPACE_CHAR);
        ws.getWkDataX12().setxGg("");
    }
}
