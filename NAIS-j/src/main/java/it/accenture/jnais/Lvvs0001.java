package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.GarIvvs0216;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.copy.ParamMovi;
import it.accenture.jnais.copy.TrchDiGarIvvs0216;
import it.accenture.jnais.copy.WpogDati;
import it.accenture.jnais.ws.AreeAppoggio;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0001Data;

/**Original name: LVVS0001<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0001
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... ESTRAZIONE VALORE FATTORE PARAMETRICO
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0001 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0001Data ws = new Lvvs0001Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0001
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0001_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //                  THRU EX-S1000
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc() && this.idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE
            //              THRU EX-S1000
            s1000Elaborazione();
        }
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0001 getInstance() {
        return ((Lvvs0001)Programs.getInstance(Lvvs0001.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                           IX-INDICI.
        initIxIndici();
        // COB_CODE: INITIALIZE                           AREE-APPOGGIO.
        initAreeAppoggio();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET WCOM-NO-TROVATO               TO TRUE.
        ws.getWcomFlagTrovato().setNoTrovato();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
        // COB_CODE: PERFORM S0005-CTRL-DATI-INPUT
        //                THRU EX-S0005.
        s0005CtrlDatiInput();
    }

    /**Original name: S0005-CTRL-DATI-INPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONTROLLO DATI INPUT
	 * ----------------------------------------------------------------*</pre>*/
    private void s0005CtrlDatiInput() {
        // COB_CODE:      IF IVVC0213-COD-PARAMETRO = SPACES OR LOW-VALUE
        //           *---    COD-PARAMETRO NON VALORIZZATO
        //                     TO IDSV0003-DESCRIZ-ERR-DB2
        //                END-IF.
        if (Characters.EQ_SPACE.test(ivvc0213.getCodParametro()) || Characters.EQ_LOW.test(ivvc0213.getCodParametroFormatted())) {
            //---    COD-PARAMETRO NON VALORIZZATO
            // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'COD-PARAMETRO NON VALORIZZATO'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("COD-PARAMETRO NON VALORIZZATO");
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE:         IF IVVC0213-ID-TRANCHE  > ZEROES AND
            //                      IVVC0213-ID-POLIZZA  = ZEROES AND
            //                      IVVC0213-ID-ADESIONE = ZEROES AND
            //                      IVVC0213-ID-GARANZIA = ZEROES
            //           *---       ID-POLIZZA ID-ADESIONE E ID-GARANZIA NON VALORIZZATI
            //                        TO IDSV0003-DESCRIZ-ERR-DB2
            //                   END-IF
            if (Characters.GT_ZERO.test(ivvc0213.getIdTrancheFormatted()) && Characters.EQ_ZERO.test(ivvc0213.getIdPolizzaFormatted()) && Characters.EQ_ZERO.test(ivvc0213.getIdAdesioneFormatted()) && Characters.EQ_ZERO.test(ivvc0213.getIdGaranziaFormatted())) {
                //---       ID-POLIZZA ID-ADESIONE E ID-GARANZIA NON VALORIZZATI
                // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'ID-POLIZZA ID-ADESIONE E ID-GARANZIA NON VALORIZZATI'
                //                 TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("ID-POLIZZA ID-ADESIONE E ID-GARANZIA NON VALORIZZATI");
            }
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE:         IF IVVC0213-ID-GARANZIA > ZEROES AND
            //                      IVVC0213-ID-POLIZZA  = ZEROES AND
            //                      IVVC0213-ID-ADESIONE = ZEROES
            //           *---       ID-POLIZZA E ID-ADESIONE NON VALORIZZATI
            //                        TO IDSV0003-DESCRIZ-ERR-DB2
            //                   END-IF
            if (Characters.GT_ZERO.test(ivvc0213.getIdGaranziaFormatted()) && Characters.EQ_ZERO.test(ivvc0213.getIdPolizzaFormatted()) && Characters.EQ_ZERO.test(ivvc0213.getIdAdesioneFormatted())) {
                //---       ID-POLIZZA E ID-ADESIONE NON VALORIZZATI
                // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'ID-POLIZZA E ID-ADESIONE NON VALORIZZATI'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("ID-POLIZZA E ID-ADESIONE NON VALORIZZATI");
            }
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE:         IF IVVC0213-ID-ADESIONE > ZEROES AND
            //                      IVVC0213-ID-POLIZZA  = ZEROES
            //           *---       ID-POLIZZA NON VALORIZZATO
            //                        TO IDSV0003-DESCRIZ-ERR-DB2
            //                   END-IF
            if (Characters.GT_ZERO.test(ivvc0213.getIdAdesioneFormatted()) && Characters.EQ_ZERO.test(ivvc0213.getIdPolizzaFormatted())) {
                //---       ID-POLIZZA NON VALORIZZATO
                // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'ID-POLIZZA NON VALORIZZATO'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("ID-POLIZZA NON VALORIZZATO");
            }
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE:         IF IVVC0213-ID-TRANCHE  = ZEROES AND
            //                      IVVC0213-ID-POLIZZA  = ZEROES AND
            //                      IVVC0213-ID-ADESIONE = ZEROES AND
            //                      IVVC0213-ID-GARANZIA = ZEROES
            //           *---       NESSUN ID VALORIZZATO
            //                        TO IDSV0003-DESCRIZ-ERR-DB2
            //                   END-IF
            if (Characters.EQ_ZERO.test(ivvc0213.getIdTrancheFormatted()) && Characters.EQ_ZERO.test(ivvc0213.getIdPolizzaFormatted()) && Characters.EQ_ZERO.test(ivvc0213.getIdAdesioneFormatted()) && Characters.EQ_ZERO.test(ivvc0213.getIdGaranziaFormatted())) {
                //---       NESSUN ID VALORIZZATO
                // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'NESSUN ID VALORIZZATO'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("NESSUN ID VALORIZZATO");
            }
        }
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 * --> RISPETTIVE AREE DCLGEN IN WORKING</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE
        ws.getIxIndici().setIxDclgen(((short)1));
        while (!(ws.getIxIndici().getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxIndici().getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxIndici().getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxIndici().getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.getIxIndici().setIxDclgen(Trunc.toShort(ws.getIxIndici().getIxDclgen() + 1, 4));
        }
        //--> ROUTINE PER CERCARE IL CODICE PARAMETRO SULLA DCLGEN
        //--> PARAMETRO OGGETTO PRESENTE A CONTESTO
        // COB_CODE: PERFORM S1200-CONTROLLO-DATI
        //              THRU S1200-CONTROLLO-DATI-EX
        //              VARYING IVVC0213-IX-TABB  FROM 1 BY 1
        //              UNTIL IVVC0213-IX-TABB > DPOG-ELE-PARAM-OGG-MAX
        //                 OR WCOM-SI-TROVATO.
        ivvc0213.setIxTabb(((short)1));
        while (!(ivvc0213.getIxTabb() > ws.getAreeAppoggio().getDpogEleParamOggMax() || ws.getWcomFlagTrovato().isSiTrovato())) {
            s1200ControlloDati();
            ivvc0213.setIxTabb(Trunc.toShort(ivvc0213.getIxTabb() + 1, 4));
        }
        // COB_CODE:      IF  IDSV0003-SUCCESSFUL-RC
        //                AND IDSV0003-SUCCESSFUL-SQL
        //                AND WCOM-NO-TROVATO
        //           *---     COD-PARAMETRO NON TROVATO
        //                      TO IDSV0003-DESCRIZ-ERR-DB2
        //                END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql() && ws.getWcomFlagTrovato().isNoTrovato()) {
            //---     COD-PARAMETRO NON TROVATO
            // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED   TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'COD-PARAMETRO NON TROVATO'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("COD-PARAMETRO NON TROVATO");
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-PARAM-OGG
        //                TO DPOG-AREA-PARAM-OGG
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxIndici().getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasParamOgg())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DPOG-AREA-PARAM-OGG
            ws.getAreeAppoggio().setDpogAreaParamOggFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxIndici().getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxIndici().getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxIndici().getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1200-CONTROLLO-DATI<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONTROLLO DATI
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200ControlloDati() {
        // COB_CODE: IF IVVC0213-COD-PARAMETRO = DPOG-COD-PARAM(IVVC0213-IX-TABB)
        //              END-IF
        //           ELSE
        //              SET WCOM-NO-TROVATO                   TO TRUE
        //           END-IF.
        if (Conditions.eq(ivvc0213.getCodParametro(), ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogCodParam())) {
            // COB_CODE: IF  DPOG-VAL-IMP(IVVC0213-IX-TABB) IS NUMERIC
            //               CONTINUE
            //           ELSE
            //               MOVE ZEROES TO DPOG-VAL-IMP(IVVC0213-IX-TABB)
            //           END-IF
            if (Functions.isNumber(ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValImp().getWpogValImp())) {
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: MOVE ZEROES TO DPOG-VAL-IMP(IVVC0213-IX-TABB)
                ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValImp().setWpogValImp(new AfDecimal(0, 15, 3));
            }
            // COB_CODE: IF  DPOG-VAL-PC (IVVC0213-IX-TABB) IS NUMERIC
            //               CONTINUE
            //           ELSE
            //               MOVE ZEROES TO DPOG-VAL-PC (IVVC0213-IX-TABB)
            //           END-IF
            if (Functions.isNumber(ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValPc().getWpogValPc())) {
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: MOVE ZEROES TO DPOG-VAL-PC (IVVC0213-IX-TABB)
                ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValPc().setWpogValPc(new AfDecimal(0, 14, 9));
            }
            // COB_CODE: IF  DPOG-VAL-IMP(IVVC0213-IX-TABB)  = ZEROES
            //           AND DPOG-VAL-PC (IVVC0213-IX-TABB)  = ZEROES
            //           AND DPOG-VAL-TXT(IVVC0213-IX-TABB)  = SPACES
            //                  THRU S1210-LEGGI-DATI-TAB-EX
            //           ELSE
            //               SET WCOM-SI-TROVATO               TO TRUE
            //           END-IF
            if (ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValImp().getWpogValImp().compareTo(0) == 0 && ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValPc().getWpogValPc().compareTo(0) == 0 && Characters.EQ_SPACE.test(ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValTxt())) {
                // COB_CODE: PERFORM S1210-LEGGI-DATI-TAB
                //              THRU S1210-LEGGI-DATI-TAB-EX
                s1210LeggiDatiTab();
            }
            else {
                // COB_CODE: PERFORM S1220-VALORIZZA-OUT
                //              THRU S1220-VALORIZZA-OUT-EX
                s1220ValorizzaOut();
                // COB_CODE: SET WCOM-SI-TROVATO               TO TRUE
                ws.getWcomFlagTrovato().setSiTrovato();
            }
        }
        else {
            // COB_CODE: SET WCOM-NO-TROVATO                   TO TRUE
            ws.getWcomFlagTrovato().setNoTrovato();
        }
        // COB_CODE: IF IVVC0213-COD-PARAMETRO = 'CEDOLAREG'
        //              COMPUTE IVVC0213-VAL-IMP-O = 360 / IVVC0213-VAL-IMP-O
        //           END-IF.
        if (Conditions.eq(ivvc0213.getCodParametro(), "CEDOLAREG")) {
            // COB_CODE: COMPUTE IVVC0213-VAL-IMP-O = 360 / IVVC0213-VAL-IMP-O
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(new AfDecimal((new AfDecimal(360, 10, 0).divide(ivvc0213.getTabOutput().getValImpO())), 17, 7), 18, 7));
        }
    }

    /**Original name: S1210-LEGGI-DATI-TAB<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA DATI DALLE TABELLE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1210LeggiDatiTab() {
        // COB_CODE:      EVALUATE IVVC0213-COD-PARAMETRO
        //                  WHEN 'IMPOARROT'
        //                  WHEN 'GRUPREDACAP'
        //           *-->       LETTURA DATI COLLETTIVA
        //                      END-IF
        //                  WHEN 'MODDURPRD'
        //           *-->       LETTURA ADESIONE
        //                      END-IF
        //                  WHEN 'NUMERORATE'
        //           *-->       LETTURA GARANZIA
        //                      END-IF
        //                  WHEN 'PERCREVEREND'
        //                  WHEN 'ANNIRENCERT'
        //           *-->       LETTURA PARAMETRO MOVIMENTO
        //                      END-IF
        //                  WHEN 'CARICOCONT'
        //           *-->       LETTURA TRANCHE DI GARANZIA
        //                      END-IF
        //                  WHEN 'LIQNOADEG'
        //                      CONTINUE
        //                  WHEN OTHER
        //                      SET  WCOM-NO-TROVATO   TO TRUE
        //                END-EVALUATE
        switch (ivvc0213.getCodParametro()) {

            case "IMPOARROT":
            case "GRUPREDACAP"://-->       LETTURA DATI COLLETTIVA
                // COB_CODE: PERFORM S1211-VALORIZZA-INPUT-DCO
                //              THRU S1211-VALORIZZA-INPUT-DCO-EX
                s1211ValorizzaInputDco();
                // COB_CODE: PERFORM LETTURA-DCO
                //              THRU LETTURA-DCO-EX
                letturaDco();
                // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
                //           AND IDSV0003-SUCCESSFUL-SQL
                //               SET  WCOM-SI-TROVATO   TO TRUE
                //           END-IF
                if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE DDCO-IMP-ARROT-PRE
                    //             TO DPOG-VAL-IMP(IVVC0213-IX-TABB)
                    ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValImp().setWpogValImp(Trunc.toDecimal(ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoImpArrotPre().getWdcoImpArrotPre(), 15, 3));
                    // COB_CODE: MOVE DDCO-FL-RICL-PRE-DA-CPT
                    //             TO DPOG-VAL-FL(IVVC0213-IX-TABB)
                    ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().setWpogValFl(ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoFlRiclPreDaCpt());
                    // COB_CODE: SET  WCOM-SI-TROVATO   TO TRUE
                    ws.getWcomFlagTrovato().setSiTrovato();
                }
                break;

            case "MODDURPRD"://-->       LETTURA ADESIONE
                // COB_CODE: PERFORM S1212-VALORIZZA-INPUT-ADE
                //              THRU S1212-VALORIZZA-INPUT-ADE-EX
                s1212ValorizzaInputAde();
                // COB_CODE: PERFORM LETTURA-ADE
                //              THRU LETTURA-ADE-EX
                letturaAde();
                // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
                //           AND IDSV0003-SUCCESSFUL-SQL
                //               SET  WCOM-SI-TROVATO   TO TRUE
                //           END-IF
                if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: SET  WCOM-SI-TROVATO   TO TRUE
                    ws.getWcomFlagTrovato().setSiTrovato();
                }
                break;

            case "NUMERORATE"://-->       LETTURA GARANZIA
                // COB_CODE: PERFORM S1213-VALORIZZA-INPUT-GRZ
                //              THRU S1213-VALORIZZA-INPUT-GRZ-EX
                s1213ValorizzaInputGrz();
                // COB_CODE: PERFORM LETTURA-GRZ
                //              THRU LETTURA-GRZ-EX
                letturaGrz();
                // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
                //           AND IDSV0003-SUCCESSFUL-SQL
                //               SET  WCOM-SI-TROVATO   TO TRUE
                //           END-IF
                if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE DGRZ-MM-1O-RAT(IVVC0213-IX-TABB)
                    //             TO DPOG-VAL-NUM(IVVC0213-IX-TABB)
                    ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValNum().setWpogValNum(ws.getAreeAppoggio().getDgrzTabGar(ivvc0213.getIxTabb()).getLccvgrz1().getDati().getWgrzMm1oRat().getWgrzMm1oRat());
                    // COB_CODE: SET  WCOM-SI-TROVATO   TO TRUE
                    ws.getWcomFlagTrovato().setSiTrovato();
                }
                break;

            case "PERCREVEREND":
            case "ANNIRENCERT"://-->       LETTURA PARAMETRO MOVIMENTO
                // COB_CODE: PERFORM S1214-VALORIZZA-INPUT-PMO
                //              THRU S1214-VALORIZZA-INPUT-PMO-EX
                s1214ValorizzaInputPmo();
                // COB_CODE: PERFORM LETTURA-PMO
                //              THRU LETTURA-PMO-EX
                letturaPmo();
                // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
                //           AND IDSV0003-SUCCESSFUL-SQL
                //               SET  WCOM-SI-TROVATO   TO TRUE
                //           END-IF
                if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE DPMO-PC-REVRSB(IVVC0213-IX-TABB)
                    //             TO DPOG-VAL-PC(IVVC0213-IX-TABB)
                    ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValPc().setWpogValPc(Trunc.toDecimal(ws.getAreeAppoggio().getDpmoTabParamMov(ivvc0213.getIxTabb()).getLccvpmo1().getDati().getWpmoPcRevrsb().getWpmoPcRevrsb(), 14, 9));
                    // COB_CODE: MOVE DPMO-AA-REN-CER(IVVC0213-IX-TABB)
                    //             TO DPOG-VAL-NUM(IVVC0213-IX-TABB)
                    ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValNum().setWpogValNum(ws.getAreeAppoggio().getDpmoTabParamMov(ivvc0213.getIxTabb()).getLccvpmo1().getDati().getWpmoAaRenCer().getWpmoAaRenCer());
                    // COB_CODE: SET  WCOM-SI-TROVATO   TO TRUE
                    ws.getWcomFlagTrovato().setSiTrovato();
                }
                break;

            case "CARICOCONT"://-->       LETTURA TRANCHE DI GARANZIA
                // COB_CODE: PERFORM S1215-VALORIZZA-INPUT-TGA
                //              THRU S1215-VALORIZZA-INPUT-TGA-EX
                s1215ValorizzaInputTga();
                // COB_CODE: PERFORM LETTURA-TGA
                //              THRU LETTURA-TGA-EX
                letturaTga();
                // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
                //           AND IDSV0003-SUCCESSFUL-SQL
                //               SET  WCOM-SI-TROVATO   TO TRUE
                //           END-IF
                if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE DTGA-FL-CAR-CONT(IVVC0213-IX-TABB)
                    //             TO DPOG-VAL-FL(IVVC0213-IX-TABB)
                    ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().setWpogValFl(ws.getAreeAppoggio().getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaFlCarCont());
                    // COB_CODE: SET  WCOM-SI-TROVATO   TO TRUE
                    ws.getWcomFlagTrovato().setSiTrovato();
                }
                break;

            case "LIQNOADEG":// COB_CODE: CONTINUE
            //continue
                break;

            default:// COB_CODE: SET  WCOM-NO-TROVATO   TO TRUE
                ws.getWcomFlagTrovato().setNoTrovato();
                break;
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //                  THRU S1220-VALORIZZA-OUT-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1220-VALORIZZA-OUT
            //              THRU S1220-VALORIZZA-OUT-EX
            s1220ValorizzaOut();
        }
    }

    /**Original name: S1211-VALORIZZA-INPUT-DCO<br>
	 * <pre>----------------------------------------------------------------*
	 *     PREPARA AREA PER LETTURA DATI COLLETTIVA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1211ValorizzaInputDco() {
        // COB_CODE: MOVE IVVC0213-ID-POLIZZA  TO DCO-ID-POLI.
        ws.getdColl().setDcoIdPoli(ivvc0213.getIdPolizza());
        // COB_CODE: MOVE ZERO                 TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                        IDSI0011-DATA-FINE-EFFETTO
        //                                        IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: SET  IDSI0011-TRATT-X-COMPETENZA  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattXCompetenza();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET  IDSI0011-SELECT      TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET  IDSI0011-ID-PADRE    TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011IdPadre();
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE 'D-COLL'             TO IDSI0011-CODICE-STR-DATO
        //                                        WKS-NOME-TABELLA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("D-COLL");
        ws.setWksNomeTabella("D-COLL");
        //--> DCLGEN TABELLA
        // COB_CODE: MOVE D-COLL               TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getdColl().getdCollFormatted());
    }

    /**Original name: S1212-VALORIZZA-INPUT-ADE<br>
	 * <pre>----------------------------------------------------------------*
	 *     PREPARA AREA PER LETTURA ADESIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1212ValorizzaInputAde() {
        // COB_CODE: MOVE IVVC0213-ID-POLIZZA      TO ADE-ID-POLI.
        ws.getAdes().setAdeIdPoli(ivvc0213.getIdPolizza());
        // COB_CODE: MOVE IVVC0213-ID-ADESIONE     TO ADE-ID-ADES.
        ws.getAdes().setAdeIdAdes(ivvc0213.getIdAdesione());
        //--> LA DATA EFFETTO VIENE SEMPRE VALORIZZATA
        // COB_CODE: MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: SET IDSI0011-TRATT-X-COMPETENZA  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattXCompetenza();
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE 'ADES'                   TO IDSI0011-CODICE-STR-DATO
        //                                            WKS-NOME-TABELLA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("ADES");
        ws.setWksNomeTabella("ADES");
        //--> DCLGEN TABELLA
        // COB_CODE: MOVE ADES                     TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getAdes().getAdesFormatted());
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSI0011-SELECT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET IDSI0011-ID               TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011Id();
    }

    /**Original name: S1213-VALORIZZA-INPUT-GRZ<br>
	 * <pre>----------------------------------------------------------------*
	 *     PREPARA AREA PER LETTURA GARANZIE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1213ValorizzaInputGrz() {
        // COB_CODE: MOVE IVVC0213-ID-GARANZIA    TO GRZ-ID-GAR.
        ws.getGar().setGrzIdGar(ivvc0213.getIdGaranzia());
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                           IDSI0011-DATA-FINE-EFFETTO
        //                                           IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: SET IDSI0011-TRATT-X-COMPETENZA TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattXCompetenza();
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE 'GAR'                   TO IDSI0011-CODICE-STR-DATO
        //                                           WKS-NOME-TABELLA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("GAR");
        ws.setWksNomeTabella("GAR");
        //--> DCLGEN TABELLA
        // COB_CODE: MOVE GAR                     TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getGar().getGarFormatted());
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSI0011-SELECT          TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET  IDSI0011-ID             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011Id();
    }

    /**Original name: S1214-VALORIZZA-INPUT-PMO<br>
	 * <pre>----------------------------------------------------------------*
	 *     PREPARA AREA PER LETTURA PARAMETRO MOVIMENTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1214ValorizzaInputPmo() {
        // COB_CODE: MOVE IVVC0213-ID-GARANZIA     TO PMO-ID-OGG.
        ws.getParamMovi().setPmoIdOgg(ivvc0213.getIdGaranzia());
        // COB_CODE: MOVE WS-GARANZIA              TO PMO-TP-OGG.
        ws.getParamMovi().setPmoTpOgg(ws.getWsTpOggetto().getGaranzia());
        // COB_CODE: MOVE IDSV0003-TIPO-MOVIMENTO  TO PMO-TP-MOVI.
        ws.getParamMovi().getPmoTpMovi().setPmoTpMovi(idsv0003.getTipoMovimento());
        //--> VALORIZZAZIONE DATE
        // COB_CODE: MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //--> VALORIZZAZIONE STORICITA'
        // COB_CODE: SET IDSI0011-TRATT-X-COMPETENZA  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattXCompetenza();
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE 'PARAM-MOVI'             TO IDSI0011-CODICE-STR-DATO
        //                                            WKS-NOME-TABELLA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("PARAM-MOVI");
        ws.setWksNomeTabella("PARAM-MOVI");
        //--> DCLGEN TABELLA
        // COB_CODE: MOVE PARAM-MOVI               TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getParamMovi().getParamMoviFormatted());
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSI0011-SELECT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET IDSI0011-ID-OGGETTO       TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011IdOggetto();
    }

    /**Original name: S1215-VALORIZZA-INPUT-TGA<br>
	 * <pre>----------------------------------------------------------------*
	 *     PREPARA AREA PER LETTURA TRANCHE DI GARANZIA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1215ValorizzaInputTga() {
        // COB_CODE: MOVE IVVC0213-ID-TRANCHE      TO TGA-ID-TRCH-DI-GAR.
        ws.getTrchDiGar().setTgaIdTrchDiGar(ivvc0213.getIdTranche());
        //--> VALORIZZAZIONE DATE
        // COB_CODE: MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: SET IDSI0011-TRATT-X-COMPETENZA  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattXCompetenza();
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE 'TRCH-DI-GAR'            TO IDSI0011-CODICE-STR-DATO
        //                                            WKS-NOME-TABELLA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("TRCH-DI-GAR");
        ws.setWksNomeTabella("TRCH-DI-GAR");
        //--> DCLGEN TABELLA
        // COB_CODE: MOVE TRCH-DI-GAR              TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getTrchDiGar().getTrchDiGarFormatted());
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSI0011-SELECT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET  IDSI0011-ID              TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011Id();
    }

    /**Original name: S1220-VALORIZZA-OUT<br>
	 * <pre>----------------------------------------------------------------*
	 *    VALORIZZA OUTPUT
	 * ----------------------------------------------------------------*</pre>*/
    private void s1220ValorizzaOut() {
        // COB_CODE: MOVE DPOG-TP-D(IVVC0213-IX-TABB)
        //             TO IVVC0213-TP-DATO-O.
        ivvc0213.getTabOutput().setIvvc0213TpDatoOFormatted(ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getDpogTpDFormatted());
        // COB_CODE: EVALUATE IVVC0213-TP-DATO-O
        //             WHEN WS-IMPORTO
        //               END-IF
        //             WHEN WS-NUMERO
        //               END-IF
        //             WHEN WS-TASSO
        //             WHEN WS-MILLESIMI
        //               END-IF
        //             WHEN WS-PERCENTUALE
        //               END-IF
        //             WHEN WS-DATA
        //               END-IF
        //             WHEN WS-STRINGA
        //               END-IF
        //             WHEN WS-FLAG
        //               END-IF
        //           END-EVALUATE.
        if (ivvc0213.getTabOutput().getTpDatoO() == ws.getWsTpDato().getImporto()) {
            // COB_CODE: IF DPOG-VAL-IMP-NULL(IVVC0213-IX-TABB) = HIGH-VALUE
            //                TO IVVC0213-VAL-IMP-O
            //           ELSE
            //                TO IVVC0213-VAL-IMP-O
            //           END-IF
            if (Characters.EQ_HIGH.test(ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValImp().getDpogValImpNullFormatted())) {
                // COB_CODE: MOVE ZEROES
                //             TO IVVC0213-VAL-IMP-O
                ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
            }
            else {
                // COB_CODE: MOVE DPOG-VAL-IMP(IVVC0213-IX-TABB)
                //             TO IVVC0213-VAL-IMP-O
                ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValImp().getWpogValImp(), 18, 7));
            }
        }
        else if (ivvc0213.getTabOutput().getTpDatoO() == ws.getWsTpDato().getNumero()) {
            // COB_CODE: IF DPOG-VAL-NUM-NULL(IVVC0213-IX-TABB) = HIGH-VALUE
            //                TO IVVC0213-VAL-IMP-O
            //           ELSE
            //                TO IVVC0213-VAL-IMP-O
            //           END-IF
            if (Characters.EQ_HIGH.test(ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValNum().getWpogValNumNullFormatted())) {
                // COB_CODE: MOVE ZEROES
                //             TO IVVC0213-VAL-IMP-O
                ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
            }
            else {
                // COB_CODE: MOVE DPOG-VAL-NUM(IVVC0213-IX-TABB)
                //             TO IVVC0213-VAL-IMP-O
                ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValNum().getWpogValNum(), 18, 7));
            }
        }
        else if ((ivvc0213.getTabOutput().getTpDatoO() == ws.getWsTpDato().getTasso()) || (ivvc0213.getTabOutput().getTpDatoO() == ws.getWsTpDato().getMillesimi())) {
            // COB_CODE: IF DPOG-VAL-TS-NULL(IVVC0213-IX-TABB) = HIGH-VALUE
            //                TO IVVC0213-VAL-PERC-O
            //           ELSE
            //                TO IVVC0213-VAL-PERC-O
            //           END-IF
            if (Characters.EQ_HIGH.test(ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValTs().getDpogValTsNullFormatted())) {
                // COB_CODE: MOVE ZEROES
                //             TO IVVC0213-VAL-PERC-O
                ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
            }
            else {
                // COB_CODE: MOVE DPOG-VAL-TS(IVVC0213-IX-TABB)
                //             TO IVVC0213-VAL-PERC-O
                ivvc0213.getTabOutput().setValPercO(TruncAbs.toDecimal(ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValTs().getWpogValTs(), 14, 9));
            }
        }
        else if (ivvc0213.getTabOutput().getTpDatoO() == ws.getWsTpDato().getPercentuale()) {
            // COB_CODE: IF DPOG-VAL-PC-NULL(IVVC0213-IX-TABB) = HIGH-VALUE
            //                TO IVVC0213-VAL-PERC-O
            //           ELSE
            //                TO IVVC0213-VAL-PERC-O
            //           END-IF
            if (Characters.EQ_HIGH.test(ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValPc().getDpogValPcNullFormatted())) {
                // COB_CODE: MOVE ZEROES
                //             TO IVVC0213-VAL-PERC-O
                ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
            }
            else {
                // COB_CODE: MOVE DPOG-VAL-PC(IVVC0213-IX-TABB)
                //             TO IVVC0213-VAL-PERC-O
                ivvc0213.getTabOutput().setValPercO(TruncAbs.toDecimal(ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValPc().getWpogValPc(), 14, 9));
            }
        }
        else if (ivvc0213.getTabOutput().getTpDatoO() == ws.getWsTpDato().getData2()) {
            // COB_CODE: IF DPOG-VAL-DT-NULL(IVVC0213-IX-TABB) = HIGH-VALUE
            //                TO IVVC0213-VAL-STR-O
            //           ELSE
            //                TO IVVC0213-VAL-STR-O
            //           END-IF
            if (Characters.EQ_HIGH.test(ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValDt().getDpogValDtNullFormatted())) {
                // COB_CODE: MOVE SPACES
                //             TO IVVC0213-VAL-STR-O
                ivvc0213.getTabOutput().setValStrO("");
            }
            else {
                // COB_CODE: MOVE DPOG-VAL-DT(IVVC0213-IX-TABB)
                //             TO IVVC0213-VAL-STR-O
                ivvc0213.getTabOutput().setValStrO(ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValDt().getDpogValDtFormatted());
            }
        }
        else if (ivvc0213.getTabOutput().getTpDatoO() == ws.getWsTpDato().getStringa()) {
            // COB_CODE: IF DPOG-VAL-TXT(IVVC0213-IX-TABB) = HIGH-VALUE
            //                TO IVVC0213-VAL-STR-O
            //           ELSE
            //                TO IVVC0213-VAL-STR-O
            //           END-IF
            if (Characters.EQ_HIGH.test(ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValTxt(), WpogDati.Len.WPOG_VAL_TXT)) {
                // COB_CODE: MOVE SPACES
                //             TO IVVC0213-VAL-STR-O
                ivvc0213.getTabOutput().setValStrO("");
            }
            else {
                // COB_CODE: MOVE DPOG-VAL-TXT(IVVC0213-IX-TABB)
                //             TO IVVC0213-VAL-STR-O
                ivvc0213.getTabOutput().setValStrO(ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValTxt());
            }
        }
        else if (ivvc0213.getTabOutput().getTpDatoO() == ws.getWsTpDato().getFlag()) {
            // COB_CODE: IF DPOG-VAL-FL(IVVC0213-IX-TABB) = HIGH-VALUE
            //                TO IVVC0213-VAL-STR-O
            //           ELSE
            //                TO IVVC0213-VAL-STR-O
            //           END-IF
            if (Conditions.eq(ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValFl(), Types.HIGH_CHAR_VAL)) {
                // COB_CODE: MOVE SPACES
                //             TO IVVC0213-VAL-STR-O
                ivvc0213.getTabOutput().setValStrO("");
            }
            else {
                // COB_CODE: MOVE DPOG-VAL-FL(IVVC0213-IX-TABB)
                //             TO IVVC0213-VAL-STR-O
                ivvc0213.getTabOutput().setValStrO(String.valueOf(ws.getAreeAppoggio().getDpogTabParamOgg(ivvc0213.getIxTabb()).getLccvpog1().getDati().getWpogValFl()));
            }
        }
    }

    /**Original name: LETTURA-DCO<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA SULLA TABELLA DATI COLLETTIVA
	 * ----------------------------------------------------------------*</pre>*/
    private void letturaDco() {
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    GESTIRE ERRORE DISPATCHER
        //                   END-STRING
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                      WHEN IDSO0011-SUCCESSFUL-SQL
            //           *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
            //                              THRU VALORIZZA-OUTPUT-DCO-EX
            //                      WHEN IDSO0011-NOT-FOUND
            //           *--->      CHIAVE NON TROVATA
            //                           END-STRING
            //                      WHEN OTHER
            //           *--->      ERRORE DI ACCESSO AL DB
            //                           END-STRING
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO D-COLL
                    ws.getdColl().setdCollFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: PERFORM VALORIZZA-OUTPUT-DCO
                    //              THRU VALORIZZA-OUTPUT-DCO-EX
                    valorizzaOutputDco();
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND://--->      CHIAVE NON TROVATA
                    // COB_CODE: SET IDSV0003-NOT-FOUND   TO TRUE
                    idsv0003.getSqlcode().setNotFound();
                    // COB_CODE: MOVE WK-PGM
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: STRING WKS-NOME-TABELLA      ';'
                    //                  IDSO0011-RETURN-CODE  ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE
                    //           INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, ws.getWksNomeTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    break;

                default://--->      ERRORE DI ACCESSO AL DB
                    // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                    // COB_CODE: MOVE WK-PGM
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: STRING WKS-NOME-TABELLA      ';'
                    //                  IDSO0011-RETURN-CODE  ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE
                    //           INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, ws.getWksNomeTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    break;
            }
        }
        else {
            //-->    GESTIRE ERRORE DISPATCHER
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING WKS-NOME-TABELLA      ';'
            //                  IDSO0011-RETURN-CODE  ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE
            //           INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, ws.getWksNomeTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
    }

    /**Original name: LETTURA-ADE<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA SULLA TABELLA ADESIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void letturaAde() {
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    GESTIRE ERRORE DISPATCHER
        //                   END-STRING
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *--->       OPERAZIONE ESEGUITA CORRETTAMENTE
            //                               THRU VALORIZZA-OUTPUT-ADE-EX
            //                      WHEN IDSO0011-NOT-FOUND
            //           *--->      CHIAVE NON TROVATA
            //                           END-STRING
            //                      WHEN OTHER
            //           *--->      ERRORE DI ACCESSO AL DB
            //                           END-STRING
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://--->       OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI   TO ADES
                    ws.getAdes().setAdesFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE 1                      TO IX-TAB-ADE
                    ws.getIxIndici().setIxTabAde(((short)1));
                    // COB_CODE: PERFORM VALORIZZA-OUTPUT-ADE
                    //              THRU VALORIZZA-OUTPUT-ADE-EX
                    //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LVVS0001.cbl:line=870, because the code is unreachable.
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND://--->      CHIAVE NON TROVATA
                    // COB_CODE: SET IDSV0003-NOT-FOUND   TO TRUE
                    idsv0003.getSqlcode().setNotFound();
                    // COB_CODE: MOVE WK-PGM
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: STRING WKS-NOME-TABELLA      ';'
                    //                  IDSO0011-RETURN-CODE  ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE
                    //           INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, ws.getWksNomeTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    break;

                default://--->      ERRORE DI ACCESSO AL DB
                    // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                    // COB_CODE: MOVE WK-PGM
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: STRING WKS-NOME-TABELLA      ';'
                    //                  IDSO0011-RETURN-CODE  ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE
                    //           INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, ws.getWksNomeTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    break;
            }
        }
        else {
            //-->    GESTIRE ERRORE DISPATCHER
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING WKS-NOME-TABELLA      ';'
            //                  IDSO0011-RETURN-CODE  ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE
            //           INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, ws.getWksNomeTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
    }

    /**Original name: LETTURA-PMO<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA SULLA TABELLA PARAMETRO MOVIMENTO
	 * ----------------------------------------------------------------*</pre>*/
    private void letturaPmo() {
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    GESTIRE ERRORE DISPATCHER
        //                   END-STRING
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
            //                             THRU VALORIZZA-OUTPUT-PMO-EX
            //                      WHEN IDSO0011-NOT-FOUND
            //           *--->      CHIAVE NON TROVATA
            //                           END-STRING
            //                      WHEN OTHER
            //           *--->      ERRORE DI ACCESSO AL DB
            //                           END-STRING
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI TO PARAM-MOVI
                    ws.getParamMovi().setParamMoviFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE 1                    TO IX-TAB-PMO
                    ws.getIxIndici().setIxTabPmo(((short)1));
                    // COB_CODE: PERFORM VALORIZZA-OUTPUT-PMO
                    //              THRU VALORIZZA-OUTPUT-PMO-EX
                    valorizzaOutputPmo();
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND://--->      CHIAVE NON TROVATA
                    // COB_CODE: SET IDSV0003-NOT-FOUND   TO TRUE
                    idsv0003.getSqlcode().setNotFound();
                    // COB_CODE: MOVE WK-PGM
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: STRING WKS-NOME-TABELLA      ';'
                    //                  IDSO0011-RETURN-CODE  ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE
                    //           INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, ws.getWksNomeTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    break;

                default://--->      ERRORE DI ACCESSO AL DB
                    // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                    // COB_CODE: MOVE WK-PGM
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: STRING WKS-NOME-TABELLA      ';'
                    //                  IDSO0011-RETURN-CODE  ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE
                    //           INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, ws.getWksNomeTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    break;
            }
        }
        else {
            //-->    GESTIRE ERRORE DISPATCHER
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING WKS-NOME-TABELLA      ';'
            //                  IDSO0011-RETURN-CODE  ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE
            //           INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, ws.getWksNomeTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
    }

    /**Original name: LETTURA-GRZ<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA SULLA TABELLA GARANZIA
	 * ----------------------------------------------------------------*</pre>*/
    private void letturaGrz() {
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    GESTIRE ERRORE DISPATCHER
        //                   END-STRING
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                      WHEN IDSO0011-SUCCESSFUL-SQL
            //           *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
            //                              THRU VALORIZZA-OUTPUT-GRZ-EX
            //                      WHEN IDSO0011-NOT-FOUND
            //           *--->      CHIAVE NON TROVATA
            //                           END-STRING
            //                      WHEN OTHER
            //           *--->      ERRORE DI ACCESSO AL DB
            //                           END-STRING
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI TO GAR
                    ws.getGar().setGarFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE 1                    TO IX-TAB-GRZ
                    ws.getIxIndici().setIxTabGrz(((short)1));
                    // COB_CODE: PERFORM VALORIZZA-OUTPUT-GRZ
                    //              THRU VALORIZZA-OUTPUT-GRZ-EX
                    valorizzaOutputGrz();
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND://--->      CHIAVE NON TROVATA
                    // COB_CODE: SET IDSV0003-NOT-FOUND   TO TRUE
                    idsv0003.getSqlcode().setNotFound();
                    // COB_CODE: MOVE WK-PGM
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: STRING WKS-NOME-TABELLA      ';'
                    //                  IDSO0011-RETURN-CODE  ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE
                    //           INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, ws.getWksNomeTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    break;

                default://--->      ERRORE DI ACCESSO AL DB
                    // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                    // COB_CODE: MOVE WK-PGM
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: STRING WKS-NOME-TABELLA      ';'
                    //                  IDSO0011-RETURN-CODE  ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE
                    //           INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, ws.getWksNomeTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    break;
            }
        }
        else {
            //-->    GESTIRE ERRORE DISPATCHER
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING WKS-NOME-TABELLA      ';'
            //                  IDSO0011-RETURN-CODE  ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE
            //           INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, ws.getWksNomeTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
    }

    /**Original name: LETTURA-TGA<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA SULLA TABELLA TRANCHE DI GARANZIA
	 * ----------------------------------------------------------------*</pre>*/
    private void letturaTga() {
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    GESTIRE ERRORE DISPATCHER
        //                   END-STRING
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                      WHEN IDSO0011-SUCCESSFUL-SQL
            //           *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
            //                              THRU VALORIZZA-OUTPUT-TGA-EX
            //                      WHEN IDSO0011-NOT-FOUND
            //           *--->      CHIAVE NON TROVATA
            //                           END-STRING
            //                      WHEN OTHER
            //           *--->      ERRORE DI ACCESSO AL DB
            //                           END-STRING
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI   TO TRCH-DI-GAR
                    ws.getTrchDiGar().setTrchDiGarFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE 1                      TO IX-TAB-TGA
                    ws.getIxIndici().setIxTabTga(((short)1));
                    // COB_CODE: PERFORM VALORIZZA-OUTPUT-TGA
                    //              THRU VALORIZZA-OUTPUT-TGA-EX
                    valorizzaOutputTga();
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND://--->      CHIAVE NON TROVATA
                    // COB_CODE: SET IDSV0003-NOT-FOUND   TO TRUE
                    idsv0003.getSqlcode().setNotFound();
                    // COB_CODE: MOVE WK-PGM
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: STRING WKS-NOME-TABELLA      ';'
                    //                  IDSO0011-RETURN-CODE  ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE
                    //           INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, ws.getWksNomeTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    break;

                default://--->      ERRORE DI ACCESSO AL DB
                    // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                    // COB_CODE: MOVE WK-PGM
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: STRING WKS-NOME-TABELLA      ';'
                    //                  IDSO0011-RETURN-CODE  ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE
                    //           INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, ws.getWksNomeTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    break;
            }
        }
        else {
            //-->    GESTIRE ERRORE DISPATCHER
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING WKS-NOME-TABELLA      ';'
            //                  IDSO0011-RETURN-CODE  ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE
            //           INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, ws.getWksNomeTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: IF NOT IDSV0003-SUCCESSFUL-SQL
        //              SET IDSV0003-FIELD-NOT-VALUED   TO TRUE
        //           END-IF.
        if (!idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED   TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
        }
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: VALORIZZA-OUTPUT-DCO<br>
	 * <pre>--> DATI COLLETTIVA
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVDCO3
	 *    ULTIMO AGG. 04 SET 2008
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputDco() {
        // COB_CODE: MOVE DCO-ID-D-COLL
        //             TO (SF)-ID-PTF
        ws.getAreeAppoggio().getLccvdco1().setIdPtf(ws.getdColl().getDcoIdDColl());
        // COB_CODE: MOVE DCO-ID-D-COLL
        //             TO (SF)-ID-D-COLL
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoIdDColl(ws.getdColl().getDcoIdDColl());
        // COB_CODE: MOVE DCO-ID-POLI
        //             TO (SF)-ID-POLI
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoIdPoli(ws.getdColl().getDcoIdPoli());
        // COB_CODE: MOVE DCO-ID-MOVI-CRZ
        //             TO (SF)-ID-MOVI-CRZ
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoIdMoviCrz(ws.getdColl().getDcoIdMoviCrz());
        // COB_CODE: IF DCO-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //                TO (SF)-ID-MOVI-CHIU-NULL
        //           ELSE
        //                TO (SF)-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdColl().getDcoIdMoviChiu().getDcoIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE DCO-ID-MOVI-CHIU-NULL
            //             TO (SF)-ID-MOVI-CHIU-NULL
            ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoIdMoviChiu().setWdcoIdMoviChiuNull(ws.getdColl().getDcoIdMoviChiu().getDcoIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE DCO-ID-MOVI-CHIU
            //             TO (SF)-ID-MOVI-CHIU
            ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoIdMoviChiu().setWdcoIdMoviChiu(ws.getdColl().getDcoIdMoviChiu().getDcoIdMoviChiu());
        }
        // COB_CODE: MOVE DCO-DT-INI-EFF
        //             TO (SF)-DT-INI-EFF
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoDtIniEff(ws.getdColl().getDcoDtIniEff());
        // COB_CODE: MOVE DCO-DT-END-EFF
        //             TO (SF)-DT-END-EFF
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoDtEndEff(ws.getdColl().getDcoDtEndEff());
        // COB_CODE: MOVE DCO-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoCodCompAnia(ws.getdColl().getDcoCodCompAnia());
        // COB_CODE: IF DCO-IMP-ARROT-PRE-NULL = HIGH-VALUES
        //                TO (SF)-IMP-ARROT-PRE-NULL
        //           ELSE
        //                TO (SF)-IMP-ARROT-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdColl().getDcoImpArrotPre().getDcoImpArrotPreNullFormatted())) {
            // COB_CODE: MOVE DCO-IMP-ARROT-PRE-NULL
            //             TO (SF)-IMP-ARROT-PRE-NULL
            ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoImpArrotPre().setWdcoImpArrotPreNull(ws.getdColl().getDcoImpArrotPre().getDcoImpArrotPreNull());
        }
        else {
            // COB_CODE: MOVE DCO-IMP-ARROT-PRE
            //             TO (SF)-IMP-ARROT-PRE
            ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoImpArrotPre().setWdcoImpArrotPre(Trunc.toDecimal(ws.getdColl().getDcoImpArrotPre().getDcoImpArrotPre(), 15, 3));
        }
        // COB_CODE: IF DCO-PC-SCON-NULL = HIGH-VALUES
        //                TO (SF)-PC-SCON-NULL
        //           ELSE
        //                TO (SF)-PC-SCON
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdColl().getDcoPcScon().getDcoPcSconNullFormatted())) {
            // COB_CODE: MOVE DCO-PC-SCON-NULL
            //             TO (SF)-PC-SCON-NULL
            ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoPcScon().setWdcoPcSconNull(ws.getdColl().getDcoPcScon().getDcoPcSconNull());
        }
        else {
            // COB_CODE: MOVE DCO-PC-SCON
            //             TO (SF)-PC-SCON
            ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoPcScon().setWdcoPcScon(Trunc.toDecimal(ws.getdColl().getDcoPcScon().getDcoPcScon(), 6, 3));
        }
        // COB_CODE: IF DCO-FL-ADES-SING-NULL = HIGH-VALUES
        //                TO (SF)-FL-ADES-SING-NULL
        //           ELSE
        //                TO (SF)-FL-ADES-SING
        //           END-IF
        if (Conditions.eq(ws.getdColl().getDcoFlAdesSing(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE DCO-FL-ADES-SING-NULL
            //             TO (SF)-FL-ADES-SING-NULL
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoFlAdesSing(ws.getdColl().getDcoFlAdesSing());
        }
        else {
            // COB_CODE: MOVE DCO-FL-ADES-SING
            //             TO (SF)-FL-ADES-SING
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoFlAdesSing(ws.getdColl().getDcoFlAdesSing());
        }
        // COB_CODE: IF DCO-TP-IMP-NULL = HIGH-VALUES
        //                TO (SF)-TP-IMP-NULL
        //           ELSE
        //                TO (SF)-TP-IMP
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdColl().getDcoTpImpFormatted())) {
            // COB_CODE: MOVE DCO-TP-IMP-NULL
            //             TO (SF)-TP-IMP-NULL
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoTpImp(ws.getdColl().getDcoTpImp());
        }
        else {
            // COB_CODE: MOVE DCO-TP-IMP
            //             TO (SF)-TP-IMP
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoTpImp(ws.getdColl().getDcoTpImp());
        }
        // COB_CODE: IF DCO-FL-RICL-PRE-DA-CPT-NULL = HIGH-VALUES
        //                TO (SF)-FL-RICL-PRE-DA-CPT-NULL
        //           ELSE
        //                TO (SF)-FL-RICL-PRE-DA-CPT
        //           END-IF
        if (Conditions.eq(ws.getdColl().getDcoFlRiclPreDaCpt(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE DCO-FL-RICL-PRE-DA-CPT-NULL
            //             TO (SF)-FL-RICL-PRE-DA-CPT-NULL
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoFlRiclPreDaCpt(ws.getdColl().getDcoFlRiclPreDaCpt());
        }
        else {
            // COB_CODE: MOVE DCO-FL-RICL-PRE-DA-CPT
            //             TO (SF)-FL-RICL-PRE-DA-CPT
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoFlRiclPreDaCpt(ws.getdColl().getDcoFlRiclPreDaCpt());
        }
        // COB_CODE: IF DCO-TP-ADES-NULL = HIGH-VALUES
        //                TO (SF)-TP-ADES-NULL
        //           ELSE
        //                TO (SF)-TP-ADES
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdColl().getDcoTpAdesFormatted())) {
            // COB_CODE: MOVE DCO-TP-ADES-NULL
            //             TO (SF)-TP-ADES-NULL
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoTpAdes(ws.getdColl().getDcoTpAdes());
        }
        else {
            // COB_CODE: MOVE DCO-TP-ADES
            //             TO (SF)-TP-ADES
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoTpAdes(ws.getdColl().getDcoTpAdes());
        }
        // COB_CODE: IF DCO-DT-ULT-RINN-TAC-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-RINN-TAC-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-RINN-TAC
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdColl().getDcoDtUltRinnTac().getDcoDtUltRinnTacNullFormatted())) {
            // COB_CODE: MOVE DCO-DT-ULT-RINN-TAC-NULL
            //             TO (SF)-DT-ULT-RINN-TAC-NULL
            ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoDtUltRinnTac().setWdcoDtUltRinnTacNull(ws.getdColl().getDcoDtUltRinnTac().getDcoDtUltRinnTacNull());
        }
        else {
            // COB_CODE: MOVE DCO-DT-ULT-RINN-TAC
            //             TO (SF)-DT-ULT-RINN-TAC
            ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoDtUltRinnTac().setWdcoDtUltRinnTac(ws.getdColl().getDcoDtUltRinnTac().getDcoDtUltRinnTac());
        }
        // COB_CODE: IF DCO-IMP-SCON-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SCON-NULL
        //           ELSE
        //                TO (SF)-IMP-SCON
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdColl().getDcoImpScon().getDcoImpSconNullFormatted())) {
            // COB_CODE: MOVE DCO-IMP-SCON-NULL
            //             TO (SF)-IMP-SCON-NULL
            ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoImpScon().setWdcoImpSconNull(ws.getdColl().getDcoImpScon().getDcoImpSconNull());
        }
        else {
            // COB_CODE: MOVE DCO-IMP-SCON
            //             TO (SF)-IMP-SCON
            ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoImpScon().setWdcoImpScon(Trunc.toDecimal(ws.getdColl().getDcoImpScon().getDcoImpScon(), 15, 3));
        }
        // COB_CODE: IF DCO-FRAZ-DFLT-NULL = HIGH-VALUES
        //                TO (SF)-FRAZ-DFLT-NULL
        //           ELSE
        //                TO (SF)-FRAZ-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdColl().getDcoFrazDflt().getDcoFrazDfltNullFormatted())) {
            // COB_CODE: MOVE DCO-FRAZ-DFLT-NULL
            //             TO (SF)-FRAZ-DFLT-NULL
            ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoFrazDflt().setWdcoFrazDfltNull(ws.getdColl().getDcoFrazDflt().getDcoFrazDfltNull());
        }
        else {
            // COB_CODE: MOVE DCO-FRAZ-DFLT
            //             TO (SF)-FRAZ-DFLT
            ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoFrazDflt().setWdcoFrazDflt(ws.getdColl().getDcoFrazDflt().getDcoFrazDflt());
        }
        // COB_CODE: IF DCO-ETA-SCAD-MASC-DFLT-NULL = HIGH-VALUES
        //                TO (SF)-ETA-SCAD-MASC-DFLT-NULL
        //           ELSE
        //                TO (SF)-ETA-SCAD-MASC-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdColl().getDcoEtaScadMascDflt().getDcoEtaScadMascDfltNullFormatted())) {
            // COB_CODE: MOVE DCO-ETA-SCAD-MASC-DFLT-NULL
            //             TO (SF)-ETA-SCAD-MASC-DFLT-NULL
            ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoEtaScadMascDflt().setWdcoEtaScadMascDfltNull(ws.getdColl().getDcoEtaScadMascDflt().getDcoEtaScadMascDfltNull());
        }
        else {
            // COB_CODE: MOVE DCO-ETA-SCAD-MASC-DFLT
            //             TO (SF)-ETA-SCAD-MASC-DFLT
            ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoEtaScadMascDflt().setWdcoEtaScadMascDflt(ws.getdColl().getDcoEtaScadMascDflt().getDcoEtaScadMascDflt());
        }
        // COB_CODE: IF DCO-ETA-SCAD-FEMM-DFLT-NULL = HIGH-VALUES
        //                TO (SF)-ETA-SCAD-FEMM-DFLT-NULL
        //           ELSE
        //                TO (SF)-ETA-SCAD-FEMM-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdColl().getDcoEtaScadFemmDflt().getDcoEtaScadFemmDfltNullFormatted())) {
            // COB_CODE: MOVE DCO-ETA-SCAD-FEMM-DFLT-NULL
            //             TO (SF)-ETA-SCAD-FEMM-DFLT-NULL
            ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoEtaScadFemmDflt().setWdcoEtaScadFemmDfltNull(ws.getdColl().getDcoEtaScadFemmDflt().getDcoEtaScadFemmDfltNull());
        }
        else {
            // COB_CODE: MOVE DCO-ETA-SCAD-FEMM-DFLT
            //             TO (SF)-ETA-SCAD-FEMM-DFLT
            ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoEtaScadFemmDflt().setWdcoEtaScadFemmDflt(ws.getdColl().getDcoEtaScadFemmDflt().getDcoEtaScadFemmDflt());
        }
        // COB_CODE: IF DCO-TP-DFLT-DUR-NULL = HIGH-VALUES
        //                TO (SF)-TP-DFLT-DUR-NULL
        //           ELSE
        //                TO (SF)-TP-DFLT-DUR
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdColl().getDcoTpDfltDurFormatted())) {
            // COB_CODE: MOVE DCO-TP-DFLT-DUR-NULL
            //             TO (SF)-TP-DFLT-DUR-NULL
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoTpDfltDur(ws.getdColl().getDcoTpDfltDur());
        }
        else {
            // COB_CODE: MOVE DCO-TP-DFLT-DUR
            //             TO (SF)-TP-DFLT-DUR
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoTpDfltDur(ws.getdColl().getDcoTpDfltDur());
        }
        // COB_CODE: IF DCO-DUR-AA-ADES-DFLT-NULL = HIGH-VALUES
        //                TO (SF)-DUR-AA-ADES-DFLT-NULL
        //           ELSE
        //                TO (SF)-DUR-AA-ADES-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdColl().getDcoDurAaAdesDflt().getDcoDurAaAdesDfltNullFormatted())) {
            // COB_CODE: MOVE DCO-DUR-AA-ADES-DFLT-NULL
            //             TO (SF)-DUR-AA-ADES-DFLT-NULL
            ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoDurAaAdesDflt().setWdcoDurAaAdesDfltNull(ws.getdColl().getDcoDurAaAdesDflt().getDcoDurAaAdesDfltNull());
        }
        else {
            // COB_CODE: MOVE DCO-DUR-AA-ADES-DFLT
            //             TO (SF)-DUR-AA-ADES-DFLT
            ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoDurAaAdesDflt().setWdcoDurAaAdesDflt(ws.getdColl().getDcoDurAaAdesDflt().getDcoDurAaAdesDflt());
        }
        // COB_CODE: IF DCO-DUR-MM-ADES-DFLT-NULL = HIGH-VALUES
        //                TO (SF)-DUR-MM-ADES-DFLT-NULL
        //           ELSE
        //                TO (SF)-DUR-MM-ADES-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdColl().getDcoDurMmAdesDflt().getDcoDurMmAdesDfltNullFormatted())) {
            // COB_CODE: MOVE DCO-DUR-MM-ADES-DFLT-NULL
            //             TO (SF)-DUR-MM-ADES-DFLT-NULL
            ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoDurMmAdesDflt().setWdcoDurMmAdesDfltNull(ws.getdColl().getDcoDurMmAdesDflt().getDcoDurMmAdesDfltNull());
        }
        else {
            // COB_CODE: MOVE DCO-DUR-MM-ADES-DFLT
            //             TO (SF)-DUR-MM-ADES-DFLT
            ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoDurMmAdesDflt().setWdcoDurMmAdesDflt(ws.getdColl().getDcoDurMmAdesDflt().getDcoDurMmAdesDflt());
        }
        // COB_CODE: IF DCO-DUR-GG-ADES-DFLT-NULL = HIGH-VALUES
        //                TO (SF)-DUR-GG-ADES-DFLT-NULL
        //           ELSE
        //                TO (SF)-DUR-GG-ADES-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdColl().getDcoDurGgAdesDflt().getDcoDurGgAdesDfltNullFormatted())) {
            // COB_CODE: MOVE DCO-DUR-GG-ADES-DFLT-NULL
            //             TO (SF)-DUR-GG-ADES-DFLT-NULL
            ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoDurGgAdesDflt().setWdcoDurGgAdesDfltNull(ws.getdColl().getDcoDurGgAdesDflt().getDcoDurGgAdesDfltNull());
        }
        else {
            // COB_CODE: MOVE DCO-DUR-GG-ADES-DFLT
            //             TO (SF)-DUR-GG-ADES-DFLT
            ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoDurGgAdesDflt().setWdcoDurGgAdesDflt(ws.getdColl().getDcoDurGgAdesDflt().getDcoDurGgAdesDflt());
        }
        // COB_CODE: IF DCO-DT-SCAD-ADES-DFLT-NULL = HIGH-VALUES
        //                TO (SF)-DT-SCAD-ADES-DFLT-NULL
        //           ELSE
        //                TO (SF)-DT-SCAD-ADES-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdColl().getDcoDtScadAdesDflt().getDcoDtScadAdesDfltNullFormatted())) {
            // COB_CODE: MOVE DCO-DT-SCAD-ADES-DFLT-NULL
            //             TO (SF)-DT-SCAD-ADES-DFLT-NULL
            ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoDtScadAdesDflt().setWdcoDtScadAdesDfltNull(ws.getdColl().getDcoDtScadAdesDflt().getDcoDtScadAdesDfltNull());
        }
        else {
            // COB_CODE: MOVE DCO-DT-SCAD-ADES-DFLT
            //             TO (SF)-DT-SCAD-ADES-DFLT
            ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoDtScadAdesDflt().setWdcoDtScadAdesDflt(ws.getdColl().getDcoDtScadAdesDflt().getDcoDtScadAdesDflt());
        }
        // COB_CODE: IF DCO-COD-FND-DFLT-NULL = HIGH-VALUES
        //                TO (SF)-COD-FND-DFLT-NULL
        //           ELSE
        //                TO (SF)-COD-FND-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdColl().getDcoCodFndDfltFormatted())) {
            // COB_CODE: MOVE DCO-COD-FND-DFLT-NULL
            //             TO (SF)-COD-FND-DFLT-NULL
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoCodFndDflt(ws.getdColl().getDcoCodFndDflt());
        }
        else {
            // COB_CODE: MOVE DCO-COD-FND-DFLT
            //             TO (SF)-COD-FND-DFLT
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoCodFndDflt(ws.getdColl().getDcoCodFndDflt());
        }
        // COB_CODE: IF DCO-TP-DUR-NULL = HIGH-VALUES
        //                TO (SF)-TP-DUR-NULL
        //           ELSE
        //                TO (SF)-TP-DUR
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdColl().getDcoTpDurFormatted())) {
            // COB_CODE: MOVE DCO-TP-DUR-NULL
            //             TO (SF)-TP-DUR-NULL
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoTpDur(ws.getdColl().getDcoTpDur());
        }
        else {
            // COB_CODE: MOVE DCO-TP-DUR
            //             TO (SF)-TP-DUR
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoTpDur(ws.getdColl().getDcoTpDur());
        }
        // COB_CODE: IF DCO-TP-CALC-DUR-NULL = HIGH-VALUES
        //                TO (SF)-TP-CALC-DUR-NULL
        //           ELSE
        //                TO (SF)-TP-CALC-DUR
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdColl().getDcoTpCalcDurFormatted())) {
            // COB_CODE: MOVE DCO-TP-CALC-DUR-NULL
            //             TO (SF)-TP-CALC-DUR-NULL
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoTpCalcDur(ws.getdColl().getDcoTpCalcDur());
        }
        else {
            // COB_CODE: MOVE DCO-TP-CALC-DUR
            //             TO (SF)-TP-CALC-DUR
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoTpCalcDur(ws.getdColl().getDcoTpCalcDur());
        }
        // COB_CODE: IF DCO-FL-NO-ADERENTI-NULL = HIGH-VALUES
        //                TO (SF)-FL-NO-ADERENTI-NULL
        //           ELSE
        //                TO (SF)-FL-NO-ADERENTI
        //           END-IF
        if (Conditions.eq(ws.getdColl().getDcoFlNoAderenti(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE DCO-FL-NO-ADERENTI-NULL
            //             TO (SF)-FL-NO-ADERENTI-NULL
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoFlNoAderenti(ws.getdColl().getDcoFlNoAderenti());
        }
        else {
            // COB_CODE: MOVE DCO-FL-NO-ADERENTI
            //             TO (SF)-FL-NO-ADERENTI
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoFlNoAderenti(ws.getdColl().getDcoFlNoAderenti());
        }
        // COB_CODE: IF DCO-FL-DISTINTA-CNBTVA-NULL = HIGH-VALUES
        //                TO (SF)-FL-DISTINTA-CNBTVA-NULL
        //           ELSE
        //                TO (SF)-FL-DISTINTA-CNBTVA
        //           END-IF
        if (Conditions.eq(ws.getdColl().getDcoFlDistintaCnbtva(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE DCO-FL-DISTINTA-CNBTVA-NULL
            //             TO (SF)-FL-DISTINTA-CNBTVA-NULL
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoFlDistintaCnbtva(ws.getdColl().getDcoFlDistintaCnbtva());
        }
        else {
            // COB_CODE: MOVE DCO-FL-DISTINTA-CNBTVA
            //             TO (SF)-FL-DISTINTA-CNBTVA
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoFlDistintaCnbtva(ws.getdColl().getDcoFlDistintaCnbtva());
        }
        // COB_CODE: IF DCO-FL-CNBT-AUTES-NULL = HIGH-VALUES
        //                TO (SF)-FL-CNBT-AUTES-NULL
        //           ELSE
        //                TO (SF)-FL-CNBT-AUTES
        //           END-IF
        if (Conditions.eq(ws.getdColl().getDcoFlCnbtAutes(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE DCO-FL-CNBT-AUTES-NULL
            //             TO (SF)-FL-CNBT-AUTES-NULL
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoFlCnbtAutes(ws.getdColl().getDcoFlCnbtAutes());
        }
        else {
            // COB_CODE: MOVE DCO-FL-CNBT-AUTES
            //             TO (SF)-FL-CNBT-AUTES
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoFlCnbtAutes(ws.getdColl().getDcoFlCnbtAutes());
        }
        // COB_CODE: IF DCO-FL-QTZ-POST-EMIS-NULL = HIGH-VALUES
        //                TO (SF)-FL-QTZ-POST-EMIS-NULL
        //           ELSE
        //                TO (SF)-FL-QTZ-POST-EMIS
        //           END-IF
        if (Conditions.eq(ws.getdColl().getDcoFlQtzPostEmis(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE DCO-FL-QTZ-POST-EMIS-NULL
            //             TO (SF)-FL-QTZ-POST-EMIS-NULL
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoFlQtzPostEmis(ws.getdColl().getDcoFlQtzPostEmis());
        }
        else {
            // COB_CODE: MOVE DCO-FL-QTZ-POST-EMIS
            //             TO (SF)-FL-QTZ-POST-EMIS
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoFlQtzPostEmis(ws.getdColl().getDcoFlQtzPostEmis());
        }
        // COB_CODE: IF DCO-FL-COMNZ-FND-IS-NULL = HIGH-VALUES
        //                TO (SF)-FL-COMNZ-FND-IS-NULL
        //           ELSE
        //                TO (SF)-FL-COMNZ-FND-IS
        //           END-IF
        if (Conditions.eq(ws.getdColl().getDcoFlComnzFndIs(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE DCO-FL-COMNZ-FND-IS-NULL
            //             TO (SF)-FL-COMNZ-FND-IS-NULL
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoFlComnzFndIs(ws.getdColl().getDcoFlComnzFndIs());
        }
        else {
            // COB_CODE: MOVE DCO-FL-COMNZ-FND-IS
            //             TO (SF)-FL-COMNZ-FND-IS
            ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoFlComnzFndIs(ws.getdColl().getDcoFlComnzFndIs());
        }
        // COB_CODE: MOVE DCO-DS-RIGA
        //             TO (SF)-DS-RIGA
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoDsRiga(ws.getdColl().getDcoDsRiga());
        // COB_CODE: MOVE DCO-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoDsOperSql(ws.getdColl().getDcoDsOperSql());
        // COB_CODE: MOVE DCO-DS-VER
        //             TO (SF)-DS-VER
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoDsVer(ws.getdColl().getDcoDsVer());
        // COB_CODE: MOVE DCO-DS-TS-INI-CPTZ
        //             TO (SF)-DS-TS-INI-CPTZ
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoDsTsIniCptz(ws.getdColl().getDcoDsTsIniCptz());
        // COB_CODE: MOVE DCO-DS-TS-END-CPTZ
        //             TO (SF)-DS-TS-END-CPTZ
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoDsTsEndCptz(ws.getdColl().getDcoDsTsEndCptz());
        // COB_CODE: MOVE DCO-DS-UTENTE
        //             TO (SF)-DS-UTENTE
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoDsUtente(ws.getdColl().getDcoDsUtente());
        // COB_CODE: MOVE DCO-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB.
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoDsStatoElab(ws.getdColl().getDcoDsStatoElab());
    }

    /**Original name: VALORIZZA-OUTPUT-PMO<br>
	 * <pre>--> PARAMETRO MOVIMENTO
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVPMO3
	 *    ULTIMO AGG. 17 FEB 2015
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputPmo() {
        // COB_CODE: MOVE PMO-ID-PARAM-MOVI
        //             TO (SF)-ID-PTF(IX-TAB-PMO)
        ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().setIdPtf(ws.getParamMovi().getPmoIdParamMovi());
        // COB_CODE: MOVE PMO-ID-PARAM-MOVI
        //             TO (SF)-ID-PARAM-MOVI(IX-TAB-PMO)
        ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoIdParamMovi(ws.getParamMovi().getPmoIdParamMovi());
        // COB_CODE: MOVE PMO-ID-OGG
        //             TO (SF)-ID-OGG(IX-TAB-PMO)
        ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoIdOgg(ws.getParamMovi().getPmoIdOgg());
        // COB_CODE: MOVE PMO-TP-OGG
        //             TO (SF)-TP-OGG(IX-TAB-PMO)
        ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpOgg(ws.getParamMovi().getPmoTpOgg());
        // COB_CODE: MOVE PMO-ID-MOVI-CRZ
        //             TO (SF)-ID-MOVI-CRZ(IX-TAB-PMO)
        ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoIdMoviCrz(ws.getParamMovi().getPmoIdMoviCrz());
        // COB_CODE: IF PMO-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-ID-MOVI-CHIU(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoIdMoviChiu().getPmoIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE PMO-ID-MOVI-CHIU-NULL
            //             TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoIdMoviChiu().setWpmoIdMoviChiuNull(ws.getParamMovi().getPmoIdMoviChiu().getPmoIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE PMO-ID-MOVI-CHIU
            //             TO (SF)-ID-MOVI-CHIU(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoIdMoviChiu().setWpmoIdMoviChiu(ws.getParamMovi().getPmoIdMoviChiu().getPmoIdMoviChiu());
        }
        // COB_CODE: MOVE PMO-DT-INI-EFF
        //             TO (SF)-DT-INI-EFF(IX-TAB-PMO)
        ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoDtIniEff(ws.getParamMovi().getPmoDtIniEff());
        // COB_CODE: MOVE PMO-DT-END-EFF
        //             TO (SF)-DT-END-EFF(IX-TAB-PMO)
        ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoDtEndEff(ws.getParamMovi().getPmoDtEndEff());
        // COB_CODE: MOVE PMO-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA(IX-TAB-PMO)
        ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoCodCompAnia(ws.getParamMovi().getPmoCodCompAnia());
        // COB_CODE: IF PMO-TP-MOVI-NULL = HIGH-VALUES
        //                TO (SF)-TP-MOVI-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-TP-MOVI(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoTpMovi().getPmoTpMoviNullFormatted())) {
            // COB_CODE: MOVE PMO-TP-MOVI-NULL
            //             TO (SF)-TP-MOVI-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpMovi().setWpmoTpMoviNull(ws.getParamMovi().getPmoTpMovi().getPmoTpMoviNull());
        }
        else {
            // COB_CODE: MOVE PMO-TP-MOVI
            //             TO (SF)-TP-MOVI(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpMovi().setWpmoTpMovi(ws.getParamMovi().getPmoTpMovi().getPmoTpMovi());
        }
        // COB_CODE: IF PMO-FRQ-MOVI-NULL = HIGH-VALUES
        //                TO (SF)-FRQ-MOVI-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-FRQ-MOVI(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoFrqMovi().getPmoFrqMoviNullFormatted())) {
            // COB_CODE: MOVE PMO-FRQ-MOVI-NULL
            //             TO (SF)-FRQ-MOVI-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoFrqMovi().setWpmoFrqMoviNull(ws.getParamMovi().getPmoFrqMovi().getPmoFrqMoviNull());
        }
        else {
            // COB_CODE: MOVE PMO-FRQ-MOVI
            //             TO (SF)-FRQ-MOVI(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoFrqMovi().setWpmoFrqMovi(ws.getParamMovi().getPmoFrqMovi().getPmoFrqMovi());
        }
        // COB_CODE: IF PMO-DUR-AA-NULL = HIGH-VALUES
        //                TO (SF)-DUR-AA-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-DUR-AA(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoDurAa().getPmoDurAaNullFormatted())) {
            // COB_CODE: MOVE PMO-DUR-AA-NULL
            //             TO (SF)-DUR-AA-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoDurAa().setWpmoDurAaNull(ws.getParamMovi().getPmoDurAa().getPmoDurAaNull());
        }
        else {
            // COB_CODE: MOVE PMO-DUR-AA
            //             TO (SF)-DUR-AA(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoDurAa().setWpmoDurAa(ws.getParamMovi().getPmoDurAa().getPmoDurAa());
        }
        // COB_CODE: IF PMO-DUR-MM-NULL = HIGH-VALUES
        //                TO (SF)-DUR-MM-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-DUR-MM(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoDurMm().getPmoDurMmNullFormatted())) {
            // COB_CODE: MOVE PMO-DUR-MM-NULL
            //             TO (SF)-DUR-MM-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoDurMm().setWpmoDurMmNull(ws.getParamMovi().getPmoDurMm().getPmoDurMmNull());
        }
        else {
            // COB_CODE: MOVE PMO-DUR-MM
            //             TO (SF)-DUR-MM(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoDurMm().setWpmoDurMm(ws.getParamMovi().getPmoDurMm().getPmoDurMm());
        }
        // COB_CODE: IF PMO-DUR-GG-NULL = HIGH-VALUES
        //                TO (SF)-DUR-GG-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-DUR-GG(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoDurGg().getPmoDurGgNullFormatted())) {
            // COB_CODE: MOVE PMO-DUR-GG-NULL
            //             TO (SF)-DUR-GG-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoDurGg().setWpmoDurGgNull(ws.getParamMovi().getPmoDurGg().getPmoDurGgNull());
        }
        else {
            // COB_CODE: MOVE PMO-DUR-GG
            //             TO (SF)-DUR-GG(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoDurGg().setWpmoDurGg(ws.getParamMovi().getPmoDurGg().getPmoDurGg());
        }
        // COB_CODE: IF PMO-DT-RICOR-PREC-NULL = HIGH-VALUES
        //                TO (SF)-DT-RICOR-PREC-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-DT-RICOR-PREC(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoDtRicorPrec().getPmoDtRicorPrecNullFormatted())) {
            // COB_CODE: MOVE PMO-DT-RICOR-PREC-NULL
            //             TO (SF)-DT-RICOR-PREC-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtRicorPrec().setWpmoDtRicorPrecNull(ws.getParamMovi().getPmoDtRicorPrec().getPmoDtRicorPrecNull());
        }
        else {
            // COB_CODE: MOVE PMO-DT-RICOR-PREC
            //             TO (SF)-DT-RICOR-PREC(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtRicorPrec().setWpmoDtRicorPrec(ws.getParamMovi().getPmoDtRicorPrec().getPmoDtRicorPrec());
        }
        // COB_CODE: IF PMO-DT-RICOR-SUCC-NULL = HIGH-VALUES
        //                TO (SF)-DT-RICOR-SUCC-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-DT-RICOR-SUCC(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoDtRicorSucc().getPmoDtRicorSuccNullFormatted())) {
            // COB_CODE: MOVE PMO-DT-RICOR-SUCC-NULL
            //             TO (SF)-DT-RICOR-SUCC-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().setWpmoDtRicorSuccNull(ws.getParamMovi().getPmoDtRicorSucc().getPmoDtRicorSuccNull());
        }
        else {
            // COB_CODE: MOVE PMO-DT-RICOR-SUCC
            //             TO (SF)-DT-RICOR-SUCC(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().setWpmoDtRicorSucc(ws.getParamMovi().getPmoDtRicorSucc().getPmoDtRicorSucc());
        }
        // COB_CODE: IF PMO-PC-INTR-FRAZ-NULL = HIGH-VALUES
        //                TO (SF)-PC-INTR-FRAZ-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-PC-INTR-FRAZ(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoPcIntrFraz().getPmoPcIntrFrazNullFormatted())) {
            // COB_CODE: MOVE PMO-PC-INTR-FRAZ-NULL
            //             TO (SF)-PC-INTR-FRAZ-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcIntrFraz().setWpmoPcIntrFrazNull(ws.getParamMovi().getPmoPcIntrFraz().getPmoPcIntrFrazNull());
        }
        else {
            // COB_CODE: MOVE PMO-PC-INTR-FRAZ
            //             TO (SF)-PC-INTR-FRAZ(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcIntrFraz().setWpmoPcIntrFraz(Trunc.toDecimal(ws.getParamMovi().getPmoPcIntrFraz().getPmoPcIntrFraz(), 6, 3));
        }
        // COB_CODE: IF PMO-IMP-BNS-DA-SCO-TOT-NULL = HIGH-VALUES
        //                TO (SF)-IMP-BNS-DA-SCO-TOT-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-IMP-BNS-DA-SCO-TOT(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoImpBnsDaScoTot().getPmoImpBnsDaScoTotNullFormatted())) {
            // COB_CODE: MOVE PMO-IMP-BNS-DA-SCO-TOT-NULL
            //             TO (SF)-IMP-BNS-DA-SCO-TOT-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpBnsDaScoTot().setWpmoImpBnsDaScoTotNull(ws.getParamMovi().getPmoImpBnsDaScoTot().getPmoImpBnsDaScoTotNull());
        }
        else {
            // COB_CODE: MOVE PMO-IMP-BNS-DA-SCO-TOT
            //             TO (SF)-IMP-BNS-DA-SCO-TOT(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpBnsDaScoTot().setWpmoImpBnsDaScoTot(Trunc.toDecimal(ws.getParamMovi().getPmoImpBnsDaScoTot().getPmoImpBnsDaScoTot(), 15, 3));
        }
        // COB_CODE: IF PMO-IMP-BNS-DA-SCO-NULL = HIGH-VALUES
        //                TO (SF)-IMP-BNS-DA-SCO-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-IMP-BNS-DA-SCO(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoImpBnsDaSco().getPmoImpBnsDaScoNullFormatted())) {
            // COB_CODE: MOVE PMO-IMP-BNS-DA-SCO-NULL
            //             TO (SF)-IMP-BNS-DA-SCO-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpBnsDaSco().setWpmoImpBnsDaScoNull(ws.getParamMovi().getPmoImpBnsDaSco().getPmoImpBnsDaScoNull());
        }
        else {
            // COB_CODE: MOVE PMO-IMP-BNS-DA-SCO
            //             TO (SF)-IMP-BNS-DA-SCO(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpBnsDaSco().setWpmoImpBnsDaSco(Trunc.toDecimal(ws.getParamMovi().getPmoImpBnsDaSco().getPmoImpBnsDaSco(), 15, 3));
        }
        // COB_CODE: IF PMO-PC-ANTIC-BNS-NULL = HIGH-VALUES
        //                TO (SF)-PC-ANTIC-BNS-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-PC-ANTIC-BNS(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoPcAnticBns().getPmoPcAnticBnsNullFormatted())) {
            // COB_CODE: MOVE PMO-PC-ANTIC-BNS-NULL
            //             TO (SF)-PC-ANTIC-BNS-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcAnticBns().setWpmoPcAnticBnsNull(ws.getParamMovi().getPmoPcAnticBns().getPmoPcAnticBnsNull());
        }
        else {
            // COB_CODE: MOVE PMO-PC-ANTIC-BNS
            //             TO (SF)-PC-ANTIC-BNS(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcAnticBns().setWpmoPcAnticBns(Trunc.toDecimal(ws.getParamMovi().getPmoPcAnticBns().getPmoPcAnticBns(), 6, 3));
        }
        // COB_CODE: IF PMO-TP-RINN-COLL-NULL = HIGH-VALUES
        //                TO (SF)-TP-RINN-COLL-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-TP-RINN-COLL(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoTpRinnCollFormatted())) {
            // COB_CODE: MOVE PMO-TP-RINN-COLL-NULL
            //             TO (SF)-TP-RINN-COLL-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpRinnColl(ws.getParamMovi().getPmoTpRinnColl());
        }
        else {
            // COB_CODE: MOVE PMO-TP-RINN-COLL
            //             TO (SF)-TP-RINN-COLL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpRinnColl(ws.getParamMovi().getPmoTpRinnColl());
        }
        // COB_CODE: IF PMO-TP-RIVAL-PRE-NULL = HIGH-VALUES
        //                TO (SF)-TP-RIVAL-PRE-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-TP-RIVAL-PRE(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoTpRivalPreFormatted())) {
            // COB_CODE: MOVE PMO-TP-RIVAL-PRE-NULL
            //             TO (SF)-TP-RIVAL-PRE-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpRivalPre(ws.getParamMovi().getPmoTpRivalPre());
        }
        else {
            // COB_CODE: MOVE PMO-TP-RIVAL-PRE
            //             TO (SF)-TP-RIVAL-PRE(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpRivalPre(ws.getParamMovi().getPmoTpRivalPre());
        }
        // COB_CODE: IF PMO-TP-RIVAL-PRSTZ-NULL = HIGH-VALUES
        //                TO (SF)-TP-RIVAL-PRSTZ-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-TP-RIVAL-PRSTZ(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoTpRivalPrstzFormatted())) {
            // COB_CODE: MOVE PMO-TP-RIVAL-PRSTZ-NULL
            //             TO (SF)-TP-RIVAL-PRSTZ-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpRivalPrstz(ws.getParamMovi().getPmoTpRivalPrstz());
        }
        else {
            // COB_CODE: MOVE PMO-TP-RIVAL-PRSTZ
            //             TO (SF)-TP-RIVAL-PRSTZ(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpRivalPrstz(ws.getParamMovi().getPmoTpRivalPrstz());
        }
        // COB_CODE: IF PMO-FL-EVID-RIVAL-NULL = HIGH-VALUES
        //                TO (SF)-FL-EVID-RIVAL-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-FL-EVID-RIVAL(IX-TAB-PMO)
        //           END-IF
        if (Conditions.eq(ws.getParamMovi().getPmoFlEvidRival(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE PMO-FL-EVID-RIVAL-NULL
            //             TO (SF)-FL-EVID-RIVAL-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoFlEvidRival(ws.getParamMovi().getPmoFlEvidRival());
        }
        else {
            // COB_CODE: MOVE PMO-FL-EVID-RIVAL
            //             TO (SF)-FL-EVID-RIVAL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoFlEvidRival(ws.getParamMovi().getPmoFlEvidRival());
        }
        // COB_CODE: IF PMO-ULT-PC-PERD-NULL = HIGH-VALUES
        //                TO (SF)-ULT-PC-PERD-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-ULT-PC-PERD(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoUltPcPerd().getPmoUltPcPerdNullFormatted())) {
            // COB_CODE: MOVE PMO-ULT-PC-PERD-NULL
            //             TO (SF)-ULT-PC-PERD-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoUltPcPerd().setWpmoUltPcPerdNull(ws.getParamMovi().getPmoUltPcPerd().getPmoUltPcPerdNull());
        }
        else {
            // COB_CODE: MOVE PMO-ULT-PC-PERD
            //             TO (SF)-ULT-PC-PERD(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoUltPcPerd().setWpmoUltPcPerd(Trunc.toDecimal(ws.getParamMovi().getPmoUltPcPerd().getPmoUltPcPerd(), 6, 3));
        }
        // COB_CODE: IF PMO-TOT-AA-GIA-PROR-NULL = HIGH-VALUES
        //                TO (SF)-TOT-AA-GIA-PROR-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-TOT-AA-GIA-PROR(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoTotAaGiaPror().getPmoTotAaGiaProrNullFormatted())) {
            // COB_CODE: MOVE PMO-TOT-AA-GIA-PROR-NULL
            //             TO (SF)-TOT-AA-GIA-PROR-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoTotAaGiaPror().setWpmoTotAaGiaProrNull(ws.getParamMovi().getPmoTotAaGiaPror().getPmoTotAaGiaProrNull());
        }
        else {
            // COB_CODE: MOVE PMO-TOT-AA-GIA-PROR
            //             TO (SF)-TOT-AA-GIA-PROR(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoTotAaGiaPror().setWpmoTotAaGiaPror(ws.getParamMovi().getPmoTotAaGiaPror().getPmoTotAaGiaPror());
        }
        // COB_CODE: IF PMO-TP-OPZ-NULL = HIGH-VALUES
        //                TO (SF)-TP-OPZ-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-TP-OPZ(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoTpOpzFormatted())) {
            // COB_CODE: MOVE PMO-TP-OPZ-NULL
            //             TO (SF)-TP-OPZ-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpOpz(ws.getParamMovi().getPmoTpOpz());
        }
        else {
            // COB_CODE: MOVE PMO-TP-OPZ
            //             TO (SF)-TP-OPZ(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpOpz(ws.getParamMovi().getPmoTpOpz());
        }
        // COB_CODE: IF PMO-AA-REN-CER-NULL = HIGH-VALUES
        //                TO (SF)-AA-REN-CER-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-AA-REN-CER(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoAaRenCer().getPmoAaRenCerNullFormatted())) {
            // COB_CODE: MOVE PMO-AA-REN-CER-NULL
            //             TO (SF)-AA-REN-CER-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoAaRenCer().setWpmoAaRenCerNull(ws.getParamMovi().getPmoAaRenCer().getPmoAaRenCerNull());
        }
        else {
            // COB_CODE: MOVE PMO-AA-REN-CER
            //             TO (SF)-AA-REN-CER(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoAaRenCer().setWpmoAaRenCer(ws.getParamMovi().getPmoAaRenCer().getPmoAaRenCer());
        }
        // COB_CODE: IF PMO-PC-REVRSB-NULL = HIGH-VALUES
        //                TO (SF)-PC-REVRSB-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-PC-REVRSB(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoPcRevrsb().getPmoPcRevrsbNullFormatted())) {
            // COB_CODE: MOVE PMO-PC-REVRSB-NULL
            //             TO (SF)-PC-REVRSB-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcRevrsb().setWpmoPcRevrsbNull(ws.getParamMovi().getPmoPcRevrsb().getPmoPcRevrsbNull());
        }
        else {
            // COB_CODE: MOVE PMO-PC-REVRSB
            //             TO (SF)-PC-REVRSB(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcRevrsb().setWpmoPcRevrsb(Trunc.toDecimal(ws.getParamMovi().getPmoPcRevrsb().getPmoPcRevrsb(), 6, 3));
        }
        // COB_CODE: IF PMO-IMP-RISC-PARZ-PRGT-NULL = HIGH-VALUES
        //                TO (SF)-IMP-RISC-PARZ-PRGT-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-IMP-RISC-PARZ-PRGT(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoImpRiscParzPrgt().getPmoImpRiscParzPrgtNullFormatted())) {
            // COB_CODE: MOVE PMO-IMP-RISC-PARZ-PRGT-NULL
            //             TO (SF)-IMP-RISC-PARZ-PRGT-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpRiscParzPrgt().setWpmoImpRiscParzPrgtNull(ws.getParamMovi().getPmoImpRiscParzPrgt().getPmoImpRiscParzPrgtNull());
        }
        else {
            // COB_CODE: MOVE PMO-IMP-RISC-PARZ-PRGT
            //             TO (SF)-IMP-RISC-PARZ-PRGT(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpRiscParzPrgt().setWpmoImpRiscParzPrgt(Trunc.toDecimal(ws.getParamMovi().getPmoImpRiscParzPrgt().getPmoImpRiscParzPrgt(), 15, 3));
        }
        // COB_CODE: IF PMO-IMP-LRD-DI-RAT-NULL = HIGH-VALUES
        //                TO (SF)-IMP-LRD-DI-RAT-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-IMP-LRD-DI-RAT(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoImpLrdDiRat().getPmoImpLrdDiRatNullFormatted())) {
            // COB_CODE: MOVE PMO-IMP-LRD-DI-RAT-NULL
            //             TO (SF)-IMP-LRD-DI-RAT-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpLrdDiRat().setWpmoImpLrdDiRatNull(ws.getParamMovi().getPmoImpLrdDiRat().getPmoImpLrdDiRatNull());
        }
        else {
            // COB_CODE: MOVE PMO-IMP-LRD-DI-RAT
            //             TO (SF)-IMP-LRD-DI-RAT(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpLrdDiRat().setWpmoImpLrdDiRat(Trunc.toDecimal(ws.getParamMovi().getPmoImpLrdDiRat().getPmoImpLrdDiRat(), 15, 3));
        }
        // COB_CODE: IF PMO-IB-OGG-NULL = HIGH-VALUES
        //                TO (SF)-IB-OGG-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-IB-OGG(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoIbOgg(), ParamMovi.Len.PMO_IB_OGG)) {
            // COB_CODE: MOVE PMO-IB-OGG-NULL
            //             TO (SF)-IB-OGG-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoIbOgg(ws.getParamMovi().getPmoIbOgg());
        }
        else {
            // COB_CODE: MOVE PMO-IB-OGG
            //             TO (SF)-IB-OGG(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoIbOgg(ws.getParamMovi().getPmoIbOgg());
        }
        // COB_CODE: IF PMO-COS-ONER-NULL = HIGH-VALUES
        //                TO (SF)-COS-ONER-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-COS-ONER(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoCosOner().getPmoCosOnerNullFormatted())) {
            // COB_CODE: MOVE PMO-COS-ONER-NULL
            //             TO (SF)-COS-ONER-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoCosOner().setWpmoCosOnerNull(ws.getParamMovi().getPmoCosOner().getPmoCosOnerNull());
        }
        else {
            // COB_CODE: MOVE PMO-COS-ONER
            //             TO (SF)-COS-ONER(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoCosOner().setWpmoCosOner(Trunc.toDecimal(ws.getParamMovi().getPmoCosOner().getPmoCosOner(), 15, 3));
        }
        // COB_CODE: IF PMO-SPE-PC-NULL = HIGH-VALUES
        //                TO (SF)-SPE-PC-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-SPE-PC(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoSpePc().getPmoSpePcNullFormatted())) {
            // COB_CODE: MOVE PMO-SPE-PC-NULL
            //             TO (SF)-SPE-PC-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoSpePc().setWpmoSpePcNull(ws.getParamMovi().getPmoSpePc().getPmoSpePcNull());
        }
        else {
            // COB_CODE: MOVE PMO-SPE-PC
            //             TO (SF)-SPE-PC(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoSpePc().setWpmoSpePc(Trunc.toDecimal(ws.getParamMovi().getPmoSpePc().getPmoSpePc(), 6, 3));
        }
        // COB_CODE: IF PMO-FL-ATTIV-GAR-NULL = HIGH-VALUES
        //                TO (SF)-FL-ATTIV-GAR-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-FL-ATTIV-GAR(IX-TAB-PMO)
        //           END-IF
        if (Conditions.eq(ws.getParamMovi().getPmoFlAttivGar(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE PMO-FL-ATTIV-GAR-NULL
            //             TO (SF)-FL-ATTIV-GAR-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoFlAttivGar(ws.getParamMovi().getPmoFlAttivGar());
        }
        else {
            // COB_CODE: MOVE PMO-FL-ATTIV-GAR
            //             TO (SF)-FL-ATTIV-GAR(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoFlAttivGar(ws.getParamMovi().getPmoFlAttivGar());
        }
        // COB_CODE: IF PMO-CAMBIO-VER-PROD-NULL = HIGH-VALUES
        //                TO (SF)-CAMBIO-VER-PROD-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-CAMBIO-VER-PROD(IX-TAB-PMO)
        //           END-IF
        if (Conditions.eq(ws.getParamMovi().getPmoCambioVerProd(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE PMO-CAMBIO-VER-PROD-NULL
            //             TO (SF)-CAMBIO-VER-PROD-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoCambioVerProd(ws.getParamMovi().getPmoCambioVerProd());
        }
        else {
            // COB_CODE: MOVE PMO-CAMBIO-VER-PROD
            //             TO (SF)-CAMBIO-VER-PROD(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoCambioVerProd(ws.getParamMovi().getPmoCambioVerProd());
        }
        // COB_CODE: IF PMO-MM-DIFF-NULL = HIGH-VALUES
        //                TO (SF)-MM-DIFF-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-MM-DIFF(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoMmDiff().getPmoMmDiffNullFormatted())) {
            // COB_CODE: MOVE PMO-MM-DIFF-NULL
            //             TO (SF)-MM-DIFF-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoMmDiff().setWpmoMmDiffNull(ws.getParamMovi().getPmoMmDiff().getPmoMmDiffNull());
        }
        else {
            // COB_CODE: MOVE PMO-MM-DIFF
            //             TO (SF)-MM-DIFF(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoMmDiff().setWpmoMmDiff(ws.getParamMovi().getPmoMmDiff().getPmoMmDiff());
        }
        // COB_CODE: IF PMO-IMP-RAT-MANFEE-NULL = HIGH-VALUES
        //                TO (SF)-IMP-RAT-MANFEE-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-IMP-RAT-MANFEE(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoImpRatManfee().getPmoImpRatManfeeNullFormatted())) {
            // COB_CODE: MOVE PMO-IMP-RAT-MANFEE-NULL
            //             TO (SF)-IMP-RAT-MANFEE-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpRatManfee().setWpmoImpRatManfeeNull(ws.getParamMovi().getPmoImpRatManfee().getPmoImpRatManfeeNull());
        }
        else {
            // COB_CODE: MOVE PMO-IMP-RAT-MANFEE
            //             TO (SF)-IMP-RAT-MANFEE(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpRatManfee().setWpmoImpRatManfee(Trunc.toDecimal(ws.getParamMovi().getPmoImpRatManfee().getPmoImpRatManfee(), 15, 3));
        }
        // COB_CODE: IF PMO-DT-ULT-EROG-MANFEE-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-EROG-MANFEE-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-DT-ULT-EROG-MANFEE(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoDtUltErogManfee().getPmoDtUltErogManfeeNullFormatted())) {
            // COB_CODE: MOVE PMO-DT-ULT-EROG-MANFEE-NULL
            //             TO (SF)-DT-ULT-EROG-MANFEE-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtUltErogManfee().setWpmoDtUltErogManfeeNull(ws.getParamMovi().getPmoDtUltErogManfee().getPmoDtUltErogManfeeNull());
        }
        else {
            // COB_CODE: MOVE PMO-DT-ULT-EROG-MANFEE
            //             TO (SF)-DT-ULT-EROG-MANFEE(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtUltErogManfee().setWpmoDtUltErogManfee(ws.getParamMovi().getPmoDtUltErogManfee().getPmoDtUltErogManfee());
        }
        // COB_CODE: IF PMO-TP-OGG-RIVAL-NULL = HIGH-VALUES
        //                TO (SF)-TP-OGG-RIVAL-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-TP-OGG-RIVAL(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoTpOggRivalFormatted())) {
            // COB_CODE: MOVE PMO-TP-OGG-RIVAL-NULL
            //             TO (SF)-TP-OGG-RIVAL-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpOggRival(ws.getParamMovi().getPmoTpOggRival());
        }
        else {
            // COB_CODE: MOVE PMO-TP-OGG-RIVAL
            //             TO (SF)-TP-OGG-RIVAL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpOggRival(ws.getParamMovi().getPmoTpOggRival());
        }
        // COB_CODE: IF PMO-SOM-ASSTA-GARAC-NULL = HIGH-VALUES
        //                TO (SF)-SOM-ASSTA-GARAC-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-SOM-ASSTA-GARAC(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoSomAsstaGarac().getPmoSomAsstaGaracNullFormatted())) {
            // COB_CODE: MOVE PMO-SOM-ASSTA-GARAC-NULL
            //             TO (SF)-SOM-ASSTA-GARAC-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoSomAsstaGarac().setWpmoSomAsstaGaracNull(ws.getParamMovi().getPmoSomAsstaGarac().getPmoSomAsstaGaracNull());
        }
        else {
            // COB_CODE: MOVE PMO-SOM-ASSTA-GARAC
            //             TO (SF)-SOM-ASSTA-GARAC(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoSomAsstaGarac().setWpmoSomAsstaGarac(Trunc.toDecimal(ws.getParamMovi().getPmoSomAsstaGarac().getPmoSomAsstaGarac(), 15, 3));
        }
        // COB_CODE: IF PMO-PC-APPLZ-OPZ-NULL = HIGH-VALUES
        //                TO (SF)-PC-APPLZ-OPZ-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-PC-APPLZ-OPZ(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoPcApplzOpz().getPmoPcApplzOpzNullFormatted())) {
            // COB_CODE: MOVE PMO-PC-APPLZ-OPZ-NULL
            //             TO (SF)-PC-APPLZ-OPZ-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcApplzOpz().setWpmoPcApplzOpzNull(ws.getParamMovi().getPmoPcApplzOpz().getPmoPcApplzOpzNull());
        }
        else {
            // COB_CODE: MOVE PMO-PC-APPLZ-OPZ
            //             TO (SF)-PC-APPLZ-OPZ(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcApplzOpz().setWpmoPcApplzOpz(Trunc.toDecimal(ws.getParamMovi().getPmoPcApplzOpz().getPmoPcApplzOpz(), 6, 3));
        }
        // COB_CODE: IF PMO-ID-ADES-NULL = HIGH-VALUES
        //                TO (SF)-ID-ADES-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-ID-ADES(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoIdAdes().getPmoIdAdesNullFormatted())) {
            // COB_CODE: MOVE PMO-ID-ADES-NULL
            //             TO (SF)-ID-ADES-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoIdAdes().setWpmoIdAdesNull(ws.getParamMovi().getPmoIdAdes().getPmoIdAdesNull());
        }
        else {
            // COB_CODE: MOVE PMO-ID-ADES
            //             TO (SF)-ID-ADES(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoIdAdes().setWpmoIdAdes(ws.getParamMovi().getPmoIdAdes().getPmoIdAdes());
        }
        // COB_CODE: MOVE PMO-ID-POLI
        //             TO (SF)-ID-POLI(IX-TAB-PMO)
        ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoIdPoli(ws.getParamMovi().getPmoIdPoli());
        // COB_CODE: MOVE PMO-TP-FRM-ASSVA
        //             TO (SF)-TP-FRM-ASSVA(IX-TAB-PMO)
        ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpFrmAssva(ws.getParamMovi().getPmoTpFrmAssva());
        // COB_CODE: MOVE PMO-DS-RIGA
        //             TO (SF)-DS-RIGA(IX-TAB-PMO)
        ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoDsRiga(ws.getParamMovi().getPmoDsRiga());
        // COB_CODE: MOVE PMO-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL(IX-TAB-PMO)
        ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoDsOperSql(ws.getParamMovi().getPmoDsOperSql());
        // COB_CODE: MOVE PMO-DS-VER
        //             TO (SF)-DS-VER(IX-TAB-PMO)
        ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoDsVer(ws.getParamMovi().getPmoDsVer());
        // COB_CODE: MOVE PMO-DS-TS-INI-CPTZ
        //             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-PMO)
        ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoDsTsIniCptz(ws.getParamMovi().getPmoDsTsIniCptz());
        // COB_CODE: MOVE PMO-DS-TS-END-CPTZ
        //             TO (SF)-DS-TS-END-CPTZ(IX-TAB-PMO)
        ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoDsTsEndCptz(ws.getParamMovi().getPmoDsTsEndCptz());
        // COB_CODE: MOVE PMO-DS-UTENTE
        //             TO (SF)-DS-UTENTE(IX-TAB-PMO)
        ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoDsUtente(ws.getParamMovi().getPmoDsUtente());
        // COB_CODE: MOVE PMO-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB(IX-TAB-PMO)
        ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoDsStatoElab(ws.getParamMovi().getPmoDsStatoElab());
        // COB_CODE: IF PMO-TP-ESTR-CNT-NULL = HIGH-VALUES
        //                TO (SF)-TP-ESTR-CNT-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-TP-ESTR-CNT(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoTpEstrCntFormatted())) {
            // COB_CODE: MOVE PMO-TP-ESTR-CNT-NULL
            //             TO (SF)-TP-ESTR-CNT-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpEstrCnt(ws.getParamMovi().getPmoTpEstrCnt());
        }
        else {
            // COB_CODE: MOVE PMO-TP-ESTR-CNT
            //             TO (SF)-TP-ESTR-CNT(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoTpEstrCnt(ws.getParamMovi().getPmoTpEstrCnt());
        }
        // COB_CODE: IF PMO-COD-RAMO-NULL = HIGH-VALUES
        //                TO (SF)-COD-RAMO-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-COD-RAMO(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoCodRamoFormatted())) {
            // COB_CODE: MOVE PMO-COD-RAMO-NULL
            //             TO (SF)-COD-RAMO-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoCodRamo(ws.getParamMovi().getPmoCodRamo());
        }
        else {
            // COB_CODE: MOVE PMO-COD-RAMO
            //             TO (SF)-COD-RAMO(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoCodRamo(ws.getParamMovi().getPmoCodRamo());
        }
        // COB_CODE: IF PMO-GEN-DA-SIN-NULL = HIGH-VALUES
        //                TO (SF)-GEN-DA-SIN-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-GEN-DA-SIN(IX-TAB-PMO)
        //           END-IF
        if (Conditions.eq(ws.getParamMovi().getPmoGenDaSin(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE PMO-GEN-DA-SIN-NULL
            //             TO (SF)-GEN-DA-SIN-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoGenDaSin(ws.getParamMovi().getPmoGenDaSin());
        }
        else {
            // COB_CODE: MOVE PMO-GEN-DA-SIN
            //             TO (SF)-GEN-DA-SIN(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoGenDaSin(ws.getParamMovi().getPmoGenDaSin());
        }
        // COB_CODE: IF PMO-COD-TARI-NULL = HIGH-VALUES
        //                TO (SF)-COD-TARI-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-COD-TARI(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoCodTariFormatted())) {
            // COB_CODE: MOVE PMO-COD-TARI-NULL
            //             TO (SF)-COD-TARI-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoCodTari(ws.getParamMovi().getPmoCodTari());
        }
        else {
            // COB_CODE: MOVE PMO-COD-TARI
            //             TO (SF)-COD-TARI(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().setWpmoCodTari(ws.getParamMovi().getPmoCodTari());
        }
        // COB_CODE: IF PMO-NUM-RAT-PAG-PRE-NULL = HIGH-VALUES
        //                TO (SF)-NUM-RAT-PAG-PRE-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-NUM-RAT-PAG-PRE(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoNumRatPagPre().getPmoNumRatPagPreNullFormatted())) {
            // COB_CODE: MOVE PMO-NUM-RAT-PAG-PRE-NULL
            //             TO (SF)-NUM-RAT-PAG-PRE-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoNumRatPagPre().setWpmoNumRatPagPreNull(ws.getParamMovi().getPmoNumRatPagPre().getPmoNumRatPagPreNull());
        }
        else {
            // COB_CODE: MOVE PMO-NUM-RAT-PAG-PRE
            //             TO (SF)-NUM-RAT-PAG-PRE(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoNumRatPagPre().setWpmoNumRatPagPre(ws.getParamMovi().getPmoNumRatPagPre().getPmoNumRatPagPre());
        }
        // COB_CODE: IF PMO-PC-SERV-VAL-NULL = HIGH-VALUES
        //                TO (SF)-PC-SERV-VAL-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-PC-SERV-VAL(IX-TAB-PMO)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoPcServVal().getPmoPcServValNullFormatted())) {
            // COB_CODE: MOVE PMO-PC-SERV-VAL-NULL
            //             TO (SF)-PC-SERV-VAL-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcServVal().setWpmoPcServValNull(ws.getParamMovi().getPmoPcServVal().getPmoPcServValNull());
        }
        else {
            // COB_CODE: MOVE PMO-PC-SERV-VAL
            //             TO (SF)-PC-SERV-VAL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcServVal().setWpmoPcServVal(Trunc.toDecimal(ws.getParamMovi().getPmoPcServVal().getPmoPcServVal(), 6, 3));
        }
        // COB_CODE: IF PMO-ETA-AA-SOGL-BNFICR-NULL = HIGH-VALUES
        //                TO (SF)-ETA-AA-SOGL-BNFICR-NULL(IX-TAB-PMO)
        //           ELSE
        //                TO (SF)-ETA-AA-SOGL-BNFICR(IX-TAB-PMO)
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoEtaAaSoglBnficr().getPmoEtaAaSoglBnficrNullFormatted())) {
            // COB_CODE: MOVE PMO-ETA-AA-SOGL-BNFICR-NULL
            //             TO (SF)-ETA-AA-SOGL-BNFICR-NULL(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoEtaAaSoglBnficr().setWpmoEtaAaSoglBnficrNull(ws.getParamMovi().getPmoEtaAaSoglBnficr().getPmoEtaAaSoglBnficrNull());
        }
        else {
            // COB_CODE: MOVE PMO-ETA-AA-SOGL-BNFICR
            //             TO (SF)-ETA-AA-SOGL-BNFICR(IX-TAB-PMO)
            ws.getAreeAppoggio().getDpmoTabParamMov(ws.getIxIndici().getIxTabPmo()).getLccvpmo1().getDati().getWpmoEtaAaSoglBnficr().setWpmoEtaAaSoglBnficr(ws.getParamMovi().getPmoEtaAaSoglBnficr().getPmoEtaAaSoglBnficr());
        }
    }

    /**Original name: VALORIZZA-OUTPUT-GRZ<br>
	 * <pre>--> GARANZIA
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVGRZ3
	 *    ULTIMO AGG. 31 OTT 2013
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputGrz() {
        // COB_CODE: MOVE GRZ-ID-GAR
        //             TO (SF)-ID-PTF(IX-TAB-GRZ)
        ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().setIdPtf(ws.getGar().getGrzIdGar());
        // COB_CODE: MOVE GRZ-ID-GAR
        //             TO (SF)-ID-GAR(IX-TAB-GRZ)
        ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzIdGar(ws.getGar().getGrzIdGar());
        // COB_CODE: IF GRZ-ID-ADES-NULL = HIGH-VALUES
        //                TO (SF)-ID-ADES-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-ID-ADES(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzIdAdes().getGrzIdAdesNullFormatted())) {
            // COB_CODE: MOVE GRZ-ID-ADES-NULL
            //             TO (SF)-ID-ADES-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzIdAdes().setWgrzIdAdesNull(ws.getGar().getGrzIdAdes().getGrzIdAdesNull());
        }
        else {
            // COB_CODE: MOVE GRZ-ID-ADES
            //             TO (SF)-ID-ADES(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzIdAdes().setWgrzIdAdes(ws.getGar().getGrzIdAdes().getGrzIdAdes());
        }
        // COB_CODE: MOVE GRZ-ID-POLI
        //             TO (SF)-ID-POLI(IX-TAB-GRZ)
        ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzIdPoli(ws.getGar().getGrzIdPoli());
        // COB_CODE: MOVE GRZ-ID-MOVI-CRZ
        //             TO (SF)-ID-MOVI-CRZ(IX-TAB-GRZ)
        ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzIdMoviCrz(ws.getGar().getGrzIdMoviCrz());
        // COB_CODE: IF GRZ-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-ID-MOVI-CHIU(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzIdMoviChiu().getGrzIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE GRZ-ID-MOVI-CHIU-NULL
            //             TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzIdMoviChiu().setWgrzIdMoviChiuNull(ws.getGar().getGrzIdMoviChiu().getGrzIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE GRZ-ID-MOVI-CHIU
            //             TO (SF)-ID-MOVI-CHIU(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzIdMoviChiu().setWgrzIdMoviChiu(ws.getGar().getGrzIdMoviChiu().getGrzIdMoviChiu());
        }
        // COB_CODE: MOVE GRZ-DT-INI-EFF
        //             TO (SF)-DT-INI-EFF(IX-TAB-GRZ)
        ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzDtIniEff(ws.getGar().getGrzDtIniEff());
        // COB_CODE: MOVE GRZ-DT-END-EFF
        //             TO (SF)-DT-END-EFF(IX-TAB-GRZ)
        ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzDtEndEff(ws.getGar().getGrzDtEndEff());
        // COB_CODE: MOVE GRZ-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA(IX-TAB-GRZ)
        ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzCodCompAnia(ws.getGar().getGrzCodCompAnia());
        // COB_CODE: IF GRZ-IB-OGG-NULL = HIGH-VALUES
        //                TO (SF)-IB-OGG-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-IB-OGG(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzIbOgg(), GarIvvs0216.Len.GRZ_IB_OGG)) {
            // COB_CODE: MOVE GRZ-IB-OGG-NULL
            //             TO (SF)-IB-OGG-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzIbOgg(ws.getGar().getGrzIbOgg());
        }
        else {
            // COB_CODE: MOVE GRZ-IB-OGG
            //             TO (SF)-IB-OGG(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzIbOgg(ws.getGar().getGrzIbOgg());
        }
        // COB_CODE: IF GRZ-DT-DECOR-NULL = HIGH-VALUES
        //                TO (SF)-DT-DECOR-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-DT-DECOR(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzDtDecor().getGrzDtDecorNullFormatted())) {
            // COB_CODE: MOVE GRZ-DT-DECOR-NULL
            //             TO (SF)-DT-DECOR-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzDtDecor().setWgrzDtDecorNull(ws.getGar().getGrzDtDecor().getGrzDtDecorNull());
        }
        else {
            // COB_CODE: MOVE GRZ-DT-DECOR
            //             TO (SF)-DT-DECOR(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzDtDecor().setWgrzDtDecor(ws.getGar().getGrzDtDecor().getGrzDtDecor());
        }
        // COB_CODE: IF GRZ-DT-SCAD-NULL = HIGH-VALUES
        //                TO (SF)-DT-SCAD-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-DT-SCAD(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzDtScad().getGrzDtScadNullFormatted())) {
            // COB_CODE: MOVE GRZ-DT-SCAD-NULL
            //             TO (SF)-DT-SCAD-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzDtScad().setWgrzDtScadNull(ws.getGar().getGrzDtScad().getGrzDtScadNull());
        }
        else {
            // COB_CODE: MOVE GRZ-DT-SCAD
            //             TO (SF)-DT-SCAD(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzDtScad().setWgrzDtScad(ws.getGar().getGrzDtScad().getGrzDtScad());
        }
        // COB_CODE: IF GRZ-COD-SEZ-NULL = HIGH-VALUES
        //                TO (SF)-COD-SEZ-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-COD-SEZ(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzCodSezFormatted())) {
            // COB_CODE: MOVE GRZ-COD-SEZ-NULL
            //             TO (SF)-COD-SEZ-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzCodSez(ws.getGar().getGrzCodSez());
        }
        else {
            // COB_CODE: MOVE GRZ-COD-SEZ
            //             TO (SF)-COD-SEZ(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzCodSez(ws.getGar().getGrzCodSez());
        }
        // COB_CODE: MOVE GRZ-COD-TARI
        //             TO (SF)-COD-TARI(IX-TAB-GRZ)
        ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzCodTari(ws.getGar().getGrzCodTari());
        // COB_CODE: IF GRZ-RAMO-BILA-NULL = HIGH-VALUES
        //                TO (SF)-RAMO-BILA-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-RAMO-BILA(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzRamoBilaFormatted())) {
            // COB_CODE: MOVE GRZ-RAMO-BILA-NULL
            //             TO (SF)-RAMO-BILA-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzRamoBila(ws.getGar().getGrzRamoBila());
        }
        else {
            // COB_CODE: MOVE GRZ-RAMO-BILA
            //             TO (SF)-RAMO-BILA(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzRamoBila(ws.getGar().getGrzRamoBila());
        }
        // COB_CODE: IF GRZ-DT-INI-VAL-TAR-NULL = HIGH-VALUES
        //                TO (SF)-DT-INI-VAL-TAR-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-DT-INI-VAL-TAR(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzDtIniValTar().getGrzDtIniValTarNullFormatted())) {
            // COB_CODE: MOVE GRZ-DT-INI-VAL-TAR-NULL
            //             TO (SF)-DT-INI-VAL-TAR-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzDtIniValTar().setWgrzDtIniValTarNull(ws.getGar().getGrzDtIniValTar().getGrzDtIniValTarNull());
        }
        else {
            // COB_CODE: MOVE GRZ-DT-INI-VAL-TAR
            //             TO (SF)-DT-INI-VAL-TAR(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzDtIniValTar().setWgrzDtIniValTar(ws.getGar().getGrzDtIniValTar().getGrzDtIniValTar());
        }
        // COB_CODE: IF GRZ-ID-1O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ID-1O-ASSTO-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-ID-1O-ASSTO(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzId1oAssto().getGrzId1oAsstoNullFormatted())) {
            // COB_CODE: MOVE GRZ-ID-1O-ASSTO-NULL
            //             TO (SF)-ID-1O-ASSTO-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzId1oAssto().setWgrzId1oAsstoNull(ws.getGar().getGrzId1oAssto().getGrzId1oAsstoNull());
        }
        else {
            // COB_CODE: MOVE GRZ-ID-1O-ASSTO
            //             TO (SF)-ID-1O-ASSTO(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzId1oAssto().setWgrzId1oAssto(ws.getGar().getGrzId1oAssto().getGrzId1oAssto());
        }
        // COB_CODE: IF GRZ-ID-2O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ID-2O-ASSTO-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-ID-2O-ASSTO(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzId2oAssto().getGrzId2oAsstoNullFormatted())) {
            // COB_CODE: MOVE GRZ-ID-2O-ASSTO-NULL
            //             TO (SF)-ID-2O-ASSTO-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzId2oAssto().setWgrzId2oAsstoNull(ws.getGar().getGrzId2oAssto().getGrzId2oAsstoNull());
        }
        else {
            // COB_CODE: MOVE GRZ-ID-2O-ASSTO
            //             TO (SF)-ID-2O-ASSTO(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzId2oAssto().setWgrzId2oAssto(ws.getGar().getGrzId2oAssto().getGrzId2oAssto());
        }
        // COB_CODE: IF GRZ-ID-3O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ID-3O-ASSTO-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-ID-3O-ASSTO(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzId3oAssto().getGrzId3oAsstoNullFormatted())) {
            // COB_CODE: MOVE GRZ-ID-3O-ASSTO-NULL
            //             TO (SF)-ID-3O-ASSTO-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzId3oAssto().setWgrzId3oAsstoNull(ws.getGar().getGrzId3oAssto().getGrzId3oAsstoNull());
        }
        else {
            // COB_CODE: MOVE GRZ-ID-3O-ASSTO
            //             TO (SF)-ID-3O-ASSTO(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzId3oAssto().setWgrzId3oAssto(ws.getGar().getGrzId3oAssto().getGrzId3oAssto());
        }
        // COB_CODE: IF GRZ-TP-GAR-NULL = HIGH-VALUES
        //                TO (SF)-TP-GAR-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-GAR(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTpGar().getGrzTpGarNullFormatted())) {
            // COB_CODE: MOVE GRZ-TP-GAR-NULL
            //             TO (SF)-TP-GAR-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzTpGar().setWgrzTpGarNull(ws.getGar().getGrzTpGar().getGrzTpGarNull());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-GAR
            //             TO (SF)-TP-GAR(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzTpGar().setWgrzTpGar(ws.getGar().getGrzTpGar().getGrzTpGar());
        }
        // COB_CODE: IF GRZ-TP-RSH-NULL = HIGH-VALUES
        //                TO (SF)-TP-RSH-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-RSH(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTpRshFormatted())) {
            // COB_CODE: MOVE GRZ-TP-RSH-NULL
            //             TO (SF)-TP-RSH-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpRsh(ws.getGar().getGrzTpRsh());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-RSH
            //             TO (SF)-TP-RSH(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpRsh(ws.getGar().getGrzTpRsh());
        }
        // COB_CODE: IF GRZ-TP-INVST-NULL = HIGH-VALUES
        //                TO (SF)-TP-INVST-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-INVST(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTpInvst().getGrzTpInvstNullFormatted())) {
            // COB_CODE: MOVE GRZ-TP-INVST-NULL
            //             TO (SF)-TP-INVST-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzTpInvst().setWgrzTpInvstNull(ws.getGar().getGrzTpInvst().getGrzTpInvstNull());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-INVST
            //             TO (SF)-TP-INVST(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzTpInvst().setWgrzTpInvst(ws.getGar().getGrzTpInvst().getGrzTpInvst());
        }
        // COB_CODE: IF GRZ-MOD-PAG-GARCOL-NULL = HIGH-VALUES
        //                TO (SF)-MOD-PAG-GARCOL-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-MOD-PAG-GARCOL(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzModPagGarcolFormatted())) {
            // COB_CODE: MOVE GRZ-MOD-PAG-GARCOL-NULL
            //             TO (SF)-MOD-PAG-GARCOL-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzModPagGarcol(ws.getGar().getGrzModPagGarcol());
        }
        else {
            // COB_CODE: MOVE GRZ-MOD-PAG-GARCOL
            //             TO (SF)-MOD-PAG-GARCOL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzModPagGarcol(ws.getGar().getGrzModPagGarcol());
        }
        // COB_CODE: IF GRZ-TP-PER-PRE-NULL = HIGH-VALUES
        //                TO (SF)-TP-PER-PRE-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-PER-PRE(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTpPerPreFormatted())) {
            // COB_CODE: MOVE GRZ-TP-PER-PRE-NULL
            //             TO (SF)-TP-PER-PRE-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpPerPre(ws.getGar().getGrzTpPerPre());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-PER-PRE
            //             TO (SF)-TP-PER-PRE(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpPerPre(ws.getGar().getGrzTpPerPre());
        }
        // COB_CODE: IF GRZ-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-AA-1O-ASSTO-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-ETA-AA-1O-ASSTO(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzEtaAa1oAssto().getGrzEtaAa1oAsstoNullFormatted())) {
            // COB_CODE: MOVE GRZ-ETA-AA-1O-ASSTO-NULL
            //             TO (SF)-ETA-AA-1O-ASSTO-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzEtaAa1oAssto().setWgrzEtaAa1oAsstoNull(ws.getGar().getGrzEtaAa1oAssto().getGrzEtaAa1oAsstoNull());
        }
        else {
            // COB_CODE: MOVE GRZ-ETA-AA-1O-ASSTO
            //             TO (SF)-ETA-AA-1O-ASSTO(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzEtaAa1oAssto().setWgrzEtaAa1oAssto(ws.getGar().getGrzEtaAa1oAssto().getGrzEtaAa1oAssto());
        }
        // COB_CODE: IF GRZ-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-MM-1O-ASSTO-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-ETA-MM-1O-ASSTO(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzEtaMm1oAssto().getGrzEtaMm1oAsstoNullFormatted())) {
            // COB_CODE: MOVE GRZ-ETA-MM-1O-ASSTO-NULL
            //             TO (SF)-ETA-MM-1O-ASSTO-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzEtaMm1oAssto().setWgrzEtaMm1oAsstoNull(ws.getGar().getGrzEtaMm1oAssto().getGrzEtaMm1oAsstoNull());
        }
        else {
            // COB_CODE: MOVE GRZ-ETA-MM-1O-ASSTO
            //             TO (SF)-ETA-MM-1O-ASSTO(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzEtaMm1oAssto().setWgrzEtaMm1oAssto(ws.getGar().getGrzEtaMm1oAssto().getGrzEtaMm1oAssto());
        }
        // COB_CODE: IF GRZ-ETA-AA-2O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-AA-2O-ASSTO-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-ETA-AA-2O-ASSTO(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzEtaAa2oAssto().getGrzEtaAa2oAsstoNullFormatted())) {
            // COB_CODE: MOVE GRZ-ETA-AA-2O-ASSTO-NULL
            //             TO (SF)-ETA-AA-2O-ASSTO-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzEtaAa2oAssto().setWgrzEtaAa2oAsstoNull(ws.getGar().getGrzEtaAa2oAssto().getGrzEtaAa2oAsstoNull());
        }
        else {
            // COB_CODE: MOVE GRZ-ETA-AA-2O-ASSTO
            //             TO (SF)-ETA-AA-2O-ASSTO(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzEtaAa2oAssto().setWgrzEtaAa2oAssto(ws.getGar().getGrzEtaAa2oAssto().getGrzEtaAa2oAssto());
        }
        // COB_CODE: IF GRZ-ETA-MM-2O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-MM-2O-ASSTO-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-ETA-MM-2O-ASSTO(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzEtaMm2oAssto().getGrzEtaMm2oAsstoNullFormatted())) {
            // COB_CODE: MOVE GRZ-ETA-MM-2O-ASSTO-NULL
            //             TO (SF)-ETA-MM-2O-ASSTO-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzEtaMm2oAssto().setWgrzEtaMm2oAsstoNull(ws.getGar().getGrzEtaMm2oAssto().getGrzEtaMm2oAsstoNull());
        }
        else {
            // COB_CODE: MOVE GRZ-ETA-MM-2O-ASSTO
            //             TO (SF)-ETA-MM-2O-ASSTO(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzEtaMm2oAssto().setWgrzEtaMm2oAssto(ws.getGar().getGrzEtaMm2oAssto().getGrzEtaMm2oAssto());
        }
        // COB_CODE: IF GRZ-ETA-AA-3O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-AA-3O-ASSTO-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-ETA-AA-3O-ASSTO(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzEtaAa3oAssto().getGrzEtaAa3oAsstoNullFormatted())) {
            // COB_CODE: MOVE GRZ-ETA-AA-3O-ASSTO-NULL
            //             TO (SF)-ETA-AA-3O-ASSTO-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzEtaAa3oAssto().setWgrzEtaAa3oAsstoNull(ws.getGar().getGrzEtaAa3oAssto().getGrzEtaAa3oAsstoNull());
        }
        else {
            // COB_CODE: MOVE GRZ-ETA-AA-3O-ASSTO
            //             TO (SF)-ETA-AA-3O-ASSTO(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzEtaAa3oAssto().setWgrzEtaAa3oAssto(ws.getGar().getGrzEtaAa3oAssto().getGrzEtaAa3oAssto());
        }
        // COB_CODE: IF GRZ-ETA-MM-3O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-MM-3O-ASSTO-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-ETA-MM-3O-ASSTO(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzEtaMm3oAssto().getGrzEtaMm3oAsstoNullFormatted())) {
            // COB_CODE: MOVE GRZ-ETA-MM-3O-ASSTO-NULL
            //             TO (SF)-ETA-MM-3O-ASSTO-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzEtaMm3oAssto().setWgrzEtaMm3oAsstoNull(ws.getGar().getGrzEtaMm3oAssto().getGrzEtaMm3oAsstoNull());
        }
        else {
            // COB_CODE: MOVE GRZ-ETA-MM-3O-ASSTO
            //             TO (SF)-ETA-MM-3O-ASSTO(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzEtaMm3oAssto().setWgrzEtaMm3oAssto(ws.getGar().getGrzEtaMm3oAssto().getGrzEtaMm3oAssto());
        }
        // COB_CODE: IF GRZ-TP-EMIS-PUR-NULL = HIGH-VALUES
        //                TO (SF)-TP-EMIS-PUR-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-EMIS-PUR(IX-TAB-GRZ)
        //           END-IF
        if (Conditions.eq(ws.getGar().getGrzTpEmisPur(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE GRZ-TP-EMIS-PUR-NULL
            //             TO (SF)-TP-EMIS-PUR-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpEmisPur(ws.getGar().getGrzTpEmisPur());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-EMIS-PUR
            //             TO (SF)-TP-EMIS-PUR(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpEmisPur(ws.getGar().getGrzTpEmisPur());
        }
        // COB_CODE: IF GRZ-ETA-A-SCAD-NULL = HIGH-VALUES
        //                TO (SF)-ETA-A-SCAD-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-ETA-A-SCAD(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzEtaAScad().getGrzEtaAScadNullFormatted())) {
            // COB_CODE: MOVE GRZ-ETA-A-SCAD-NULL
            //             TO (SF)-ETA-A-SCAD-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzEtaAScad().setWgrzEtaAScadNull(ws.getGar().getGrzEtaAScad().getGrzEtaAScadNull());
        }
        else {
            // COB_CODE: MOVE GRZ-ETA-A-SCAD
            //             TO (SF)-ETA-A-SCAD(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzEtaAScad().setWgrzEtaAScad(ws.getGar().getGrzEtaAScad().getGrzEtaAScad());
        }
        // COB_CODE: IF GRZ-TP-CALC-PRE-PRSTZ-NULL = HIGH-VALUES
        //                TO (SF)-TP-CALC-PRE-PRSTZ-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-CALC-PRE-PRSTZ(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTpCalcPrePrstzFormatted())) {
            // COB_CODE: MOVE GRZ-TP-CALC-PRE-PRSTZ-NULL
            //             TO (SF)-TP-CALC-PRE-PRSTZ-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpCalcPrePrstz(ws.getGar().getGrzTpCalcPrePrstz());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-CALC-PRE-PRSTZ
            //             TO (SF)-TP-CALC-PRE-PRSTZ(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpCalcPrePrstz(ws.getGar().getGrzTpCalcPrePrstz());
        }
        // COB_CODE: IF GRZ-TP-PRE-NULL = HIGH-VALUES
        //                TO (SF)-TP-PRE-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-PRE(IX-TAB-GRZ)
        //           END-IF
        if (Conditions.eq(ws.getGar().getGrzTpPre(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE GRZ-TP-PRE-NULL
            //             TO (SF)-TP-PRE-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpPre(ws.getGar().getGrzTpPre());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-PRE
            //             TO (SF)-TP-PRE(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpPre(ws.getGar().getGrzTpPre());
        }
        // COB_CODE: IF GRZ-TP-DUR-NULL = HIGH-VALUES
        //                TO (SF)-TP-DUR-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-DUR(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTpDurFormatted())) {
            // COB_CODE: MOVE GRZ-TP-DUR-NULL
            //             TO (SF)-TP-DUR-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpDur(ws.getGar().getGrzTpDur());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-DUR
            //             TO (SF)-TP-DUR(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpDur(ws.getGar().getGrzTpDur());
        }
        // COB_CODE: IF GRZ-DUR-AA-NULL = HIGH-VALUES
        //                TO (SF)-DUR-AA-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-DUR-AA(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzDurAa().getGrzDurAaNullFormatted())) {
            // COB_CODE: MOVE GRZ-DUR-AA-NULL
            //             TO (SF)-DUR-AA-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzDurAa().setWgrzDurAaNull(ws.getGar().getGrzDurAa().getGrzDurAaNull());
        }
        else {
            // COB_CODE: MOVE GRZ-DUR-AA
            //             TO (SF)-DUR-AA(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzDurAa().setWgrzDurAa(ws.getGar().getGrzDurAa().getGrzDurAa());
        }
        // COB_CODE: IF GRZ-DUR-MM-NULL = HIGH-VALUES
        //                TO (SF)-DUR-MM-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-DUR-MM(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzDurMm().getGrzDurMmNullFormatted())) {
            // COB_CODE: MOVE GRZ-DUR-MM-NULL
            //             TO (SF)-DUR-MM-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzDurMm().setWgrzDurMmNull(ws.getGar().getGrzDurMm().getGrzDurMmNull());
        }
        else {
            // COB_CODE: MOVE GRZ-DUR-MM
            //             TO (SF)-DUR-MM(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzDurMm().setWgrzDurMm(ws.getGar().getGrzDurMm().getGrzDurMm());
        }
        // COB_CODE: IF GRZ-DUR-GG-NULL = HIGH-VALUES
        //                TO (SF)-DUR-GG-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-DUR-GG(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzDurGg().getGrzDurGgNullFormatted())) {
            // COB_CODE: MOVE GRZ-DUR-GG-NULL
            //             TO (SF)-DUR-GG-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzDurGg().setWgrzDurGgNull(ws.getGar().getGrzDurGg().getGrzDurGgNull());
        }
        else {
            // COB_CODE: MOVE GRZ-DUR-GG
            //             TO (SF)-DUR-GG(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzDurGg().setWgrzDurGg(ws.getGar().getGrzDurGg().getGrzDurGg());
        }
        // COB_CODE: IF GRZ-NUM-AA-PAG-PRE-NULL = HIGH-VALUES
        //                TO (SF)-NUM-AA-PAG-PRE-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-NUM-AA-PAG-PRE(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzNumAaPagPre().getGrzNumAaPagPreNullFormatted())) {
            // COB_CODE: MOVE GRZ-NUM-AA-PAG-PRE-NULL
            //             TO (SF)-NUM-AA-PAG-PRE-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzNumAaPagPre().setWgrzNumAaPagPreNull(ws.getGar().getGrzNumAaPagPre().getGrzNumAaPagPreNull());
        }
        else {
            // COB_CODE: MOVE GRZ-NUM-AA-PAG-PRE
            //             TO (SF)-NUM-AA-PAG-PRE(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzNumAaPagPre().setWgrzNumAaPagPre(ws.getGar().getGrzNumAaPagPre().getGrzNumAaPagPre());
        }
        // COB_CODE: IF GRZ-AA-PAG-PRE-UNI-NULL = HIGH-VALUES
        //                TO (SF)-AA-PAG-PRE-UNI-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-AA-PAG-PRE-UNI(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzAaPagPreUni().getGrzAaPagPreUniNullFormatted())) {
            // COB_CODE: MOVE GRZ-AA-PAG-PRE-UNI-NULL
            //             TO (SF)-AA-PAG-PRE-UNI-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzAaPagPreUni().setWgrzAaPagPreUniNull(ws.getGar().getGrzAaPagPreUni().getGrzAaPagPreUniNull());
        }
        else {
            // COB_CODE: MOVE GRZ-AA-PAG-PRE-UNI
            //             TO (SF)-AA-PAG-PRE-UNI(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzAaPagPreUni().setWgrzAaPagPreUni(ws.getGar().getGrzAaPagPreUni().getGrzAaPagPreUni());
        }
        // COB_CODE: IF GRZ-MM-PAG-PRE-UNI-NULL = HIGH-VALUES
        //                TO (SF)-MM-PAG-PRE-UNI-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-MM-PAG-PRE-UNI(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzMmPagPreUni().getGrzMmPagPreUniNullFormatted())) {
            // COB_CODE: MOVE GRZ-MM-PAG-PRE-UNI-NULL
            //             TO (SF)-MM-PAG-PRE-UNI-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzMmPagPreUni().setWgrzMmPagPreUniNull(ws.getGar().getGrzMmPagPreUni().getGrzMmPagPreUniNull());
        }
        else {
            // COB_CODE: MOVE GRZ-MM-PAG-PRE-UNI
            //             TO (SF)-MM-PAG-PRE-UNI(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzMmPagPreUni().setWgrzMmPagPreUni(ws.getGar().getGrzMmPagPreUni().getGrzMmPagPreUni());
        }
        // COB_CODE: IF GRZ-FRAZ-INI-EROG-REN-NULL = HIGH-VALUES
        //                TO (SF)-FRAZ-INI-EROG-REN-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-FRAZ-INI-EROG-REN(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzFrazIniErogRen().getGrzFrazIniErogRenNullFormatted())) {
            // COB_CODE: MOVE GRZ-FRAZ-INI-EROG-REN-NULL
            //             TO (SF)-FRAZ-INI-EROG-REN-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzFrazIniErogRen().setWgrzFrazIniErogRenNull(ws.getGar().getGrzFrazIniErogRen().getGrzFrazIniErogRenNull());
        }
        else {
            // COB_CODE: MOVE GRZ-FRAZ-INI-EROG-REN
            //             TO (SF)-FRAZ-INI-EROG-REN(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzFrazIniErogRen().setWgrzFrazIniErogRen(ws.getGar().getGrzFrazIniErogRen().getGrzFrazIniErogRen());
        }
        // COB_CODE: IF GRZ-MM-1O-RAT-NULL = HIGH-VALUES
        //                TO (SF)-MM-1O-RAT-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-MM-1O-RAT(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzMm1oRat().getGrzMm1oRatNullFormatted())) {
            // COB_CODE: MOVE GRZ-MM-1O-RAT-NULL
            //             TO (SF)-MM-1O-RAT-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzMm1oRat().setWgrzMm1oRatNull(ws.getGar().getGrzMm1oRat().getGrzMm1oRatNull());
        }
        else {
            // COB_CODE: MOVE GRZ-MM-1O-RAT
            //             TO (SF)-MM-1O-RAT(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzMm1oRat().setWgrzMm1oRat(ws.getGar().getGrzMm1oRat().getGrzMm1oRat());
        }
        // COB_CODE: IF GRZ-PC-1O-RAT-NULL = HIGH-VALUES
        //                TO (SF)-PC-1O-RAT-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-PC-1O-RAT(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzPc1oRat().getGrzPc1oRatNullFormatted())) {
            // COB_CODE: MOVE GRZ-PC-1O-RAT-NULL
            //             TO (SF)-PC-1O-RAT-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzPc1oRat().setWgrzPc1oRatNull(ws.getGar().getGrzPc1oRat().getGrzPc1oRatNull());
        }
        else {
            // COB_CODE: MOVE GRZ-PC-1O-RAT
            //             TO (SF)-PC-1O-RAT(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzPc1oRat().setWgrzPc1oRat(Trunc.toDecimal(ws.getGar().getGrzPc1oRat().getGrzPc1oRat(), 6, 3));
        }
        // COB_CODE: IF GRZ-TP-PRSTZ-ASSTA-NULL = HIGH-VALUES
        //                TO (SF)-TP-PRSTZ-ASSTA-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-PRSTZ-ASSTA(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTpPrstzAsstaFormatted())) {
            // COB_CODE: MOVE GRZ-TP-PRSTZ-ASSTA-NULL
            //             TO (SF)-TP-PRSTZ-ASSTA-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpPrstzAssta(ws.getGar().getGrzTpPrstzAssta());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-PRSTZ-ASSTA
            //             TO (SF)-TP-PRSTZ-ASSTA(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpPrstzAssta(ws.getGar().getGrzTpPrstzAssta());
        }
        // COB_CODE: IF GRZ-DT-END-CARZ-NULL = HIGH-VALUES
        //                TO (SF)-DT-END-CARZ-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-DT-END-CARZ(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzDtEndCarz().getGrzDtEndCarzNullFormatted())) {
            // COB_CODE: MOVE GRZ-DT-END-CARZ-NULL
            //             TO (SF)-DT-END-CARZ-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzDtEndCarz().setWgrzDtEndCarzNull(ws.getGar().getGrzDtEndCarz().getGrzDtEndCarzNull());
        }
        else {
            // COB_CODE: MOVE GRZ-DT-END-CARZ
            //             TO (SF)-DT-END-CARZ(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzDtEndCarz().setWgrzDtEndCarz(ws.getGar().getGrzDtEndCarz().getGrzDtEndCarz());
        }
        // COB_CODE: IF GRZ-PC-RIP-PRE-NULL = HIGH-VALUES
        //                TO (SF)-PC-RIP-PRE-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-PC-RIP-PRE(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzPcRipPre().getGrzPcRipPreNullFormatted())) {
            // COB_CODE: MOVE GRZ-PC-RIP-PRE-NULL
            //             TO (SF)-PC-RIP-PRE-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzPcRipPre().setWgrzPcRipPreNull(ws.getGar().getGrzPcRipPre().getGrzPcRipPreNull());
        }
        else {
            // COB_CODE: MOVE GRZ-PC-RIP-PRE
            //             TO (SF)-PC-RIP-PRE(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzPcRipPre().setWgrzPcRipPre(Trunc.toDecimal(ws.getGar().getGrzPcRipPre().getGrzPcRipPre(), 6, 3));
        }
        // COB_CODE: IF GRZ-COD-FND-NULL = HIGH-VALUES
        //                TO (SF)-COD-FND-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-COD-FND(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzCodFndFormatted())) {
            // COB_CODE: MOVE GRZ-COD-FND-NULL
            //             TO (SF)-COD-FND-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzCodFnd(ws.getGar().getGrzCodFnd());
        }
        else {
            // COB_CODE: MOVE GRZ-COD-FND
            //             TO (SF)-COD-FND(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzCodFnd(ws.getGar().getGrzCodFnd());
        }
        // COB_CODE: IF GRZ-AA-REN-CER-NULL = HIGH-VALUES
        //                TO (SF)-AA-REN-CER-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-AA-REN-CER(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzAaRenCerFormatted())) {
            // COB_CODE: MOVE GRZ-AA-REN-CER-NULL
            //             TO (SF)-AA-REN-CER-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzAaRenCer(ws.getGar().getGrzAaRenCer());
        }
        else {
            // COB_CODE: MOVE GRZ-AA-REN-CER
            //             TO (SF)-AA-REN-CER(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzAaRenCer(ws.getGar().getGrzAaRenCer());
        }
        // COB_CODE: IF GRZ-PC-REVRSB-NULL = HIGH-VALUES
        //                TO (SF)-PC-REVRSB-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-PC-REVRSB(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzPcRevrsb().getGrzPcRevrsbNullFormatted())) {
            // COB_CODE: MOVE GRZ-PC-REVRSB-NULL
            //             TO (SF)-PC-REVRSB-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzPcRevrsb().setWgrzPcRevrsbNull(ws.getGar().getGrzPcRevrsb().getGrzPcRevrsbNull());
        }
        else {
            // COB_CODE: MOVE GRZ-PC-REVRSB
            //             TO (SF)-PC-REVRSB(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzPcRevrsb().setWgrzPcRevrsb(Trunc.toDecimal(ws.getGar().getGrzPcRevrsb().getGrzPcRevrsb(), 6, 3));
        }
        // COB_CODE: IF GRZ-TP-PC-RIP-NULL = HIGH-VALUES
        //                TO (SF)-TP-PC-RIP-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-PC-RIP(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTpPcRipFormatted())) {
            // COB_CODE: MOVE GRZ-TP-PC-RIP-NULL
            //             TO (SF)-TP-PC-RIP-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpPcRip(ws.getGar().getGrzTpPcRip());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-PC-RIP
            //             TO (SF)-TP-PC-RIP(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpPcRip(ws.getGar().getGrzTpPcRip());
        }
        // COB_CODE: IF GRZ-PC-OPZ-NULL = HIGH-VALUES
        //                TO (SF)-PC-OPZ-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-PC-OPZ(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzPcOpz().getGrzPcOpzNullFormatted())) {
            // COB_CODE: MOVE GRZ-PC-OPZ-NULL
            //             TO (SF)-PC-OPZ-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzPcOpz().setWgrzPcOpzNull(ws.getGar().getGrzPcOpz().getGrzPcOpzNull());
        }
        else {
            // COB_CODE: MOVE GRZ-PC-OPZ
            //             TO (SF)-PC-OPZ(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzPcOpz().setWgrzPcOpz(Trunc.toDecimal(ws.getGar().getGrzPcOpz().getGrzPcOpz(), 6, 3));
        }
        // COB_CODE: IF GRZ-TP-IAS-NULL = HIGH-VALUES
        //                TO (SF)-TP-IAS-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-IAS(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTpIasFormatted())) {
            // COB_CODE: MOVE GRZ-TP-IAS-NULL
            //             TO (SF)-TP-IAS-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpIas(ws.getGar().getGrzTpIas());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-IAS
            //             TO (SF)-TP-IAS(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpIas(ws.getGar().getGrzTpIas());
        }
        // COB_CODE: IF GRZ-TP-STAB-NULL = HIGH-VALUES
        //                TO (SF)-TP-STAB-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-STAB(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTpStabFormatted())) {
            // COB_CODE: MOVE GRZ-TP-STAB-NULL
            //             TO (SF)-TP-STAB-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpStab(ws.getGar().getGrzTpStab());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-STAB
            //             TO (SF)-TP-STAB(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpStab(ws.getGar().getGrzTpStab());
        }
        // COB_CODE: IF GRZ-TP-ADEG-PRE-NULL = HIGH-VALUES
        //                TO (SF)-TP-ADEG-PRE-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-ADEG-PRE(IX-TAB-GRZ)
        //           END-IF
        if (Conditions.eq(ws.getGar().getGrzTpAdegPre(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE GRZ-TP-ADEG-PRE-NULL
            //             TO (SF)-TP-ADEG-PRE-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpAdegPre(ws.getGar().getGrzTpAdegPre());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-ADEG-PRE
            //             TO (SF)-TP-ADEG-PRE(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpAdegPre(ws.getGar().getGrzTpAdegPre());
        }
        // COB_CODE: IF GRZ-DT-VARZ-TP-IAS-NULL = HIGH-VALUES
        //                TO (SF)-DT-VARZ-TP-IAS-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-DT-VARZ-TP-IAS(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzDtVarzTpIas().getGrzDtVarzTpIasNullFormatted())) {
            // COB_CODE: MOVE GRZ-DT-VARZ-TP-IAS-NULL
            //             TO (SF)-DT-VARZ-TP-IAS-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzDtVarzTpIas().setWgrzDtVarzTpIasNull(ws.getGar().getGrzDtVarzTpIas().getGrzDtVarzTpIasNull());
        }
        else {
            // COB_CODE: MOVE GRZ-DT-VARZ-TP-IAS
            //             TO (SF)-DT-VARZ-TP-IAS(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzDtVarzTpIas().setWgrzDtVarzTpIas(ws.getGar().getGrzDtVarzTpIas().getGrzDtVarzTpIas());
        }
        // COB_CODE: IF GRZ-FRAZ-DECR-CPT-NULL = HIGH-VALUES
        //                TO (SF)-FRAZ-DECR-CPT-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-FRAZ-DECR-CPT(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzFrazDecrCpt().getGrzFrazDecrCptNullFormatted())) {
            // COB_CODE: MOVE GRZ-FRAZ-DECR-CPT-NULL
            //             TO (SF)-FRAZ-DECR-CPT-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzFrazDecrCpt().setWgrzFrazDecrCptNull(ws.getGar().getGrzFrazDecrCpt().getGrzFrazDecrCptNull());
        }
        else {
            // COB_CODE: MOVE GRZ-FRAZ-DECR-CPT
            //             TO (SF)-FRAZ-DECR-CPT(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzFrazDecrCpt().setWgrzFrazDecrCpt(ws.getGar().getGrzFrazDecrCpt().getGrzFrazDecrCpt());
        }
        // COB_CODE: IF GRZ-COD-TRAT-RIASS-NULL = HIGH-VALUES
        //                TO (SF)-COD-TRAT-RIASS-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-COD-TRAT-RIASS(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzCodTratRiassFormatted())) {
            // COB_CODE: MOVE GRZ-COD-TRAT-RIASS-NULL
            //             TO (SF)-COD-TRAT-RIASS-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzCodTratRiass(ws.getGar().getGrzCodTratRiass());
        }
        else {
            // COB_CODE: MOVE GRZ-COD-TRAT-RIASS
            //             TO (SF)-COD-TRAT-RIASS(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzCodTratRiass(ws.getGar().getGrzCodTratRiass());
        }
        // COB_CODE: IF GRZ-TP-DT-EMIS-RIASS-NULL = HIGH-VALUES
        //                TO (SF)-TP-DT-EMIS-RIASS-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-DT-EMIS-RIASS(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTpDtEmisRiassFormatted())) {
            // COB_CODE: MOVE GRZ-TP-DT-EMIS-RIASS-NULL
            //             TO (SF)-TP-DT-EMIS-RIASS-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpDtEmisRiass(ws.getGar().getGrzTpDtEmisRiass());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-DT-EMIS-RIASS
            //             TO (SF)-TP-DT-EMIS-RIASS(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpDtEmisRiass(ws.getGar().getGrzTpDtEmisRiass());
        }
        // COB_CODE: IF GRZ-TP-CESS-RIASS-NULL = HIGH-VALUES
        //                TO (SF)-TP-CESS-RIASS-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-CESS-RIASS(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTpCessRiassFormatted())) {
            // COB_CODE: MOVE GRZ-TP-CESS-RIASS-NULL
            //             TO (SF)-TP-CESS-RIASS-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpCessRiass(ws.getGar().getGrzTpCessRiass());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-CESS-RIASS
            //             TO (SF)-TP-CESS-RIASS(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpCessRiass(ws.getGar().getGrzTpCessRiass());
        }
        // COB_CODE: MOVE GRZ-DS-RIGA
        //             TO (SF)-DS-RIGA(IX-TAB-GRZ)
        ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzDsRiga(ws.getGar().getGrzDsRiga());
        // COB_CODE: MOVE GRZ-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL(IX-TAB-GRZ)
        ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzDsOperSql(ws.getGar().getGrzDsOperSql());
        // COB_CODE: MOVE GRZ-DS-VER
        //             TO (SF)-DS-VER(IX-TAB-GRZ)
        ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzDsVer(ws.getGar().getGrzDsVer());
        // COB_CODE: MOVE GRZ-DS-TS-INI-CPTZ
        //             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-GRZ)
        ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzDsTsIniCptz(ws.getGar().getGrzDsTsIniCptz());
        // COB_CODE: MOVE GRZ-DS-TS-END-CPTZ
        //             TO (SF)-DS-TS-END-CPTZ(IX-TAB-GRZ)
        ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzDsTsEndCptz(ws.getGar().getGrzDsTsEndCptz());
        // COB_CODE: MOVE GRZ-DS-UTENTE
        //             TO (SF)-DS-UTENTE(IX-TAB-GRZ)
        ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzDsUtente(ws.getGar().getGrzDsUtente());
        // COB_CODE: MOVE GRZ-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB(IX-TAB-GRZ)
        ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzDsStatoElab(ws.getGar().getGrzDsStatoElab());
        // COB_CODE: IF GRZ-AA-STAB-NULL = HIGH-VALUES
        //                TO (SF)-AA-STAB-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-AA-STAB(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzAaStab().getGrzAaStabNullFormatted())) {
            // COB_CODE: MOVE GRZ-AA-STAB-NULL
            //             TO (SF)-AA-STAB-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzAaStab().setWgrzAaStabNull(ws.getGar().getGrzAaStab().getGrzAaStabNull());
        }
        else {
            // COB_CODE: MOVE GRZ-AA-STAB
            //             TO (SF)-AA-STAB(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzAaStab().setWgrzAaStab(ws.getGar().getGrzAaStab().getGrzAaStab());
        }
        // COB_CODE: IF GRZ-TS-STAB-LIMITATA-NULL = HIGH-VALUES
        //                TO (SF)-TS-STAB-LIMITATA-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TS-STAB-LIMITATA(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTsStabLimitata().getGrzTsStabLimitataNullFormatted())) {
            // COB_CODE: MOVE GRZ-TS-STAB-LIMITATA-NULL
            //             TO (SF)-TS-STAB-LIMITATA-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzTsStabLimitata().setWgrzTsStabLimitataNull(ws.getGar().getGrzTsStabLimitata().getGrzTsStabLimitataNull());
        }
        else {
            // COB_CODE: MOVE GRZ-TS-STAB-LIMITATA
            //             TO (SF)-TS-STAB-LIMITATA(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzTsStabLimitata().setWgrzTsStabLimitata(Trunc.toDecimal(ws.getGar().getGrzTsStabLimitata().getGrzTsStabLimitata(), 14, 9));
        }
        // COB_CODE: IF GRZ-DT-PRESC-NULL = HIGH-VALUES
        //                TO (SF)-DT-PRESC-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-DT-PRESC(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzDtPresc().getGrzDtPrescNullFormatted())) {
            // COB_CODE: MOVE GRZ-DT-PRESC-NULL
            //             TO (SF)-DT-PRESC-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzDtPresc().setWgrzDtPrescNull(ws.getGar().getGrzDtPresc().getGrzDtPrescNull());
        }
        else {
            // COB_CODE: MOVE GRZ-DT-PRESC
            //             TO (SF)-DT-PRESC(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().getWgrzDtPresc().setWgrzDtPresc(ws.getGar().getGrzDtPresc().getGrzDtPresc());
        }
        // COB_CODE: IF GRZ-RSH-INVST-NULL = HIGH-VALUES
        //                TO (SF)-RSH-INVST-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-RSH-INVST(IX-TAB-GRZ)
        //           END-IF
        if (Conditions.eq(ws.getGar().getGrzRshInvst(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE GRZ-RSH-INVST-NULL
            //             TO (SF)-RSH-INVST-NULL(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzRshInvst(ws.getGar().getGrzRshInvst());
        }
        else {
            // COB_CODE: MOVE GRZ-RSH-INVST
            //             TO (SF)-RSH-INVST(IX-TAB-GRZ)
            ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzRshInvst(ws.getGar().getGrzRshInvst());
        }
        // COB_CODE: MOVE GRZ-TP-RAMO-BILA
        //             TO (SF)-TP-RAMO-BILA(IX-TAB-GRZ).
        ws.getAreeAppoggio().getDgrzTabGar(ws.getIxIndici().getIxTabGrz()).getLccvgrz1().getDati().setWgrzTpRamoBila(ws.getGar().getGrzTpRamoBila());
    }

    /**Original name: VALORIZZA-OUTPUT-TGA<br>
	 * <pre>--> TRANCHE DI GARANZIA
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVTGA3
	 *    ULTIMO AGG. 03 GIU 2019
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputTga() {
        // COB_CODE: MOVE TGA-ID-TRCH-DI-GAR
        //             TO (SF)-ID-PTF(IX-TAB-TGA)
        ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().setIdPtf(ws.getTrchDiGar().getTgaIdTrchDiGar());
        // COB_CODE: MOVE TGA-ID-TRCH-DI-GAR
        //             TO (SF)-ID-TRCH-DI-GAR(IX-TAB-TGA)
        ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaIdTrchDiGar(ws.getTrchDiGar().getTgaIdTrchDiGar());
        // COB_CODE: MOVE TGA-ID-GAR
        //             TO (SF)-ID-GAR(IX-TAB-TGA)
        ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaIdGar(ws.getTrchDiGar().getTgaIdGar());
        // COB_CODE: MOVE TGA-ID-ADES
        //             TO (SF)-ID-ADES(IX-TAB-TGA)
        ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaIdAdes(ws.getTrchDiGar().getTgaIdAdes());
        // COB_CODE: MOVE TGA-ID-POLI
        //             TO (SF)-ID-POLI(IX-TAB-TGA)
        ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaIdPoli(ws.getTrchDiGar().getTgaIdPoli());
        // COB_CODE: MOVE TGA-ID-MOVI-CRZ
        //             TO (SF)-ID-MOVI-CRZ(IX-TAB-TGA)
        ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaIdMoviCrz(ws.getTrchDiGar().getTgaIdMoviCrz());
        // COB_CODE: IF TGA-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ID-MOVI-CHIU(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaIdMoviChiu().getTgaIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE TGA-ID-MOVI-CHIU-NULL
            //             TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaIdMoviChiu().setWtgaIdMoviChiuNull(ws.getTrchDiGar().getTgaIdMoviChiu().getTgaIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE TGA-ID-MOVI-CHIU
            //             TO (SF)-ID-MOVI-CHIU(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaIdMoviChiu().setWtgaIdMoviChiu(ws.getTrchDiGar().getTgaIdMoviChiu().getTgaIdMoviChiu());
        }
        // COB_CODE: MOVE TGA-DT-INI-EFF
        //             TO (SF)-DT-INI-EFF(IX-TAB-TGA)
        ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaDtIniEff(ws.getTrchDiGar().getTgaDtIniEff());
        // COB_CODE: MOVE TGA-DT-END-EFF
        //             TO (SF)-DT-END-EFF(IX-TAB-TGA)
        ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaDtEndEff(ws.getTrchDiGar().getTgaDtEndEff());
        // COB_CODE: MOVE TGA-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA(IX-TAB-TGA)
        ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaCodCompAnia(ws.getTrchDiGar().getTgaCodCompAnia());
        // COB_CODE: MOVE TGA-DT-DECOR
        //             TO (SF)-DT-DECOR(IX-TAB-TGA)
        ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaDtDecor(ws.getTrchDiGar().getTgaDtDecor());
        // COB_CODE: IF TGA-DT-SCAD-NULL = HIGH-VALUES
        //                TO (SF)-DT-SCAD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-SCAD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtScad().getTgaDtScadNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-SCAD-NULL
            //             TO (SF)-DT-SCAD-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaDtScad().setWtgaDtScadNull(ws.getTrchDiGar().getTgaDtScad().getTgaDtScadNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-SCAD
            //             TO (SF)-DT-SCAD(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaDtScad().setWtgaDtScad(ws.getTrchDiGar().getTgaDtScad().getTgaDtScad());
        }
        // COB_CODE: IF TGA-IB-OGG-NULL = HIGH-VALUES
        //                TO (SF)-IB-OGG-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IB-OGG(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaIbOgg(), TrchDiGarIvvs0216.Len.TGA_IB_OGG)) {
            // COB_CODE: MOVE TGA-IB-OGG-NULL
            //             TO (SF)-IB-OGG-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaIbOgg(ws.getTrchDiGar().getTgaIbOgg());
        }
        else {
            // COB_CODE: MOVE TGA-IB-OGG
            //             TO (SF)-IB-OGG(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaIbOgg(ws.getTrchDiGar().getTgaIbOgg());
        }
        // COB_CODE: MOVE TGA-TP-RGM-FISC
        //             TO (SF)-TP-RGM-FISC(IX-TAB-TGA)
        ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaTpRgmFisc(ws.getTrchDiGar().getTgaTpRgmFisc());
        // COB_CODE: IF TGA-DT-EMIS-NULL = HIGH-VALUES
        //                TO (SF)-DT-EMIS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-EMIS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtEmis().getTgaDtEmisNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-EMIS-NULL
            //             TO (SF)-DT-EMIS-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaDtEmis().setWtgaDtEmisNull(ws.getTrchDiGar().getTgaDtEmis().getTgaDtEmisNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-EMIS
            //             TO (SF)-DT-EMIS(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaDtEmis().setWtgaDtEmis(ws.getTrchDiGar().getTgaDtEmis().getTgaDtEmis());
        }
        // COB_CODE: MOVE TGA-TP-TRCH
        //             TO (SF)-TP-TRCH(IX-TAB-TGA)
        ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaTpTrch(ws.getTrchDiGar().getTgaTpTrch());
        // COB_CODE: IF TGA-DUR-AA-NULL = HIGH-VALUES
        //                TO (SF)-DUR-AA-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DUR-AA(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDurAa().getTgaDurAaNullFormatted())) {
            // COB_CODE: MOVE TGA-DUR-AA-NULL
            //             TO (SF)-DUR-AA-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaDurAa().setWtgaDurAaNull(ws.getTrchDiGar().getTgaDurAa().getTgaDurAaNull());
        }
        else {
            // COB_CODE: MOVE TGA-DUR-AA
            //             TO (SF)-DUR-AA(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaDurAa().setWtgaDurAa(ws.getTrchDiGar().getTgaDurAa().getTgaDurAa());
        }
        // COB_CODE: IF TGA-DUR-MM-NULL = HIGH-VALUES
        //                TO (SF)-DUR-MM-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DUR-MM(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDurMm().getTgaDurMmNullFormatted())) {
            // COB_CODE: MOVE TGA-DUR-MM-NULL
            //             TO (SF)-DUR-MM-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaDurMm().setWtgaDurMmNull(ws.getTrchDiGar().getTgaDurMm().getTgaDurMmNull());
        }
        else {
            // COB_CODE: MOVE TGA-DUR-MM
            //             TO (SF)-DUR-MM(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaDurMm().setWtgaDurMm(ws.getTrchDiGar().getTgaDurMm().getTgaDurMm());
        }
        // COB_CODE: IF TGA-DUR-GG-NULL = HIGH-VALUES
        //                TO (SF)-DUR-GG-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DUR-GG(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDurGg().getTgaDurGgNullFormatted())) {
            // COB_CODE: MOVE TGA-DUR-GG-NULL
            //             TO (SF)-DUR-GG-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaDurGg().setWtgaDurGgNull(ws.getTrchDiGar().getTgaDurGg().getTgaDurGgNull());
        }
        else {
            // COB_CODE: MOVE TGA-DUR-GG
            //             TO (SF)-DUR-GG(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaDurGg().setWtgaDurGg(ws.getTrchDiGar().getTgaDurGg().getTgaDurGg());
        }
        // COB_CODE: IF TGA-PRE-CASO-MOR-NULL = HIGH-VALUES
        //                TO (SF)-PRE-CASO-MOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-CASO-MOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreCasoMor().getTgaPreCasoMorNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-CASO-MOR-NULL
            //             TO (SF)-PRE-CASO-MOR-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPreCasoMor().setWtgaPreCasoMorNull(ws.getTrchDiGar().getTgaPreCasoMor().getTgaPreCasoMorNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-CASO-MOR
            //             TO (SF)-PRE-CASO-MOR(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPreCasoMor().setWtgaPreCasoMor(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreCasoMor().getTgaPreCasoMor(), 15, 3));
        }
        // COB_CODE: IF TGA-PC-INTR-RIAT-NULL = HIGH-VALUES
        //                TO (SF)-PC-INTR-RIAT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PC-INTR-RIAT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPcIntrRiat().getTgaPcIntrRiatNullFormatted())) {
            // COB_CODE: MOVE TGA-PC-INTR-RIAT-NULL
            //             TO (SF)-PC-INTR-RIAT-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPcIntrRiat().setWtgaPcIntrRiatNull(ws.getTrchDiGar().getTgaPcIntrRiat().getTgaPcIntrRiatNull());
        }
        else {
            // COB_CODE: MOVE TGA-PC-INTR-RIAT
            //             TO (SF)-PC-INTR-RIAT(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPcIntrRiat().setWtgaPcIntrRiat(Trunc.toDecimal(ws.getTrchDiGar().getTgaPcIntrRiat().getTgaPcIntrRiat(), 6, 3));
        }
        // COB_CODE: IF TGA-IMP-BNS-ANTIC-NULL = HIGH-VALUES
        //                TO (SF)-IMP-BNS-ANTIC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-BNS-ANTIC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpBnsAntic().getTgaImpBnsAnticNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-BNS-ANTIC-NULL
            //             TO (SF)-IMP-BNS-ANTIC-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpBnsAntic().setWtgaImpBnsAnticNull(ws.getTrchDiGar().getTgaImpBnsAntic().getTgaImpBnsAnticNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-BNS-ANTIC
            //             TO (SF)-IMP-BNS-ANTIC(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpBnsAntic().setWtgaImpBnsAntic(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpBnsAntic().getTgaImpBnsAntic(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-INI-NET-NULL = HIGH-VALUES
        //                TO (SF)-PRE-INI-NET-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-INI-NET(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreIniNet().getTgaPreIniNetNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-INI-NET-NULL
            //             TO (SF)-PRE-INI-NET-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPreIniNet().setWtgaPreIniNetNull(ws.getTrchDiGar().getTgaPreIniNet().getTgaPreIniNetNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-INI-NET
            //             TO (SF)-PRE-INI-NET(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPreIniNet().setWtgaPreIniNet(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreIniNet().getTgaPreIniNet(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-PP-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRE-PP-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-PP-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrePpIni().getTgaPrePpIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-PP-INI-NULL
            //             TO (SF)-PRE-PP-INI-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPrePpIni().setWtgaPrePpIniNull(ws.getTrchDiGar().getTgaPrePpIni().getTgaPrePpIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-PP-INI
            //             TO (SF)-PRE-PP-INI(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPrePpIni().setWtgaPrePpIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrePpIni().getTgaPrePpIni(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-PP-ULT-NULL = HIGH-VALUES
        //                TO (SF)-PRE-PP-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-PP-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrePpUlt().getTgaPrePpUltNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-PP-ULT-NULL
            //             TO (SF)-PRE-PP-ULT-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPrePpUlt().setWtgaPrePpUltNull(ws.getTrchDiGar().getTgaPrePpUlt().getTgaPrePpUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-PP-ULT
            //             TO (SF)-PRE-PP-ULT(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPrePpUlt().setWtgaPrePpUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrePpUlt().getTgaPrePpUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-TARI-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRE-TARI-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-TARI-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreTariIni().getTgaPreTariIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-TARI-INI-NULL
            //             TO (SF)-PRE-TARI-INI-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPreTariIni().setWtgaPreTariIniNull(ws.getTrchDiGar().getTgaPreTariIni().getTgaPreTariIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-TARI-INI
            //             TO (SF)-PRE-TARI-INI(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPreTariIni().setWtgaPreTariIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreTariIni().getTgaPreTariIni(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-TARI-ULT-NULL = HIGH-VALUES
        //                TO (SF)-PRE-TARI-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-TARI-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreTariUlt().getTgaPreTariUltNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-TARI-ULT-NULL
            //             TO (SF)-PRE-TARI-ULT-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPreTariUlt().setWtgaPreTariUltNull(ws.getTrchDiGar().getTgaPreTariUlt().getTgaPreTariUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-TARI-ULT
            //             TO (SF)-PRE-TARI-ULT(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPreTariUlt().setWtgaPreTariUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreTariUlt().getTgaPreTariUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-INVRIO-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRE-INVRIO-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-INVRIO-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreInvrioIni().getTgaPreInvrioIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-INVRIO-INI-NULL
            //             TO (SF)-PRE-INVRIO-INI-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPreInvrioIni().setWtgaPreInvrioIniNull(ws.getTrchDiGar().getTgaPreInvrioIni().getTgaPreInvrioIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-INVRIO-INI
            //             TO (SF)-PRE-INVRIO-INI(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPreInvrioIni().setWtgaPreInvrioIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreInvrioIni().getTgaPreInvrioIni(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-INVRIO-ULT-NULL = HIGH-VALUES
        //                TO (SF)-PRE-INVRIO-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-INVRIO-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreInvrioUlt().getTgaPreInvrioUltNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-INVRIO-ULT-NULL
            //             TO (SF)-PRE-INVRIO-ULT-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPreInvrioUlt().setWtgaPreInvrioUltNull(ws.getTrchDiGar().getTgaPreInvrioUlt().getTgaPreInvrioUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-INVRIO-ULT
            //             TO (SF)-PRE-INVRIO-ULT(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPreInvrioUlt().setWtgaPreInvrioUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreInvrioUlt().getTgaPreInvrioUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-RIVTO-NULL = HIGH-VALUES
        //                TO (SF)-PRE-RIVTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-RIVTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreRivto().getTgaPreRivtoNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-RIVTO-NULL
            //             TO (SF)-PRE-RIVTO-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPreRivto().setWtgaPreRivtoNull(ws.getTrchDiGar().getTgaPreRivto().getTgaPreRivtoNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-RIVTO
            //             TO (SF)-PRE-RIVTO(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPreRivto().setWtgaPreRivto(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreRivto().getTgaPreRivto(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-SOPR-PROF-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SOPR-PROF-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-SOPR-PROF(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpSoprProf().getTgaImpSoprProfNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-SOPR-PROF-NULL
            //             TO (SF)-IMP-SOPR-PROF-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpSoprProf().setWtgaImpSoprProfNull(ws.getTrchDiGar().getTgaImpSoprProf().getTgaImpSoprProfNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-SOPR-PROF
            //             TO (SF)-IMP-SOPR-PROF(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpSoprProf().setWtgaImpSoprProf(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpSoprProf().getTgaImpSoprProf(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-SOPR-SAN-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SOPR-SAN-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-SOPR-SAN(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpSoprSan().getTgaImpSoprSanNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-SOPR-SAN-NULL
            //             TO (SF)-IMP-SOPR-SAN-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpSoprSan().setWtgaImpSoprSanNull(ws.getTrchDiGar().getTgaImpSoprSan().getTgaImpSoprSanNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-SOPR-SAN
            //             TO (SF)-IMP-SOPR-SAN(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpSoprSan().setWtgaImpSoprSan(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpSoprSan().getTgaImpSoprSan(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-SOPR-SPO-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SOPR-SPO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-SOPR-SPO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpSoprSpo().getTgaImpSoprSpoNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-SOPR-SPO-NULL
            //             TO (SF)-IMP-SOPR-SPO-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpSoprSpo().setWtgaImpSoprSpoNull(ws.getTrchDiGar().getTgaImpSoprSpo().getTgaImpSoprSpoNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-SOPR-SPO
            //             TO (SF)-IMP-SOPR-SPO(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpSoprSpo().setWtgaImpSoprSpo(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpSoprSpo().getTgaImpSoprSpo(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-SOPR-TEC-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SOPR-TEC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-SOPR-TEC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpSoprTec().getTgaImpSoprTecNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-SOPR-TEC-NULL
            //             TO (SF)-IMP-SOPR-TEC-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpSoprTec().setWtgaImpSoprTecNull(ws.getTrchDiGar().getTgaImpSoprTec().getTgaImpSoprTecNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-SOPR-TEC
            //             TO (SF)-IMP-SOPR-TEC(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpSoprTec().setWtgaImpSoprTec(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpSoprTec().getTgaImpSoprTec(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-ALT-SOPR-NULL = HIGH-VALUES
        //                TO (SF)-IMP-ALT-SOPR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-ALT-SOPR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpAltSopr().getTgaImpAltSoprNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-ALT-SOPR-NULL
            //             TO (SF)-IMP-ALT-SOPR-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpAltSopr().setWtgaImpAltSoprNull(ws.getTrchDiGar().getTgaImpAltSopr().getTgaImpAltSoprNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-ALT-SOPR
            //             TO (SF)-IMP-ALT-SOPR(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpAltSopr().setWtgaImpAltSopr(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpAltSopr().getTgaImpAltSopr(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-STAB-NULL = HIGH-VALUES
        //                TO (SF)-PRE-STAB-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-STAB(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreStab().getTgaPreStabNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-STAB-NULL
            //             TO (SF)-PRE-STAB-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPreStab().setWtgaPreStabNull(ws.getTrchDiGar().getTgaPreStab().getTgaPreStabNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-STAB
            //             TO (SF)-PRE-STAB(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPreStab().setWtgaPreStab(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreStab().getTgaPreStab(), 15, 3));
        }
        // COB_CODE: IF TGA-DT-EFF-STAB-NULL = HIGH-VALUES
        //                TO (SF)-DT-EFF-STAB-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-EFF-STAB(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtEffStab().getTgaDtEffStabNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-EFF-STAB-NULL
            //             TO (SF)-DT-EFF-STAB-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaDtEffStab().setWtgaDtEffStabNull(ws.getTrchDiGar().getTgaDtEffStab().getTgaDtEffStabNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-EFF-STAB
            //             TO (SF)-DT-EFF-STAB(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaDtEffStab().setWtgaDtEffStab(ws.getTrchDiGar().getTgaDtEffStab().getTgaDtEffStab());
        }
        // COB_CODE: IF TGA-TS-RIVAL-FIS-NULL = HIGH-VALUES
        //                TO (SF)-TS-RIVAL-FIS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TS-RIVAL-FIS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTsRivalFis().getTgaTsRivalFisNullFormatted())) {
            // COB_CODE: MOVE TGA-TS-RIVAL-FIS-NULL
            //             TO (SF)-TS-RIVAL-FIS-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaTsRivalFis().setWtgaTsRivalFisNull(ws.getTrchDiGar().getTgaTsRivalFis().getTgaTsRivalFisNull());
        }
        else {
            // COB_CODE: MOVE TGA-TS-RIVAL-FIS
            //             TO (SF)-TS-RIVAL-FIS(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaTsRivalFis().setWtgaTsRivalFis(Trunc.toDecimal(ws.getTrchDiGar().getTgaTsRivalFis().getTgaTsRivalFis(), 14, 9));
        }
        // COB_CODE: IF TGA-TS-RIVAL-INDICIZ-NULL = HIGH-VALUES
        //                TO (SF)-TS-RIVAL-INDICIZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TS-RIVAL-INDICIZ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTsRivalIndiciz().getTgaTsRivalIndicizNullFormatted())) {
            // COB_CODE: MOVE TGA-TS-RIVAL-INDICIZ-NULL
            //             TO (SF)-TS-RIVAL-INDICIZ-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaTsRivalIndiciz().setWtgaTsRivalIndicizNull(ws.getTrchDiGar().getTgaTsRivalIndiciz().getTgaTsRivalIndicizNull());
        }
        else {
            // COB_CODE: MOVE TGA-TS-RIVAL-INDICIZ
            //             TO (SF)-TS-RIVAL-INDICIZ(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaTsRivalIndiciz().setWtgaTsRivalIndiciz(Trunc.toDecimal(ws.getTrchDiGar().getTgaTsRivalIndiciz().getTgaTsRivalIndiciz(), 14, 9));
        }
        // COB_CODE: IF TGA-OLD-TS-TEC-NULL = HIGH-VALUES
        //                TO (SF)-OLD-TS-TEC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-OLD-TS-TEC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaOldTsTec().getTgaOldTsTecNullFormatted())) {
            // COB_CODE: MOVE TGA-OLD-TS-TEC-NULL
            //             TO (SF)-OLD-TS-TEC-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaOldTsTec().setWtgaOldTsTecNull(ws.getTrchDiGar().getTgaOldTsTec().getTgaOldTsTecNull());
        }
        else {
            // COB_CODE: MOVE TGA-OLD-TS-TEC
            //             TO (SF)-OLD-TS-TEC(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaOldTsTec().setWtgaOldTsTec(Trunc.toDecimal(ws.getTrchDiGar().getTgaOldTsTec().getTgaOldTsTec(), 14, 9));
        }
        // COB_CODE: IF TGA-RAT-LRD-NULL = HIGH-VALUES
        //                TO (SF)-RAT-LRD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-RAT-LRD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRatLrd().getTgaRatLrdNullFormatted())) {
            // COB_CODE: MOVE TGA-RAT-LRD-NULL
            //             TO (SF)-RAT-LRD-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaRatLrd().setWtgaRatLrdNull(ws.getTrchDiGar().getTgaRatLrd().getTgaRatLrdNull());
        }
        else {
            // COB_CODE: MOVE TGA-RAT-LRD
            //             TO (SF)-RAT-LRD(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaRatLrd().setWtgaRatLrd(Trunc.toDecimal(ws.getTrchDiGar().getTgaRatLrd().getTgaRatLrd(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-LRD-NULL = HIGH-VALUES
        //                TO (SF)-PRE-LRD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-LRD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreLrd().getTgaPreLrdNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-LRD-NULL
            //             TO (SF)-PRE-LRD-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPreLrd().setWtgaPreLrdNull(ws.getTrchDiGar().getTgaPreLrd().getTgaPreLrdNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-LRD
            //             TO (SF)-PRE-LRD(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPreLrd().setWtgaPreLrd(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreLrd().getTgaPreLrd(), 15, 3));
        }
        // COB_CODE: IF TGA-PRSTZ-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzIni().getTgaPrstzIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-INI-NULL
            //             TO (SF)-PRSTZ-INI-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzIni().setWtgaPrstzIniNull(ws.getTrchDiGar().getTgaPrstzIni().getTgaPrstzIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-INI
            //             TO (SF)-PRSTZ-INI(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzIni().setWtgaPrstzIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzIni().getTgaPrstzIni(), 15, 3));
        }
        // COB_CODE: IF TGA-PRSTZ-ULT-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzUlt().getTgaPrstzUltNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-ULT-NULL
            //             TO (SF)-PRSTZ-ULT-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzUlt().setWtgaPrstzUltNull(ws.getTrchDiGar().getTgaPrstzUlt().getTgaPrstzUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-ULT
            //             TO (SF)-PRSTZ-ULT(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzUlt().setWtgaPrstzUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzUlt().getTgaPrstzUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-CPT-IN-OPZ-RIVTO-NULL = HIGH-VALUES
        //                TO (SF)-CPT-IN-OPZ-RIVTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-CPT-IN-OPZ-RIVTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCptInOpzRivto().getTgaCptInOpzRivtoNullFormatted())) {
            // COB_CODE: MOVE TGA-CPT-IN-OPZ-RIVTO-NULL
            //             TO (SF)-CPT-IN-OPZ-RIVTO-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaCptInOpzRivto().setWtgaCptInOpzRivtoNull(ws.getTrchDiGar().getTgaCptInOpzRivto().getTgaCptInOpzRivtoNull());
        }
        else {
            // COB_CODE: MOVE TGA-CPT-IN-OPZ-RIVTO
            //             TO (SF)-CPT-IN-OPZ-RIVTO(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaCptInOpzRivto().setWtgaCptInOpzRivto(Trunc.toDecimal(ws.getTrchDiGar().getTgaCptInOpzRivto().getTgaCptInOpzRivto(), 15, 3));
        }
        // COB_CODE: IF TGA-PRSTZ-INI-STAB-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-INI-STAB-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-INI-STAB(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzIniStab().getTgaPrstzIniStabNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-INI-STAB-NULL
            //             TO (SF)-PRSTZ-INI-STAB-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzIniStab().setWtgaPrstzIniStabNull(ws.getTrchDiGar().getTgaPrstzIniStab().getTgaPrstzIniStabNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-INI-STAB
            //             TO (SF)-PRSTZ-INI-STAB(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzIniStab().setWtgaPrstzIniStab(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzIniStab().getTgaPrstzIniStab(), 15, 3));
        }
        // COB_CODE: IF TGA-CPT-RSH-MOR-NULL = HIGH-VALUES
        //                TO (SF)-CPT-RSH-MOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-CPT-RSH-MOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCptRshMor().getTgaCptRshMorNullFormatted())) {
            // COB_CODE: MOVE TGA-CPT-RSH-MOR-NULL
            //             TO (SF)-CPT-RSH-MOR-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaCptRshMor().setWtgaCptRshMorNull(ws.getTrchDiGar().getTgaCptRshMor().getTgaCptRshMorNull());
        }
        else {
            // COB_CODE: MOVE TGA-CPT-RSH-MOR
            //             TO (SF)-CPT-RSH-MOR(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaCptRshMor().setWtgaCptRshMor(Trunc.toDecimal(ws.getTrchDiGar().getTgaCptRshMor().getTgaCptRshMor(), 15, 3));
        }
        // COB_CODE: IF TGA-PRSTZ-RID-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-RID-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-RID-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzRidIni().getTgaPrstzRidIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-RID-INI-NULL
            //             TO (SF)-PRSTZ-RID-INI-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzRidIni().setWtgaPrstzRidIniNull(ws.getTrchDiGar().getTgaPrstzRidIni().getTgaPrstzRidIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-RID-INI
            //             TO (SF)-PRSTZ-RID-INI(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzRidIni().setWtgaPrstzRidIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzRidIni().getTgaPrstzRidIni(), 15, 3));
        }
        // COB_CODE: IF TGA-FL-CAR-CONT-NULL = HIGH-VALUES
        //                TO (SF)-FL-CAR-CONT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-FL-CAR-CONT(IX-TAB-TGA)
        //           END-IF
        if (Conditions.eq(ws.getTrchDiGar().getTgaFlCarCont(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TGA-FL-CAR-CONT-NULL
            //             TO (SF)-FL-CAR-CONT-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaFlCarCont(ws.getTrchDiGar().getTgaFlCarCont());
        }
        else {
            // COB_CODE: MOVE TGA-FL-CAR-CONT
            //             TO (SF)-FL-CAR-CONT(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaFlCarCont(ws.getTrchDiGar().getTgaFlCarCont());
        }
        // COB_CODE: IF TGA-BNS-GIA-LIQTO-NULL = HIGH-VALUES
        //                TO (SF)-BNS-GIA-LIQTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-BNS-GIA-LIQTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaBnsGiaLiqto().getTgaBnsGiaLiqtoNullFormatted())) {
            // COB_CODE: MOVE TGA-BNS-GIA-LIQTO-NULL
            //             TO (SF)-BNS-GIA-LIQTO-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaBnsGiaLiqto().setWtgaBnsGiaLiqtoNull(ws.getTrchDiGar().getTgaBnsGiaLiqto().getTgaBnsGiaLiqtoNull());
        }
        else {
            // COB_CODE: MOVE TGA-BNS-GIA-LIQTO
            //             TO (SF)-BNS-GIA-LIQTO(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaBnsGiaLiqto().setWtgaBnsGiaLiqto(Trunc.toDecimal(ws.getTrchDiGar().getTgaBnsGiaLiqto().getTgaBnsGiaLiqto(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-BNS-NULL = HIGH-VALUES
        //                TO (SF)-IMP-BNS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-BNS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpBns().getTgaImpBnsNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-BNS-NULL
            //             TO (SF)-IMP-BNS-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpBns().setWtgaImpBnsNull(ws.getTrchDiGar().getTgaImpBns().getTgaImpBnsNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-BNS
            //             TO (SF)-IMP-BNS(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpBns().setWtgaImpBns(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpBns().getTgaImpBns(), 15, 3));
        }
        // COB_CODE: MOVE TGA-COD-DVS
        //             TO (SF)-COD-DVS(IX-TAB-TGA)
        ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaCodDvs(ws.getTrchDiGar().getTgaCodDvs());
        // COB_CODE: IF TGA-PRSTZ-INI-NEWFIS-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-INI-NEWFIS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-INI-NEWFIS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzIniNewfis().getTgaPrstzIniNewfisNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-INI-NEWFIS-NULL
            //             TO (SF)-PRSTZ-INI-NEWFIS-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzIniNewfis().setWtgaPrstzIniNewfisNull(ws.getTrchDiGar().getTgaPrstzIniNewfis().getTgaPrstzIniNewfisNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-INI-NEWFIS
            //             TO (SF)-PRSTZ-INI-NEWFIS(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzIniNewfis().setWtgaPrstzIniNewfis(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzIniNewfis().getTgaPrstzIniNewfis(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-SCON-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SCON-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-SCON(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpScon().getTgaImpSconNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-SCON-NULL
            //             TO (SF)-IMP-SCON-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpScon().setWtgaImpSconNull(ws.getTrchDiGar().getTgaImpScon().getTgaImpSconNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-SCON
            //             TO (SF)-IMP-SCON(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpScon().setWtgaImpScon(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpScon().getTgaImpScon(), 15, 3));
        }
        // COB_CODE: IF TGA-ALQ-SCON-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-SCON-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-SCON(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqScon().getTgaAlqSconNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-SCON-NULL
            //             TO (SF)-ALQ-SCON-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaAlqScon().setWtgaAlqSconNull(ws.getTrchDiGar().getTgaAlqScon().getTgaAlqSconNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-SCON
            //             TO (SF)-ALQ-SCON(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaAlqScon().setWtgaAlqScon(Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqScon().getTgaAlqScon(), 6, 3));
        }
        // COB_CODE: IF TGA-IMP-CAR-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-IMP-CAR-ACQ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-CAR-ACQ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpCarAcq().getTgaImpCarAcqNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-CAR-ACQ-NULL
            //             TO (SF)-IMP-CAR-ACQ-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpCarAcq().setWtgaImpCarAcqNull(ws.getTrchDiGar().getTgaImpCarAcq().getTgaImpCarAcqNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-CAR-ACQ
            //             TO (SF)-IMP-CAR-ACQ(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpCarAcq().setWtgaImpCarAcq(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpCarAcq().getTgaImpCarAcq(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-CAR-INC-NULL = HIGH-VALUES
        //                TO (SF)-IMP-CAR-INC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-CAR-INC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpCarInc().getTgaImpCarIncNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-CAR-INC-NULL
            //             TO (SF)-IMP-CAR-INC-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpCarInc().setWtgaImpCarIncNull(ws.getTrchDiGar().getTgaImpCarInc().getTgaImpCarIncNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-CAR-INC
            //             TO (SF)-IMP-CAR-INC(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpCarInc().setWtgaImpCarInc(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpCarInc().getTgaImpCarInc(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-CAR-GEST-NULL = HIGH-VALUES
        //                TO (SF)-IMP-CAR-GEST-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-CAR-GEST(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpCarGest().getTgaImpCarGestNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-CAR-GEST-NULL
            //             TO (SF)-IMP-CAR-GEST-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpCarGest().setWtgaImpCarGestNull(ws.getTrchDiGar().getTgaImpCarGest().getTgaImpCarGestNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-CAR-GEST
            //             TO (SF)-IMP-CAR-GEST(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpCarGest().setWtgaImpCarGest(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpCarGest().getTgaImpCarGest(), 15, 3));
        }
        // COB_CODE: IF TGA-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-AA-1O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-AA-1O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaAa1oAssto().getTgaEtaAa1oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-AA-1O-ASSTO-NULL
            //             TO (SF)-ETA-AA-1O-ASSTO-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaEtaAa1oAssto().setWtgaEtaAa1oAsstoNull(ws.getTrchDiGar().getTgaEtaAa1oAssto().getTgaEtaAa1oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-AA-1O-ASSTO
            //             TO (SF)-ETA-AA-1O-ASSTO(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaEtaAa1oAssto().setWtgaEtaAa1oAssto(ws.getTrchDiGar().getTgaEtaAa1oAssto().getTgaEtaAa1oAssto());
        }
        // COB_CODE: IF TGA-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-MM-1O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-MM-1O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaMm1oAssto().getTgaEtaMm1oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-MM-1O-ASSTO-NULL
            //             TO (SF)-ETA-MM-1O-ASSTO-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaEtaMm1oAssto().setWtgaEtaMm1oAsstoNull(ws.getTrchDiGar().getTgaEtaMm1oAssto().getTgaEtaMm1oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-MM-1O-ASSTO
            //             TO (SF)-ETA-MM-1O-ASSTO(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaEtaMm1oAssto().setWtgaEtaMm1oAssto(ws.getTrchDiGar().getTgaEtaMm1oAssto().getTgaEtaMm1oAssto());
        }
        // COB_CODE: IF TGA-ETA-AA-2O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-AA-2O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-AA-2O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaAa2oAssto().getTgaEtaAa2oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-AA-2O-ASSTO-NULL
            //             TO (SF)-ETA-AA-2O-ASSTO-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaEtaAa2oAssto().setWtgaEtaAa2oAsstoNull(ws.getTrchDiGar().getTgaEtaAa2oAssto().getTgaEtaAa2oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-AA-2O-ASSTO
            //             TO (SF)-ETA-AA-2O-ASSTO(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaEtaAa2oAssto().setWtgaEtaAa2oAssto(ws.getTrchDiGar().getTgaEtaAa2oAssto().getTgaEtaAa2oAssto());
        }
        // COB_CODE: IF TGA-ETA-MM-2O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-MM-2O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-MM-2O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaMm2oAssto().getTgaEtaMm2oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-MM-2O-ASSTO-NULL
            //             TO (SF)-ETA-MM-2O-ASSTO-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaEtaMm2oAssto().setWtgaEtaMm2oAsstoNull(ws.getTrchDiGar().getTgaEtaMm2oAssto().getTgaEtaMm2oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-MM-2O-ASSTO
            //             TO (SF)-ETA-MM-2O-ASSTO(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaEtaMm2oAssto().setWtgaEtaMm2oAssto(ws.getTrchDiGar().getTgaEtaMm2oAssto().getTgaEtaMm2oAssto());
        }
        // COB_CODE: IF TGA-ETA-AA-3O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-AA-3O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-AA-3O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaAa3oAssto().getTgaEtaAa3oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-AA-3O-ASSTO-NULL
            //             TO (SF)-ETA-AA-3O-ASSTO-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaEtaAa3oAssto().setWtgaEtaAa3oAsstoNull(ws.getTrchDiGar().getTgaEtaAa3oAssto().getTgaEtaAa3oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-AA-3O-ASSTO
            //             TO (SF)-ETA-AA-3O-ASSTO(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaEtaAa3oAssto().setWtgaEtaAa3oAssto(ws.getTrchDiGar().getTgaEtaAa3oAssto().getTgaEtaAa3oAssto());
        }
        // COB_CODE: IF TGA-ETA-MM-3O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-MM-3O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-MM-3O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaMm3oAssto().getTgaEtaMm3oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-MM-3O-ASSTO-NULL
            //             TO (SF)-ETA-MM-3O-ASSTO-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaEtaMm3oAssto().setWtgaEtaMm3oAsstoNull(ws.getTrchDiGar().getTgaEtaMm3oAssto().getTgaEtaMm3oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-MM-3O-ASSTO
            //             TO (SF)-ETA-MM-3O-ASSTO(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaEtaMm3oAssto().setWtgaEtaMm3oAssto(ws.getTrchDiGar().getTgaEtaMm3oAssto().getTgaEtaMm3oAssto());
        }
        // COB_CODE: IF TGA-RENDTO-LRD-NULL = HIGH-VALUES
        //                TO (SF)-RENDTO-LRD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-RENDTO-LRD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRendtoLrd().getTgaRendtoLrdNullFormatted())) {
            // COB_CODE: MOVE TGA-RENDTO-LRD-NULL
            //             TO (SF)-RENDTO-LRD-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaRendtoLrd().setWtgaRendtoLrdNull(ws.getTrchDiGar().getTgaRendtoLrd().getTgaRendtoLrdNull());
        }
        else {
            // COB_CODE: MOVE TGA-RENDTO-LRD
            //             TO (SF)-RENDTO-LRD(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaRendtoLrd().setWtgaRendtoLrd(Trunc.toDecimal(ws.getTrchDiGar().getTgaRendtoLrd().getTgaRendtoLrd(), 14, 9));
        }
        // COB_CODE: IF TGA-PC-RETR-NULL = HIGH-VALUES
        //                TO (SF)-PC-RETR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PC-RETR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPcRetr().getTgaPcRetrNullFormatted())) {
            // COB_CODE: MOVE TGA-PC-RETR-NULL
            //             TO (SF)-PC-RETR-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPcRetr().setWtgaPcRetrNull(ws.getTrchDiGar().getTgaPcRetr().getTgaPcRetrNull());
        }
        else {
            // COB_CODE: MOVE TGA-PC-RETR
            //             TO (SF)-PC-RETR(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPcRetr().setWtgaPcRetr(Trunc.toDecimal(ws.getTrchDiGar().getTgaPcRetr().getTgaPcRetr(), 6, 3));
        }
        // COB_CODE: IF TGA-RENDTO-RETR-NULL = HIGH-VALUES
        //                TO (SF)-RENDTO-RETR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-RENDTO-RETR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRendtoRetr().getTgaRendtoRetrNullFormatted())) {
            // COB_CODE: MOVE TGA-RENDTO-RETR-NULL
            //             TO (SF)-RENDTO-RETR-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaRendtoRetr().setWtgaRendtoRetrNull(ws.getTrchDiGar().getTgaRendtoRetr().getTgaRendtoRetrNull());
        }
        else {
            // COB_CODE: MOVE TGA-RENDTO-RETR
            //             TO (SF)-RENDTO-RETR(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaRendtoRetr().setWtgaRendtoRetr(Trunc.toDecimal(ws.getTrchDiGar().getTgaRendtoRetr().getTgaRendtoRetr(), 14, 9));
        }
        // COB_CODE: IF TGA-MIN-GARTO-NULL = HIGH-VALUES
        //                TO (SF)-MIN-GARTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MIN-GARTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaMinGarto().getTgaMinGartoNullFormatted())) {
            // COB_CODE: MOVE TGA-MIN-GARTO-NULL
            //             TO (SF)-MIN-GARTO-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaMinGarto().setWtgaMinGartoNull(ws.getTrchDiGar().getTgaMinGarto().getTgaMinGartoNull());
        }
        else {
            // COB_CODE: MOVE TGA-MIN-GARTO
            //             TO (SF)-MIN-GARTO(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaMinGarto().setWtgaMinGarto(Trunc.toDecimal(ws.getTrchDiGar().getTgaMinGarto().getTgaMinGarto(), 14, 9));
        }
        // COB_CODE: IF TGA-MIN-TRNUT-NULL = HIGH-VALUES
        //                TO (SF)-MIN-TRNUT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MIN-TRNUT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaMinTrnut().getTgaMinTrnutNullFormatted())) {
            // COB_CODE: MOVE TGA-MIN-TRNUT-NULL
            //             TO (SF)-MIN-TRNUT-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaMinTrnut().setWtgaMinTrnutNull(ws.getTrchDiGar().getTgaMinTrnut().getTgaMinTrnutNull());
        }
        else {
            // COB_CODE: MOVE TGA-MIN-TRNUT
            //             TO (SF)-MIN-TRNUT(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaMinTrnut().setWtgaMinTrnut(Trunc.toDecimal(ws.getTrchDiGar().getTgaMinTrnut().getTgaMinTrnut(), 14, 9));
        }
        // COB_CODE: IF TGA-PRE-ATT-DI-TRCH-NULL = HIGH-VALUES
        //                TO (SF)-PRE-ATT-DI-TRCH-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-ATT-DI-TRCH(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreAttDiTrch().getTgaPreAttDiTrchNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-ATT-DI-TRCH-NULL
            //             TO (SF)-PRE-ATT-DI-TRCH-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPreAttDiTrch().setWtgaPreAttDiTrchNull(ws.getTrchDiGar().getTgaPreAttDiTrch().getTgaPreAttDiTrchNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-ATT-DI-TRCH
            //             TO (SF)-PRE-ATT-DI-TRCH(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPreAttDiTrch().setWtgaPreAttDiTrch(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreAttDiTrch().getTgaPreAttDiTrch(), 15, 3));
        }
        // COB_CODE: IF TGA-MATU-END2000-NULL = HIGH-VALUES
        //                TO (SF)-MATU-END2000-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MATU-END2000(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaMatuEnd2000().getTgaMatuEnd2000NullFormatted())) {
            // COB_CODE: MOVE TGA-MATU-END2000-NULL
            //             TO (SF)-MATU-END2000-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaMatuEnd2000().setWtgaMatuEnd2000Null(ws.getTrchDiGar().getTgaMatuEnd2000().getTgaMatuEnd2000Null());
        }
        else {
            // COB_CODE: MOVE TGA-MATU-END2000
            //             TO (SF)-MATU-END2000(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaMatuEnd2000().setWtgaMatuEnd2000(Trunc.toDecimal(ws.getTrchDiGar().getTgaMatuEnd2000().getTgaMatuEnd2000(), 15, 3));
        }
        // COB_CODE: IF TGA-ABB-TOT-INI-NULL = HIGH-VALUES
        //                TO (SF)-ABB-TOT-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ABB-TOT-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAbbTotIni().getTgaAbbTotIniNullFormatted())) {
            // COB_CODE: MOVE TGA-ABB-TOT-INI-NULL
            //             TO (SF)-ABB-TOT-INI-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaAbbTotIni().setWtgaAbbTotIniNull(ws.getTrchDiGar().getTgaAbbTotIni().getTgaAbbTotIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-ABB-TOT-INI
            //             TO (SF)-ABB-TOT-INI(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaAbbTotIni().setWtgaAbbTotIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaAbbTotIni().getTgaAbbTotIni(), 15, 3));
        }
        // COB_CODE: IF TGA-ABB-TOT-ULT-NULL = HIGH-VALUES
        //                TO (SF)-ABB-TOT-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ABB-TOT-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAbbTotUlt().getTgaAbbTotUltNullFormatted())) {
            // COB_CODE: MOVE TGA-ABB-TOT-ULT-NULL
            //             TO (SF)-ABB-TOT-ULT-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaAbbTotUlt().setWtgaAbbTotUltNull(ws.getTrchDiGar().getTgaAbbTotUlt().getTgaAbbTotUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-ABB-TOT-ULT
            //             TO (SF)-ABB-TOT-ULT(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaAbbTotUlt().setWtgaAbbTotUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaAbbTotUlt().getTgaAbbTotUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-ABB-ANNU-ULT-NULL = HIGH-VALUES
        //                TO (SF)-ABB-ANNU-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ABB-ANNU-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAbbAnnuUlt().getTgaAbbAnnuUltNullFormatted())) {
            // COB_CODE: MOVE TGA-ABB-ANNU-ULT-NULL
            //             TO (SF)-ABB-ANNU-ULT-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaAbbAnnuUlt().setWtgaAbbAnnuUltNull(ws.getTrchDiGar().getTgaAbbAnnuUlt().getTgaAbbAnnuUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-ABB-ANNU-ULT
            //             TO (SF)-ABB-ANNU-ULT(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaAbbAnnuUlt().setWtgaAbbAnnuUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaAbbAnnuUlt().getTgaAbbAnnuUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-DUR-ABB-NULL = HIGH-VALUES
        //                TO (SF)-DUR-ABB-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DUR-ABB(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDurAbb().getTgaDurAbbNullFormatted())) {
            // COB_CODE: MOVE TGA-DUR-ABB-NULL
            //             TO (SF)-DUR-ABB-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaDurAbb().setWtgaDurAbbNull(ws.getTrchDiGar().getTgaDurAbb().getTgaDurAbbNull());
        }
        else {
            // COB_CODE: MOVE TGA-DUR-ABB
            //             TO (SF)-DUR-ABB(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaDurAbb().setWtgaDurAbb(ws.getTrchDiGar().getTgaDurAbb().getTgaDurAbb());
        }
        // COB_CODE: IF TGA-TP-ADEG-ABB-NULL = HIGH-VALUES
        //                TO (SF)-TP-ADEG-ABB-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TP-ADEG-ABB(IX-TAB-TGA)
        //           END-IF
        if (Conditions.eq(ws.getTrchDiGar().getTgaTpAdegAbb(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TGA-TP-ADEG-ABB-NULL
            //             TO (SF)-TP-ADEG-ABB-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaTpAdegAbb(ws.getTrchDiGar().getTgaTpAdegAbb());
        }
        else {
            // COB_CODE: MOVE TGA-TP-ADEG-ABB
            //             TO (SF)-TP-ADEG-ABB(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaTpAdegAbb(ws.getTrchDiGar().getTgaTpAdegAbb());
        }
        // COB_CODE: IF TGA-MOD-CALC-NULL = HIGH-VALUES
        //                TO (SF)-MOD-CALC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MOD-CALC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaModCalcFormatted())) {
            // COB_CODE: MOVE TGA-MOD-CALC-NULL
            //             TO (SF)-MOD-CALC-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaModCalc(ws.getTrchDiGar().getTgaModCalc());
        }
        else {
            // COB_CODE: MOVE TGA-MOD-CALC
            //             TO (SF)-MOD-CALC(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaModCalc(ws.getTrchDiGar().getTgaModCalc());
        }
        // COB_CODE: IF TGA-IMP-AZ-NULL = HIGH-VALUES
        //                TO (SF)-IMP-AZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-AZ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpAz().getTgaImpAzNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-AZ-NULL
            //             TO (SF)-IMP-AZ-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpAz().setWtgaImpAzNull(ws.getTrchDiGar().getTgaImpAz().getTgaImpAzNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-AZ
            //             TO (SF)-IMP-AZ(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpAz().setWtgaImpAz(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpAz().getTgaImpAz(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-ADER-NULL = HIGH-VALUES
        //                TO (SF)-IMP-ADER-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-ADER(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpAder().getTgaImpAderNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-ADER-NULL
            //             TO (SF)-IMP-ADER-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpAder().setWtgaImpAderNull(ws.getTrchDiGar().getTgaImpAder().getTgaImpAderNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-ADER
            //             TO (SF)-IMP-ADER(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpAder().setWtgaImpAder(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpAder().getTgaImpAder(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-TFR-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TFR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-TFR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpTfr().getTgaImpTfrNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-TFR-NULL
            //             TO (SF)-IMP-TFR-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpTfr().setWtgaImpTfrNull(ws.getTrchDiGar().getTgaImpTfr().getTgaImpTfrNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-TFR
            //             TO (SF)-IMP-TFR(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpTfr().setWtgaImpTfr(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpTfr().getTgaImpTfr(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-VOLO-NULL = HIGH-VALUES
        //                TO (SF)-IMP-VOLO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-VOLO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpVolo().getTgaImpVoloNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-VOLO-NULL
            //             TO (SF)-IMP-VOLO-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpVolo().setWtgaImpVoloNull(ws.getTrchDiGar().getTgaImpVolo().getTgaImpVoloNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-VOLO
            //             TO (SF)-IMP-VOLO(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpVolo().setWtgaImpVolo(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpVolo().getTgaImpVolo(), 15, 3));
        }
        // COB_CODE: IF TGA-VIS-END2000-NULL = HIGH-VALUES
        //                TO (SF)-VIS-END2000-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-VIS-END2000(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaVisEnd2000().getTgaVisEnd2000NullFormatted())) {
            // COB_CODE: MOVE TGA-VIS-END2000-NULL
            //             TO (SF)-VIS-END2000-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaVisEnd2000().setWtgaVisEnd2000Null(ws.getTrchDiGar().getTgaVisEnd2000().getTgaVisEnd2000Null());
        }
        else {
            // COB_CODE: MOVE TGA-VIS-END2000
            //             TO (SF)-VIS-END2000(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaVisEnd2000().setWtgaVisEnd2000(Trunc.toDecimal(ws.getTrchDiGar().getTgaVisEnd2000().getTgaVisEnd2000(), 15, 3));
        }
        // COB_CODE: IF TGA-DT-VLDT-PROD-NULL = HIGH-VALUES
        //                TO (SF)-DT-VLDT-PROD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-VLDT-PROD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtVldtProd().getTgaDtVldtProdNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-VLDT-PROD-NULL
            //             TO (SF)-DT-VLDT-PROD-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaDtVldtProd().setWtgaDtVldtProdNull(ws.getTrchDiGar().getTgaDtVldtProd().getTgaDtVldtProdNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-VLDT-PROD
            //             TO (SF)-DT-VLDT-PROD(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaDtVldtProd().setWtgaDtVldtProd(ws.getTrchDiGar().getTgaDtVldtProd().getTgaDtVldtProd());
        }
        // COB_CODE: IF TGA-DT-INI-VAL-TAR-NULL = HIGH-VALUES
        //                TO (SF)-DT-INI-VAL-TAR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-INI-VAL-TAR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtIniValTar().getTgaDtIniValTarNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-INI-VAL-TAR-NULL
            //             TO (SF)-DT-INI-VAL-TAR-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaDtIniValTar().setWtgaDtIniValTarNull(ws.getTrchDiGar().getTgaDtIniValTar().getTgaDtIniValTarNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-INI-VAL-TAR
            //             TO (SF)-DT-INI-VAL-TAR(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaDtIniValTar().setWtgaDtIniValTar(ws.getTrchDiGar().getTgaDtIniValTar().getTgaDtIniValTar());
        }
        // COB_CODE: IF TGA-IMPB-VIS-END2000-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-VIS-END2000-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-VIS-END2000(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbVisEnd2000().getTgaImpbVisEnd2000NullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-VIS-END2000-NULL
            //             TO (SF)-IMPB-VIS-END2000-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpbVisEnd2000().setWtgaImpbVisEnd2000Null(ws.getTrchDiGar().getTgaImpbVisEnd2000().getTgaImpbVisEnd2000Null());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-VIS-END2000
            //             TO (SF)-IMPB-VIS-END2000(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpbVisEnd2000().setWtgaImpbVisEnd2000(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbVisEnd2000().getTgaImpbVisEnd2000(), 15, 3));
        }
        // COB_CODE: IF TGA-REN-INI-TS-TEC-0-NULL = HIGH-VALUES
        //                TO (SF)-REN-INI-TS-TEC-0-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-REN-INI-TS-TEC-0(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRenIniTsTec0().getTgaRenIniTsTec0NullFormatted())) {
            // COB_CODE: MOVE TGA-REN-INI-TS-TEC-0-NULL
            //             TO (SF)-REN-INI-TS-TEC-0-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaRenIniTsTec0().setWtgaRenIniTsTec0Null(ws.getTrchDiGar().getTgaRenIniTsTec0().getTgaRenIniTsTec0Null());
        }
        else {
            // COB_CODE: MOVE TGA-REN-INI-TS-TEC-0
            //             TO (SF)-REN-INI-TS-TEC-0(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaRenIniTsTec0().setWtgaRenIniTsTec0(Trunc.toDecimal(ws.getTrchDiGar().getTgaRenIniTsTec0().getTgaRenIniTsTec0(), 15, 3));
        }
        // COB_CODE: IF TGA-PC-RIP-PRE-NULL = HIGH-VALUES
        //                TO (SF)-PC-RIP-PRE-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PC-RIP-PRE(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPcRipPre().getTgaPcRipPreNullFormatted())) {
            // COB_CODE: MOVE TGA-PC-RIP-PRE-NULL
            //             TO (SF)-PC-RIP-PRE-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPcRipPre().setWtgaPcRipPreNull(ws.getTrchDiGar().getTgaPcRipPre().getTgaPcRipPreNull());
        }
        else {
            // COB_CODE: MOVE TGA-PC-RIP-PRE
            //             TO (SF)-PC-RIP-PRE(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPcRipPre().setWtgaPcRipPre(Trunc.toDecimal(ws.getTrchDiGar().getTgaPcRipPre().getTgaPcRipPre(), 6, 3));
        }
        // COB_CODE: IF TGA-FL-IMPORTI-FORZ-NULL = HIGH-VALUES
        //                TO (SF)-FL-IMPORTI-FORZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-FL-IMPORTI-FORZ(IX-TAB-TGA)
        //           END-IF
        if (Conditions.eq(ws.getTrchDiGar().getTgaFlImportiForz(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TGA-FL-IMPORTI-FORZ-NULL
            //             TO (SF)-FL-IMPORTI-FORZ-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaFlImportiForz(ws.getTrchDiGar().getTgaFlImportiForz());
        }
        else {
            // COB_CODE: MOVE TGA-FL-IMPORTI-FORZ
            //             TO (SF)-FL-IMPORTI-FORZ(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaFlImportiForz(ws.getTrchDiGar().getTgaFlImportiForz());
        }
        // COB_CODE: IF TGA-PRSTZ-INI-NFORZ-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-INI-NFORZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-INI-NFORZ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzIniNforz().getTgaPrstzIniNforzNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-INI-NFORZ-NULL
            //             TO (SF)-PRSTZ-INI-NFORZ-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzIniNforz().setWtgaPrstzIniNforzNull(ws.getTrchDiGar().getTgaPrstzIniNforz().getTgaPrstzIniNforzNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-INI-NFORZ
            //             TO (SF)-PRSTZ-INI-NFORZ(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzIniNforz().setWtgaPrstzIniNforz(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzIniNforz().getTgaPrstzIniNforz(), 15, 3));
        }
        // COB_CODE: IF TGA-VIS-END2000-NFORZ-NULL = HIGH-VALUES
        //                TO (SF)-VIS-END2000-NFORZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-VIS-END2000-NFORZ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaVisEnd2000Nforz().getTgaVisEnd2000NforzNullFormatted())) {
            // COB_CODE: MOVE TGA-VIS-END2000-NFORZ-NULL
            //             TO (SF)-VIS-END2000-NFORZ-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaVisEnd2000Nforz().setWtgaVisEnd2000NforzNull(ws.getTrchDiGar().getTgaVisEnd2000Nforz().getTgaVisEnd2000NforzNull());
        }
        else {
            // COB_CODE: MOVE TGA-VIS-END2000-NFORZ
            //             TO (SF)-VIS-END2000-NFORZ(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaVisEnd2000Nforz().setWtgaVisEnd2000Nforz(Trunc.toDecimal(ws.getTrchDiGar().getTgaVisEnd2000Nforz().getTgaVisEnd2000Nforz(), 15, 3));
        }
        // COB_CODE: IF TGA-INTR-MORA-NULL = HIGH-VALUES
        //                TO (SF)-INTR-MORA-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-INTR-MORA(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaIntrMora().getTgaIntrMoraNullFormatted())) {
            // COB_CODE: MOVE TGA-INTR-MORA-NULL
            //             TO (SF)-INTR-MORA-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaIntrMora().setWtgaIntrMoraNull(ws.getTrchDiGar().getTgaIntrMora().getTgaIntrMoraNull());
        }
        else {
            // COB_CODE: MOVE TGA-INTR-MORA
            //             TO (SF)-INTR-MORA(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaIntrMora().setWtgaIntrMora(Trunc.toDecimal(ws.getTrchDiGar().getTgaIntrMora().getTgaIntrMora(), 15, 3));
        }
        // COB_CODE: IF TGA-MANFEE-ANTIC-NULL = HIGH-VALUES
        //                TO (SF)-MANFEE-ANTIC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MANFEE-ANTIC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaManfeeAntic().getTgaManfeeAnticNullFormatted())) {
            // COB_CODE: MOVE TGA-MANFEE-ANTIC-NULL
            //             TO (SF)-MANFEE-ANTIC-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaManfeeAntic().setWtgaManfeeAnticNull(ws.getTrchDiGar().getTgaManfeeAntic().getTgaManfeeAnticNull());
        }
        else {
            // COB_CODE: MOVE TGA-MANFEE-ANTIC
            //             TO (SF)-MANFEE-ANTIC(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaManfeeAntic().setWtgaManfeeAntic(Trunc.toDecimal(ws.getTrchDiGar().getTgaManfeeAntic().getTgaManfeeAntic(), 15, 3));
        }
        // COB_CODE: IF TGA-MANFEE-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-MANFEE-RICOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MANFEE-RICOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaManfeeRicor().getTgaManfeeRicorNullFormatted())) {
            // COB_CODE: MOVE TGA-MANFEE-RICOR-NULL
            //             TO (SF)-MANFEE-RICOR-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaManfeeRicor().setWtgaManfeeRicorNull(ws.getTrchDiGar().getTgaManfeeRicor().getTgaManfeeRicorNull());
        }
        else {
            // COB_CODE: MOVE TGA-MANFEE-RICOR
            //             TO (SF)-MANFEE-RICOR(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaManfeeRicor().setWtgaManfeeRicor(Trunc.toDecimal(ws.getTrchDiGar().getTgaManfeeRicor().getTgaManfeeRicor(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-UNI-RIVTO-NULL = HIGH-VALUES
        //                TO (SF)-PRE-UNI-RIVTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-UNI-RIVTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreUniRivto().getTgaPreUniRivtoNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-UNI-RIVTO-NULL
            //             TO (SF)-PRE-UNI-RIVTO-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPreUniRivto().setWtgaPreUniRivtoNull(ws.getTrchDiGar().getTgaPreUniRivto().getTgaPreUniRivtoNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-UNI-RIVTO
            //             TO (SF)-PRE-UNI-RIVTO(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPreUniRivto().setWtgaPreUniRivto(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreUniRivto().getTgaPreUniRivto(), 15, 3));
        }
        // COB_CODE: IF TGA-PROV-1AA-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-PROV-1AA-ACQ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PROV-1AA-ACQ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaProv1aaAcq().getTgaProv1aaAcqNullFormatted())) {
            // COB_CODE: MOVE TGA-PROV-1AA-ACQ-NULL
            //             TO (SF)-PROV-1AA-ACQ-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaProv1aaAcq().setWtgaProv1aaAcqNull(ws.getTrchDiGar().getTgaProv1aaAcq().getTgaProv1aaAcqNull());
        }
        else {
            // COB_CODE: MOVE TGA-PROV-1AA-ACQ
            //             TO (SF)-PROV-1AA-ACQ(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaProv1aaAcq().setWtgaProv1aaAcq(Trunc.toDecimal(ws.getTrchDiGar().getTgaProv1aaAcq().getTgaProv1aaAcq(), 15, 3));
        }
        // COB_CODE: IF TGA-PROV-2AA-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-PROV-2AA-ACQ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PROV-2AA-ACQ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaProv2aaAcq().getTgaProv2aaAcqNullFormatted())) {
            // COB_CODE: MOVE TGA-PROV-2AA-ACQ-NULL
            //             TO (SF)-PROV-2AA-ACQ-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaProv2aaAcq().setWtgaProv2aaAcqNull(ws.getTrchDiGar().getTgaProv2aaAcq().getTgaProv2aaAcqNull());
        }
        else {
            // COB_CODE: MOVE TGA-PROV-2AA-ACQ
            //             TO (SF)-PROV-2AA-ACQ(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaProv2aaAcq().setWtgaProv2aaAcq(Trunc.toDecimal(ws.getTrchDiGar().getTgaProv2aaAcq().getTgaProv2aaAcq(), 15, 3));
        }
        // COB_CODE: IF TGA-PROV-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-PROV-RICOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PROV-RICOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaProvRicor().getTgaProvRicorNullFormatted())) {
            // COB_CODE: MOVE TGA-PROV-RICOR-NULL
            //             TO (SF)-PROV-RICOR-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaProvRicor().setWtgaProvRicorNull(ws.getTrchDiGar().getTgaProvRicor().getTgaProvRicorNull());
        }
        else {
            // COB_CODE: MOVE TGA-PROV-RICOR
            //             TO (SF)-PROV-RICOR(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaProvRicor().setWtgaProvRicor(Trunc.toDecimal(ws.getTrchDiGar().getTgaProvRicor().getTgaProvRicor(), 15, 3));
        }
        // COB_CODE: IF TGA-PROV-INC-NULL = HIGH-VALUES
        //                TO (SF)-PROV-INC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PROV-INC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaProvInc().getTgaProvIncNullFormatted())) {
            // COB_CODE: MOVE TGA-PROV-INC-NULL
            //             TO (SF)-PROV-INC-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaProvInc().setWtgaProvIncNull(ws.getTrchDiGar().getTgaProvInc().getTgaProvIncNull());
        }
        else {
            // COB_CODE: MOVE TGA-PROV-INC
            //             TO (SF)-PROV-INC(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaProvInc().setWtgaProvInc(Trunc.toDecimal(ws.getTrchDiGar().getTgaProvInc().getTgaProvInc(), 15, 3));
        }
        // COB_CODE: IF TGA-ALQ-PROV-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-PROV-ACQ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-PROV-ACQ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqProvAcq().getTgaAlqProvAcqNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-PROV-ACQ-NULL
            //             TO (SF)-ALQ-PROV-ACQ-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaAlqProvAcq().setWtgaAlqProvAcqNull(ws.getTrchDiGar().getTgaAlqProvAcq().getTgaAlqProvAcqNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-PROV-ACQ
            //             TO (SF)-ALQ-PROV-ACQ(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaAlqProvAcq().setWtgaAlqProvAcq(Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqProvAcq().getTgaAlqProvAcq(), 6, 3));
        }
        // COB_CODE: IF TGA-ALQ-PROV-INC-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-PROV-INC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-PROV-INC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqProvInc().getTgaAlqProvIncNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-PROV-INC-NULL
            //             TO (SF)-ALQ-PROV-INC-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaAlqProvInc().setWtgaAlqProvIncNull(ws.getTrchDiGar().getTgaAlqProvInc().getTgaAlqProvIncNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-PROV-INC
            //             TO (SF)-ALQ-PROV-INC(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaAlqProvInc().setWtgaAlqProvInc(Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqProvInc().getTgaAlqProvInc(), 6, 3));
        }
        // COB_CODE: IF TGA-ALQ-PROV-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-PROV-RICOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-PROV-RICOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqProvRicor().getTgaAlqProvRicorNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-PROV-RICOR-NULL
            //             TO (SF)-ALQ-PROV-RICOR-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaAlqProvRicor().setWtgaAlqProvRicorNull(ws.getTrchDiGar().getTgaAlqProvRicor().getTgaAlqProvRicorNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-PROV-RICOR
            //             TO (SF)-ALQ-PROV-RICOR(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaAlqProvRicor().setWtgaAlqProvRicor(Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqProvRicor().getTgaAlqProvRicor(), 6, 3));
        }
        // COB_CODE: IF TGA-IMPB-PROV-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-PROV-ACQ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-PROV-ACQ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbProvAcq().getTgaImpbProvAcqNullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-PROV-ACQ-NULL
            //             TO (SF)-IMPB-PROV-ACQ-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpbProvAcq().setWtgaImpbProvAcqNull(ws.getTrchDiGar().getTgaImpbProvAcq().getTgaImpbProvAcqNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-PROV-ACQ
            //             TO (SF)-IMPB-PROV-ACQ(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpbProvAcq().setWtgaImpbProvAcq(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbProvAcq().getTgaImpbProvAcq(), 15, 3));
        }
        // COB_CODE: IF TGA-IMPB-PROV-INC-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-PROV-INC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-PROV-INC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbProvInc().getTgaImpbProvIncNullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-PROV-INC-NULL
            //             TO (SF)-IMPB-PROV-INC-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpbProvInc().setWtgaImpbProvIncNull(ws.getTrchDiGar().getTgaImpbProvInc().getTgaImpbProvIncNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-PROV-INC
            //             TO (SF)-IMPB-PROV-INC(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpbProvInc().setWtgaImpbProvInc(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbProvInc().getTgaImpbProvInc(), 15, 3));
        }
        // COB_CODE: IF TGA-IMPB-PROV-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-PROV-RICOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-PROV-RICOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbProvRicor().getTgaImpbProvRicorNullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-PROV-RICOR-NULL
            //             TO (SF)-IMPB-PROV-RICOR-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpbProvRicor().setWtgaImpbProvRicorNull(ws.getTrchDiGar().getTgaImpbProvRicor().getTgaImpbProvRicorNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-PROV-RICOR
            //             TO (SF)-IMPB-PROV-RICOR(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpbProvRicor().setWtgaImpbProvRicor(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbProvRicor().getTgaImpbProvRicor(), 15, 3));
        }
        // COB_CODE: IF TGA-FL-PROV-FORZ-NULL = HIGH-VALUES
        //                TO (SF)-FL-PROV-FORZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-FL-PROV-FORZ(IX-TAB-TGA)
        //           END-IF
        if (Conditions.eq(ws.getTrchDiGar().getTgaFlProvForz(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TGA-FL-PROV-FORZ-NULL
            //             TO (SF)-FL-PROV-FORZ-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaFlProvForz(ws.getTrchDiGar().getTgaFlProvForz());
        }
        else {
            // COB_CODE: MOVE TGA-FL-PROV-FORZ
            //             TO (SF)-FL-PROV-FORZ(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaFlProvForz(ws.getTrchDiGar().getTgaFlProvForz());
        }
        // COB_CODE: IF TGA-PRSTZ-AGG-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-AGG-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-AGG-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzAggIni().getTgaPrstzAggIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-AGG-INI-NULL
            //             TO (SF)-PRSTZ-AGG-INI-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzAggIni().setWtgaPrstzAggIniNull(ws.getTrchDiGar().getTgaPrstzAggIni().getTgaPrstzAggIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-AGG-INI
            //             TO (SF)-PRSTZ-AGG-INI(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzAggIni().setWtgaPrstzAggIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzAggIni().getTgaPrstzAggIni(), 15, 3));
        }
        // COB_CODE: IF TGA-INCR-PRE-NULL = HIGH-VALUES
        //                TO (SF)-INCR-PRE-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-INCR-PRE(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaIncrPre().getTgaIncrPreNullFormatted())) {
            // COB_CODE: MOVE TGA-INCR-PRE-NULL
            //             TO (SF)-INCR-PRE-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaIncrPre().setWtgaIncrPreNull(ws.getTrchDiGar().getTgaIncrPre().getTgaIncrPreNull());
        }
        else {
            // COB_CODE: MOVE TGA-INCR-PRE
            //             TO (SF)-INCR-PRE(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaIncrPre().setWtgaIncrPre(Trunc.toDecimal(ws.getTrchDiGar().getTgaIncrPre().getTgaIncrPre(), 15, 3));
        }
        // COB_CODE: IF TGA-INCR-PRSTZ-NULL = HIGH-VALUES
        //                TO (SF)-INCR-PRSTZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-INCR-PRSTZ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaIncrPrstz().getTgaIncrPrstzNullFormatted())) {
            // COB_CODE: MOVE TGA-INCR-PRSTZ-NULL
            //             TO (SF)-INCR-PRSTZ-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaIncrPrstz().setWtgaIncrPrstzNull(ws.getTrchDiGar().getTgaIncrPrstz().getTgaIncrPrstzNull());
        }
        else {
            // COB_CODE: MOVE TGA-INCR-PRSTZ
            //             TO (SF)-INCR-PRSTZ(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaIncrPrstz().setWtgaIncrPrstz(Trunc.toDecimal(ws.getTrchDiGar().getTgaIncrPrstz().getTgaIncrPrstz(), 15, 3));
        }
        // COB_CODE: IF TGA-DT-ULT-ADEG-PRE-PR-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-ADEG-PRE-PR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-ULT-ADEG-PRE-PR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtUltAdegPrePr().getTgaDtUltAdegPrePrNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-ULT-ADEG-PRE-PR-NULL
            //             TO (SF)-DT-ULT-ADEG-PRE-PR-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().setWtgaDtUltAdegPrePrNull(ws.getTrchDiGar().getTgaDtUltAdegPrePr().getTgaDtUltAdegPrePrNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-ULT-ADEG-PRE-PR
            //             TO (SF)-DT-ULT-ADEG-PRE-PR(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().setWtgaDtUltAdegPrePr(ws.getTrchDiGar().getTgaDtUltAdegPrePr().getTgaDtUltAdegPrePr());
        }
        // COB_CODE: IF TGA-PRSTZ-AGG-ULT-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-AGG-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-AGG-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzAggUlt().getTgaPrstzAggUltNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-AGG-ULT-NULL
            //             TO (SF)-PRSTZ-AGG-ULT-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzAggUlt().setWtgaPrstzAggUltNull(ws.getTrchDiGar().getTgaPrstzAggUlt().getTgaPrstzAggUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-AGG-ULT
            //             TO (SF)-PRSTZ-AGG-ULT(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzAggUlt().setWtgaPrstzAggUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzAggUlt().getTgaPrstzAggUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-TS-RIVAL-NET-NULL = HIGH-VALUES
        //                TO (SF)-TS-RIVAL-NET-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TS-RIVAL-NET(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTsRivalNet().getTgaTsRivalNetNullFormatted())) {
            // COB_CODE: MOVE TGA-TS-RIVAL-NET-NULL
            //             TO (SF)-TS-RIVAL-NET-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaTsRivalNet().setWtgaTsRivalNetNull(ws.getTrchDiGar().getTgaTsRivalNet().getTgaTsRivalNetNull());
        }
        else {
            // COB_CODE: MOVE TGA-TS-RIVAL-NET
            //             TO (SF)-TS-RIVAL-NET(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaTsRivalNet().setWtgaTsRivalNet(Trunc.toDecimal(ws.getTrchDiGar().getTgaTsRivalNet().getTgaTsRivalNet(), 14, 9));
        }
        // COB_CODE: IF TGA-PRE-PATTUITO-NULL = HIGH-VALUES
        //                TO (SF)-PRE-PATTUITO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-PATTUITO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrePattuito().getTgaPrePattuitoNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-PATTUITO-NULL
            //             TO (SF)-PRE-PATTUITO-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPrePattuito().setWtgaPrePattuitoNull(ws.getTrchDiGar().getTgaPrePattuito().getTgaPrePattuitoNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-PATTUITO
            //             TO (SF)-PRE-PATTUITO(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPrePattuito().setWtgaPrePattuito(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrePattuito().getTgaPrePattuito(), 15, 3));
        }
        // COB_CODE: IF TGA-TP-RIVAL-NULL = HIGH-VALUES
        //                TO (SF)-TP-RIVAL-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TP-RIVAL(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTpRivalFormatted())) {
            // COB_CODE: MOVE TGA-TP-RIVAL-NULL
            //             TO (SF)-TP-RIVAL-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaTpRival(ws.getTrchDiGar().getTgaTpRival());
        }
        else {
            // COB_CODE: MOVE TGA-TP-RIVAL
            //             TO (SF)-TP-RIVAL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaTpRival(ws.getTrchDiGar().getTgaTpRival());
        }
        // COB_CODE: IF TGA-RIS-MAT-NULL = HIGH-VALUES
        //                TO (SF)-RIS-MAT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-RIS-MAT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRisMat().getTgaRisMatNullFormatted())) {
            // COB_CODE: MOVE TGA-RIS-MAT-NULL
            //             TO (SF)-RIS-MAT-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaRisMat().setWtgaRisMatNull(ws.getTrchDiGar().getTgaRisMat().getTgaRisMatNull());
        }
        else {
            // COB_CODE: MOVE TGA-RIS-MAT
            //             TO (SF)-RIS-MAT(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaRisMat().setWtgaRisMat(Trunc.toDecimal(ws.getTrchDiGar().getTgaRisMat().getTgaRisMat(), 15, 3));
        }
        // COB_CODE: IF TGA-CPT-MIN-SCAD-NULL = HIGH-VALUES
        //                TO (SF)-CPT-MIN-SCAD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-CPT-MIN-SCAD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCptMinScad().getTgaCptMinScadNullFormatted())) {
            // COB_CODE: MOVE TGA-CPT-MIN-SCAD-NULL
            //             TO (SF)-CPT-MIN-SCAD-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaCptMinScad().setWtgaCptMinScadNull(ws.getTrchDiGar().getTgaCptMinScad().getTgaCptMinScadNull());
        }
        else {
            // COB_CODE: MOVE TGA-CPT-MIN-SCAD
            //             TO (SF)-CPT-MIN-SCAD(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaCptMinScad().setWtgaCptMinScad(Trunc.toDecimal(ws.getTrchDiGar().getTgaCptMinScad().getTgaCptMinScad(), 15, 3));
        }
        // COB_CODE: IF TGA-COMMIS-GEST-NULL = HIGH-VALUES
        //                TO (SF)-COMMIS-GEST-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-COMMIS-GEST(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCommisGest().getTgaCommisGestNullFormatted())) {
            // COB_CODE: MOVE TGA-COMMIS-GEST-NULL
            //             TO (SF)-COMMIS-GEST-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaCommisGest().setWtgaCommisGestNull(ws.getTrchDiGar().getTgaCommisGest().getTgaCommisGestNull());
        }
        else {
            // COB_CODE: MOVE TGA-COMMIS-GEST
            //             TO (SF)-COMMIS-GEST(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaCommisGest().setWtgaCommisGest(Trunc.toDecimal(ws.getTrchDiGar().getTgaCommisGest().getTgaCommisGest(), 15, 3));
        }
        // COB_CODE: IF TGA-TP-MANFEE-APPL-NULL = HIGH-VALUES
        //                TO (SF)-TP-MANFEE-APPL-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TP-MANFEE-APPL(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTpManfeeApplFormatted())) {
            // COB_CODE: MOVE TGA-TP-MANFEE-APPL-NULL
            //             TO (SF)-TP-MANFEE-APPL-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaTpManfeeAppl(ws.getTrchDiGar().getTgaTpManfeeAppl());
        }
        else {
            // COB_CODE: MOVE TGA-TP-MANFEE-APPL
            //             TO (SF)-TP-MANFEE-APPL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaTpManfeeAppl(ws.getTrchDiGar().getTgaTpManfeeAppl());
        }
        // COB_CODE: MOVE TGA-DS-RIGA
        //             TO (SF)-DS-RIGA(IX-TAB-TGA)
        ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaDsRiga(ws.getTrchDiGar().getTgaDsRiga());
        // COB_CODE: MOVE TGA-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL(IX-TAB-TGA)
        ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaDsOperSql(ws.getTrchDiGar().getTgaDsOperSql());
        // COB_CODE: MOVE TGA-DS-VER
        //             TO (SF)-DS-VER(IX-TAB-TGA)
        ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaDsVer(ws.getTrchDiGar().getTgaDsVer());
        // COB_CODE: MOVE TGA-DS-TS-INI-CPTZ
        //             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-TGA)
        ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaDsTsIniCptz(ws.getTrchDiGar().getTgaDsTsIniCptz());
        // COB_CODE: MOVE TGA-DS-TS-END-CPTZ
        //             TO (SF)-DS-TS-END-CPTZ(IX-TAB-TGA)
        ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaDsTsEndCptz(ws.getTrchDiGar().getTgaDsTsEndCptz());
        // COB_CODE: MOVE TGA-DS-UTENTE
        //             TO (SF)-DS-UTENTE(IX-TAB-TGA)
        ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaDsUtente(ws.getTrchDiGar().getTgaDsUtente());
        // COB_CODE: MOVE TGA-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB(IX-TAB-TGA)
        ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaDsStatoElab(ws.getTrchDiGar().getTgaDsStatoElab());
        // COB_CODE: IF TGA-PC-COMMIS-GEST-NULL = HIGH-VALUES
        //                TO (SF)-PC-COMMIS-GEST-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PC-COMMIS-GEST(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPcCommisGest().getTgaPcCommisGestNullFormatted())) {
            // COB_CODE: MOVE TGA-PC-COMMIS-GEST-NULL
            //             TO (SF)-PC-COMMIS-GEST-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPcCommisGest().setWtgaPcCommisGestNull(ws.getTrchDiGar().getTgaPcCommisGest().getTgaPcCommisGestNull());
        }
        else {
            // COB_CODE: MOVE TGA-PC-COMMIS-GEST
            //             TO (SF)-PC-COMMIS-GEST(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaPcCommisGest().setWtgaPcCommisGest(Trunc.toDecimal(ws.getTrchDiGar().getTgaPcCommisGest().getTgaPcCommisGest(), 6, 3));
        }
        // COB_CODE: IF TGA-NUM-GG-RIVAL-NULL = HIGH-VALUES
        //                TO (SF)-NUM-GG-RIVAL-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-NUM-GG-RIVAL(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaNumGgRival().getTgaNumGgRivalNullFormatted())) {
            // COB_CODE: MOVE TGA-NUM-GG-RIVAL-NULL
            //             TO (SF)-NUM-GG-RIVAL-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaNumGgRival().setWtgaNumGgRivalNull(ws.getTrchDiGar().getTgaNumGgRival().getTgaNumGgRivalNull());
        }
        else {
            // COB_CODE: MOVE TGA-NUM-GG-RIVAL
            //             TO (SF)-NUM-GG-RIVAL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaNumGgRival().setWtgaNumGgRival(ws.getTrchDiGar().getTgaNumGgRival().getTgaNumGgRival());
        }
        // COB_CODE: IF TGA-IMP-TRASFE-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TRASFE-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-TRASFE(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpTrasfe().getTgaImpTrasfeNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-TRASFE-NULL
            //             TO (SF)-IMP-TRASFE-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpTrasfe().setWtgaImpTrasfeNull(ws.getTrchDiGar().getTgaImpTrasfe().getTgaImpTrasfeNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-TRASFE
            //             TO (SF)-IMP-TRASFE(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpTrasfe().setWtgaImpTrasfe(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpTrasfe().getTgaImpTrasfe(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-TFR-STRC-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-TFR-STRC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpTfrStrc().getTgaImpTfrStrcNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-TFR-STRC-NULL
            //             TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpTfrStrc().setWtgaImpTfrStrcNull(ws.getTrchDiGar().getTgaImpTfrStrc().getTgaImpTfrStrcNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-TFR-STRC
            //             TO (SF)-IMP-TFR-STRC(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpTfrStrc().setWtgaImpTfrStrc(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpTfrStrc().getTgaImpTfrStrc(), 15, 3));
        }
        // COB_CODE: IF TGA-ACQ-EXP-NULL = HIGH-VALUES
        //                TO (SF)-ACQ-EXP-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ACQ-EXP(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAcqExp().getTgaAcqExpNullFormatted())) {
            // COB_CODE: MOVE TGA-ACQ-EXP-NULL
            //             TO (SF)-ACQ-EXP-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaAcqExp().setWtgaAcqExpNull(ws.getTrchDiGar().getTgaAcqExp().getTgaAcqExpNull());
        }
        else {
            // COB_CODE: MOVE TGA-ACQ-EXP
            //             TO (SF)-ACQ-EXP(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaAcqExp().setWtgaAcqExp(Trunc.toDecimal(ws.getTrchDiGar().getTgaAcqExp().getTgaAcqExp(), 15, 3));
        }
        // COB_CODE: IF TGA-REMUN-ASS-NULL = HIGH-VALUES
        //                TO (SF)-REMUN-ASS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-REMUN-ASS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRemunAss().getTgaRemunAssNullFormatted())) {
            // COB_CODE: MOVE TGA-REMUN-ASS-NULL
            //             TO (SF)-REMUN-ASS-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaRemunAss().setWtgaRemunAssNull(ws.getTrchDiGar().getTgaRemunAss().getTgaRemunAssNull());
        }
        else {
            // COB_CODE: MOVE TGA-REMUN-ASS
            //             TO (SF)-REMUN-ASS(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaRemunAss().setWtgaRemunAss(Trunc.toDecimal(ws.getTrchDiGar().getTgaRemunAss().getTgaRemunAss(), 15, 3));
        }
        // COB_CODE: IF TGA-COMMIS-INTER-NULL = HIGH-VALUES
        //                TO (SF)-COMMIS-INTER-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-COMMIS-INTER(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCommisInter().getTgaCommisInterNullFormatted())) {
            // COB_CODE: MOVE TGA-COMMIS-INTER-NULL
            //             TO (SF)-COMMIS-INTER-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaCommisInter().setWtgaCommisInterNull(ws.getTrchDiGar().getTgaCommisInter().getTgaCommisInterNull());
        }
        else {
            // COB_CODE: MOVE TGA-COMMIS-INTER
            //             TO (SF)-COMMIS-INTER(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaCommisInter().setWtgaCommisInter(Trunc.toDecimal(ws.getTrchDiGar().getTgaCommisInter().getTgaCommisInter(), 15, 3));
        }
        // COB_CODE: IF TGA-ALQ-REMUN-ASS-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-REMUN-ASS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-REMUN-ASS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqRemunAss().getTgaAlqRemunAssNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-REMUN-ASS-NULL
            //             TO (SF)-ALQ-REMUN-ASS-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaAlqRemunAss().setWtgaAlqRemunAssNull(ws.getTrchDiGar().getTgaAlqRemunAss().getTgaAlqRemunAssNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-REMUN-ASS
            //             TO (SF)-ALQ-REMUN-ASS(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaAlqRemunAss().setWtgaAlqRemunAss(Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqRemunAss().getTgaAlqRemunAss(), 6, 3));
        }
        // COB_CODE: IF TGA-ALQ-COMMIS-INTER-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-COMMIS-INTER-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-COMMIS-INTER(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqCommisInter().getTgaAlqCommisInterNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-COMMIS-INTER-NULL
            //             TO (SF)-ALQ-COMMIS-INTER-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaAlqCommisInter().setWtgaAlqCommisInterNull(ws.getTrchDiGar().getTgaAlqCommisInter().getTgaAlqCommisInterNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-COMMIS-INTER
            //             TO (SF)-ALQ-COMMIS-INTER(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaAlqCommisInter().setWtgaAlqCommisInter(Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqCommisInter().getTgaAlqCommisInter(), 6, 3));
        }
        // COB_CODE: IF TGA-IMPB-REMUN-ASS-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-REMUN-ASS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-REMUN-ASS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbRemunAss().getTgaImpbRemunAssNullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-REMUN-ASS-NULL
            //             TO (SF)-IMPB-REMUN-ASS-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpbRemunAss().setWtgaImpbRemunAssNull(ws.getTrchDiGar().getTgaImpbRemunAss().getTgaImpbRemunAssNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-REMUN-ASS
            //             TO (SF)-IMPB-REMUN-ASS(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpbRemunAss().setWtgaImpbRemunAss(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbRemunAss().getTgaImpbRemunAss(), 15, 3));
        }
        // COB_CODE: IF TGA-IMPB-COMMIS-INTER-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-COMMIS-INTER-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-COMMIS-INTER(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbCommisInter().getTgaImpbCommisInterNullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-COMMIS-INTER-NULL
            //             TO (SF)-IMPB-COMMIS-INTER-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpbCommisInter().setWtgaImpbCommisInterNull(ws.getTrchDiGar().getTgaImpbCommisInter().getTgaImpbCommisInterNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-COMMIS-INTER
            //             TO (SF)-IMPB-COMMIS-INTER(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaImpbCommisInter().setWtgaImpbCommisInter(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbCommisInter().getTgaImpbCommisInter(), 15, 3));
        }
        // COB_CODE: IF TGA-COS-RUN-ASSVA-NULL = HIGH-VALUES
        //                TO (SF)-COS-RUN-ASSVA-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-COS-RUN-ASSVA(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCosRunAssva().getTgaCosRunAssvaNullFormatted())) {
            // COB_CODE: MOVE TGA-COS-RUN-ASSVA-NULL
            //             TO (SF)-COS-RUN-ASSVA-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaCosRunAssva().setWtgaCosRunAssvaNull(ws.getTrchDiGar().getTgaCosRunAssva().getTgaCosRunAssvaNull());
        }
        else {
            // COB_CODE: MOVE TGA-COS-RUN-ASSVA
            //             TO (SF)-COS-RUN-ASSVA(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaCosRunAssva().setWtgaCosRunAssva(Trunc.toDecimal(ws.getTrchDiGar().getTgaCosRunAssva().getTgaCosRunAssva(), 15, 3));
        }
        // COB_CODE: IF TGA-COS-RUN-ASSVA-IDC-NULL = HIGH-VALUES
        //                TO (SF)-COS-RUN-ASSVA-IDC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-COS-RUN-ASSVA-IDC(IX-TAB-TGA)
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCosRunAssvaIdc().getTgaCosRunAssvaIdcNullFormatted())) {
            // COB_CODE: MOVE TGA-COS-RUN-ASSVA-IDC-NULL
            //             TO (SF)-COS-RUN-ASSVA-IDC-NULL(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaCosRunAssvaIdc().setWtgaCosRunAssvaIdcNull(ws.getTrchDiGar().getTgaCosRunAssvaIdc().getTgaCosRunAssvaIdcNull());
        }
        else {
            // COB_CODE: MOVE TGA-COS-RUN-ASSVA-IDC
            //             TO (SF)-COS-RUN-ASSVA-IDC(IX-TAB-TGA)
            ws.getAreeAppoggio().getDtgaTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaCosRunAssvaIdc().setWtgaCosRunAssvaIdc(Trunc.toDecimal(ws.getTrchDiGar().getTgaCosRunAssvaIdc().getTgaCosRunAssvaIdc(), 15, 3));
        }
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES CALL DISPATCHER
	 * ----------------------------------------------------------------*
	 * ****************************************************************
	 *          CALL DISPATCHER
	 * ****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0003-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(idsv0003.getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: MOVE IDSV0003-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(idsv0003.getCodMainBatch());
        // COB_CODE: MOVE IDSV0003-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(idsv0003.getTipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0003-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(idsv0003.getSessione());
        // COB_CODE: MOVE IDSV0003-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(idsv0003.getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //                TO IDSI0011-DATA-INIZIO-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
            //             TO IDSI0011-DATA-INIZIO-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(idsv0003.getDataInizioEffetto());
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231 TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231 TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(idsv0003.getDataCompAggStor());
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                                       TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA
            //                                    TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE: MOVE IDSV0003-FORMATO-DATA-DB
        //                                       TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(idsv0003.getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE 0                      TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE    TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        //     SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        //     MOVE IDSI0011-AREA          TO IDSV0012-AREA-I-O
        //     SET WS-ADDRESS-DIS          TO ADDRESS OF IDSV0012
        // COB_CODE: MOVE 'IDSS0010'             TO WK-CALL-PGM
        ws.setWkCallPgm("IDSS0010");
        //     CALL WK-CALL-PGM           USING DISPATCHER-ADDRESS
        //                                      IDSV0012.
        //     MOVE IDSV0012-AREA-I-O      TO IDSO0011-AREA.
        // COB_CODE: CALL WK-CALL-PGM            USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                 TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    public void initIxIndici() {
        ws.getIxIndici().setIxTabParamOgg(((short)0));
        ws.getIxIndici().setIxTabDco(((short)0));
        ws.getIxIndici().setIxTabAde(((short)0));
        ws.getIxIndici().setIxTabGrz(((short)0));
        ws.getIxIndici().setIxTabTga(((short)0));
        ws.getIxIndici().setIxTabPog(((short)0));
        ws.getIxIndici().setIxTabPmo(((short)0));
        ws.getIxIndici().setContFetch(((short)0));
        ws.getIxIndici().setIxDclgen(((short)0));
    }

    public void initAreeAppoggio() {
        ws.getAreeAppoggio().setDdcoEleDcoMax(((short)0));
        ws.getAreeAppoggio().getLccvdco1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getAreeAppoggio().getLccvdco1().setIdPtf(0);
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoIdDColl(0);
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoIdPoli(0);
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoIdMoviCrz(0);
        ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoIdMoviChiu().setWdcoIdMoviChiu(0);
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoDtIniEff(0);
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoDtEndEff(0);
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoCodCompAnia(0);
        ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoImpArrotPre().setWdcoImpArrotPre(new AfDecimal(0, 15, 3));
        ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoPcScon().setWdcoPcScon(new AfDecimal(0, 6, 3));
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoFlAdesSing(Types.SPACE_CHAR);
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoTpImp("");
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoFlRiclPreDaCpt(Types.SPACE_CHAR);
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoTpAdes("");
        ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoDtUltRinnTac().setWdcoDtUltRinnTac(0);
        ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoImpScon().setWdcoImpScon(new AfDecimal(0, 15, 3));
        ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoFrazDflt().setWdcoFrazDflt(0);
        ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoEtaScadMascDflt().setWdcoEtaScadMascDflt(0);
        ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoEtaScadFemmDflt().setWdcoEtaScadFemmDflt(0);
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoTpDfltDur("");
        ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoDurAaAdesDflt().setWdcoDurAaAdesDflt(0);
        ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoDurMmAdesDflt().setWdcoDurMmAdesDflt(0);
        ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoDurGgAdesDflt().setWdcoDurGgAdesDflt(0);
        ws.getAreeAppoggio().getLccvdco1().getDati().getWdcoDtScadAdesDflt().setWdcoDtScadAdesDflt(0);
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoCodFndDflt("");
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoTpDur("");
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoTpCalcDur("");
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoFlNoAderenti(Types.SPACE_CHAR);
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoFlDistintaCnbtva(Types.SPACE_CHAR);
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoFlCnbtAutes(Types.SPACE_CHAR);
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoFlQtzPostEmis(Types.SPACE_CHAR);
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoFlComnzFndIs(Types.SPACE_CHAR);
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoDsRiga(0);
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoDsOperSql(Types.SPACE_CHAR);
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoDsVer(0);
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoDsTsIniCptz(0);
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoDsTsEndCptz(0);
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoDsUtente("");
        ws.getAreeAppoggio().getLccvdco1().getDati().setWdcoDsStatoElab(Types.SPACE_CHAR);
        ws.getAreeAppoggio().setDgrzEleGarMax(((short)0));
        for (int idx0 = 1; idx0 <= AreeAppoggio.DGRZ_TAB_GAR_MAXOCCURS; idx0++) {
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().setIdPtf(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIdGar(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzIdAdes().setWgrzIdAdes(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIdPoli(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIdMoviCrz(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzIdMoviChiu().setWgrzIdMoviChiu(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDtIniEff(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDtEndEff(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodCompAnia(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIbOgg("");
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtDecor().setWgrzDtDecor(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtScad().setWgrzDtScad(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodSez("");
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodTari("");
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzRamoBila("");
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtIniValTar().setWgrzDtIniValTar(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzId1oAssto().setWgrzId1oAssto(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzId2oAssto().setWgrzId2oAssto(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzId3oAssto().setWgrzId3oAssto(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzTpGar().setWgrzTpGar(((short)0));
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpRsh("");
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzTpInvst().setWgrzTpInvst(((short)0));
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzModPagGarcol("");
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPerPre("");
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa1oAssto().setWgrzEtaAa1oAssto(((short)0));
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm1oAssto().setWgrzEtaMm1oAssto(((short)0));
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa2oAssto().setWgrzEtaAa2oAssto(((short)0));
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm2oAssto().setWgrzEtaMm2oAssto(((short)0));
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa3oAssto().setWgrzEtaAa3oAssto(((short)0));
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm3oAssto().setWgrzEtaMm3oAssto(((short)0));
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpEmisPur(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAScad().setWgrzEtaAScad(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpCalcPrePrstz("");
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPre(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpDur("");
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDurAa().setWgrzDurAa(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDurMm().setWgrzDurMm(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDurGg().setWgrzDurGg(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzNumAaPagPre().setWgrzNumAaPagPre(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzAaPagPreUni().setWgrzAaPagPreUni(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzMmPagPreUni().setWgrzMmPagPreUni(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzFrazIniErogRen().setWgrzFrazIniErogRen(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzMm1oRat().setWgrzMm1oRat(((short)0));
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPc1oRat().setWgrzPc1oRat(new AfDecimal(0, 6, 3));
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPrstzAssta("");
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtEndCarz().setWgrzDtEndCarz(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPcRipPre().setWgrzPcRipPre(new AfDecimal(0, 6, 3));
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodFnd("");
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzAaRenCer("");
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPcRevrsb().setWgrzPcRevrsb(new AfDecimal(0, 6, 3));
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPcRip("");
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPcOpz().setWgrzPcOpz(new AfDecimal(0, 6, 3));
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpIas("");
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpStab("");
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpAdegPre(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtVarzTpIas().setWgrzDtVarzTpIas(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzFrazDecrCpt().setWgrzFrazDecrCpt(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodTratRiass("");
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpDtEmisRiass("");
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpCessRiass("");
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsRiga(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsOperSql(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsVer(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsTsIniCptz(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsTsEndCptz(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsUtente("");
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsStatoElab(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzAaStab().setWgrzAaStab(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzTsStabLimitata().setWgrzTsStabLimitata(new AfDecimal(0, 14, 9));
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtPresc().setWgrzDtPresc(0);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzRshInvst(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpRamoBila("");
        }
        ws.getAreeAppoggio().setDadeEleAdesMax(((short)0));
        for (int idx0 = 1; idx0 <= AreeAppoggio.DADE_TAB_ADES_MAXOCCURS; idx0++) {
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().setIdPtf(0);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeIdAdes(0);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeIdPoli(0);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeIdMoviCrz(0);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadeIdMoviChiu().setWadeIdMoviChiu(0);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeDtIniEff(0);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeDtEndEff(0);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeIbPrev("");
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeIbOgg("");
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeCodCompAnia(0);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadeDtDecor().setWadeDtDecor(0);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadeDtScad().setWadeDtScad(0);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadeEtaAScad().setWadeEtaAScad(0);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadeDurAa().setWadeDurAa(0);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadeDurMm().setWadeDurMm(0);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadeDurGg().setWadeDurGg(0);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeTpRgmFisc("");
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeTpRiat("");
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeTpModPagTit("");
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeTpIas("");
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadeDtVarzTpIas().setWadeDtVarzTpIas(0);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadePreNetInd().setWadePreNetInd(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadePreLrdInd().setWadePreLrdInd(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadeRatLrdInd().setWadeRatLrdInd(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadePrstzIniInd().setWadePrstzIniInd(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeFlCoincAssto(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeIbDflt("");
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeModCalc("");
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeTpFntCnbtva("");
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadeImpAz().setWadeImpAz(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadeImpAder().setWadeImpAder(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadeImpTfr().setWadeImpTfr(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadeImpVolo().setWadeImpVolo(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadePcAz().setWadePcAz(new AfDecimal(0, 6, 3));
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadePcAder().setWadePcAder(new AfDecimal(0, 6, 3));
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadePcTfr().setWadePcTfr(new AfDecimal(0, 6, 3));
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadePcVolo().setWadePcVolo(new AfDecimal(0, 6, 3));
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadeDtNovaRgmFisc().setWadeDtNovaRgmFisc(0);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeFlAttiv(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadeImpRecRitVis().setWadeImpRecRitVis(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadeImpRecRitAcc().setWadeImpRecRitAcc(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeFlVarzStatTbgc(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeFlProvzaMigraz(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadeImpbVisDaRec().setWadeImpbVisDaRec(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadeDtDecorPrestBan().setWadeDtDecorPrestBan(0);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadeDtEffVarzStatT().setWadeDtEffVarzStatT(0);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeDsRiga(0);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeDsOperSql(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeDsVer(0);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeDsTsIniCptz(0);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeDsTsEndCptz(0);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeDsUtente("");
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeDsStatoElab(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadeCumCnbtCap().setWadeCumCnbtCap(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadeImpGarCnbt().setWadeImpGarCnbt(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadeDtUltConsCnbt().setWadeDtUltConsCnbt(0);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeIdenIscFnd("");
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadeNumRatPian().setWadeNumRatPian(new AfDecimal(0, 12, 5));
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().getWadeDtPresc().setWadeDtPresc(0);
            ws.getAreeAppoggio().getDadeTabAdes(idx0).getLccvade1().getDati().setWadeConcsPrest(Types.SPACE_CHAR);
        }
        ws.getAreeAppoggio().setDpmoEleParamMoviMax(((short)0));
        for (int idx0 = 1; idx0 <= AreeAppoggio.DPMO_TAB_PARAM_MOV_MAXOCCURS; idx0++) {
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().setIdPtf(0);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIdParamMovi(0);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIdOgg(0);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpOgg("");
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIdMoviCrz(0);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoIdMoviChiu().setWpmoIdMoviChiu(0);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDtIniEff(0);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDtEndEff(0);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoCodCompAnia(0);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoTpMovi().setWpmoTpMovi(0);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoFrqMovi().setWpmoFrqMovi(0);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDurAa().setWpmoDurAa(0);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDurMm().setWpmoDurMm(0);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDurGg().setWpmoDurGg(0);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDtRicorPrec().setWpmoDtRicorPrec(0);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDtRicorSucc().setWpmoDtRicorSucc(0);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcIntrFraz().setWpmoPcIntrFraz(new AfDecimal(0, 6, 3));
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpBnsDaScoTot().setWpmoImpBnsDaScoTot(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpBnsDaSco().setWpmoImpBnsDaSco(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcAnticBns().setWpmoPcAnticBns(new AfDecimal(0, 6, 3));
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpRinnColl("");
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpRivalPre("");
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpRivalPrstz("");
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoFlEvidRival(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoUltPcPerd().setWpmoUltPcPerd(new AfDecimal(0, 6, 3));
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoTotAaGiaPror().setWpmoTotAaGiaPror(0);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpOpz("");
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoAaRenCer().setWpmoAaRenCer(0);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcRevrsb().setWpmoPcRevrsb(new AfDecimal(0, 6, 3));
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpRiscParzPrgt().setWpmoImpRiscParzPrgt(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpLrdDiRat().setWpmoImpLrdDiRat(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIbOgg("");
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoCosOner().setWpmoCosOner(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoSpePc().setWpmoSpePc(new AfDecimal(0, 6, 3));
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoFlAttivGar(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoCambioVerProd(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoMmDiff().setWpmoMmDiff(((short)0));
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpRatManfee().setWpmoImpRatManfee(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDtUltErogManfee().setWpmoDtUltErogManfee(0);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpOggRival("");
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoSomAsstaGarac().setWpmoSomAsstaGarac(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcApplzOpz().setWpmoPcApplzOpz(new AfDecimal(0, 6, 3));
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoIdAdes().setWpmoIdAdes(0);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIdPoli(0);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpFrmAssva("");
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsRiga(0);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsOperSql(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsVer(0);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsTsIniCptz(0);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsTsEndCptz(0);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsUtente("");
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsStatoElab(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpEstrCnt("");
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoCodRamo("");
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoGenDaSin(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoCodTari("");
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoNumRatPagPre().setWpmoNumRatPagPre(0);
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcServVal().setWpmoPcServVal(new AfDecimal(0, 6, 3));
            ws.getAreeAppoggio().getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoEtaAaSoglBnficr().setWpmoEtaAaSoglBnficr(((short)0));
        }
        ws.getAreeAppoggio().setDtgaEleTgaMax(((short)0));
        for (int idx0 = 1; idx0 <= AreeAppoggio.DTGA_TAB_TRAN_MAXOCCURS; idx0++) {
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().setIdPtf(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdTrchDiGar(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdGar(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdAdes(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdPoli(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdMoviCrz(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIdMoviChiu().setWtgaIdMoviChiu(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDtIniEff(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDtEndEff(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaCodCompAnia(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDtDecor(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtScad().setWtgaDtScad(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIbOgg("");
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpRgmFisc("");
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtEmis().setWtgaDtEmis(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpTrch("");
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurAa().setWtgaDurAa(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurMm().setWtgaDurMm(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurGg().setWtgaDurGg(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreCasoMor().setWtgaPreCasoMor(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcIntrRiat().setWtgaPcIntrRiat(new AfDecimal(0, 6, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpBnsAntic().setWtgaImpBnsAntic(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreIniNet().setWtgaPreIniNet(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrePpIni().setWtgaPrePpIni(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrePpUlt().setWtgaPrePpUlt(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreTariIni().setWtgaPreTariIni(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreTariUlt().setWtgaPreTariUlt(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreInvrioIni().setWtgaPreInvrioIni(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreInvrioUlt().setWtgaPreInvrioUlt(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreRivto().setWtgaPreRivto(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprProf().setWtgaImpSoprProf(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprSan().setWtgaImpSoprSan(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprSpo().setWtgaImpSoprSpo(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprTec().setWtgaImpSoprTec(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpAltSopr().setWtgaImpAltSopr(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreStab().setWtgaPreStab(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtEffStab().setWtgaDtEffStab(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaTsRivalFis().setWtgaTsRivalFis(new AfDecimal(0, 14, 9));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaTsRivalIndiciz().setWtgaTsRivalIndiciz(new AfDecimal(0, 14, 9));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaOldTsTec().setWtgaOldTsTec(new AfDecimal(0, 14, 9));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRatLrd().setWtgaRatLrd(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreLrd().setWtgaPreLrd(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIni().setWtgaPrstzIni(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzUlt().setWtgaPrstzUlt(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCptInOpzRivto().setWtgaCptInOpzRivto(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIniStab().setWtgaPrstzIniStab(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCptRshMor().setWtgaCptRshMor(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzRidIni().setWtgaPrstzRidIni(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaFlCarCont(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaBnsGiaLiqto().setWtgaBnsGiaLiqto(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpBns().setWtgaImpBns(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaCodDvs("");
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIniNewfis().setWtgaPrstzIniNewfis(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpScon().setWtgaImpScon(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqScon().setWtgaAlqScon(new AfDecimal(0, 6, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpCarAcq().setWtgaImpCarAcq(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpCarInc().setWtgaImpCarInc(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpCarGest().setWtgaImpCarGest(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaAa1oAssto().setWtgaEtaAa1oAssto(((short)0));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaMm1oAssto().setWtgaEtaMm1oAssto(((short)0));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaAa2oAssto().setWtgaEtaAa2oAssto(((short)0));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaMm2oAssto().setWtgaEtaMm2oAssto(((short)0));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaAa3oAssto().setWtgaEtaAa3oAssto(((short)0));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaMm3oAssto().setWtgaEtaMm3oAssto(((short)0));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRendtoLrd().setWtgaRendtoLrd(new AfDecimal(0, 14, 9));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcRetr().setWtgaPcRetr(new AfDecimal(0, 6, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRendtoRetr().setWtgaRendtoRetr(new AfDecimal(0, 14, 9));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaMinGarto().setWtgaMinGarto(new AfDecimal(0, 14, 9));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaMinTrnut().setWtgaMinTrnut(new AfDecimal(0, 14, 9));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreAttDiTrch().setWtgaPreAttDiTrch(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaMatuEnd2000().setWtgaMatuEnd2000(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAbbTotIni().setWtgaAbbTotIni(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAbbTotUlt().setWtgaAbbTotUlt(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAbbAnnuUlt().setWtgaAbbAnnuUlt(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurAbb().setWtgaDurAbb(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpAdegAbb(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaModCalc("");
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpAz().setWtgaImpAz(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpAder().setWtgaImpAder(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpTfr().setWtgaImpTfr(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpVolo().setWtgaImpVolo(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaVisEnd2000().setWtgaVisEnd2000(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtVldtProd().setWtgaDtVldtProd(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtIniValTar().setWtgaDtIniValTar(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbVisEnd2000().setWtgaImpbVisEnd2000(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRenIniTsTec0().setWtgaRenIniTsTec0(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcRipPre().setWtgaPcRipPre(new AfDecimal(0, 6, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaFlImportiForz(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIniNforz().setWtgaPrstzIniNforz(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaVisEnd2000Nforz().setWtgaVisEnd2000Nforz(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIntrMora().setWtgaIntrMora(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaManfeeAntic().setWtgaManfeeAntic(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaManfeeRicor().setWtgaManfeeRicor(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreUniRivto().setWtgaPreUniRivto(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProv1aaAcq().setWtgaProv1aaAcq(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProv2aaAcq().setWtgaProv2aaAcq(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProvRicor().setWtgaProvRicor(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProvInc().setWtgaProvInc(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqProvAcq().setWtgaAlqProvAcq(new AfDecimal(0, 6, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqProvInc().setWtgaAlqProvInc(new AfDecimal(0, 6, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqProvRicor().setWtgaAlqProvRicor(new AfDecimal(0, 6, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbProvAcq().setWtgaImpbProvAcq(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbProvInc().setWtgaImpbProvInc(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbProvRicor().setWtgaImpbProvRicor(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaFlProvForz(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzAggIni().setWtgaPrstzAggIni(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIncrPre().setWtgaIncrPre(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIncrPrstz().setWtgaIncrPrstz(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().setWtgaDtUltAdegPrePr(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzAggUlt().setWtgaPrstzAggUlt(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaTsRivalNet().setWtgaTsRivalNet(new AfDecimal(0, 14, 9));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrePattuito().setWtgaPrePattuito(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpRival("");
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRisMat().setWtgaRisMat(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCptMinScad().setWtgaCptMinScad(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCommisGest().setWtgaCommisGest(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpManfeeAppl("");
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsRiga(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsOperSql(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsVer(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsTsIniCptz(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsTsEndCptz(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsUtente("");
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsStatoElab(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcCommisGest().setWtgaPcCommisGest(new AfDecimal(0, 6, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaNumGgRival().setWtgaNumGgRival(0);
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpTrasfe().setWtgaImpTrasfe(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpTfrStrc().setWtgaImpTfrStrc(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAcqExp().setWtgaAcqExp(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRemunAss().setWtgaRemunAss(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCommisInter().setWtgaCommisInter(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqRemunAss().setWtgaAlqRemunAss(new AfDecimal(0, 6, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqCommisInter().setWtgaAlqCommisInter(new AfDecimal(0, 6, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbRemunAss().setWtgaImpbRemunAss(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbCommisInter().setWtgaImpbCommisInter(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCosRunAssva().setWtgaCosRunAssva(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCosRunAssvaIdc().setWtgaCosRunAssvaIdc(new AfDecimal(0, 15, 3));
        }
        ws.getAreeAppoggio().setDpogEleParamOggMax(((short)0));
        for (int idx0 = 1; idx0 <= AreeAppoggio.DPOG_TAB_PARAM_OGG_MAXOCCURS; idx0++) {
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().setIdPtf(0);
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getDati().setWpogIdParamOgg(0);
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getDati().setWpogIdOgg(0);
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getDati().setWpogTpOgg("");
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getDati().setWpogIdMoviCrz(0);
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getDati().getWpogIdMoviChiu().setWpogIdMoviChiu(0);
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getDati().setWpogDtIniEff(0);
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getDati().setWpogDtEndEff(0);
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getDati().setWpogCodCompAnia(0);
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getDati().setWpogCodParam("");
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getDati().setWpogTpParam(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getDati().setWpogTpD("");
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getDati().getWpogValImp().setWpogValImp(new AfDecimal(0, 15, 3));
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getDati().getWpogValDt().setWpogValDt(0);
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getDati().getWpogValTs().setWpogValTs(new AfDecimal(0, 14, 9));
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getDati().setWpogValTxt("");
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getDati().setWpogValFl(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getDati().getWpogValNum().setWpogValNum(0);
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getDati().getWpogValPc().setWpogValPc(new AfDecimal(0, 14, 9));
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getDati().setWpogDsRiga(0);
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getDati().setWpogDsOperSql(Types.SPACE_CHAR);
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getDati().setWpogDsVer(0);
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getDati().setWpogDsTsIniCptz(0);
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getDati().setWpogDsTsEndCptz(0);
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getDati().setWpogDsUtente("");
            ws.getAreeAppoggio().getDpogTabParamOgg(idx0).getLccvpog1().getDati().setWpogDsStatoElab(Types.SPACE_CHAR);
        }
    }
}
