package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.MoviFinrioDao;
import it.accenture.jnais.commons.data.to.IMoviFinrio;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbsmfz0Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.MoviFinrioLdbs5950;
import it.accenture.jnais.ws.redefines.MfzCosOprz;
import it.accenture.jnais.ws.redefines.MfzDtEffMoviFinrio;
import it.accenture.jnais.ws.redefines.MfzDtElab;
import it.accenture.jnais.ws.redefines.MfzDtRichMovi;
import it.accenture.jnais.ws.redefines.MfzIdAdes;
import it.accenture.jnais.ws.redefines.MfzIdLiq;
import it.accenture.jnais.ws.redefines.MfzIdMoviChiu;
import it.accenture.jnais.ws.redefines.MfzIdTitCont;

/**Original name: IDBSMFZ0<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  09 LUG 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsmfz0 extends Program implements IMoviFinrio {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private MoviFinrioDao moviFinrioDao = new MoviFinrioDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsmfz0Data ws = new Idbsmfz0Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: MOVI-FINRIO
    private MoviFinrioLdbs5950 moviFinrio;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSMFZ0_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, MoviFinrioLdbs5950 moviFinrio) {
        this.idsv0003 = idsv0003;
        this.moviFinrio = moviFinrio;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsmfz0 getInstance() {
        return ((Idbsmfz0)Programs.getInstance(Idbsmfz0.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSMFZ0'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSMFZ0");
        // COB_CODE: MOVE 'MOVI_FINRIO' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("MOVI_FINRIO");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_MOVI_FINRIO
        //                ,ID_ADES
        //                ,ID_LIQ
        //                ,ID_TIT_CONT
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_MOVI_FINRIO
        //                ,DT_EFF_MOVI_FINRIO
        //                ,DT_ELAB
        //                ,DT_RICH_MOVI
        //                ,COS_OPRZ
        //                ,STAT_MOVI
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_CAUS_RETTIFICA
        //             INTO
        //                :MFZ-ID-MOVI-FINRIO
        //               ,:MFZ-ID-ADES
        //                :IND-MFZ-ID-ADES
        //               ,:MFZ-ID-LIQ
        //                :IND-MFZ-ID-LIQ
        //               ,:MFZ-ID-TIT-CONT
        //                :IND-MFZ-ID-TIT-CONT
        //               ,:MFZ-ID-MOVI-CRZ
        //               ,:MFZ-ID-MOVI-CHIU
        //                :IND-MFZ-ID-MOVI-CHIU
        //               ,:MFZ-DT-INI-EFF-DB
        //               ,:MFZ-DT-END-EFF-DB
        //               ,:MFZ-COD-COMP-ANIA
        //               ,:MFZ-TP-MOVI-FINRIO
        //               ,:MFZ-DT-EFF-MOVI-FINRIO-DB
        //                :IND-MFZ-DT-EFF-MOVI-FINRIO
        //               ,:MFZ-DT-ELAB-DB
        //                :IND-MFZ-DT-ELAB
        //               ,:MFZ-DT-RICH-MOVI-DB
        //                :IND-MFZ-DT-RICH-MOVI
        //               ,:MFZ-COS-OPRZ
        //                :IND-MFZ-COS-OPRZ
        //               ,:MFZ-STAT-MOVI
        //               ,:MFZ-DS-RIGA
        //               ,:MFZ-DS-OPER-SQL
        //               ,:MFZ-DS-VER
        //               ,:MFZ-DS-TS-INI-CPTZ
        //               ,:MFZ-DS-TS-END-CPTZ
        //               ,:MFZ-DS-UTENTE
        //               ,:MFZ-DS-STATO-ELAB
        //               ,:MFZ-TP-CAUS-RETTIFICA
        //                :IND-MFZ-TP-CAUS-RETTIFICA
        //             FROM MOVI_FINRIO
        //             WHERE     DS_RIGA = :MFZ-DS-RIGA
        //           END-EXEC.
        moviFinrioDao.selectByMfzDsRiga(moviFinrio.getMfzDsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO MOVI_FINRIO
            //                  (
            //                     ID_MOVI_FINRIO
            //                    ,ID_ADES
            //                    ,ID_LIQ
            //                    ,ID_TIT_CONT
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,COD_COMP_ANIA
            //                    ,TP_MOVI_FINRIO
            //                    ,DT_EFF_MOVI_FINRIO
            //                    ,DT_ELAB
            //                    ,DT_RICH_MOVI
            //                    ,COS_OPRZ
            //                    ,STAT_MOVI
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,TP_CAUS_RETTIFICA
            //                  )
            //              VALUES
            //                  (
            //                    :MFZ-ID-MOVI-FINRIO
            //                    ,:MFZ-ID-ADES
            //                     :IND-MFZ-ID-ADES
            //                    ,:MFZ-ID-LIQ
            //                     :IND-MFZ-ID-LIQ
            //                    ,:MFZ-ID-TIT-CONT
            //                     :IND-MFZ-ID-TIT-CONT
            //                    ,:MFZ-ID-MOVI-CRZ
            //                    ,:MFZ-ID-MOVI-CHIU
            //                     :IND-MFZ-ID-MOVI-CHIU
            //                    ,:MFZ-DT-INI-EFF-DB
            //                    ,:MFZ-DT-END-EFF-DB
            //                    ,:MFZ-COD-COMP-ANIA
            //                    ,:MFZ-TP-MOVI-FINRIO
            //                    ,:MFZ-DT-EFF-MOVI-FINRIO-DB
            //                     :IND-MFZ-DT-EFF-MOVI-FINRIO
            //                    ,:MFZ-DT-ELAB-DB
            //                     :IND-MFZ-DT-ELAB
            //                    ,:MFZ-DT-RICH-MOVI-DB
            //                     :IND-MFZ-DT-RICH-MOVI
            //                    ,:MFZ-COS-OPRZ
            //                     :IND-MFZ-COS-OPRZ
            //                    ,:MFZ-STAT-MOVI
            //                    ,:MFZ-DS-RIGA
            //                    ,:MFZ-DS-OPER-SQL
            //                    ,:MFZ-DS-VER
            //                    ,:MFZ-DS-TS-INI-CPTZ
            //                    ,:MFZ-DS-TS-END-CPTZ
            //                    ,:MFZ-DS-UTENTE
            //                    ,:MFZ-DS-STATO-ELAB
            //                    ,:MFZ-TP-CAUS-RETTIFICA
            //                     :IND-MFZ-TP-CAUS-RETTIFICA
            //                  )
            //           END-EXEC
            moviFinrioDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE MOVI_FINRIO SET
        //                   ID_MOVI_FINRIO         =
        //                :MFZ-ID-MOVI-FINRIO
        //                  ,ID_ADES                =
        //                :MFZ-ID-ADES
        //                                       :IND-MFZ-ID-ADES
        //                  ,ID_LIQ                 =
        //                :MFZ-ID-LIQ
        //                                       :IND-MFZ-ID-LIQ
        //                  ,ID_TIT_CONT            =
        //                :MFZ-ID-TIT-CONT
        //                                       :IND-MFZ-ID-TIT-CONT
        //                  ,ID_MOVI_CRZ            =
        //                :MFZ-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :MFZ-ID-MOVI-CHIU
        //                                       :IND-MFZ-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :MFZ-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :MFZ-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :MFZ-COD-COMP-ANIA
        //                  ,TP_MOVI_FINRIO         =
        //                :MFZ-TP-MOVI-FINRIO
        //                  ,DT_EFF_MOVI_FINRIO     =
        //           :MFZ-DT-EFF-MOVI-FINRIO-DB
        //                                       :IND-MFZ-DT-EFF-MOVI-FINRIO
        //                  ,DT_ELAB                =
        //           :MFZ-DT-ELAB-DB
        //                                       :IND-MFZ-DT-ELAB
        //                  ,DT_RICH_MOVI           =
        //           :MFZ-DT-RICH-MOVI-DB
        //                                       :IND-MFZ-DT-RICH-MOVI
        //                  ,COS_OPRZ               =
        //                :MFZ-COS-OPRZ
        //                                       :IND-MFZ-COS-OPRZ
        //                  ,STAT_MOVI              =
        //                :MFZ-STAT-MOVI
        //                  ,DS_RIGA                =
        //                :MFZ-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :MFZ-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :MFZ-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :MFZ-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :MFZ-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :MFZ-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :MFZ-DS-STATO-ELAB
        //                  ,TP_CAUS_RETTIFICA      =
        //                :MFZ-TP-CAUS-RETTIFICA
        //                                       :IND-MFZ-TP-CAUS-RETTIFICA
        //                WHERE     DS_RIGA = :MFZ-DS-RIGA
        //           END-EXEC.
        moviFinrioDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM MOVI_FINRIO
        //                WHERE     DS_RIGA = :MFZ-DS-RIGA
        //           END-EXEC.
        moviFinrioDao.deleteByMfzDsRiga(moviFinrio.getMfzDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-ID-UPD-EFF-MFZ CURSOR FOR
        //              SELECT
        //                     ID_MOVI_FINRIO
        //                    ,ID_ADES
        //                    ,ID_LIQ
        //                    ,ID_TIT_CONT
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_MOVI_FINRIO
        //                    ,DT_EFF_MOVI_FINRIO
        //                    ,DT_ELAB
        //                    ,DT_RICH_MOVI
        //                    ,COS_OPRZ
        //                    ,STAT_MOVI
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TP_CAUS_RETTIFICA
        //              FROM MOVI_FINRIO
        //              WHERE     ID_MOVI_FINRIO = :MFZ-ID-MOVI-FINRIO
        //                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_INI_EFF ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_MOVI_FINRIO
        //                ,ID_ADES
        //                ,ID_LIQ
        //                ,ID_TIT_CONT
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_MOVI_FINRIO
        //                ,DT_EFF_MOVI_FINRIO
        //                ,DT_ELAB
        //                ,DT_RICH_MOVI
        //                ,COS_OPRZ
        //                ,STAT_MOVI
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_CAUS_RETTIFICA
        //             INTO
        //                :MFZ-ID-MOVI-FINRIO
        //               ,:MFZ-ID-ADES
        //                :IND-MFZ-ID-ADES
        //               ,:MFZ-ID-LIQ
        //                :IND-MFZ-ID-LIQ
        //               ,:MFZ-ID-TIT-CONT
        //                :IND-MFZ-ID-TIT-CONT
        //               ,:MFZ-ID-MOVI-CRZ
        //               ,:MFZ-ID-MOVI-CHIU
        //                :IND-MFZ-ID-MOVI-CHIU
        //               ,:MFZ-DT-INI-EFF-DB
        //               ,:MFZ-DT-END-EFF-DB
        //               ,:MFZ-COD-COMP-ANIA
        //               ,:MFZ-TP-MOVI-FINRIO
        //               ,:MFZ-DT-EFF-MOVI-FINRIO-DB
        //                :IND-MFZ-DT-EFF-MOVI-FINRIO
        //               ,:MFZ-DT-ELAB-DB
        //                :IND-MFZ-DT-ELAB
        //               ,:MFZ-DT-RICH-MOVI-DB
        //                :IND-MFZ-DT-RICH-MOVI
        //               ,:MFZ-COS-OPRZ
        //                :IND-MFZ-COS-OPRZ
        //               ,:MFZ-STAT-MOVI
        //               ,:MFZ-DS-RIGA
        //               ,:MFZ-DS-OPER-SQL
        //               ,:MFZ-DS-VER
        //               ,:MFZ-DS-TS-INI-CPTZ
        //               ,:MFZ-DS-TS-END-CPTZ
        //               ,:MFZ-DS-UTENTE
        //               ,:MFZ-DS-STATO-ELAB
        //               ,:MFZ-TP-CAUS-RETTIFICA
        //                :IND-MFZ-TP-CAUS-RETTIFICA
        //             FROM MOVI_FINRIO
        //             WHERE     ID_MOVI_FINRIO = :MFZ-ID-MOVI-FINRIO
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        moviFinrioDao.selectRec(moviFinrio.getMfzIdMoviFinrio(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE MOVI_FINRIO SET
        //                   ID_MOVI_FINRIO         =
        //                :MFZ-ID-MOVI-FINRIO
        //                  ,ID_ADES                =
        //                :MFZ-ID-ADES
        //                                       :IND-MFZ-ID-ADES
        //                  ,ID_LIQ                 =
        //                :MFZ-ID-LIQ
        //                                       :IND-MFZ-ID-LIQ
        //                  ,ID_TIT_CONT            =
        //                :MFZ-ID-TIT-CONT
        //                                       :IND-MFZ-ID-TIT-CONT
        //                  ,ID_MOVI_CRZ            =
        //                :MFZ-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :MFZ-ID-MOVI-CHIU
        //                                       :IND-MFZ-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :MFZ-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :MFZ-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :MFZ-COD-COMP-ANIA
        //                  ,TP_MOVI_FINRIO         =
        //                :MFZ-TP-MOVI-FINRIO
        //                  ,DT_EFF_MOVI_FINRIO     =
        //           :MFZ-DT-EFF-MOVI-FINRIO-DB
        //                                       :IND-MFZ-DT-EFF-MOVI-FINRIO
        //                  ,DT_ELAB                =
        //           :MFZ-DT-ELAB-DB
        //                                       :IND-MFZ-DT-ELAB
        //                  ,DT_RICH_MOVI           =
        //           :MFZ-DT-RICH-MOVI-DB
        //                                       :IND-MFZ-DT-RICH-MOVI
        //                  ,COS_OPRZ               =
        //                :MFZ-COS-OPRZ
        //                                       :IND-MFZ-COS-OPRZ
        //                  ,STAT_MOVI              =
        //                :MFZ-STAT-MOVI
        //                  ,DS_RIGA                =
        //                :MFZ-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :MFZ-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :MFZ-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :MFZ-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :MFZ-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :MFZ-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :MFZ-DS-STATO-ELAB
        //                  ,TP_CAUS_RETTIFICA      =
        //                :MFZ-TP-CAUS-RETTIFICA
        //                                       :IND-MFZ-TP-CAUS-RETTIFICA
        //                WHERE     DS_RIGA = :MFZ-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        moviFinrioDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-MFZ
        //           END-EXEC.
        moviFinrioDao.openCIdUpdEffMfz(moviFinrio.getMfzIdMoviFinrio(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-MFZ
        //           END-EXEC.
        moviFinrioDao.closeCIdUpdEffMfz();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-MFZ
        //           INTO
        //                :MFZ-ID-MOVI-FINRIO
        //               ,:MFZ-ID-ADES
        //                :IND-MFZ-ID-ADES
        //               ,:MFZ-ID-LIQ
        //                :IND-MFZ-ID-LIQ
        //               ,:MFZ-ID-TIT-CONT
        //                :IND-MFZ-ID-TIT-CONT
        //               ,:MFZ-ID-MOVI-CRZ
        //               ,:MFZ-ID-MOVI-CHIU
        //                :IND-MFZ-ID-MOVI-CHIU
        //               ,:MFZ-DT-INI-EFF-DB
        //               ,:MFZ-DT-END-EFF-DB
        //               ,:MFZ-COD-COMP-ANIA
        //               ,:MFZ-TP-MOVI-FINRIO
        //               ,:MFZ-DT-EFF-MOVI-FINRIO-DB
        //                :IND-MFZ-DT-EFF-MOVI-FINRIO
        //               ,:MFZ-DT-ELAB-DB
        //                :IND-MFZ-DT-ELAB
        //               ,:MFZ-DT-RICH-MOVI-DB
        //                :IND-MFZ-DT-RICH-MOVI
        //               ,:MFZ-COS-OPRZ
        //                :IND-MFZ-COS-OPRZ
        //               ,:MFZ-STAT-MOVI
        //               ,:MFZ-DS-RIGA
        //               ,:MFZ-DS-OPER-SQL
        //               ,:MFZ-DS-VER
        //               ,:MFZ-DS-TS-INI-CPTZ
        //               ,:MFZ-DS-TS-END-CPTZ
        //               ,:MFZ-DS-UTENTE
        //               ,:MFZ-DS-STATO-ELAB
        //               ,:MFZ-TP-CAUS-RETTIFICA
        //                :IND-MFZ-TP-CAUS-RETTIFICA
        //           END-EXEC.
        moviFinrioDao.fetchCIdUpdEffMfz(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-EFF-MFZ CURSOR FOR
        //              SELECT
        //                     ID_MOVI_FINRIO
        //                    ,ID_ADES
        //                    ,ID_LIQ
        //                    ,ID_TIT_CONT
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_MOVI_FINRIO
        //                    ,DT_EFF_MOVI_FINRIO
        //                    ,DT_ELAB
        //                    ,DT_RICH_MOVI
        //                    ,COS_OPRZ
        //                    ,STAT_MOVI
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TP_CAUS_RETTIFICA
        //              FROM MOVI_FINRIO
        //              WHERE     ID_ADES = :MFZ-ID-ADES
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_MOVI_FINRIO ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_MOVI_FINRIO
        //                ,ID_ADES
        //                ,ID_LIQ
        //                ,ID_TIT_CONT
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_MOVI_FINRIO
        //                ,DT_EFF_MOVI_FINRIO
        //                ,DT_ELAB
        //                ,DT_RICH_MOVI
        //                ,COS_OPRZ
        //                ,STAT_MOVI
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_CAUS_RETTIFICA
        //             INTO
        //                :MFZ-ID-MOVI-FINRIO
        //               ,:MFZ-ID-ADES
        //                :IND-MFZ-ID-ADES
        //               ,:MFZ-ID-LIQ
        //                :IND-MFZ-ID-LIQ
        //               ,:MFZ-ID-TIT-CONT
        //                :IND-MFZ-ID-TIT-CONT
        //               ,:MFZ-ID-MOVI-CRZ
        //               ,:MFZ-ID-MOVI-CHIU
        //                :IND-MFZ-ID-MOVI-CHIU
        //               ,:MFZ-DT-INI-EFF-DB
        //               ,:MFZ-DT-END-EFF-DB
        //               ,:MFZ-COD-COMP-ANIA
        //               ,:MFZ-TP-MOVI-FINRIO
        //               ,:MFZ-DT-EFF-MOVI-FINRIO-DB
        //                :IND-MFZ-DT-EFF-MOVI-FINRIO
        //               ,:MFZ-DT-ELAB-DB
        //                :IND-MFZ-DT-ELAB
        //               ,:MFZ-DT-RICH-MOVI-DB
        //                :IND-MFZ-DT-RICH-MOVI
        //               ,:MFZ-COS-OPRZ
        //                :IND-MFZ-COS-OPRZ
        //               ,:MFZ-STAT-MOVI
        //               ,:MFZ-DS-RIGA
        //               ,:MFZ-DS-OPER-SQL
        //               ,:MFZ-DS-VER
        //               ,:MFZ-DS-TS-INI-CPTZ
        //               ,:MFZ-DS-TS-END-CPTZ
        //               ,:MFZ-DS-UTENTE
        //               ,:MFZ-DS-STATO-ELAB
        //               ,:MFZ-TP-CAUS-RETTIFICA
        //                :IND-MFZ-TP-CAUS-RETTIFICA
        //             FROM MOVI_FINRIO
        //             WHERE     ID_ADES = :MFZ-ID-ADES
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        moviFinrioDao.selectRec1(moviFinrio.getMfzIdAdes().getMfzIdAdes(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-EFF-MFZ
        //           END-EXEC.
        moviFinrioDao.openCIdpEffMfz(moviFinrio.getMfzIdAdes().getMfzIdAdes(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-EFF-MFZ
        //           END-EXEC.
        moviFinrioDao.closeCIdpEffMfz();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-EFF-MFZ
        //           INTO
        //                :MFZ-ID-MOVI-FINRIO
        //               ,:MFZ-ID-ADES
        //                :IND-MFZ-ID-ADES
        //               ,:MFZ-ID-LIQ
        //                :IND-MFZ-ID-LIQ
        //               ,:MFZ-ID-TIT-CONT
        //                :IND-MFZ-ID-TIT-CONT
        //               ,:MFZ-ID-MOVI-CRZ
        //               ,:MFZ-ID-MOVI-CHIU
        //                :IND-MFZ-ID-MOVI-CHIU
        //               ,:MFZ-DT-INI-EFF-DB
        //               ,:MFZ-DT-END-EFF-DB
        //               ,:MFZ-COD-COMP-ANIA
        //               ,:MFZ-TP-MOVI-FINRIO
        //               ,:MFZ-DT-EFF-MOVI-FINRIO-DB
        //                :IND-MFZ-DT-EFF-MOVI-FINRIO
        //               ,:MFZ-DT-ELAB-DB
        //                :IND-MFZ-DT-ELAB
        //               ,:MFZ-DT-RICH-MOVI-DB
        //                :IND-MFZ-DT-RICH-MOVI
        //               ,:MFZ-COS-OPRZ
        //                :IND-MFZ-COS-OPRZ
        //               ,:MFZ-STAT-MOVI
        //               ,:MFZ-DS-RIGA
        //               ,:MFZ-DS-OPER-SQL
        //               ,:MFZ-DS-VER
        //               ,:MFZ-DS-TS-INI-CPTZ
        //               ,:MFZ-DS-TS-END-CPTZ
        //               ,:MFZ-DS-UTENTE
        //               ,:MFZ-DS-STATO-ELAB
        //               ,:MFZ-TP-CAUS-RETTIFICA
        //                :IND-MFZ-TP-CAUS-RETTIFICA
        //           END-EXEC.
        moviFinrioDao.fetchCIdpEffMfz(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_MOVI_FINRIO
        //                ,ID_ADES
        //                ,ID_LIQ
        //                ,ID_TIT_CONT
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_MOVI_FINRIO
        //                ,DT_EFF_MOVI_FINRIO
        //                ,DT_ELAB
        //                ,DT_RICH_MOVI
        //                ,COS_OPRZ
        //                ,STAT_MOVI
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_CAUS_RETTIFICA
        //             INTO
        //                :MFZ-ID-MOVI-FINRIO
        //               ,:MFZ-ID-ADES
        //                :IND-MFZ-ID-ADES
        //               ,:MFZ-ID-LIQ
        //                :IND-MFZ-ID-LIQ
        //               ,:MFZ-ID-TIT-CONT
        //                :IND-MFZ-ID-TIT-CONT
        //               ,:MFZ-ID-MOVI-CRZ
        //               ,:MFZ-ID-MOVI-CHIU
        //                :IND-MFZ-ID-MOVI-CHIU
        //               ,:MFZ-DT-INI-EFF-DB
        //               ,:MFZ-DT-END-EFF-DB
        //               ,:MFZ-COD-COMP-ANIA
        //               ,:MFZ-TP-MOVI-FINRIO
        //               ,:MFZ-DT-EFF-MOVI-FINRIO-DB
        //                :IND-MFZ-DT-EFF-MOVI-FINRIO
        //               ,:MFZ-DT-ELAB-DB
        //                :IND-MFZ-DT-ELAB
        //               ,:MFZ-DT-RICH-MOVI-DB
        //                :IND-MFZ-DT-RICH-MOVI
        //               ,:MFZ-COS-OPRZ
        //                :IND-MFZ-COS-OPRZ
        //               ,:MFZ-STAT-MOVI
        //               ,:MFZ-DS-RIGA
        //               ,:MFZ-DS-OPER-SQL
        //               ,:MFZ-DS-VER
        //               ,:MFZ-DS-TS-INI-CPTZ
        //               ,:MFZ-DS-TS-END-CPTZ
        //               ,:MFZ-DS-UTENTE
        //               ,:MFZ-DS-STATO-ELAB
        //               ,:MFZ-TP-CAUS-RETTIFICA
        //                :IND-MFZ-TP-CAUS-RETTIFICA
        //             FROM MOVI_FINRIO
        //             WHERE     ID_MOVI_FINRIO = :MFZ-ID-MOVI-FINRIO
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        moviFinrioDao.selectRec2(moviFinrio.getMfzIdMoviFinrio(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-CPZ-MFZ CURSOR FOR
        //              SELECT
        //                     ID_MOVI_FINRIO
        //                    ,ID_ADES
        //                    ,ID_LIQ
        //                    ,ID_TIT_CONT
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_MOVI_FINRIO
        //                    ,DT_EFF_MOVI_FINRIO
        //                    ,DT_ELAB
        //                    ,DT_RICH_MOVI
        //                    ,COS_OPRZ
        //                    ,STAT_MOVI
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TP_CAUS_RETTIFICA
        //              FROM MOVI_FINRIO
        //              WHERE     ID_ADES = :MFZ-ID-ADES
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_MOVI_FINRIO ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_MOVI_FINRIO
        //                ,ID_ADES
        //                ,ID_LIQ
        //                ,ID_TIT_CONT
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_MOVI_FINRIO
        //                ,DT_EFF_MOVI_FINRIO
        //                ,DT_ELAB
        //                ,DT_RICH_MOVI
        //                ,COS_OPRZ
        //                ,STAT_MOVI
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_CAUS_RETTIFICA
        //             INTO
        //                :MFZ-ID-MOVI-FINRIO
        //               ,:MFZ-ID-ADES
        //                :IND-MFZ-ID-ADES
        //               ,:MFZ-ID-LIQ
        //                :IND-MFZ-ID-LIQ
        //               ,:MFZ-ID-TIT-CONT
        //                :IND-MFZ-ID-TIT-CONT
        //               ,:MFZ-ID-MOVI-CRZ
        //               ,:MFZ-ID-MOVI-CHIU
        //                :IND-MFZ-ID-MOVI-CHIU
        //               ,:MFZ-DT-INI-EFF-DB
        //               ,:MFZ-DT-END-EFF-DB
        //               ,:MFZ-COD-COMP-ANIA
        //               ,:MFZ-TP-MOVI-FINRIO
        //               ,:MFZ-DT-EFF-MOVI-FINRIO-DB
        //                :IND-MFZ-DT-EFF-MOVI-FINRIO
        //               ,:MFZ-DT-ELAB-DB
        //                :IND-MFZ-DT-ELAB
        //               ,:MFZ-DT-RICH-MOVI-DB
        //                :IND-MFZ-DT-RICH-MOVI
        //               ,:MFZ-COS-OPRZ
        //                :IND-MFZ-COS-OPRZ
        //               ,:MFZ-STAT-MOVI
        //               ,:MFZ-DS-RIGA
        //               ,:MFZ-DS-OPER-SQL
        //               ,:MFZ-DS-VER
        //               ,:MFZ-DS-TS-INI-CPTZ
        //               ,:MFZ-DS-TS-END-CPTZ
        //               ,:MFZ-DS-UTENTE
        //               ,:MFZ-DS-STATO-ELAB
        //               ,:MFZ-TP-CAUS-RETTIFICA
        //                :IND-MFZ-TP-CAUS-RETTIFICA
        //             FROM MOVI_FINRIO
        //             WHERE     ID_ADES = :MFZ-ID-ADES
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        moviFinrioDao.selectRec3(moviFinrio.getMfzIdAdes().getMfzIdAdes(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-CPZ-MFZ
        //           END-EXEC.
        moviFinrioDao.openCIdpCpzMfz(moviFinrio.getMfzIdAdes().getMfzIdAdes(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-CPZ-MFZ
        //           END-EXEC.
        moviFinrioDao.closeCIdpCpzMfz();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-CPZ-MFZ
        //           INTO
        //                :MFZ-ID-MOVI-FINRIO
        //               ,:MFZ-ID-ADES
        //                :IND-MFZ-ID-ADES
        //               ,:MFZ-ID-LIQ
        //                :IND-MFZ-ID-LIQ
        //               ,:MFZ-ID-TIT-CONT
        //                :IND-MFZ-ID-TIT-CONT
        //               ,:MFZ-ID-MOVI-CRZ
        //               ,:MFZ-ID-MOVI-CHIU
        //                :IND-MFZ-ID-MOVI-CHIU
        //               ,:MFZ-DT-INI-EFF-DB
        //               ,:MFZ-DT-END-EFF-DB
        //               ,:MFZ-COD-COMP-ANIA
        //               ,:MFZ-TP-MOVI-FINRIO
        //               ,:MFZ-DT-EFF-MOVI-FINRIO-DB
        //                :IND-MFZ-DT-EFF-MOVI-FINRIO
        //               ,:MFZ-DT-ELAB-DB
        //                :IND-MFZ-DT-ELAB
        //               ,:MFZ-DT-RICH-MOVI-DB
        //                :IND-MFZ-DT-RICH-MOVI
        //               ,:MFZ-COS-OPRZ
        //                :IND-MFZ-COS-OPRZ
        //               ,:MFZ-STAT-MOVI
        //               ,:MFZ-DS-RIGA
        //               ,:MFZ-DS-OPER-SQL
        //               ,:MFZ-DS-VER
        //               ,:MFZ-DS-TS-INI-CPTZ
        //               ,:MFZ-DS-TS-END-CPTZ
        //               ,:MFZ-DS-UTENTE
        //               ,:MFZ-DS-STATO-ELAB
        //               ,:MFZ-TP-CAUS-RETTIFICA
        //                :IND-MFZ-TP-CAUS-RETTIFICA
        //           END-EXEC.
        moviFinrioDao.fetchCIdpCpzMfz(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-MFZ-ID-ADES = -1
        //              MOVE HIGH-VALUES TO MFZ-ID-ADES-NULL
        //           END-IF
        if (ws.getIndMoviFinrio().getIdAdes() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MFZ-ID-ADES-NULL
            moviFinrio.getMfzIdAdes().setMfzIdAdesNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MfzIdAdes.Len.MFZ_ID_ADES_NULL));
        }
        // COB_CODE: IF IND-MFZ-ID-LIQ = -1
        //              MOVE HIGH-VALUES TO MFZ-ID-LIQ-NULL
        //           END-IF
        if (ws.getIndMoviFinrio().getIdLiq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MFZ-ID-LIQ-NULL
            moviFinrio.getMfzIdLiq().setMfzIdLiqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MfzIdLiq.Len.MFZ_ID_LIQ_NULL));
        }
        // COB_CODE: IF IND-MFZ-ID-TIT-CONT = -1
        //              MOVE HIGH-VALUES TO MFZ-ID-TIT-CONT-NULL
        //           END-IF
        if (ws.getIndMoviFinrio().getIdTitCont() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MFZ-ID-TIT-CONT-NULL
            moviFinrio.getMfzIdTitCont().setMfzIdTitContNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MfzIdTitCont.Len.MFZ_ID_TIT_CONT_NULL));
        }
        // COB_CODE: IF IND-MFZ-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO MFZ-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndMoviFinrio().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MFZ-ID-MOVI-CHIU-NULL
            moviFinrio.getMfzIdMoviChiu().setMfzIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MfzIdMoviChiu.Len.MFZ_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-MFZ-DT-EFF-MOVI-FINRIO = -1
        //              MOVE HIGH-VALUES TO MFZ-DT-EFF-MOVI-FINRIO-NULL
        //           END-IF
        if (ws.getIndMoviFinrio().getDtEffMoviFinrio() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MFZ-DT-EFF-MOVI-FINRIO-NULL
            moviFinrio.getMfzDtEffMoviFinrio().setMfzDtEffMoviFinrioNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MfzDtEffMoviFinrio.Len.MFZ_DT_EFF_MOVI_FINRIO_NULL));
        }
        // COB_CODE: IF IND-MFZ-DT-ELAB = -1
        //              MOVE HIGH-VALUES TO MFZ-DT-ELAB-NULL
        //           END-IF
        if (ws.getIndMoviFinrio().getDtElab() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MFZ-DT-ELAB-NULL
            moviFinrio.getMfzDtElab().setMfzDtElabNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MfzDtElab.Len.MFZ_DT_ELAB_NULL));
        }
        // COB_CODE: IF IND-MFZ-DT-RICH-MOVI = -1
        //              MOVE HIGH-VALUES TO MFZ-DT-RICH-MOVI-NULL
        //           END-IF
        if (ws.getIndMoviFinrio().getDtRichMovi() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MFZ-DT-RICH-MOVI-NULL
            moviFinrio.getMfzDtRichMovi().setMfzDtRichMoviNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MfzDtRichMovi.Len.MFZ_DT_RICH_MOVI_NULL));
        }
        // COB_CODE: IF IND-MFZ-COS-OPRZ = -1
        //              MOVE HIGH-VALUES TO MFZ-COS-OPRZ-NULL
        //           END-IF
        if (ws.getIndMoviFinrio().getCosOprz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MFZ-COS-OPRZ-NULL
            moviFinrio.getMfzCosOprz().setMfzCosOprzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MfzCosOprz.Len.MFZ_COS_OPRZ_NULL));
        }
        // COB_CODE: IF IND-MFZ-TP-CAUS-RETTIFICA = -1
        //              MOVE HIGH-VALUES TO MFZ-TP-CAUS-RETTIFICA-NULL
        //           END-IF.
        if (ws.getIndMoviFinrio().getTpCausRettifica() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MFZ-TP-CAUS-RETTIFICA-NULL
            moviFinrio.setMfzTpCausRettifica(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MoviFinrioLdbs5950.Len.MFZ_TP_CAUS_RETTIFICA));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO MFZ-DS-OPER-SQL
        moviFinrio.setMfzDsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO MFZ-DS-VER
        moviFinrio.setMfzDsVer(0);
        // COB_CODE: INITIALIZE WK-ID-OGG
        ws.setWkIdOggFormatted("000000000");
        // COB_CODE: MOVE MFZ-ID-ADES
        //             TO WK-ID-OGG
        ws.setWkIdOgg(TruncAbs.toInt(moviFinrio.getMfzIdAdes().getMfzIdAdes(), 9));
        // COB_CODE: MOVE WK-ID-OGG(8:2)
        //             TO MFZ-DS-VER
        moviFinrio.setMfzDsVerFormatted(ws.getWkIdOggFormatted().substring((8) - 1, 9));
        // COB_CODE: IF MFZ-DS-VER = 00
        //                TO MFZ-DS-VER
        //           END-IF
        if (moviFinrio.getMfzDsVer() == 0) {
            // COB_CODE: MOVE 100
            //             TO MFZ-DS-VER
            moviFinrio.setMfzDsVer(100);
        }
        // COB_CODE: MOVE IDSV0003-USER-NAME TO MFZ-DS-UTENTE
        moviFinrio.setMfzDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO MFZ-DS-STATO-ELAB.
        moviFinrio.setMfzDsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO MFZ-DS-OPER-SQL
        moviFinrio.setMfzDsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO MFZ-DS-UTENTE.
        moviFinrio.setMfzDsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF MFZ-ID-ADES-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-MFZ-ID-ADES
        //           ELSE
        //              MOVE 0 TO IND-MFZ-ID-ADES
        //           END-IF
        if (Characters.EQ_HIGH.test(moviFinrio.getMfzIdAdes().getMfzIdAdesNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-MFZ-ID-ADES
            ws.getIndMoviFinrio().setIdAdes(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-MFZ-ID-ADES
            ws.getIndMoviFinrio().setIdAdes(((short)0));
        }
        // COB_CODE: IF MFZ-ID-LIQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-MFZ-ID-LIQ
        //           ELSE
        //              MOVE 0 TO IND-MFZ-ID-LIQ
        //           END-IF
        if (Characters.EQ_HIGH.test(moviFinrio.getMfzIdLiq().getMfzIdLiqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-MFZ-ID-LIQ
            ws.getIndMoviFinrio().setIdLiq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-MFZ-ID-LIQ
            ws.getIndMoviFinrio().setIdLiq(((short)0));
        }
        // COB_CODE: IF MFZ-ID-TIT-CONT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-MFZ-ID-TIT-CONT
        //           ELSE
        //              MOVE 0 TO IND-MFZ-ID-TIT-CONT
        //           END-IF
        if (Characters.EQ_HIGH.test(moviFinrio.getMfzIdTitCont().getMfzIdTitContNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-MFZ-ID-TIT-CONT
            ws.getIndMoviFinrio().setIdTitCont(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-MFZ-ID-TIT-CONT
            ws.getIndMoviFinrio().setIdTitCont(((short)0));
        }
        // COB_CODE: IF MFZ-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-MFZ-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-MFZ-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(moviFinrio.getMfzIdMoviChiu().getMfzIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-MFZ-ID-MOVI-CHIU
            ws.getIndMoviFinrio().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-MFZ-ID-MOVI-CHIU
            ws.getIndMoviFinrio().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF MFZ-DT-EFF-MOVI-FINRIO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-MFZ-DT-EFF-MOVI-FINRIO
        //           ELSE
        //              MOVE 0 TO IND-MFZ-DT-EFF-MOVI-FINRIO
        //           END-IF
        if (Characters.EQ_HIGH.test(moviFinrio.getMfzDtEffMoviFinrio().getMfzDtEffMoviFinrioNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-MFZ-DT-EFF-MOVI-FINRIO
            ws.getIndMoviFinrio().setDtEffMoviFinrio(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-MFZ-DT-EFF-MOVI-FINRIO
            ws.getIndMoviFinrio().setDtEffMoviFinrio(((short)0));
        }
        // COB_CODE: IF MFZ-DT-ELAB-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-MFZ-DT-ELAB
        //           ELSE
        //              MOVE 0 TO IND-MFZ-DT-ELAB
        //           END-IF
        if (Characters.EQ_HIGH.test(moviFinrio.getMfzDtElab().getMfzDtElabNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-MFZ-DT-ELAB
            ws.getIndMoviFinrio().setDtElab(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-MFZ-DT-ELAB
            ws.getIndMoviFinrio().setDtElab(((short)0));
        }
        // COB_CODE: IF MFZ-DT-RICH-MOVI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-MFZ-DT-RICH-MOVI
        //           ELSE
        //              MOVE 0 TO IND-MFZ-DT-RICH-MOVI
        //           END-IF
        if (Characters.EQ_HIGH.test(moviFinrio.getMfzDtRichMovi().getMfzDtRichMoviNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-MFZ-DT-RICH-MOVI
            ws.getIndMoviFinrio().setDtRichMovi(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-MFZ-DT-RICH-MOVI
            ws.getIndMoviFinrio().setDtRichMovi(((short)0));
        }
        // COB_CODE: IF MFZ-COS-OPRZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-MFZ-COS-OPRZ
        //           ELSE
        //              MOVE 0 TO IND-MFZ-COS-OPRZ
        //           END-IF
        if (Characters.EQ_HIGH.test(moviFinrio.getMfzCosOprz().getMfzCosOprzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-MFZ-COS-OPRZ
            ws.getIndMoviFinrio().setCosOprz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-MFZ-COS-OPRZ
            ws.getIndMoviFinrio().setCosOprz(((short)0));
        }
        // COB_CODE: IF MFZ-TP-CAUS-RETTIFICA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-MFZ-TP-CAUS-RETTIFICA
        //           ELSE
        //              MOVE 0 TO IND-MFZ-TP-CAUS-RETTIFICA
        //           END-IF.
        if (Characters.EQ_HIGH.test(moviFinrio.getMfzTpCausRettificaFormatted())) {
            // COB_CODE: MOVE -1 TO IND-MFZ-TP-CAUS-RETTIFICA
            ws.getIndMoviFinrio().setTpCausRettifica(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-MFZ-TP-CAUS-RETTIFICA
            ws.getIndMoviFinrio().setTpCausRettifica(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : MFZ-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE MOVI-FINRIO TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(moviFinrio.getMoviFinrioFormatted());
        // COB_CODE: MOVE MFZ-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(moviFinrio.getMfzIdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO MFZ-ID-MOVI-CHIU
                moviFinrio.getMfzIdMoviChiu().setMfzIdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO MFZ-DS-TS-END-CPTZ
                moviFinrio.setMfzDsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO MFZ-ID-MOVI-CRZ
                    moviFinrio.setMfzIdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO MFZ-ID-MOVI-CHIU-NULL
                    moviFinrio.getMfzIdMoviChiu().setMfzIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MfzIdMoviChiu.Len.MFZ_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO MFZ-DT-END-EFF
                    moviFinrio.setMfzDtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO MFZ-DS-TS-INI-CPTZ
                    moviFinrio.setMfzDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO MFZ-DS-TS-END-CPTZ
                    moviFinrio.setMfzDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
        // COB_CODE: MOVE MOVI-FINRIO TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(moviFinrio.getMoviFinrioFormatted());
        // COB_CODE: MOVE MFZ-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(moviFinrio.getMfzIdMoviCrz(), 9));
        // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.
        z600InsertNuovaRigaStorica();
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO MOVI-FINRIO.
        moviFinrio.setMoviFinrioFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO MFZ-ID-MOVI-CRZ.
        moviFinrio.setMfzIdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO MFZ-ID-MOVI-CHIU-NULL.
        moviFinrio.getMfzIdMoviChiu().setMfzIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MfzIdMoviChiu.Len.MFZ_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO MFZ-DT-INI-EFF.
        moviFinrio.setMfzDtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO MFZ-DT-END-EFF.
        moviFinrio.setMfzDtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO MFZ-DS-TS-INI-CPTZ.
        moviFinrio.setMfzDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO MFZ-DS-TS-END-CPTZ.
        moviFinrio.setMfzDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO MFZ-COD-COMP-ANIA.
        moviFinrio.setMfzCodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE MFZ-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(moviFinrio.getMfzDtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO MFZ-DT-INI-EFF-DB
        ws.getMoviFinrioDb().setIniEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE MFZ-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(moviFinrio.getMfzDtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO MFZ-DT-END-EFF-DB
        ws.getMoviFinrioDb().setEndEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-MFZ-DT-EFF-MOVI-FINRIO = 0
        //               MOVE WS-DATE-X      TO MFZ-DT-EFF-MOVI-FINRIO-DB
        //           END-IF
        if (ws.getIndMoviFinrio().getDtEffMoviFinrio() == 0) {
            // COB_CODE: MOVE MFZ-DT-EFF-MOVI-FINRIO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(moviFinrio.getMfzDtEffMoviFinrio().getMfzDtEffMoviFinrio(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO MFZ-DT-EFF-MOVI-FINRIO-DB
            ws.getMoviFinrioDb().setIniCopDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-MFZ-DT-ELAB = 0
        //               MOVE WS-DATE-X      TO MFZ-DT-ELAB-DB
        //           END-IF
        if (ws.getIndMoviFinrio().getDtElab() == 0) {
            // COB_CODE: MOVE MFZ-DT-ELAB TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(moviFinrio.getMfzDtElab().getMfzDtElab(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO MFZ-DT-ELAB-DB
            ws.getMoviFinrioDb().setEndCopDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-MFZ-DT-RICH-MOVI = 0
        //               MOVE WS-DATE-X      TO MFZ-DT-RICH-MOVI-DB
        //           END-IF.
        if (ws.getIndMoviFinrio().getDtRichMovi() == 0) {
            // COB_CODE: MOVE MFZ-DT-RICH-MOVI TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(moviFinrio.getMfzDtRichMovi().getMfzDtRichMovi(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO MFZ-DT-RICH-MOVI-DB
            ws.getMoviFinrioDb().setEsiTitDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE MFZ-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getMoviFinrioDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO MFZ-DT-INI-EFF
        moviFinrio.setMfzDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE MFZ-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getMoviFinrioDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO MFZ-DT-END-EFF
        moviFinrio.setMfzDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-MFZ-DT-EFF-MOVI-FINRIO = 0
        //               MOVE WS-DATE-N      TO MFZ-DT-EFF-MOVI-FINRIO
        //           END-IF
        if (ws.getIndMoviFinrio().getDtEffMoviFinrio() == 0) {
            // COB_CODE: MOVE MFZ-DT-EFF-MOVI-FINRIO-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getMoviFinrioDb().getIniCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO MFZ-DT-EFF-MOVI-FINRIO
            moviFinrio.getMfzDtEffMoviFinrio().setMfzDtEffMoviFinrio(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-MFZ-DT-ELAB = 0
        //               MOVE WS-DATE-N      TO MFZ-DT-ELAB
        //           END-IF
        if (ws.getIndMoviFinrio().getDtElab() == 0) {
            // COB_CODE: MOVE MFZ-DT-ELAB-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getMoviFinrioDb().getEndCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO MFZ-DT-ELAB
            moviFinrio.getMfzDtElab().setMfzDtElab(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-MFZ-DT-RICH-MOVI = 0
        //               MOVE WS-DATE-N      TO MFZ-DT-RICH-MOVI
        //           END-IF.
        if (ws.getIndMoviFinrio().getDtRichMovi() == 0) {
            // COB_CODE: MOVE MFZ-DT-RICH-MOVI-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getMoviFinrioDb().getEsiTitDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO MFZ-DT-RICH-MOVI
            moviFinrio.getMfzDtRichMovi().setMfzDtRichMovi(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return moviFinrio.getMfzCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.moviFinrio.setMfzCodCompAnia(codCompAnia);
    }

    @Override
    public AfDecimal getCosOprz() {
        return moviFinrio.getMfzCosOprz().getMfzCosOprz();
    }

    @Override
    public void setCosOprz(AfDecimal cosOprz) {
        this.moviFinrio.getMfzCosOprz().setMfzCosOprz(cosOprz.copy());
    }

    @Override
    public AfDecimal getCosOprzObj() {
        if (ws.getIndMoviFinrio().getCosOprz() >= 0) {
            return getCosOprz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCosOprzObj(AfDecimal cosOprzObj) {
        if (cosOprzObj != null) {
            setCosOprz(new AfDecimal(cosOprzObj, 15, 3));
            ws.getIndMoviFinrio().setCosOprz(((short)0));
        }
        else {
            ws.getIndMoviFinrio().setCosOprz(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return moviFinrio.getMfzDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.moviFinrio.setMfzDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return moviFinrio.getMfzDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.moviFinrio.setMfzDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return moviFinrio.getMfzDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.moviFinrio.setMfzDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return moviFinrio.getMfzDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.moviFinrio.setMfzDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return moviFinrio.getMfzDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.moviFinrio.setMfzDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return moviFinrio.getMfzDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.moviFinrio.setMfzDsVer(dsVer);
    }

    @Override
    public String getDtEffMoviFinrioDb() {
        return ws.getMoviFinrioDb().getIniCopDb();
    }

    @Override
    public void setDtEffMoviFinrioDb(String dtEffMoviFinrioDb) {
        this.ws.getMoviFinrioDb().setIniCopDb(dtEffMoviFinrioDb);
    }

    @Override
    public String getDtEffMoviFinrioDbObj() {
        if (ws.getIndMoviFinrio().getDtEffMoviFinrio() >= 0) {
            return getDtEffMoviFinrioDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEffMoviFinrioDbObj(String dtEffMoviFinrioDbObj) {
        if (dtEffMoviFinrioDbObj != null) {
            setDtEffMoviFinrioDb(dtEffMoviFinrioDbObj);
            ws.getIndMoviFinrio().setDtEffMoviFinrio(((short)0));
        }
        else {
            ws.getIndMoviFinrio().setDtEffMoviFinrio(((short)-1));
        }
    }

    @Override
    public String getDtElabDb() {
        return ws.getMoviFinrioDb().getEndCopDb();
    }

    @Override
    public void setDtElabDb(String dtElabDb) {
        this.ws.getMoviFinrioDb().setEndCopDb(dtElabDb);
    }

    @Override
    public String getDtElabDbObj() {
        if (ws.getIndMoviFinrio().getDtElab() >= 0) {
            return getDtElabDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtElabDbObj(String dtElabDbObj) {
        if (dtElabDbObj != null) {
            setDtElabDb(dtElabDbObj);
            ws.getIndMoviFinrio().setDtElab(((short)0));
        }
        else {
            ws.getIndMoviFinrio().setDtElab(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getMoviFinrioDb().getEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getMoviFinrioDb().setEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getMoviFinrioDb().getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getMoviFinrioDb().setIniEffDb(dtIniEffDb);
    }

    @Override
    public String getDtRichMoviDb() {
        return ws.getMoviFinrioDb().getEsiTitDb();
    }

    @Override
    public void setDtRichMoviDb(String dtRichMoviDb) {
        this.ws.getMoviFinrioDb().setEsiTitDb(dtRichMoviDb);
    }

    @Override
    public String getDtRichMoviDbObj() {
        if (ws.getIndMoviFinrio().getDtRichMovi() >= 0) {
            return getDtRichMoviDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtRichMoviDbObj(String dtRichMoviDbObj) {
        if (dtRichMoviDbObj != null) {
            setDtRichMoviDb(dtRichMoviDbObj);
            ws.getIndMoviFinrio().setDtRichMovi(((short)0));
        }
        else {
            ws.getIndMoviFinrio().setDtRichMovi(((short)-1));
        }
    }

    @Override
    public int getIdAdes() {
        return moviFinrio.getMfzIdAdes().getMfzIdAdes();
    }

    @Override
    public void setIdAdes(int idAdes) {
        this.moviFinrio.getMfzIdAdes().setMfzIdAdes(idAdes);
    }

    @Override
    public Integer getIdAdesObj() {
        if (ws.getIndMoviFinrio().getIdAdes() >= 0) {
            return ((Integer)getIdAdes());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdAdesObj(Integer idAdesObj) {
        if (idAdesObj != null) {
            setIdAdes(((int)idAdesObj));
            ws.getIndMoviFinrio().setIdAdes(((short)0));
        }
        else {
            ws.getIndMoviFinrio().setIdAdes(((short)-1));
        }
    }

    @Override
    public int getIdLiq() {
        return moviFinrio.getMfzIdLiq().getMfzIdLiq();
    }

    @Override
    public void setIdLiq(int idLiq) {
        this.moviFinrio.getMfzIdLiq().setMfzIdLiq(idLiq);
    }

    @Override
    public Integer getIdLiqObj() {
        if (ws.getIndMoviFinrio().getIdLiq() >= 0) {
            return ((Integer)getIdLiq());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdLiqObj(Integer idLiqObj) {
        if (idLiqObj != null) {
            setIdLiq(((int)idLiqObj));
            ws.getIndMoviFinrio().setIdLiq(((short)0));
        }
        else {
            ws.getIndMoviFinrio().setIdLiq(((short)-1));
        }
    }

    @Override
    public int getIdMoviChiu() {
        return moviFinrio.getMfzIdMoviChiu().getMfzIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.moviFinrio.getMfzIdMoviChiu().setMfzIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndMoviFinrio().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndMoviFinrio().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndMoviFinrio().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return moviFinrio.getMfzIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.moviFinrio.setMfzIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdMoviFinrio() {
        return moviFinrio.getMfzIdMoviFinrio();
    }

    @Override
    public void setIdMoviFinrio(int idMoviFinrio) {
        this.moviFinrio.setMfzIdMoviFinrio(idMoviFinrio);
    }

    @Override
    public int getIdTitCont() {
        return moviFinrio.getMfzIdTitCont().getMfzIdTitCont();
    }

    @Override
    public void setIdTitCont(int idTitCont) {
        this.moviFinrio.getMfzIdTitCont().setMfzIdTitCont(idTitCont);
    }

    @Override
    public Integer getIdTitContObj() {
        if (ws.getIndMoviFinrio().getIdTitCont() >= 0) {
            return ((Integer)getIdTitCont());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdTitContObj(Integer idTitContObj) {
        if (idTitContObj != null) {
            setIdTitCont(((int)idTitContObj));
            ws.getIndMoviFinrio().setIdTitCont(((short)0));
        }
        else {
            ws.getIndMoviFinrio().setIdTitCont(((short)-1));
        }
    }

    @Override
    public long getMfzDsRiga() {
        return moviFinrio.getMfzDsRiga();
    }

    @Override
    public void setMfzDsRiga(long mfzDsRiga) {
        this.moviFinrio.setMfzDsRiga(mfzDsRiga);
    }

    @Override
    public String getStatMovi() {
        return moviFinrio.getMfzStatMovi();
    }

    @Override
    public void setStatMovi(String statMovi) {
        this.moviFinrio.setMfzStatMovi(statMovi);
    }

    @Override
    public String getTpCausRettifica() {
        return moviFinrio.getMfzTpCausRettifica();
    }

    @Override
    public void setTpCausRettifica(String tpCausRettifica) {
        this.moviFinrio.setMfzTpCausRettifica(tpCausRettifica);
    }

    @Override
    public String getTpCausRettificaObj() {
        if (ws.getIndMoviFinrio().getTpCausRettifica() >= 0) {
            return getTpCausRettifica();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCausRettificaObj(String tpCausRettificaObj) {
        if (tpCausRettificaObj != null) {
            setTpCausRettifica(tpCausRettificaObj);
            ws.getIndMoviFinrio().setTpCausRettifica(((short)0));
        }
        else {
            ws.getIndMoviFinrio().setTpCausRettifica(((short)-1));
        }
    }

    @Override
    public String getTpMoviFinrio() {
        return moviFinrio.getMfzTpMoviFinrio();
    }

    @Override
    public void setTpMoviFinrio(String tpMoviFinrio) {
        this.moviFinrio.setMfzTpMoviFinrio(tpMoviFinrio);
    }
}
