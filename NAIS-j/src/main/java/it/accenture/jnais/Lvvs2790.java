package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs2790Data;

/**Original name: LVVS2790<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2013.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS2790
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... NUOVA FISCALITA'
 *                   CALCOLO DELLA VARIABILE CED_III
 * **------------------------------------------------------------***
 * ----------------------------------------------------------------*
 *    COPY LCCV
 * ----------------------------------------------------------------*
 *     COPY LCCVLQU3  REPLACING ==(SF)== BY ==WLQU==.
 *     COPY LCCVLQU4  REPLACING ==(SF)== BY ==WLQU==.</pre>*/
public class Lvvs2790 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs2790Data ws = new Lvvs2790Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS2790
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS2790_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs2790 getInstance() {
        return ((Lvvs2790)Programs.getInstance(Lvvs2790.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
        // COB_CODE: SET COMUN-TROV-NO                 TO TRUE.
        ws.getFlagComunTrov().setNo();
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: INITIALIZE AREA-IO-POLI.
        initAreaIoPoli();
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        // COB_CODE: PERFORM VERIFICA-MOVIMENTO
        //              THRU VERIFICA-MOVIMENTO-EX
        verificaMovimento();
        //--> LETTURA DELLA POLIZZA PER REC DATA DECORRENZA
        // COB_CODE:      IF  IDSV0003-SUCCESSFUL-RC
        //           *    AND IDSV0003-SUCCESSFUL-SQL
        //                       THRU S1110-EX
        //                END-IF
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            //    AND IDSV0003-SUCCESSFUL-SQL
            // COB_CODE: PERFORM S1110-LETTURA-LIQ
            //              THRU S1110-EX
            s1110LetturaLiq();
        }
        // COB_CODE: MOVE 0   TO WK-SOMMA.
        ws.setWkSomma(Trunc.toDecimal(0, 15, 3));
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-POLI
        //                TO DPOL-AREA-POLI
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasPoli())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DPOL-AREA-POLI
            ws.setDpolAreaPoliFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1110-LETTURA-LIQ<br>
	 * <pre>----------------------------------------------------------------*
	 *    LETTURA DELLA POLIZZA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1110LetturaLiq() {
        Idbslqu0 idbslqu0 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE LIQ
        initLiq();
        //               IX-TAB-LQU
        //               WLQU-ELE-LIQ-MAX.
        // COB_CODE: SET FINE-LQU-NO           TO TRUE.
        ws.getWkVariabili().getFineLqu().setNo();
        // COB_CODE: MOVE IVVC0213-ID-ADESIONE TO LQU-ID-OGG.
        ws.getLiq().setLquIdOgg(ivvc0213.getIdAdesione());
        // COB_CODE: MOVE 'AD'                 TO LQU-TP-OGG.
        ws.getLiq().setLquTpOgg("AD");
        //  --> Tipo operazione
        // COB_CODE: SET IDSV0003-FETCH-FIRST       TO TRUE.
        idsv0003.getOperazione().setFetchFirst();
        //  --> Tipo livello
        // COB_CODE: SET IDSV0003-ID-OGGETTO        TO TRUE.
        idsv0003.getLivelloOperazione().setIdsi0011IdOggetto();
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA TO TRUE.
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC     TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL    TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: PERFORM UNTIL FINE-LQU-SI
        //           END-IF
        //           END-PERFORM.
        while (!ws.getWkVariabili().getFineLqu().isSi()) {
            // COB_CODE: CALL IDBSLQU0    USING IDSV0003 LIQ
            //           ON EXCEPTION
            //              SET IDSV0003-INVALID-OPER   TO TRUE
            //           END-CALL
            try {
                idbslqu0 = Idbslqu0.getInstance();
                idbslqu0.run(idsv0003, ws.getLiq());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE IDBSLQU0               TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getIdbslqu0());
                // COB_CODE: MOVE 'CALL-IDBSLQU0 ERRORE CHIAMATA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-IDBSLQU0 ERRORE CHIAMATA");
                // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE:      IF IDSV0003-SUCCESSFUL-RC
            //                      END-EVALUATE
            //                ELSE
            //           *--->   ERRORE DI ACCESSO AL DB
            //                      SET FINE-LQU-SI                TO TRUE
            //                END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE:         EVALUATE TRUE
                //                      WHEN IDSV0003-NOT-FOUND
                //           *-->    NESSUN DATO IN TABELLA
                //                             END-IF
                //                          WHEN IDSV0003-SUCCESSFUL-SQL
                //           *                   ADD 1       TO IX-TAB-LQU
                //           *                   PERFORM VALORIZZA-OUTPUT-LQU
                //           *                      THRU VALORIZZA-OUTPUT-LQU-EX
                //           *                   MOVE IX-TAB-LQU TO WLQU-ELE-LIQ-MAX
                //                               SET IDSV0003-FETCH-NEXT  TO TRUE
                //                          WHEN OTHER
                //           *--->   ERRORE DI ACCESSO AL DB
                //                               SET FINE-LQU-SI                TO TRUE
                //                      END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.NOT_FOUND://-->    NESSUN DATO IN TABELLA
                        // COB_CODE: SET FINE-LQU-SI             TO TRUE
                        ws.getWkVariabili().getFineLqu().setSi();
                        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                        idsv0003.getSqlcode().setSuccessfulSql();
                        // COB_CODE: IF IDSV0003-FETCH-FIRST
                        //              END-STRING
                        //           END-IF
                        if (idsv0003.getOperazione().isFetchFirst()) {
                            // COB_CODE: MOVE WK-PGM
                            //             TO IDSV0003-COD-SERVIZIO-BE
                            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                            // COB_CODE: STRING 'ERRORE - NON TROVATE LIQUIDAZIONI'
                            //                   IDSV0003-RETURN-CODE ';'
                            //                   IDSV0003-SQLCODE
                            //                   DELIMITED BY SIZE INTO
                            //                   IDSV0003-DESCRIZ-ERR-DB2
                            //           END-STRING
                            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE - NON TROVATE LIQUIDAZIONI", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                        }
                        break;

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL://                   ADD 1       TO IX-TAB-LQU
                        //                   PERFORM VALORIZZA-OUTPUT-LQU
                        //                      THRU VALORIZZA-OUTPUT-LQU-EX
                        //                   MOVE IX-TAB-LQU TO WLQU-ELE-LIQ-MAX
                        // COB_CODE: IF  LQU-TP-LIQ  = 'CE'
                        //                  THRU S1111-EX
                        //           END-IF
                        if (Conditions.eq(ws.getLiq().getLquTpLiq(), "CE")) {
                            // COB_CODE: PERFORM S1111-LETTURA-TRANCHE-LIQ
                            //              THRU S1111-EX
                            s1111LetturaTrancheLiq();
                        }
                        // COB_CODE: SET IDSV0003-FETCH-NEXT  TO TRUE
                        idsv0003.getOperazione().setFetchNext();
                        break;

                    default://--->   ERRORE DI ACCESSO AL DB
                        // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                        // COB_CODE: MOVE WK-PGM
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: STRING 'ERRORE LETTURA TABELLA LIQUIDAZ ;'
                        //                  IDSV0003-RETURN-CODE ';'
                        //                  IDSV0003-SQLCODE
                        //                  DELIMITED BY SIZE INTO
                        //                  IDSV0003-DESCRIZ-ERR-DB2
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA LIQUIDAZ ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                        idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                        // COB_CODE: SET FINE-LQU-SI                TO TRUE
                        ws.getWkVariabili().getFineLqu().setSi();
                        break;
                }
            }
            else {
                //--->   ERRORE DI ACCESSO AL DB
                // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                // COB_CODE: MOVE WK-PGM
                //                     TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING 'ERRORE LETTURA TABELLA LIQUIDAZ ;'
                //                  IDSV0003-RETURN-CODE ';'
                //                  IDSV0003-SQLCODE
                //                  DELIMITED BY SIZE INTO
                //                  IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA LIQUIDAZ ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                // COB_CODE: SET FINE-LQU-SI                TO TRUE
                ws.getWkVariabili().getFineLqu().setSi();
            }
        }
    }

    /**Original name: S1111-LETTURA-TRANCHE-LIQ<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 * --> TIPO OPERAZIONE
	 *     SET IDSV0003-FETCH-FIRST       TO TRUE.
	 *     SET FINE-TDL-NO                TO TRUE.
	 *     SET INDEX-NO                   TO TRUE.
	 *     SET RAMO-III-NO                TO TRUE.
	 * --> INIZIALIZZA CODICE DI RITORNO
	 *     SET IDSV0003-SUCCESSFUL-RC     TO TRUE.
	 *     SET IDSV0003-SUCCESSFUL-SQL    TO TRUE.
	 *     PERFORM UNTIL FINE-TDL-SI
	 *        PERFORM INIZIA-TRANCHE-LIQ
	 *           THRU INIZIA-TRANCHE-LIQ-EX
	 *        CALL LDBS1560 USING IDSV0003 TRCH-LIQ
	 *        ON EXCEPTION
	 *           MOVE LDBS7480
	 *             TO IDSV0003-COD-SERVIZIO-BE
	 *           MOVE 'ERRORE CALL LDBS7480 FETCH'
	 *             TO IDSV0003-DESCRIZ-ERR-DB2
	 *           SET IDSV0003-INVALID-OPER TO TRUE
	 *        END-CALL
	 *        IF IDSV0003-SUCCESSFUL-RC
	 *           EVALUATE TRUE
	 *               WHEN IDSV0003-NOT-FOUND
	 * -->    NESSUN DATO IN TABELLA
	 *                    SET FINE-TDL-SI             TO TRUE
	 *                    SET IDSV0003-SUCCESSFUL-SQL TO TRUE
	 *               WHEN IDSV0003-SUCCESSFUL-SQL
	 *                    PERFORM RECUPERA-TRCH-DI-GAR
	 *                       THRU RECUPERA-TRCH-DI-GAR-EX
	 *                    SET IDSV0003-FETCH-NEXT  TO TRUE
	 *                    PERFORM INIZIA-TRANCHE-LIQ
	 *                       THRU INIZIA-TRANCHE-LIQ-EX
	 *               WHEN OTHER
	 * --->   ERRORE DI ACCESSO AL DB
	 *                    SET IDSV0003-INVALID-OPER  TO TRUE
	 *                    MOVE WK-PGM
	 *                      TO IDSV0003-COD-SERVIZIO-BE
	 *                    STRING 'ERRORE LETT TAB TRANCHE DI LIQ ;'
	 *                           IDSV0003-RETURN-CODE ';'
	 *                           IDSV0003-SQLCODE
	 *                           DELIMITED BY SIZE INTO
	 *                           IDSV0003-DESCRIZ-ERR-DB2
	 *                    END-STRING
	 *                    SET FINE-TDL-SI                TO TRUE
	 *           END-EVALUATE
	 *        ELSE
	 *           SET IDSV0003-INVALID-OPER  TO TRUE
	 *           MOVE WK-PGM
	 *             TO IDSV0003-COD-SERVIZIO-BE
	 *           STRING 'ERRORE LETT TAB TRANCHE DI LIQ ;'
	 *                   IDSV0003-RETURN-CODE ';'
	 *                   IDSV0003-SQLCODE
	 *                   DELIMITED BY SIZE INTO
	 *                   IDSV0003-DESCRIZ-ERR-DB2
	 *           END-STRING
	 *           SET FINE-TDL-SI                TO TRUE
	 *        END-IF
	 *     END-PERFORM.</pre>*/
    private void s1111LetturaTrancheLiq() {
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //                  THRU S1150-CALCOLO-ANNO-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1150-CALCOLO-ANNO
            //              THRU S1150-CALCOLO-ANNO-EX
            s1150CalcoloAnno();
        }
        //--> PERFORM DI CALCOLO DELL'IMPORTO
        // COB_CODE:      IF  IDSV0003-SUCCESSFUL-RC
        //                AND IDSV0003-SUCCESSFUL-SQL
        //           *    AND RAMO-III-SI
        //                       THRU S1200-CALCOLO-IMPORTO-EX
        //                END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            //    AND RAMO-III-SI
            // COB_CODE: PERFORM S1200-CALCOLO-IMPORTO
            //              THRU S1200-CALCOLO-IMPORTO-EX
            s1200CalcoloImporto();
        }
    }

    /**Original name: S1150-CALCOLO-ANNO<br>
	 * <pre>----------------------------------------------------------------*
	 *    CALCOLO ANNO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1150CalcoloAnno() {
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //             TO WK-DATA-FINE
        ws.getWkDataFine().setWkDataFineFormatted(idsv0003.getDataInizioEffettoFormatted());
        //    IF INDEX-NO
        //       MOVE 20120101
        //         TO WK-DATA-INIZIO-D
        //    ELSE
        // COB_CODE: MOVE DPOL-DT-DECOR
        //             TO WK-DATA-INIZIO.
        ws.getWkDataInizio().setWkDataInizioFormatted(ws.getLccvpol1().getDati().getWpolDtDecorFormatted());
    }

    /**Original name: S1200-CALCOLO-IMPORTO<br>
	 * <pre>----------------------------------------------------------------*
	 *    CALCOLO IMPORTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200CalcoloImporto() {
        // COB_CODE:      IF LQU-TP-LIQ  = 'CE'
        //                   AND LQU-DT-INI-EFF >= WK-DATA-INIZIO-D
        //                   AND LQU-DT-INI-EFF <= WK-DATA-FINE-D
        //           *---> LA SOMMATORIA AVVIENE SOLO PER I NON TASSATI
        //           *---> CIOE' SE TOT-IMP-IS T NULLO O PARI A 0
        //           *       IF WLQU-TOT-IMP-IS-NULL(IX-TAB-LQU)
        //           *              EQUAL HIGH-VALUES OR  LOW-VALUES
        //           *                    OR  SPACES
        //           *            IF WLQU-TOT-IMP-LRD-LIQTO-NULL(IX-TAB-LQU)
        //           *                   EQUAL HIGH-VALUES OR  LOW-VALUES
        //           *                         OR  SPACES
        //           *                   CONTINUE
        //           *            ELSE
        //           *               COMPUTE WK-SOMMA = WK-SOMMA +
        //           *                       WLQU-TOT-IMP-LRD-LIQTO(IX-TAB-LQU)
        //           *            END-IF
        //           *       ELSE
        //           *          IF WLQU-TOT-IMP-IS(IX-TAB-LQU) EQUAL ZERO
        //           *            IF WLQU-TOT-IMP-LRD-LIQTO-NULL(IX-TAB-LQU)
        //           *                   EQUAL HIGH-VALUES OR  LOW-VALUES
        //           *                         OR  SPACES
        //           *                   CONTINUE
        //           *            ELSE
        //           *               COMPUTE WK-SOMMA = WK-SOMMA +
        //           *                       WLQU-TOT-IMP-LRD-LIQTO(IX-TAB-LQU)
        //           *            END-IF
        //           *          ELSE
        //           *            CONTINUE
        //           *          END-IF
        //                   END-IF
        //                END-IF.
        if (Conditions.eq(ws.getLiq().getLquTpLiq(), "CE") && ws.getLiq().getLquDtIniEff() >= ws.getWkDataInizio().getWkDataInizioD() && ws.getLiq().getLquDtIniEff() <= ws.getWkDataFine().getWkDataFineD()) {
            //---> LA SOMMATORIA AVVIENE SOLO PER I NON TASSATI
            //---> CIOE' SE TOT-IMP-IS T NULLO O PARI A 0
            //       IF WLQU-TOT-IMP-IS-NULL(IX-TAB-LQU)
            //              EQUAL HIGH-VALUES OR  LOW-VALUES
            //                    OR  SPACES
            //            IF WLQU-TOT-IMP-LRD-LIQTO-NULL(IX-TAB-LQU)
            //                   EQUAL HIGH-VALUES OR  LOW-VALUES
            //                         OR  SPACES
            //                   CONTINUE
            //            ELSE
            //               COMPUTE WK-SOMMA = WK-SOMMA +
            //                       WLQU-TOT-IMP-LRD-LIQTO(IX-TAB-LQU)
            //            END-IF
            //       ELSE
            //          IF WLQU-TOT-IMP-IS(IX-TAB-LQU) EQUAL ZERO
            //            IF WLQU-TOT-IMP-LRD-LIQTO-NULL(IX-TAB-LQU)
            //                   EQUAL HIGH-VALUES OR  LOW-VALUES
            //                         OR  SPACES
            //                   CONTINUE
            //            ELSE
            //               COMPUTE WK-SOMMA = WK-SOMMA +
            //                       WLQU-TOT-IMP-LRD-LIQTO(IX-TAB-LQU)
            //            END-IF
            //          ELSE
            //            CONTINUE
            //          END-IF
            // COB_CODE: IF (LQU-TOT-IMP-IS-NULL EQUAL
            //                              HIGH-VALUE OR LOW-VALUE OR SPACES) OR
            //               LQU-TOT-IMP-IS EQUAL ZEROES
            //               END-IF
            //           END-IF
            if (Characters.EQ_HIGH.test(ws.getLiq().getLquTotImpIs().getLquTotImpIsNullFormatted()) || Characters.EQ_LOW.test(ws.getLiq().getLquTotImpIs().getLquTotImpIsNullFormatted()) || Characters.EQ_SPACE.test(ws.getLiq().getLquTotImpIs().getLquTotImpIsNull()) || ws.getLiq().getLquTotImpIs().getLquTotImpIs().compareTo(0) == 0) {
                // COB_CODE: IF (LQU-IMPST-SOST-1382011-NULL EQUAL
                //                          HIGH-VALUE OR LOW-VALUE OR SPACES) OR
                //               LQU-IMPST-SOST-1382011 EQUAL ZEROES
                //               END-IF
                //           END-IF
                if (Characters.EQ_HIGH.test(ws.getLiq().getLquImpstSost1382011().getLquImpstSost1382011NullFormatted()) || Characters.EQ_LOW.test(ws.getLiq().getLquImpstSost1382011().getLquImpstSost1382011NullFormatted()) || Characters.EQ_SPACE.test(ws.getLiq().getLquImpstSost1382011().getLquImpstSost1382011Null()) || ws.getLiq().getLquImpstSost1382011().getLquImpstSost1382011().compareTo(0) == 0) {
                    // COB_CODE: IF (LQU-IMPST-SOST-662014-NULL EQUAL
                    //                      HIGH-VALUE OR LOW-VALUE OR SPACES) OR
                    //              LQU-IMPST-SOST-662014 EQUAL ZEROES
                    //              END-IF
                    //           END-IF
                    if (Characters.EQ_HIGH.test(ws.getLiq().getLquImpstSost662014().getLquImpstSost662014NullFormatted()) || Characters.EQ_LOW.test(ws.getLiq().getLquImpstSost662014().getLquImpstSost662014NullFormatted()) || Characters.EQ_SPACE.test(ws.getLiq().getLquImpstSost662014().getLquImpstSost662014Null()) || ws.getLiq().getLquImpstSost662014().getLquImpstSost662014().compareTo(0) == 0) {
                        // COB_CODE: IF LQU-TOT-IMP-LRD-LIQTO-NULL
                        //                  EQUAL HIGH-VALUES OR  LOW-VALUES
                        //                                    OR  SPACES
                        //              CONTINUE
                        //           ELSE
                        //                      LQU-TOT-IMP-LRD-LIQTO
                        //           END-IF
                        if (Characters.EQ_HIGH.test(ws.getLiq().getLquTotImpLrdLiqto().getLquTotImpLrdLiqtoNullFormatted()) || Characters.EQ_LOW.test(ws.getLiq().getLquTotImpLrdLiqto().getLquTotImpLrdLiqtoNullFormatted()) || Characters.EQ_SPACE.test(ws.getLiq().getLquTotImpLrdLiqto().getLquTotImpLrdLiqtoNull())) {
                        // COB_CODE: CONTINUE
                        //continue
                        }
                        else {
                            // COB_CODE: COMPUTE WK-SOMMA = WK-SOMMA +
                            //                   LQU-TOT-IMP-LRD-LIQTO
                            ws.setWkSomma(Trunc.toDecimal(ws.getWkSomma().add(ws.getLiq().getLquTotImpLrdLiqto().getLquTotImpLrdLiqto()), 15, 3));
                        }
                    }
                }
            }
        }
    }

    /**Original name: VERIFICA-MOVIMENTO<br>
	 * <pre>----------------------------------------------------------------*
	 *     VERIFICA MOVIMENTO
	 * ----------------------------------------------------------------*
	 * --> SE CI TROVIAMO IN FASE DI LIQUIDAZIONE DEVO RECUPERARE LE
	 * --> IMMAGINI IN FASE DI COMUNICAZIONE</pre>*/
    private void verificaMovimento() {
        // COB_CODE: MOVE IVVC0213-TIPO-MOVI-ORIG
        //             TO WS-MOVIMENTO
        ws.getWsMovimento().setWsMovimentoFormatted(ivvc0213.getTipoMoviOrigFormatted());
        // COB_CODE:      IF LIQUI-RISTOT-IND OR LIQUI-RISPAR-POLIND OR
        //                   LIQUI-SCAPOL OR LIQUI-SININD OR LIQUI-RECIND OR
        //                   LIQUI-RPP-TAKE-PROFIT
        //                OR LIQUI-RISTOT-INCAPIENZA
        //                OR LIQUI-RPP-REDDITO-PROGR
        //                OR LIQUI-RPP-BENEFICIO-CONTR
        //                OR LIQUI-RISTOT-INCAP
        //           *--> RECUPERO IL MOVIMENTO DI COMUNICAZIONE
        //                   END-IF
        //                END-IF.
        if (ws.getWsMovimento().isLiquiRistotInd() || ws.getWsMovimento().isLiquiRisparPolind() || ws.getWsMovimento().isLiquiScapol() || ws.getWsMovimento().isLiquiSinind() || ws.getWsMovimento().isLiquiRecind() || ws.getWsMovimento().isLiquiRppTakeProfit() || ws.getWsMovimento().isLiquiRistotIncapienza() || ws.getWsMovimento().isLiquiRppRedditoProgr() || ws.getWsMovimento().isLiquiRppBeneficioContr() || ws.getWsMovimento().isLiquiRistotIncap()) {
            //--> RECUPERO IL MOVIMENTO DI COMUNICAZIONE
            // COB_CODE: PERFORM RECUP-MOVI-COMUN
            //              THRU RECUP-MOVI-COMUN-EX
            recupMoviComun();
            // COB_CODE: IF COMUN-TROV-SI
            //                   IDSV0003-DATA-FINE-EFFETTO
            //           END-IF
            if (ws.getFlagComunTrov().isSi()) {
                // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA
                //             TO WK-DATA-CPTZ-RIP
                ws.getWkVarMoviComun().setDataCptzRip(idsv0003.getDataCompetenza());
                // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                //             TO WK-DATA-EFF-RIP
                ws.getWkVarMoviComun().setDataEffRip(idsv0003.getDataInizioEffetto());
                // COB_CODE: MOVE WK-DATA-CPTZ-PREC
                //             TO IDSV0003-DATA-COMPETENZA
                idsv0003.setDataCompetenza(ws.getWkVarMoviComun().getDataCptzPrec());
                // COB_CODE: MOVE WK-DATA-EFF-PREC
                //             TO IDSV0003-DATA-INIZIO-EFFETTO
                //                IDSV0003-DATA-FINE-EFFETTO
                idsv0003.setDataInizioEffetto(ws.getWkVarMoviComun().getDataEffPrec());
                idsv0003.setDataFineEffetto(ws.getWkVarMoviComun().getDataEffPrec());
            }
        }
    }

    /**Original name: RECUP-MOVI-COMUN<br>
	 * <pre>----------------------------------------------------------------*
	 *     Recupero il movimento di comunicazione
	 * ----------------------------------------------------------------*</pre>*/
    private void recupMoviComun() {
        Ldbs6040 ldbs6040 = null;
        // COB_CODE: SET INIT-CUR-MOV               TO TRUE
        ws.getFlagCurMov().setInitCurMov();
        // COB_CODE: SET COMUN-TROV-NO              TO TRUE
        ws.getFlagComunTrov().setNo();
        // COB_CODE: INITIALIZE MOVI
        initMovi();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-FETCH-FIRST       TO TRUE
        idsv0003.getOperazione().setFetchFirst();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET  NO-ULTIMA-LETTURA         TO TRUE
        ws.getFlagUltimaLettura().setNoUltimaLettura();
        //
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                      OR FINE-CUR-MOV
        //                      OR COMUN-TROV-SI
        //                 END-IF
        //           END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || ws.getFlagCurMov().isFineCurMov() || ws.getFlagComunTrov().isSi())) {
            // COB_CODE: INITIALIZE MOVI
            initMovi();
            //
            // COB_CODE: MOVE IVVC0213-ID-POLIZZA    TO MOV-ID-OGG
            ws.getMovi().getMovIdOgg().setMovIdOgg(ivvc0213.getIdPolizza());
            // COB_CODE: MOVE 'PO'                   TO MOV-TP-OGG
            ws.getMovi().setMovTpOgg("PO");
            // COB_CODE: SET IDSV0003-TRATT-SENZA-STOR   TO TRUE
            idsv0003.getTrattamentoStoricita().setTrattSenzaStor();
            //--> LIVELLO OPERAZIONE
            // COB_CODE: SET IDSV0003-WHERE-CONDITION TO TRUE
            idsv0003.getLivelloOperazione().setWhereCondition();
            //--> INIZIALIZZA CODICE DI RITORNO
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC  TO TRUE
            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL TO TRUE
            idsv0003.getSqlcode().setSuccessfulSql();
            // COB_CODE: CALL  LDBS6040   USING IDSV0003 MOVI
            //             ON EXCEPTION
            //                SET IDSV0003-INVALID-OPER   TO TRUE
            //           END-CALL
            try {
                ldbs6040 = Ldbs6040.getInstance();
                ldbs6040.run(idsv0003, ws.getMovi());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE LDBS6040          TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA");
                // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE:           IF IDSV0003-SUCCESSFUL-RC
            //                         END-EVALUATE
            //                      ELSE
            //           *--> GESTIRE ERRORE
            //                         SET IDSV0003-INVALID-OPER   TO TRUE
            //                      END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE:               EVALUATE TRUE
                //                             WHEN IDSV0003-NOT-FOUND
                //           *-->    NESSUN DATO IN TABELLA
                //           *-->     LIQUIDAZIONE CONTESTUALE - UTILIZZO L'AREA DEL
                //           *-->     BOLLO IN INPUT
                //                               SET FINE-CUR-MOV TO TRUE
                //                             WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->   OPERAZIONE ESEGUITA CORRETTAMENTE
                //           *                   MOVE IDSV0003-BUFFER-DATI TO MOVI
                //           *      TROVO IL MOVIMENTO DI COMUNICAZIONE RISCATTO PARZIALE
                //                               END-IF
                //                             WHEN OTHER
                //           *--->   ERRORE DI ACCESSO AL DB
                //                                  SET IDSV0003-INVALID-OPER   TO TRUE
                //                         END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.NOT_FOUND://-->    NESSUN DATO IN TABELLA
                        //-->     LIQUIDAZIONE CONTESTUALE - UTILIZZO L'AREA DEL
                        //-->     BOLLO IN INPUT
                        // COB_CODE: SET FINE-CUR-MOV TO TRUE
                        ws.getFlagCurMov().setFineCurMov();
                        break;

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->   OPERAZIONE ESEGUITA CORRETTAMENTE
                        //                   MOVE IDSV0003-BUFFER-DATI TO MOVI
                        //      TROVO IL MOVIMENTO DI COMUNICAZIONE RISCATTO PARZIALE
                        // COB_CODE: MOVE MOV-TP-MOVI      TO WS-MOVIMENTO
                        ws.getWsMovimento().setWsMovimento(TruncAbs.toInt(ws.getMovi().getMovTpMovi().getMovTpMovi(), 5));
                        // COB_CODE: IF COMUN-RISPAR-IND
                        //           OR RISTO-INDIVI
                        //           OR VARIA-OPZION
                        //           OR SINIS-INDIVI
                        //           OR RECES-INDIVI
                        //           OR RPP-TAKE-PROFIT
                        //           OR COMUN-RISTOT-INCAPIENZA
                        //           OR RPP-REDDITO-PROGRAMMATO
                        //           OR RPP-BENEFICIO-CONTR
                        //           OR COMUN-RISTOT-INCAP
                        //              SET FINE-CUR-MOV   TO TRUE
                        //           ELSE
                        //              SET IDSV0003-FETCH-NEXT   TO TRUE
                        //           END-IF
                        if (ws.getWsMovimento().isComunRisparInd() || ws.getWsMovimento().isRistoIndivi() || ws.getWsMovimento().isVariaOpzion() || ws.getWsMovimento().isSinisIndivi() || ws.getWsMovimento().isRecesIndivi() || ws.getWsMovimento().isRppTakeProfit() || ws.getWsMovimento().isComunRistotIncapienza() || ws.getWsMovimento().isRppRedditoProgrammato() || ws.getWsMovimento().isRppBeneficioContr() || ws.getWsMovimento().isComunRistotIncap()) {
                            // COB_CODE: MOVE MOV-ID-MOVI    TO WK-ID-MOVI-COMUN
                            ws.getWkVarMoviComun().setIdMoviComun(ws.getMovi().getMovIdMovi());
                            // COB_CODE: SET SI-ULTIMA-LETTURA  TO TRUE
                            ws.getFlagUltimaLettura().setSiUltimaLettura();
                            // COB_CODE: MOVE MOV-DT-EFF     TO WK-DATA-EFF-PREC
                            ws.getWkVarMoviComun().setDataEffPrec(ws.getMovi().getMovDtEff());
                            // COB_CODE: COMPUTE WK-DATA-CPTZ-PREC =  MOV-DS-TS-CPTZ
                            //                                     - 1
                            ws.getWkVarMoviComun().setDataCptzPrec(Trunc.toLong(ws.getMovi().getMovDsTsCptz() - 1, 18));
                            // COB_CODE: SET COMUN-TROV-SI   TO TRUE
                            ws.getFlagComunTrov().setSi();
                            // COB_CODE: PERFORM CLOSE-MOVI
                            //              THRU CLOSE-MOVI-EX
                            closeMovi();
                            // COB_CODE: SET FINE-CUR-MOV   TO TRUE
                            ws.getFlagCurMov().setFineCurMov();
                        }
                        else {
                            // COB_CODE: SET IDSV0003-FETCH-NEXT   TO TRUE
                            idsv0003.getOperazione().setFetchNext();
                        }
                        break;

                    default://--->   ERRORE DI ACCESSO AL DB
                        // COB_CODE: MOVE LDBS6040
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                        // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                        //             TO IDSV0003-DESCRIZ-ERR-DB2
                        idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA");
                        // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                        break;
                }
            }
            else {
                //--> GESTIRE ERRORE
                // COB_CODE: MOVE LDBS6040
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA");
                // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
        // COB_CODE: MOVE IDSV0003-TIPO-MOVIMENTO TO WS-MOVIMENTO.
        ws.getWsMovimento().setWsMovimentoFormatted(idsv0003.getTipoMovimentoFormatted());
    }

    /**Original name: CLOSE-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *     CHIUDO IL CURSORE SULLA TABELLA MOVIMENTO LDBS6040
	 * ----------------------------------------------------------------*</pre>*/
    private void closeMovi() {
        Ldbs6040 ldbs6040 = null;
        // COB_CODE: SET IDSV0003-CLOSE-CURSOR            TO TRUE.
        idsv0003.getOperazione().setIdsv0003CloseCursor();
        // COB_CODE: CALL LDBS6040 USING IDSV0003 MOVI
        //           ON EXCEPTION
        //              SET IDSV0003-INVALID-OPER TO TRUE
        //           END-CALL.
        try {
            ldbs6040 = Ldbs6040.getInstance();
            ldbs6040.run(idsv0003, ws.getMovi());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE LDBS6040
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
            // COB_CODE: MOVE 'ERRORE CALL LDBS6040 CLOSE'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS6040 CLOSE");
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF NOT IDSV0003-SUCCESSFUL-RC
        //           OR NOT IDSV0003-SUCCESSFUL-SQL
        //                TO TRUE
        //           END-IF.
        if (!idsv0003.getReturnCode().isSuccessfulRc() || !idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'ERRORE CLOSE CURSORE LDBS6040'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CLOSE CURSORE LDBS6040");
            // COB_CODE: SET IDSV0003-INVALID-OPER
            //            TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: MOVE SPACES                     TO IVVC0213-VAL-STR-O.
        ivvc0213.getTabOutput().setValStrO("");
        // COB_CODE: MOVE 0                          TO IVVC0213-VAL-PERC-O.
        ivvc0213.getTabOutput().setValPercO(Trunc.toDecimal(0, 14, 9));
        // COB_CODE: MOVE WK-SOMMA                   TO IVVC0213-VAL-IMP-O.
        ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getWkSomma(), 18, 7));
        // COB_CODE: IF COMUN-TROV-SI
        //                   IDSV0003-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getFlagComunTrov().isSi()) {
            // COB_CODE: MOVE WK-DATA-CPTZ-RIP
            //             TO IDSV0003-DATA-COMPETENZA
            idsv0003.setDataCompetenza(ws.getWkVarMoviComun().getDataCptzRip());
            // COB_CODE: MOVE WK-DATA-EFF-RIP
            //             TO IDSV0003-DATA-INIZIO-EFFETTO
            //                IDSV0003-DATA-FINE-EFFETTO
            idsv0003.setDataInizioEffetto(ws.getWkVarMoviComun().getDataEffRip());
            idsv0003.setDataFineEffetto(ws.getWkVarMoviComun().getDataEffRip());
        }
        //
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
        ws.setIxTabIso(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initAreaIoPoli() {
        ws.setDpolElePoliMax(((short)0));
        ws.getLccvpol1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getLccvpol1().setIdPtf(0);
        ws.getLccvpol1().getDati().setWpolIdPoli(0);
        ws.getLccvpol1().getDati().setWpolIdMoviCrz(0);
        ws.getLccvpol1().getDati().getWpolIdMoviChiu().setWpolIdMoviChiu(0);
        ws.getLccvpol1().getDati().setWpolIbOgg("");
        ws.getLccvpol1().getDati().setWpolIbProp("");
        ws.getLccvpol1().getDati().getWpolDtProp().setWpolDtProp(0);
        ws.getLccvpol1().getDati().setWpolDtIniEff(0);
        ws.getLccvpol1().getDati().setWpolDtEndEff(0);
        ws.getLccvpol1().getDati().setWpolCodCompAnia(0);
        ws.getLccvpol1().getDati().setWpolDtDecor(0);
        ws.getLccvpol1().getDati().setWpolDtEmis(0);
        ws.getLccvpol1().getDati().setWpolTpPoli("");
        ws.getLccvpol1().getDati().getWpolDurAa().setWpolDurAa(0);
        ws.getLccvpol1().getDati().getWpolDurMm().setWpolDurMm(0);
        ws.getLccvpol1().getDati().getWpolDtScad().setWpolDtScad(0);
        ws.getLccvpol1().getDati().setWpolCodProd("");
        ws.getLccvpol1().getDati().setWpolDtIniVldtProd(0);
        ws.getLccvpol1().getDati().setWpolCodConv("");
        ws.getLccvpol1().getDati().setWpolCodRamo("");
        ws.getLccvpol1().getDati().getWpolDtIniVldtConv().setWpolDtIniVldtConv(0);
        ws.getLccvpol1().getDati().getWpolDtApplzConv().setWpolDtApplzConv(0);
        ws.getLccvpol1().getDati().setWpolTpFrmAssva("");
        ws.getLccvpol1().getDati().setWpolTpRgmFisc("");
        ws.getLccvpol1().getDati().setWpolFlEstas(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlRshComun(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlRshComunCond(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolTpLivGenzTit("");
        ws.getLccvpol1().getDati().setWpolFlCopFinanz(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolTpApplzDir("");
        ws.getLccvpol1().getDati().getWpolSpeMed().setWpolSpeMed(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().getWpolDirEmis().setWpolDirEmis(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().getWpolDir1oVers().setWpolDir1oVers(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().getWpolDirVersAgg().setWpolDirVersAgg(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().setWpolCodDvs("");
        ws.getLccvpol1().getDati().setWpolFlFntAz(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlFntAder(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlFntTfr(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlFntVolo(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolTpOpzAScad("");
        ws.getLccvpol1().getDati().getWpolAaDiffProrDflt().setWpolAaDiffProrDflt(0);
        ws.getLccvpol1().getDati().setWpolFlVerProd("");
        ws.getLccvpol1().getDati().getWpolDurGg().setWpolDurGg(0);
        ws.getLccvpol1().getDati().getWpolDirQuiet().setWpolDirQuiet(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().setWpolTpPtfEstno("");
        ws.getLccvpol1().getDati().setWpolFlCumPreCntr(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlAmmbMovi(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolConvGeco("");
        ws.getLccvpol1().getDati().setWpolDsRiga(0);
        ws.getLccvpol1().getDati().setWpolDsOperSql(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolDsVer(0);
        ws.getLccvpol1().getDati().setWpolDsTsIniCptz(0);
        ws.getLccvpol1().getDati().setWpolDsTsEndCptz(0);
        ws.getLccvpol1().getDati().setWpolDsUtente("");
        ws.getLccvpol1().getDati().setWpolDsStatoElab(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlScudoFisc(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlTrasfe(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlTfrStrc(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().getWpolDtPresc().setWpolDtPresc(0);
        ws.getLccvpol1().getDati().setWpolCodConvAgg("");
        ws.getLccvpol1().getDati().setWpolSubcatProd("");
        ws.getLccvpol1().getDati().setWpolFlQuestAdegzAss(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolCodTpa("");
        ws.getLccvpol1().getDati().getWpolIdAccComm().setWpolIdAccComm(0);
        ws.getLccvpol1().getDati().setWpolFlPoliCpiPr(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlPoliBundling(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolIndPoliPrinColl(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlVndBundle(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolIbBs("");
        ws.getLccvpol1().getDati().setWpolFlPoliIfp(Types.SPACE_CHAR);
    }

    public void initLiq() {
        ws.getLiq().setLquIdLiq(0);
        ws.getLiq().setLquIdOgg(0);
        ws.getLiq().setLquTpOgg("");
        ws.getLiq().setLquIdMoviCrz(0);
        ws.getLiq().getLquIdMoviChiu().setLquIdMoviChiu(0);
        ws.getLiq().setLquDtIniEff(0);
        ws.getLiq().setLquDtEndEff(0);
        ws.getLiq().setLquCodCompAnia(0);
        ws.getLiq().setLquIbOgg("");
        ws.getLiq().setLquTpLiq("");
        ws.getLiq().setLquDescCauEveSinLen(((short)0));
        ws.getLiq().setLquDescCauEveSin("");
        ws.getLiq().setLquCodCauSin("");
        ws.getLiq().setLquCodSinCatstrf("");
        ws.getLiq().getLquDtMor().setLquDtMor(0);
        ws.getLiq().getLquDtDen().setLquDtDen(0);
        ws.getLiq().getLquDtPervDen().setLquDtPervDen(0);
        ws.getLiq().getLquDtRich().setLquDtRich(0);
        ws.getLiq().setLquTpSin("");
        ws.getLiq().setLquTpRisc("");
        ws.getLiq().getLquTpMetRisc().setLquTpMetRisc(((short)0));
        ws.getLiq().getLquDtLiq().setLquDtLiq(0);
        ws.getLiq().setLquCodDvs("");
        ws.getLiq().getLquTotImpLrdLiqto().setLquTotImpLrdLiqto(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquTotImpPrest().setLquTotImpPrest(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquTotImpIntrPrest().setLquTotImpIntrPrest(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquTotImpUti().setLquTotImpUti(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquTotImpRitTfr().setLquTotImpRitTfr(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquTotImpRitAcc().setLquTotImpRitAcc(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquTotImpRitVis().setLquTotImpRitVis(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquTotImpbTfr().setLquTotImpbTfr(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquTotImpbAcc().setLquTotImpbAcc(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquTotImpbVis().setLquTotImpbVis(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquTotImpRimb().setLquTotImpRimb(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpbImpstPrvr().setLquImpbImpstPrvr(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpstPrvr().setLquImpstPrvr(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpbImpst252().setLquImpbImpst252(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpst252().setLquImpst252(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquTotImpIs().setLquTotImpIs(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpDirLiq().setLquImpDirLiq(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquTotImpNetLiqto().setLquTotImpNetLiqto(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquMontEnd2000().setLquMontEnd2000(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquMontEnd2006().setLquMontEnd2006(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquPcRen().setLquPcRen(new AfDecimal(0, 6, 3));
        ws.getLiq().getLquImpPnl().setLquImpPnl(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpbIrpef().setLquImpbIrpef(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpstIrpef().setLquImpstIrpef(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquDtVlt().setLquDtVlt(0);
        ws.getLiq().getLquDtEndIstr().setLquDtEndIstr(0);
        ws.getLiq().setLquTpRimb("");
        ws.getLiq().getLquSpeRcs().setLquSpeRcs(new AfDecimal(0, 15, 3));
        ws.getLiq().setLquIbLiq("");
        ws.getLiq().getLquTotIasOnerPrvnt().setLquTotIasOnerPrvnt(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquTotIasMggSin().setLquTotIasMggSin(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquTotIasRstDpst().setLquTotIasRstDpst(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpOnerLiq().setLquImpOnerLiq(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquComponTaxRimb().setLquComponTaxRimb(new AfDecimal(0, 15, 3));
        ws.getLiq().setLquTpMezPag("");
        ws.getLiq().getLquImpExcontr().setLquImpExcontr(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpIntrRitPag().setLquImpIntrRitPag(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquBnsNonGoduto().setLquBnsNonGoduto(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquCnbtInpstfm().setLquCnbtInpstfm(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpstDaRimb().setLquImpstDaRimb(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpbIs().setLquImpbIs(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquTaxSep().setLquTaxSep(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpbTaxSep().setLquImpbTaxSep(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpbIntrSuPrest().setLquImpbIntrSuPrest(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquAddizComun().setLquAddizComun(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpbAddizComun().setLquImpbAddizComun(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquAddizRegion().setLquAddizRegion(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpbAddizRegion().setLquImpbAddizRegion(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquMontDal2007().setLquMontDal2007(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpbCnbtInpstfm().setLquImpbCnbtInpstfm(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpLrdDaRimb().setLquImpLrdDaRimb(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpDirDaRimb().setLquImpDirDaRimb(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquRisMat().setLquRisMat(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquRisSpe().setLquRisSpe(new AfDecimal(0, 15, 3));
        ws.getLiq().setLquDsRiga(0);
        ws.getLiq().setLquDsOperSql(Types.SPACE_CHAR);
        ws.getLiq().setLquDsVer(0);
        ws.getLiq().setLquDsTsIniCptz(0);
        ws.getLiq().setLquDsTsEndCptz(0);
        ws.getLiq().setLquDsUtente("");
        ws.getLiq().setLquDsStatoElab(Types.SPACE_CHAR);
        ws.getLiq().getLquTotIasPnl().setLquTotIasPnl(new AfDecimal(0, 15, 3));
        ws.getLiq().setLquFlEveGarto(Types.SPACE_CHAR);
        ws.getLiq().getLquImpRenK1().setLquImpRenK1(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpRenK2().setLquImpRenK2(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpRenK3().setLquImpRenK3(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquPcRenK1().setLquPcRenK1(new AfDecimal(0, 6, 3));
        ws.getLiq().getLquPcRenK2().setLquPcRenK2(new AfDecimal(0, 6, 3));
        ws.getLiq().getLquPcRenK3().setLquPcRenK3(new AfDecimal(0, 6, 3));
        ws.getLiq().setLquTpCausAntic("");
        ws.getLiq().getLquImpLrdLiqtoRilt().setLquImpLrdLiqtoRilt(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpstApplRilt().setLquImpstApplRilt(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquPcRiscParz().setLquPcRiscParz(new AfDecimal(0, 12, 5));
        ws.getLiq().getLquImpstBolloTotV().setLquImpstBolloTotV(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpstBolloDettC().setLquImpstBolloDettC(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpstBolloTotSw().setLquImpstBolloTotSw(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpstBolloTotAa().setLquImpstBolloTotAa(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpbVis1382011().setLquImpbVis1382011(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpstVis1382011().setLquImpstVis1382011(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpbIs1382011().setLquImpbIs1382011(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpstSost1382011().setLquImpstSost1382011(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquPcAbbTitStat().setLquPcAbbTitStat(new AfDecimal(0, 6, 3));
        ws.getLiq().getLquImpbBolloDettC().setLquImpbBolloDettC(new AfDecimal(0, 15, 3));
        ws.getLiq().setLquFlPreComp(Types.SPACE_CHAR);
        ws.getLiq().getLquImpbVis662014().setLquImpbVis662014(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpstVis662014().setLquImpstVis662014(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpbIs662014().setLquImpbIs662014(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquImpstSost662014().setLquImpstSost662014(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquPcAbbTs662014().setLquPcAbbTs662014(new AfDecimal(0, 6, 3));
        ws.getLiq().getLquImpLrdCalcCp().setLquImpLrdCalcCp(new AfDecimal(0, 15, 3));
        ws.getLiq().getLquCosTunnelUscita().setLquCosTunnelUscita(new AfDecimal(0, 15, 3));
    }

    public void initMovi() {
        ws.getMovi().setMovIdMovi(0);
        ws.getMovi().setMovCodCompAnia(0);
        ws.getMovi().getMovIdOgg().setMovIdOgg(0);
        ws.getMovi().setMovIbOgg("");
        ws.getMovi().setMovIbMovi("");
        ws.getMovi().setMovTpOgg("");
        ws.getMovi().getMovIdRich().setMovIdRich(0);
        ws.getMovi().getMovTpMovi().setMovTpMovi(0);
        ws.getMovi().setMovDtEff(0);
        ws.getMovi().getMovIdMoviAnn().setMovIdMoviAnn(0);
        ws.getMovi().getMovIdMoviCollg().setMovIdMoviCollg(0);
        ws.getMovi().setMovDsOperSql(Types.SPACE_CHAR);
        ws.getMovi().setMovDsVer(0);
        ws.getMovi().setMovDsTsCptz(0);
        ws.getMovi().setMovDsUtente("");
        ws.getMovi().setMovDsStatoElab(Types.SPACE_CHAR);
    }
}
